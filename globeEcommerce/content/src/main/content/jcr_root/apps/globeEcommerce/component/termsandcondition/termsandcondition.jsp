<%@ include file="/libs/foundation/global.jsp"%>

<html dir="ltr" lang="en" data-placeholder-focus="false">
    <cq:includeClientLib categories="productdetails"/>
<head>

<link rel="canonical" href="http://shop.globe.com.ph/" />

<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Checkout</title>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/checkout-common-form.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/pikaday.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/dropzone.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/dropzone.min.css" media="screen" />



</head>

    <body><div id="mboxImported-default-target-global-mbox-0" style="visibility: visible; display: block;"></div>
<style>.loading {background: #FFFFFF url('/catalog/view/theme/default/image/loading.gif') 10px center no-repeat;border: 1px solid #B8E2FB;	padding: 10px 10px 10px 33px;margin-bottom: 15px;color: #555555;}</style>
<!-- Google Tag Manager -->
<noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM7FZN" height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PM7FZN');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSV552"
height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSV552');</script>
<!-- End Google Tag Manager -->

<script>
   var a = "//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958"; //script location
   var domainList = ["www.globe.com.ph", "shop.globe.com.ph"]; //List of production domains

  
   if (domainList.indexOf(location.hostname) < 0) {
     a += "-staging"
   }

   document.write(unescape('%3Cscript type="text/javascript" src="'+a+'.js"%3E%3C/script%3E')); //write to page
</script><script type="text/javascript" src="//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958.js"></script><script type="text/javascript" id="">(function(){var a=window._fbq||(window._fbq=[]);if(!a.loaded){var b=document.createElement("script");b.async=!0;b.src="//connect.facebook.net/en_US/fbds.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);a.loaded=!0}a.push(["addPixelId","1394807297452790"])})();window._fbq=window._fbq||[];window._fbq.push(["track","PixelInitialized",{}]);</script>
<noscript>&lt;img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1394807297452790&amp;amp;ev=NoScript"&gt;</noscript><script type="text/javascript" id="">(function(){var a=window._fbq||(window._fbq=[]);if(!a.loaded){var b=document.createElement("script");b.async=!0;b.src="//connect.facebook.net/en_US/fbds.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);a.loaded=!0}a.push(["addPixelId","1394807297452790"])})();window._fbq=window._fbq||[];window._fbq.push(["track","PixelInitialized",{}]);</script>
<noscript>&lt;img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1394807297452790&amp;amp;ev=NoScript"&gt;</noscript>

<script type="text/javascript" id="">(function(){var a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"==document.location.protocol?"https://ph-ssl":"http://ph-cdn")+".effectivemeasure.net/em.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();</script>
<noscript>
	&lt;img src="https://ph.effectivemeasure.net/em_image" alt="" style="position:absolute; left:-5px;"&gt;
</noscript>
<script src="https://assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/mbox-contents-ff1ad802b98ac2538d582ed5a65a3bf97208778c.js"></script><script type="text/javascript" id="" src="//pixel.mathtag.com/event/js?mt_id=826864&amp;mt_adid=143609&amp;v1=&amp;v2=&amp;v3=&amp;s1=&amp;s2=&amp;s3="></script><style>.mboxDefault { visibility:hidden; }</style><script src="//cdn.tt.omtrdc.net/cdn/target.js"></script>

 

<script type="text/javascript">


  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }
    
  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }

  //Input types
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });
  
  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);
    
    
    if($("#rwd-searchbox").val() == "") {
      
      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
      
      },500)
  }); 


  //Empty input on blur of input 
  $('input[name=\'searchterms\']').on('blur', function(e){
      
      setTimeout(function(){
       
      },500);
      var ifIE = msieversion();
      
      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {
          
        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
          
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

          // IE 11 => return version number
          var rv = ua.indexOf('rv:');
          return true;
          
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
        
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  //General function for search - not used in input because of preventdefault in input
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
    
    }
  });
  
  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }
    
  });
  //rwd of account-sigout-trigger above
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }
    
  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>
<div id="container" class="margTop">
<section class="journey-header row">
    <div class="container">
        
    </div>
</section>
<div id="notification" class="notification_cart">
  </div>

<div id="checkout-content" class="container" style="min-height: 208px;"><div class="required-prompt"><span></span></div>

<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="checkout-form-container">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
<div class="row" id="form-container" style="height: 478px;">
<div id="section-container">
<form action="" enctype="multipart/form-data" id="app_form" method="post">
<section class="panel" id="personal-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="additional-personal-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="shipping-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="financial-details">
<div class="panel-content"><div class="panel-heading">Step 4: Employment Details</div>
<div class="panel-body">
                                        <div class="panel-row row" id="occupation-container" style="z-index: 7;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-12" id="occupation-form-group" style="z-index: 2; height: auto; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">Occupation</label>
            
                                    <select name="financial_occupation" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="accountant">
                                    Accountant                                </option>
                                                            <option value="administrative_staff">
                                    Administrative Staff                                </option>
                                                            <option value="architect">
                                    Architect                                </option>
                                                            <option value="call_center_agent">
                                    Call Center Agent                                </option>
                                                            <option value="consultant">
                                    Consultant                                </option>
                                                            <option value="dentist">
                                    Dentist                                </option>
                                                            <option value="doctor">
                                    Doctor                                </option>
                                                            <option value="engineer">
                                    Engineer                                </option>
                                                            <option value="executive">
                                    Executive                                </option>
                                                            <option value="government_employee">
                                    Goverment Employee                                </option>
                                                            <option value="government_legislative_officials">
                                    Government Legislative Officials                                </option>
                                                            <option value="hospital_employee">
                                    Hospital Employee                                </option>
                                                            <option value="junior_official">
                                    Junior Official                                </option>
                                                            <option value="lawyer">
                                    Lawyer                                </option>
                                                            <option value="lgu_officials">
                                    LGU Officials                                </option>
                                                            <option value="manager_director">
                                    Manager/Director                                </option>
                                                            <option value="medical_field_employee">
                                    Medical Field Employee                                </option>
                                                            <option value="middle_management">
                                    Middle Management                                </option>
                                                            <option value="national_government_officials">
                                    National Government Officials                                </option>
                                                            <option value="ofw_beneficiary_allottee">
                                    OFW Benificiary/Allottee                                </option>
                                                            <option value="owner">
                                    Owner                                </option>
                                                            <option value="president_coo_ceo">
                                    President/COO/CEO                                </option>
                                                            <option value="private_corporate_employee">
                                    Private Corporate Employee                                </option>
                                                            <option value="retired">
                                    Retired                                </option>
                                                            <option value="sales_staff">
                                    Sales Staff                                </option>
                                                            <option value="school_employee">
                                    School Employee                                </option>
                                                            <option value="self_employed">
                                    Self Employed                                </option>
                                                            <option value="senior_officer">
                                    Senior Officer                                </option>
                                                            <option value="sole_proprietor_entrepreneur">
                                    Sole Proprietor/Entrepreneur                                </option>
                                                            <option value="staff_associate_clerk">
                                    Staff/Associate/Clerk                                </option>
                                                            <option value="student">
                                    Student                                </option>
                                                            <option value="supervisor">
                                    Supervisor                                </option>
                                                            <option value="unemployed">
                                    Unemployed                                </option>
                                                            <option value="vice_president_svp">
                                    Vice President/SVP                                </option>
                                                                        </select>
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">Employer/Business Name</label>
            
                                    <input type="text" data-required="required" class=" required-data" name="financial_employer" value="" maxlength="255">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 6;">
            
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 3; height: auto;">
                <label class="field-label">Years in Company</label>
            
                                    <input type="text" data-required="required" class="checkout-forms-text-numeric required-data" name="financial_office_years_in_company" value="" maxlength="3">
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 2; height: auto; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">Monthly Gross Income</label>
            
                                    <select name="financial_office_gross_monthly_income" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="8,000 - 9,999">
                                    8,000 - 9,999                                </option>
                                                            <option value="10,000 - 12,999">
                                    10,000 - 12,999                                </option>
                                                            <option value="13,000 - 14,999">
                                    13,000 - 14,999                                </option>
                                                            <option value="15,000 - 19,999">
                                    15,000 - 19,999                                </option>
                                                            <option value="20,000 - 24,999">
                                    20,000 - 24,999                                </option>
                                                            <option value="25,000 - 29,999">
                                    25,000 - 29,999                                </option>
                                                            <option value="30,000 - 34,999">
                                    30,000 - 34,999                                </option>
                                                            <option value="35,000 - 49,999">
                                    35,000 - 49,999                                </option>
                                                            <option value="50,000 - 59,999">
                                    50,000 - 59,999                                </option>
                                                            <option value="60,000 - 89,999">
                                    60,000 - 89,999                                </option>
                                                            <option value="90,000 and up">
                                    90,000 and up                                </option>
                                                                        </select>
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">Industry</label>
            
                                    <select name="financial_industry" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="banking_and_finance">
                                    Banking and Finance                                </option>
                                                            <option value="it_telecommunication">
                                    IT/Telecommunication                                </option>
                                                            <option value="manufacturing">
                                    Manufacturing                                </option>
                                                            <option value="other">
                                    Other                                </option>
                                                            <option value="power_utilities">
                                    Power/Utilities                                </option>
                                                            <option value="service">
                                    Service                                </option>
                                                                        </select>
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 5;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">Current Position Held</label>
            
                                    <input type="text" data-required="required" class=" required-data" name="financial_office_current_position" value="" maxlength="100">
                            </div>

                        </div>
            
                        
                                        <div class="panel-row row" style="z-index: 4;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">Full Company Address (House #/Floor, Street, Village Name, Barangay)</label>
            
                                    <input type="text" data-required="required" class=" required-data" name="financial_office_house_number" value="" maxlength="255">
                
                    <input type="hidden" id="financial_office_street" name="financial_office_street" value="">
                    <input type="hidden" id="financial_office_barangay" name="financial_office_barangay" value="">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 3;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: auto;">
                <label class="field-label">Area (Makati, Quezon City, etc.)</label>
            
                                    <input type="text" placeholder="Makati, Quezon City, etc" data-required="required" class="required-data ui-autocomplete-input area-selected" name="financial_office_city_temp" value="" maxlength="255" autocomplete="off">

                    <input type="hidden" id="financial_office_city" name="financial_office_city" class=" required-data" value="1189">
                    <input type="hidden" id="financial_office_province" name="financial_office_province" class=" required-data" value="58">
                    <input type="hidden" id="financial_office_region" name="financial_office_region" class=" required-data" value="7">
                    <input type="hidden" id="financial_office_country" name="financial_office_country" class=" required-data" value="168">    
                    <div id="financial-details-val" class="details-val" style="display: block;">
                        <span class="check-icon"></span>
                        <text></text>
                    </div>
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: auto; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">Zip Code</label>
            
                                    <select name="financial_office_zip_code" class="required-data" data-required="required" data-default=""><option value="" selected="selected">Please Select</option><option value="1667">5302</option></select>
                            </div>

                        </div>
            
                        
                                        <div class="panel-row row" style="z-index: 2;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">Office Email Address (optional)</label>
            
                                    <input type="text" data-required="optional" class=" optional-data" name="financial_office_email_address" value="" maxlength="255">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 1;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: auto;">
                <label class="field-label">Office Phone Number (optional)</label>
            
                                    <input type="text" data-required="optional" class="checkout-forms-text-numeric optional-data" name="financial_office_phone_number" value="" maxlength="15">
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: auto;">
                <label class="field-label">Company/HR Number (optional)</label>
            
                                    <input type="text" data-required="optional" class="checkout-forms-text-numeric optional-data" name="financial_office_company_number" value="" maxlength="15">
                            </div>

                        </div>
            
                        
    <input type="hidden" id="checkout_type" value="postpaid">
</div>
<div id="panel-bottom" class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a id="prev-step" onclick="setPreviousPanel('shipping-details', 'financial-details')">Previous Step</a>
        <button id="button-financial-details" class="btn">Next Step</button>
    </div>
</div>

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/financial_details.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    getSidebar('financial-details', 'postpaid');
})
</script>

<script type="text/javascript">
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 4, 'option': ''},
        'products': [
                        {                            
                'name': 'Samsung Galaxy S6 edge',     
                'id': '134',
                'price': '25299.00',
                'brand': 'Samsung',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 999, 24 months, White Pearl, 64 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                    ]
     }
   }
});
</script>
</div>
</section>

<section class="panel prev" id="document-upload-details">
<div class="panel-content"></div>
</section>

<section class="panel current" id="terms-condition-details">
    <div class="panel-content"><div class="panel-heading">${properties.pagetitle}</div>
<div class="panel-body no-border">
    <div class="panel-row row" style="z-index: 1;">
        <div id="terms-condition-content" class="col-md-12 col-sm-12">
            <div class="terms-condition-inner">
                <p style="margin-left: 40px;">&nbsp;</p>

                <p><span style="font-size:18px;"><strong>${properties.title}</strong></span></p>
               ${properties.inner}
<p>&nbsp;</p>
            </div>
        </div>
        <div id="terms-condition-caveat" class="col-md-12 col-sm-12">
            <h4>Clicking the “Agree and Review My Order” button means you have read and agree to the Terms and Conditions stated above.</h4>
        </div>

        <input type="hidden" name="terms_action" id="terms_action" value="https://shop.globe.com.ph/checkout/terms/validate">
        <input type="hidden" id="application_type" value="new">
        <input type="hidden" id="checkout_type" value="postpaid">
    </div>
</div>
<div id="panel-bottom" class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5">
        <a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                <a id="prev-step" onclick="setPreviousPanel('document-upload-details', 'terms-condition-details')">Previous Step</a>
        <button id="button-terms-condition-details" class="btn"><a href="/content/globeEcommerce/reviewpage.html?wcmmode=disabled" style="color:white;"/>Agree and Review My Order</button>
    </div>
    
</div>

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/terms.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    getSidebar('terms', 'postpaid');
});


</script>

<script type="text/javascript">
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 6, 'option': ''},
        'products': [
                        {                            
                'name': 'Samsung Galaxy S6 edge',     
                'id': '134',
                'price': '25299.00',
                'brand': 'Samsung',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 999, 24 months, White Pearl, 64 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                    ]
     }
   }
});
</script>
</div>
</section>

<section class="panel" id="review-order-details">
<div class="panel-content"></div>
</section>
</form>
</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="sidebar"><p><img class="img-responsive" id="norton-secured" src="https://shps3dv01.s3.amazonaws.com/media/data/norton_secured.png"></p>

<div class="row" id="contact-details">
<div class="info-block col-lg-12 col-md-12 col-sm-12 col-xs-4">
<p>Your personal information is safe and will always be kept private.</p>
</div>
</div>
</div>
</div></div>

<div id="review-order-content" style="display: none;"></div>

<div id="checkout-footer">
    <a target="_blank" href="http://www.globe.com.ph/privacy-policy">Privacy Policy</a>&nbsp;|&nbsp;<a target="_blank" href="http://www.globe.com.ph/terms-and-conditions">Terms and Conditions</a><br>© 2016 Globe Telecom Inc.</div>

<script type="text/javascript">
$(document).ready(function() {
			// Personal Details
		$.ajax({
			url: 'checkout/personal_details',
			dataType: 'html',
			beforeSend: function() {
				$('#personal-details .panel-content').html('<div align="center"><img src="catalog/view/theme/broadwaytheme/images/loader.gif" alt="" /></div>');
			},		
			success: function(html) {
				$('#personal-details .panel-content').html(html);
				$('#review-order-content').html('');
				$('#review-order-content').hide();
                $('#checkout-content').show();
				adjustFormHeight();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});	
	});
</script>

</div>

  
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Talk2GLOBE CHAT</h4>
      </div>
      <div class="modal-body" align="center">
        <iframe id="chatFrame" src="" frameborder="0" height="459" width="99.6%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Item Added -->
<div class="modal fade" id="pdpCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: green; ">Item successfully added to cart.</h4>
        <br>
        <div class="cart-btn-view">
          <a href="http://shop.globe.com.ph/cart" class="btn btn-bw-orange">View Cart</a>
        </div> 
        <br>
      </div>
    </div>
  </div>
</div>

<!-- Item Out of Stock -->
<div class="modal fade" id="pdpCartErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: red; " id="pdpCartErrorModalMsg">Item is out of stock.</h4>
        <br>
        <div class="cart-btn-view">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
        <br>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.btn-chat').click(function() {
      $('#chatFrame').attr('src', 'http://webchat.globe.com.ph/chatc41/');
    });
  });
</script>

<!-- Ethnio -->
  <!-- End Ethnio -->

<!-- Custom Script -->
<script type="text/javascript">_satellite.pageBottom();</script>

<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=ZKgM1Va";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  &lt;iframe src="//s.thebrighttag.com/iframe?c=ZKgM1Va" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"&gt;&lt;/iframe&gt;

</noscript>


<!---Mouseflow--->

<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/ded37ad7-9807-4070-ad67-9d72e900c70a.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<!---Mouseflow--->
<!-- End Custom Script -->
    </body>
</html>
