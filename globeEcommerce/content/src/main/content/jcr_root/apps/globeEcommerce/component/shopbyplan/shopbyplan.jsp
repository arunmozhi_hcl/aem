<%@ include file="/libs/foundation/global.jsp"%>
<html dir="ltr" lang="en" data-placeholder-focus="false">

 <cq:includeClientLib categories="productdetails"/>
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="cache-control" content="public">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<meta name="robots" content="index,follow" />
<link rel="canonical" href="http://shop.globe.com.ph/shop-by-plan" />

<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Shop by Postpaid Plans - myStarter Plan, myLifeStyle Plan | Globe Online Shop</title>



<meta name="description" content="Enjoy lots of perks and privileges from Globe’s Postpaid Plans. Check out Shop by Plans to find the best and most beneficial plan for your outgoing lifestyle!" />

    <link href="http://d11fuji4mn7bm2.cloudfront.net/media/data/Favicon/favicon.png" rel="icon" />


</head>
<body>
<style>.loading {background: #FFFFFF url('/catalog/view/theme/default/image/loading.gif') 10px center no-repeat;border: 1px solid #B8E2FB;	padding: 10px 10px 10px 33px;margin-bottom: 15px;color: #555555;}</style>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM7FZN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PM7FZN');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSV552"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSV552');</script>
<!-- End Google Tag Manager -->

<script>
   var a = "//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958"; //script location
   var domainList = ["www.globe.com.ph", "shop.globe.com.ph"]; //List of production domains

  
   if (domainList.indexOf(location.hostname) < 0) {
     a += "-staging"
   }

   document.write(unescape('%3Cscript type="text/javascript" src="'+a+'.js"%3E%3C/script%3E')); 
</script>

<!-- BROADWAY HEADER -->
<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  <style type="text/css">.back2site a{
color:#fff;
}
.top-header-new{
background: #58595b;
padding: 3px 3% 3px 3%;
color: #fff;
}
span.call-text-top{
color: #fff;
}
span.call-text-top a{
color: #fff;
text-decoration: none;
}
span.m-left-right{
margin-left:10px;
margin-right: 10px;
}
.store-locator a{
	color:#fff;
}
.store-locator a:hover{
	color:#fff;
}

  .back2site a u, span.store-locator a{
      font-size:12px !important;

    }
</style>


<div class="headerNavTop">
  <div id="new-header" class="">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 logo-wrapper header-blocks">
            <div class="dropdown">
              <div class="menu-list">
                <button class="btn dropdown-toggle" type="button" id="dropdown-button" data-toggle="dropdown">
                  <span class="icon-menu"></span>
                </button>
                <div class="dropdown-menu" role="menu" aria-labelledby="dropdown-button">
                  <div class="nav-list">
                    <ul>
                      <li id="1" role="presentation"><a id="header-about-globe" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
                      <li id="2" role="presentation"><a id="header-personal" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Personal</a></li>
                      <li id="3" role="presentation"><a id="header-sme" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://mybusiness.globe.com.ph/">SME</a></li>
                      <li id="4" role="presentation"><a id="header-enterprise" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://business.globe.com.ph/">Enterprise</a></li>
                      <li id="5" role="presentation"><a id="header-help-support" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Help & Support</a></li>
                      <li id="6" role="presentation"><a id="header-myaccount" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">My Account</a></li>
                    </ul>
                  </div>
                  <div class="nav-container">
                    <div class="nav-result1">
                    </div>
                    <div class="nav-result2">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-personal-shop" href="http://shop.globe.com.ph">
                                  <span class="icon icon-phones"></span>
                                  <p class="text-center">Shop</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-postpaid" href="http://www.globe.com.ph/postpaid">
                                  <span class="icon icon-postpaid"></span>
                                  <p class="text-center">Postpaid</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-prepaid" href="http://www.globe.com.ph/prepaid ">
                                  <span class="icon icon-prepaid"></span>
                                  <p class="text-center">Prepaid</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-internet" href="http://www.globe.com.ph/internet">
                                  <span class="icon icon-internet"></span>
                                  <p class="text-center">Internet</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-international" href="http://www.globe.com.ph/international">
                                  <span class="icon icon-international"></span>
                                  <p class="text-center">International</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-gocash" href="http://www.globe.com.ph/gcash">
                                  <span class="icon icon-gcash"></span>
                                  <p class="text-center">GCash</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-rewards" href="http://www.globe.com.ph/rewards">
                                  <span class="icon icon-rewards"></span>
                                  <p class="text-center">Rewards</p>
                              </a>
                          </li>
                           <li>
                              <a id="header-personal-entertainments" href="http://downloads.globe.com.ph/">
                                  <span class="icon icon-entertainment"></span>
                                  <p class="text-center">Entertainment</p>
                              </a>
                          </li>
                      </ul>
                    </div>
                    <div class=" nav-result3"></div>
                    <div class=" nav-result4"></div>
                    <div class=" nav-result5">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-help&support-faq" href="http://www.globe.com.ph/help">
                                  <span class="icon icon-faq"></span>
                                  <p class="text-center">FAQs</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-phoneconfig" href="http://www.globe.com.ph/help/guide">
                                  <span class="icon icon-phoneconfig"></span>
                                  <p class="text-center">Phone Configuration</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-troubleshoot" href="http://www.globe.com.ph/help/troubleshooting">
                                  <span class="icon icon-troubleshoot"></span>
                                  <p class="text-center">Basic Troubleshooting</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-community" href="http://community.globe.com.ph">
                                  <span class="icon icon-community"></span>
                                  <p class="text-center">Ask the Community</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-storelocator" href="http://www.globe.com.ph/store-locator">
                                  <span class="icon icon-locator"></span>
                                  <p class="text-center">Store Locator</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-contactus" href="http://www.globe.com.ph/contactus">
                                  <span class="icon icon-contactus"></span>
                                  <p class="text-center">Contact Us</p>
                              </a>
                          </li>
                                                    <li>
                              <a id="header-help&support-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">
                                  <span class="icon icon-livechat"></span>
                                  <p class="text-center">Chat</p>
                              </a>
                          </li>
                                                </ul>
                    </div>

                    <div class=" nav-result6">
                      <div class="container">
                        <div class="row">
                          <div class="col-xs-1 button-group-holder text-center">
                            <h2 class="h2-first">Enjoy more of Globe</h2>
                                                          <a id="header-myaccount-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan" id="login" class="btn btn-block">Login</a>
                              <hr>
                              <a id="header-myaccount-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan" id="signup" class="btn btn-block">Sign Up</a>
                                                        </div>
                          <div class="col-xs-6 want-info-holder">
                              <h2 class="h2-second"><span class="icon-myaccount"></span>I want to</h2>
                              <div class="row">
                                <div class="col-xs-5">
                                  <a id="header-myaccount-iwant1" href="http://www.globe.com.ph/enrolled-accounts"> <p>&#8226; View/edit my account details</p> </a>
                                  <a id="header-myaccount-iwant2" href="http://www.globe.com.ph/paybill"><p> &#8226; Pay my bill</p></a>
                                  <a id="header-myaccount-iwant3" href="http://www.globe.com.ph/paperlessreg"><p> &#8226; Sign up for paperless billing</p></a>
                                  <a id="header-myaccount-iwant4" href="http://www.globe.com.ph/autopayreg"><p> &#8226; Enroll in Auto Pay service</p></a>
                                  <a id="header-myaccount-iwant5" href="http://www.globe.com.ph/form-transfer-of-ownership" target="_blank"><p> &#8226; Transfer my account</p></a>
                                </div>
                                <div class="col-xs-7">
                                  <a id="header-myaccount-iwant6" href="https://mygcash.globe.com.ph/gcashonline"><p> &#8226; Access my GCash </p></a>
                                  <a id="header-myaccount-iwant7" class="modal-trigger" href="//www.globe.com.ph/form-check-activation-status"><p> &#8226; Check my line activation status</p></a>
                                  <a id="header-myaccount-iwant8" class="modal-trigger" href="//www.globe.com.ph/form-check-outstanding-balance"><p> &#8226; Check my postpaid outstanding balance</p></a>
                                  <a id="header-myaccount-iwant9" href="http://dbschenkerepod.com.ph/globe/customerview.php" target="_blank"><p> &#8226; Track the delivery status of my device/SIM</p></a>
                                  <a id="header-myaccount-iwant10" href="http://www.globe.com.ph/updateinfo" target="_blank"><p> &#8226; Update my account information </p></a>
                                </div>
                              </div>
                          </div>
                          <div class="col-xs-5 help-info-holder">
                            <h2 class="h2-third"><span class="icon-question"></span>Get help with</h2>
                              <a id="header-myaccount-gethelp1" href="http://www.globe.com.ph/help/guide"><p> &#8226; Configuring my phone</p></a>
                              <a id="header-myaccount-gethelp2" href="http://www.globe.com.ph/help/troubleshooting"><p> &#8226; Troubleshooting my device</p></a>
                              <a id="header-myaccount-gethelp3" href="http://www.globe.com.ph/help/roaming/about-international-roaming"><p> &#8226; Activating my international roaming</p></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <script>
                      if($('#new-header .nav-container .nav-result6').is('.f1731'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"min-height": "initial !important"});
                      }
                      if($('#new-header .nav-container .nav-result6').is('.f1731x'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"height": "172px"});
                      }
                    </script>

                  </div>
                </div>
              </div>
            </div>
                            <div id="logo-holder">
                <a href="http://shop.globe.com.ph/">
                  <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
                </a>
              </div>
                        </div>
          
          <div class="col-lg-3 category-dropdown header-blocks">
              <a id="header-shop-by-category" class="btn fontsize24 btn-lg absolute-center dropdown-toggle " data-toggle="dropdown">
                Shop by Plan <span class="glyphicon caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                              <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                              <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                            </ul>
          </div>

          <div class="col-lg-4 search-wrapper header-blocks ">
            <div class="search-inner-addon ">
              <form>
                <fieldset>
                  <img class="searchIcon" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
                  <input id="searchbox" name="searchterms" type="search" class="form-control" placeholder="Search Here" maxlength="50" />
                </fieldset>
                <ul id="searchResult">
                   <li class="result-holder"></li>
                   <li class="plainText">We Recommend</li>
                   <li class="searchRecommend"></li>
                   <li class="see-all">See All</li>
                </ul>
            </form>
            </div>
          </div>

            <div class="search-account-wrapper">
              <div class="col-lg-1 cart-wrapper header-blocks" id="cart-header">
  <div class="btn-cart  cart-total-hide-empty">
    <a id="header-cart-btn" class="dropdown-toggle" data-toggle="dropdown" onclick="toggleCart()">
      <span class="badge custom-badge " id="cart-total">0</span>
      <img class="absolute-center cart-image" src="catalog/view/theme/broadwaytheme/images/icons/icon-cart.png">
    </a>
        <ul class="dropdown-menu" id="cart-content" role="menu">
          <li>
        <h4 style="text-align: center; margin-top: 0px;">Your shopping cart is empty!</h4>
      </li>
        </ul>

    <ul class="dropdown-menu" id="cart-notif" role="menu" style="display: none;">
      <li>
        <div class="cart-item-thumbnail">
          <img src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/check-50x50.png" alt="" title="" />
        </div>
        <div class="cart-item-detail cart-item-detail-success">
          <h4>Item successfully added to cart</h4>
        </div>
      </li>
      <li>
        <div class="cart-btn-view">
        <a href="http://shop.globe.com.ph/cart" class="btn btn-block btn-bw-orange">View Cart</a>
        </div>
      </li>
    </ul>
  </div>
</div>
              <div class="col-lg-1 account-wrapper ">
                  
                                      <a id="header-account-signin-btn" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan">
                      <img class="" src="catalog/view/theme/broadwaytheme/images/icons/icon-user2.png">
                      <span class="account-text">Sign In</span>
                    </a>
                                </div><!--End of account wrapper--> 
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="rwd-header-tablet" class="collapse hidden-xs-*">
  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
      <li class="dropdown">
        <a id="header-personal" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-shop" href="http://shop.globe.com.ph">Shop</a></li>
          <li><a id="header-postpaid" href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
          <li><a id="header-prepaid" href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
          <li><a id="header-internet" href="http://www.globe.com.ph/internet">Internet</a></li>
          <li><a id="header-international" href="http://www.globe.com.ph/international">International</a></li>
          <li><a id="header-gcash" href="http://www.globe.com.ph/gcash">GCash</a></li>
          <li><a id="header-rewards" href="http://www.globe.com.ph/rewards">Rewards</a></li>
          <li><a id="header-entertainment" href="http://downloads.globe.com.ph/">Entertainment</a></li>
        </ul>
      </li>
      <li><a id="header-sme" href="http://mybusiness.globe.com.ph/">SME</a></li>
      <li><a id="header-enterprise" href="http://business.globe.com.ph/">Enterprise</a></li>
      <li class="dropdown">
        <a id="header-help&support" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-faqs" href="http://www.globe.com.ph/help">FAQs</a></li>
          <li><a id="header-phoneconfig" href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
          <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
          <li><a id="header-community" href="http://community.globe.com.ph">Ask the Community</a></li>
          <li><a id="header-store-locator" href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
          <li><a id="header-contactus" href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                    <li><a id="header-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                  </ul>
      </li>
      <li class="dropdown">
        <a id="header-myaccount" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
                      <li><a id="header-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan">Login</a></li>
            <li><a id="header-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan">Sign Up</a></li>
                  </ul>
      </li>
    </ul>
  </div>
</div>

<nav id="rwd-header" class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button class="btn dropdown-toggle rwd-header-dropdown-button" type="button" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="icon-menu"></span>
      </button>
              <div class="logo-holder">
          <a href="http://shop.globe.com.ph/">
            <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
          </a>
        </div>
              <div class="user-holder ">
            <div class="user-holder-indicator"></div>
                      <a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan">
              <img class="" src="catalog/view/theme/broadwaytheme/images/icons/icon-user2.png" />
            </a>
                  </div>

        <div class="cart-holder"></div>
        <script type="text/javascript">
          function headerRwdChangeLocation() {
            var winWidth = $(window).width();
            var isFirefox = typeof InstallTrigger !== 'undefined';
            if(isFirefox) {
              if(winWidth <= 950) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 950) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }

            else {
              if(winWidth <= 949) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 949) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }
            
            if(winWidth > 991) {
              $("#rwd-search").css('display', 'none');
            }
          }
          $(document).ready(function($) {
            $(window).resize(function() {
              headerRwdChangeLocation();
            });
            headerRwdChangeLocation();
          });
        </script>
        <div class="search-holder">
          <a href="" class="rwd-search-trigger">
            <img class="searchIcon hidden-xs hidden-sm hidden-md" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
            <img class="searchIconRwd visible-md-*" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
          </a>
        </div>
    </div>
    <div id="rwd-navbar" class="navbar-collapse collapse">
      <button class="rwd-navbar-close" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="blue-close"></span>
      </button>
      <br />
      <ul class="nav navbar-nav">
        <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle open rwd-header-personal-trigger" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://shop.globe.com.ph">Shop</a></li>
            <li><a href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
            <li><a href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
            <li><a href="http://www.globe.com.ph/internet">Internet</a></li>
            <li><a href="http://www.globe.com.ph/international">International</a></li>
            <li><a href="http://www.globe.com.ph/gcash">GCash</a></li>
            <li><a href="http://www.globe.com.ph/rewards">Rewards</a></li>
            <li><a href="http://downloads.globe.com.ph/">Entertainment</a></li>
          </ul>
        </li>
        <li><a href="http://mybusiness.globe.com.ph/">SME</a></li>
        <li><a href="http://business.globe.com.ph/">Enterprise</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://www.globe.com.ph/help">FAQs</a></li>
            <li><a href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
            <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
            <li><a href="http://community.globe.com.ph">Ask the Community</a></li>
            <li><a href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
            <li><a href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                        <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                      </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
                          <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan">Login</a></li>
              <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/shop-by-plan">Sign Up</a></li>
                      </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="rwd-search">
  <input id="rwd-searchbox"  type="search" class="form-control" placeholder="Search Here" maxlength="50" />
  <img id="rwd-search-trigger" src="catalog/view/theme/broadwaytheme/images/icons/icon-search.png" />
  <div class="rwd-search-results">
      <ul id="rwd-search-results-list">
         <li class="rwd-result-holder">
            <div class="text-result">test</div>
         </li>
         <li class="plain-text blue-line">WE RECOMMEND</li>
         <li class="img-result first-img-result"></li>
         <li class="plain-text rwd-search-see-all"><a>See All</a></li>
      </ul>
  </div> <!-- End of rwd-search-results-recommended -->
</div>

<nav id="rwd-sub-header" class="navbar navbar-default ">
  <div class="container rwd-sub-header-medium">
    <div class="navbar-header">
      <div class="menu-item">
        <a type="button" data-toggle="collapse" data-target="#rwd-sub-navbar" aria-expanded="false" aria-controls="rwd-sub-navbar">
          Shop by Plan <span class="caret"></span>
        </a>
      </div>
      <a class="dot-menu" type="button" data-toggle="collapse" data-target="#rwd-sub-navbar-2" aria-expanded="false" aria-controls="rwd-sub-navbar-2">
        <img src="catalog/view/theme/broadwaytheme/images/icons/dot-menu.png">
      </a>
    </div>
    <div id="rwd-sub-navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                  <li class="rwd-sub-navbar-active"><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
              </ul>
    </div>
    <div id="rwd-sub-navbar-2" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
            
                                                    
                          <li><a href="http://shop.globe.com.ph/contact">Request a call</a></li>

                          <li><a href="tel:+027301010">(02) 730-1010</a></li>
                                    </ul>
    </div>
  </div>
  <div class="container rwd-sub-header-small">
    <div class="collapse navbar-collapse sub-header-sm" id="bs-example-navbar-collapse-7">
      <ul class="nav navbar-nav">
        <li role="presentation" class="dropdown offset">
            <a class="link" href="#" data-toggle="dropdown">
              Shop by Plan <span class="glyphicon caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                          <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                          <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                        </ul>
        </li>
      </ul>

    </div>
  </div>
</nav>

<script type="text/javascript">


  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }
    
  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }

  //Input types
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });
  
  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);
    
    
    if($("#rwd-searchbox").val() == "") {
      
      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){
      
      $('.rwd-search-results').slideUp();
      
      },500)
  }); 


  
  $('input[name=\'searchterms\']').on('blur', function(e){
      
      setTimeout(function(){
        
      },500);
      var ifIE = msieversion();
      
      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {
         
        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
         
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

          
          var rv = ua.indexOf('rv:');
          return true;
          
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
        
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
     
    }
  });
  
  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }
    
  });
  
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }
    
  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>
<div id="container" class="margTop">

<!-- BREADCRUMBS START -->
<div class="container-fluid breadcrumb-container headerNav" itemscope itemtype="http://schema.org/BreadcrumbList">
  <div class="container">
    <div class="row">
      <div class="breadcrumb pull-left">
                <div></div>
        <div id="breadcrumb-0" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/" itemprop="item">
            <span class="blue-link" itemprop="name">Shop Home</span>
            <meta itemprop="position" content="1" />
          </a>
        </div>
                <div> | </div>
        <div id="breadcrumb-1" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/shop-by-plan" itemprop="item">
            <span class="black-link" itemprop="name">Shop by Plan</span>
            <meta itemprop="position" content="2" />
          </a>
        </div>
              </div>

  </div>
</div>
<!-- BREADCRUMBS END -->

<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  </div>

<div id="notification"></div>

<div id="filters-overlay" style="display:none;">
  <div class="filters-loader" align="center">
    <img src="catalog/view/theme/broadwaytheme/images/default.gif" border="0" />
  </div>
</div>

<div class="container container-gallery">
  <div id="filter-wrapper" class="gallery-sidebar">
    <div class="section-filter">
      <div class="filter-container">

                    <div class="filter-categories">
            <div class="filter-categories-label">
                <label>Select Type</label>
            </div>  
            <ul id="filter-maincat">  
              <!-- Subcategories -->
                              <li>
                  <label>
                    <input data-link=""  value="105" type="radio" name="category-filter" class="filter-subcat-input-105">Broadband</label>
                </li>
                              <li>
                  <label>
                    <input data-link=""  value="4" type="radio" name="category-filter" class="filter-subcat-input-4">Mobile Broadband</label>
                </li>
                              <li>
                  <label>
                    <input data-link=""  value="5" type="radio" name="category-filter" class="filter-subcat-input-5">MyLifestyle Plans</label>
                </li>
                              <li>
                  <label>
                    <input data-link=""  value="99" type="radio" name="category-filter" class="filter-subcat-input-99">MyStarter Plan</label>
                </li>
                              <li>
                  <label>
                    <input data-link=""  value="100" type="radio" name="category-filter" class="filter-subcat-input-100">Best Value Gadget Bundles</label>
                </li>
                          </ul>
          </div>

          <div id="accordion" class="accordion">
                      </div>

          <div class="filter-links-container">
                      </div>

                    <div class="filter-clear">
            <a onclick="clearFilter('');">Clear all filters </a>
          </div>

      </div>
            <div class="filter-container-rwd hidden-lg hidden-md">
        <div id="filter-button-rwd">
          <h4><span class="label label-default"><a id="filters-btn">Filters &#43;</a></span></h4>
        </div>
      </div>
      
    </div>
  </div>
  <div class="gallery-main">
    <div class="section-banner">
        <div class="custom-banner-bg container-fluid" style="background: rgb(255, 255, 255); padding: 0px;"><img alt="Shop by Plan Image Here" src="${properties.fileReference}" style="width: 100%;" title="Get the plan that offers more to your life than simply connecting you to the Internet. Shop by plan now and choose the one that fits your lifestyle!" />
<div class="container">
</div>
</div>
    </div>


      <div class="section-filter-rwd-toggle hidden-lg hidden-md" style="display: none;">
        
                <div class="section-subcategories-rwd col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="btn-group btn-block filter-group-rwd-subcat">
              <div class="filter-group-label">
                <label>Select Type</label>
              </div>
              <button type="button" class="btn btn-default left-btn btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Please Select <span class="caret"></span>
              </button>
              <ul class="filter-menudropdown dropdown-menu btn-block">
                                  <li><label><input data-link="http://shop.globe.com.ph/shop-by-plan/broadband"  value="105" type="radio" name="category-subcat-rwd" class="filter-subcat-input-rwd-105">Broadband</label></li>
                                  <li><label><input data-link="http://shop.globe.com.ph/shop-by-plan/mobile-broadband"  value="4" type="radio" name="category-subcat-rwd" class="filter-subcat-input-rwd-4">Mobile Broadband</label></li>
                                  <li><label><input data-link="http://shop.globe.com.ph/shop-by-plan/mylifestyle-plans"  value="5" type="radio" name="category-subcat-rwd" class="filter-subcat-input-rwd-5">MyLifestyle Plans</label></li>
                                  <li><label><input data-link="http://shop.globe.com.ph/shop-by-plan/mystarter-plan"  value="99" type="radio" name="category-subcat-rwd" class="filter-subcat-input-rwd-99">MyStarter Plan</label></li>
                                  <li><label><input data-link="http://shop.globe.com.ph/shop-by-plan/best-value-gadget-bundles-plan"  value="100" type="radio" name="category-subcat-rwd" class="filter-subcat-input-rwd-100">Best Value Gadget Bundles</label></li>
                              </ul>
            </div>
          </div>
        </div>
        
        <div class="section-filters-only-container">
                  </div>

        <div class="section-hyperlinks-rwd col-md-12 col-sm-12 col-xs-12">
                </div>

                <div class="section-clear-rwd col-md-12 col-sm-12 col-xs-12 text-center">
          <a  >Clear all filters</a>
        </div> 

      </div>
    </div>
    
    <div class="section-pagination-rwd"></div>
    <div class="section-total-result-rwd"></div>
    <div class="section-products">
        <!-- SEXY GALLERY STARTS HERE-->
        <div class="sexy-gallery-container sexy-gallery-container-hide">
          <div id="sexy-gallery" class="sexy-gallery">
            <div class="container-products">
              <div class="row sexy-center">

                  <div id="products"><input type="hidden" id="cart_redirect" value="http://shop.globe.com.ph/cart">
                      <div class="col-lg-3 sexy-gallery-item" id="item-39" data-state="grid" data-id="39">
                          <div class="item-container" style="padding: 10px 33px 33px 239px;">  
                              <div class="item-element-container">      
                                  <div class="item-img-container">
                                      <a href="http://shop.globe.com.ph/products/plan/mylifestyle-plan-999?xpath=1&option=174">
                                          <img class="group grid-group-image img-responsive" alt="Img Here" src="${properties.imgpath}" alt="" />
                                      </a>









                                      <div class="item-badge"></div>  
                                  </div>      
                                  <div class="item-title">
											<% 
										ServletContext sc = getServletContext();
   										 String plan = sc.getAttribute("planValue").toString();
                                       String price = sc.getAttribute("monthlyprice").toString();%>
                                      <a class="item-title-link" href="/content/globeEcommerce/addtocartpage.html"><%= plan%></a></div>    
                                  <div class="item-filters" id="chosen-product-39">		
                                      <input type="hidden" name="category" class="category-val" value="Shop by Plan">	
                                      <input type="hidden" name="product_id" value="39"><div class="">	
                                      <span class="item-attr-label">Plan: </span><span class="item-attr-value"><%= price%></span></div><div class="">	<span class="item-attr-label">Device: </span><span class="item-attr-value"><a href="/content/globeEcommerce/addtocartpage.html">Select your device</a></span></div><input type="hidden" class="hndSetPlan" value="myLifestyle Plan 999"><input type="hidden" id="option-39-92" name="option[92]" value="174"><input type="hidden" class="hndSetby" value="">      </div><div class="sexy-CTA-blue" data-id="chosen-product-39"><a id="category-product-addtocart" class="btn-add-to-cart" data-id="chosen-product-39" href="/content/globeEcommerce/addtocartpage.html">VIEW PLAN DETAILS</a></div> 

                              </div>  </div>  

                  <div class="animation_image gallery_loader" style="display:none;" align="center">
                    <img src="catalog/view/theme/broadwaytheme/images/loader.gif" style="width: 20px; height: 20px;" border="0" />&nbsp;&nbsp;Please wait...
                    <br /><br />
                  </div>
              </div>      
            </div>
          </div>
        </div>
        <div class="sexy-gallery-loader-first" style="display:none;" align="center">
          <br /><br />
          <img src="catalog/view/theme/broadwaytheme/images/loader.gif" style="width: 20px; height: 20px;" border="0" />&nbsp;&nbsp;Please wait...
        </div>
        <!-- SEXY GALLERY ENDS HERE-->
        <div class="hidden-filters">
          <input type="hidden" id="url_filter" name="url_filter" value="" />
          <input type="hidden" id="product-current-category" name="current_category_id" value="" />
          <input type="hidden" name="category_id" value="1" />
          <input type="hidden" id="page" name="page" value="1" />
          <input type="hidden" id="limit" name="limit" value="12" />
          <input type="hidden" id="product_total" name="product_total" value="26" />
          <input type="hidden" id="sort" name="sort" value="default" />
          <input type="hidden" id="sort_url" name="sort_url" value="" />
          <input type="hidden" id="path" name="path" value="1" />
        </div>
    </div>

    <div class="section-nav">
      <div class="section-nav-data bottom">
        <div class="row section-nav-data-bottom">

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 section-nav-data-top">
            <a class="gallery-back-to-top">Back to Top</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid wrapper-seo">
  <div class="container container-seo">
    <div class="section-seo">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <hr />
<p><span style="line-height: 1.6em; font-size: 14px;"><b>Get the plan that offers more to your life than simply connecting you to the Internet. Shop by plan now and choose the one that fits your lifestyle!</b></span></p>

<p><span style="line-height: 1.6em; font-size: 12.5px;">The new Globe Online Shop will help you get the best mobile postpaid plan that lets you listen to your favorite tracks, check the next shopping or airfare sale, stay connected with family or friends at home or abroad, and do a whole lot more!</span></p>

<p><strong><span style="line-height: 1.6em; font-size: 12.5px;">Today’s featured myLifestyle Plan lets you stay connected with your pals and gets you your daily dose of chitchat and LOL from your BFFs! With myLifestyle Plan 999, choose your preferred Lifestyle Packs on top of your built-in Unli calls and texts to Globe and TM.</span></strong></p>

<p><span style="line-height: 1.6em; font-size: 12.5px;">You can also check out our diverse selection of mobile postpaid plans, with two (2) types to choose from: <strong>myLifestyle Plans</strong> or <strong>myStarter Plans</strong>. myLifestyle Plans allow you to choose your preferred surfing allowance, lifestyle pack, and other consumables. With the budget-friendly myStarter Plans, set your monthly plan limit, select your gadget, and use your consumables for Prepaid promos.</span></p>

<p><span style="line-height: 1.6em; font-size: 12.5px;">Choose your Globe postpaid plan now! Grab your virtual shopping cart now and go to our plan gallery.</span></p>

<p><span style="font-size:12.5px;"><strong>How to Order</strong><br />
Once you’re done choosing a plan, simply tap Add to Cart, Checkout, and then fill out the online application form. Make sure that you submit correct and complete requirements so you can get your online application approved much faster. Remember to check for misspelled words or&nbsp;typos,&nbsp;and fill out empty boxes. The files you attach must also be clear, valid, and up-to-date. Please see guidelines in the online application form for more details.</span></p>

<p><span style="font-size:12.5px;"><strong>Payment Methods</strong><br />
We have three payment methods for you—straight-payment Credit Card, Cash-on-Delivery, or GCash. If you’re using GCash, your GCash wallet should have enough funds to cover the initial cost of your order.</span></p>

<p><span style="font-size:12.5px;"><strong>Safe and Secure Online Shopping</strong><br />
We are committed in providing a wonderful online shopping experience for you. Rest assured that your transactions are secured and that we won't distribute your information for any reason.</span></p>

<hr />
<style type="text/css">.myHeader{
    color:#000000;
    float:left;
  	  
  }
  .ulStyle li
  {
    display:block;
     text-align: left;
  }
    
    .ulStyle li a:hover
  {
    color:#3d1d5e;
  }
</style>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="header myHeader">
<p>Shop by Plan</p>
</div>

<p>&nbsp;</p>

<hr />


</div>
</div>

<hr style="margin-top:14px" /></div>

<p style="text-align: center;"><span style="font-size:14px;"><strong>Thank you and enjoy your online shopping with Globe!</strong></span></p>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(window).load(function(){
    RWDSexyGallery();
    submitCart();
  
    //display products after rendering
    $("#products").show();
    $("#products").css('opacity', 0);
  
    $("#products").css('opacity', 1);
    $(".animation_image").hide();
    
    sexygallery_ComputeHeight();
  
    // Analytics
    dataLayer.push({
      'ecommerce': {
        'currencyCode': 'PHP',                       // Local currency is optional.
        'impressions': [{"name":"Plan","id":"39","price":"PHP 999.00","brand":"Globe","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":1},{"name":"Plan","id":"822","price":"PHP 0.00","brand":"","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":2},{"name":"Plan","id":"43","price":"PHP 1,499.00","brand":"Globe","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":3},{"name":"Plan","id":"793","price":"PHP 0.00","brand":"","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":4},{"name":"Plan","id":"794","price":"PHP 0.00","brand":"","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":5},{"name":"Plan","id":"42","price":"PHP 599.00","brand":"Globe","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":6},{"name":"Plan","id":"969","price":"PHP 0.00","brand":"","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":7},{"name":"Installation","id":"1516","price":"PHP 1,299.00","brand":"","category":"Tattoo Home","variant":"","list":"Shop by Plan","position":8},{"name":"Installation","id":"1519","price":"PHP 1,599.00","brand":"","category":"Tattoo Home","variant":"","list":"Shop by Plan","position":9},{"name":"Installation","id":"1522","price":"PHP 1,899.00","brand":"","category":"Tattoo Home","variant":"","list":"Shop by Plan","position":10},{"name":"Plan","id":"1508","price":"PHP 1,886.00","brand":"","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":11},{"name":"Plan","id":"1512","price":"PHP 3,027.00","brand":"","category":"Mobile Postpaid","variant":"","list":"Shop by Plan","position":12}]      }
    });
});
</script>

</div>

    <div id="footer-top-wrap" class="container-fluid" style="background: #333333;">
    <div class="container footer-top-holder">
      <!-- INSERT FOOTER CONTENT HERE -->
      <style type="text/css">#footer hr.footer-divider{
    border-top: none !important;
  }

  #footer-top-wrap{
  background: #f6f6f6 !important;
  }

  .bg-gray{
   background:#f6f6f6;
  }
  .padd-top-bot{
     padding-top: 5px;
    padding-bottom: 0px;
  }
  .padd-top-bot10{
  padding-top: 10px;
  padding-bottom: 10px;
  }
  .f-shipping{
    text-align: center;
  }
  .f-payment{
    text-align: center;
  }
  .f-call{
    text-align: center;
  }
  .f-shipping a {
     text-decoration: none;
     color: #333;
  }
  .footer-options div{
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .f-shipping img{
  margin-right: 10px;
  margin-bottom: 0px;

  }
  .f-payment img{
  margin-right: 10px;
  margin-bottom: 0px;
  }
  .f-call img{
  margin-right: 10px;
  margin-bottom: 0px;
  }

   @media screen and (max-width: 768px) {
   .f-shipping{
      border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-payment{
       border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-call{
    text-align: center;
  }
}
@media screen and (max-width: 445px) {
.indent-mobile{
margin-left: 12%;
}
}

@media screen and (max-width: 391px) {
.indent-mobile{
margin-left: 12%;
}
}
@media screen and (max-width: 389px) {
.indent-mobile{
margin-left: 0%;
}
}
</style>
<div id="footer-top">
<div class="container-fluid bg-gray padd-top-bot">
<div class="container">
<div class="row footer-options">
<div class="col-sm-4 col-md-4 col-lg-4 f-shipping"><a href="http://shop.globe.com.ph/help" style="color:black" title="Click to learn more about our shipping, delivery and returns policies"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/shippingA.png" /><span class="m-top" title="Click to learn more about our shipping, delivery and returns policies">FREE SHIPPING ON ALL ORDERS</span></a></div>

<div class="col-sm-5 col-md-5 col-lg-5 f-payment"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/payment-method.png" /><span class="m-top">CASH ON DELIVERY, MAJOR<span class="indent-mobile"> CREDIT CARDS &amp; GCASH</span></span></a></div>

<div class="col-sm-3 col-md-3 col-lg-3 f-call"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/call.png" /><span class="m-top">CALL (02)730-1010</span></a></div>
</div>
</div>
</div>
</div>
    </div>
  </div>
  <div id="footer">
    <hr class="footer-divider">
    <div class="container footer-holder">
        <footer class="main shadow row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl>
                  <dt><a href="http://www.globe.com.ph/about-globe">Globe Telecom</a></dt>
                  <dd><a href="http://www.globe.com.ph/about-globe/company-info">Corporate Information</a></dd>
                  <dd><a href="http://www.globe.com.ph/corporate-governance">Corporate Governance</a></dd>
                  <dd><a href="http://www.globe.com.ph/investor-relations">Investor Relations</a></dd>
                  <dd><a href="http://www.globe.com.ph/careers">Careers</a></dd>
                  <dd><a href="http://www.globe.com.ph/press-room">Press Room</a></dd>
                  <dd><a href="http://www.globe.com.ph/globelife">GlobeLife</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl>
                  <dt>Products and Services</dt>
                  <dd><a href="http://www.globe.com.ph/postpaid">Postpaid Plans</a></dd>
                  <dd><a href="http://www.globe.com.ph/prepaid/call-and-text-offers">Prepaid Promos</a></dd>
                  <dd><a href="http://www.globe.com.ph/surf">Mobile Internet</a></dd>
                  <dd><a href="http://tattoo.globe.com.ph/">Tattoo Broadband</a></dd>
                  <dd><a href="http://www.globe.com.ph/gcash">GCash</a></dd>
                  <dd><a href="http://shop.globe.com.ph/home">Online Shop</a></dd>
              </dl>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear1">
                  <dt><a href="http://business.globe.com.ph/">Business</a></dt>
                      <dd><a href="http://business.globe.com.ph">Enterprise and Wholesale</a></dd>
                      <dd><a href="http://mybusiness.globe.com.ph">Small and Medium Business</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear2">
                  <dt><a href="http://www.globe.com.ph/worry-free-guarantee">Globe Guarantee</a></dt>
                  <dd><a href="http://www.globe.com.ph/terms-and-conditions">Terms and Conditions</a></dd>
                  <dd><a href="http://www.globe.com.ph/privacy-policy">Privacy Policy</a></dd>
                  <dd><a href="http://www.globe.com.ph/aup">Fair Use Policy</a></dd>
              </dl>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear3">
                  <dt><a href="http://www.globe.com.ph/contactus">Contact Us</a></dt>
                  <dd><a href="http://www.globe.com.ph/help">Help and Support</a></dd>
                  <dd><a href="http://community.globe.com.ph/">Ask the Community</a></dd>
                  <dd><a href="http://www.globe.com.ph/store-locator">Store Locator</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=shop.globe.com.ph&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script> </div>
        </footer>
    </div>
    <div class="footer-legals">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div class="content-holder text-center footer-links">
              <p>&copy; 2016 Globe Telecom Inc.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  


</body>
</html>
     <script type="text/javascript">
$(document).ready(function(){
    
    });
$("#header-shop-by-category").click(function(){
    
$.ajax({
    type:"GET",
    
url : "/bin/epcortex/plandetails",
    success:function(data){
        window.location.href="http://localhost:4504/content/globeEcommerce/shopbyplanpage.html";
	}
});
});

     </script>
