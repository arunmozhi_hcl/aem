<%@ include file="/libs/foundation/global.jsp"%>

<html>
     <cq:includeClientLib categories="productdetails"/>
    <head>


<title>Checkout</title>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/checkout-common-form.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/pikaday.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/dropzone.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/dropzone.min.css" media="screen" />




</head>


 <body>


<div id="checkout-content" class="container" style="min-height: 388px;">
	<div class="required-prompt"><span></span></div>
	<div id="checkout-form-container" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
			<div id="form-container" class="row" style="height: 432px;">
				<div id="section-container">
					<form id="app_form" class="" action=""  enctype="multipart/form-data">
				    <section id="personal-details" class="panel">
				        <div class="panel-content"></div>
				    </section>
				    <section id="additional-personal-details" class="panel prev">
				        <div class="panel-content"></div>
				    </section>
				    <section id="shipping-details" class="panel current">
				        <div class="panel-content"><div class="panel-heading">Step 3: Shipping Details</div>
<div class="panel-body no-border">
            <div class="panel-row row" style="z-index: 4;">
            <div class="form-group" style="z-index: 1; height: auto;">
                <label class="field-label">Payment Method 
                                        <text>You will only be charged when your application has been approved.</text>
                                    </label>
                <div class="radio-group sm row payment_method">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                            <label class="radio" data-code="cod">
                              <span id = "payment_mode" style = "background-image: url('/content/dam/globeEcommerce/radio_button_small_selected.png') !important;" 
							  class="radio-button setpayment selected"></span>Cash On Delivery                           </label> 

                        
                                                                    
                            <label class="radio" data-code="cc">
                              <span style = "background-image: url('/content/dam/globeEcommerce/radio_button_small.png') !important;"  
							  class="radio-button setpayment " data-code="cc"></span> Credit Card                            </label>

                                                </div>
                        
                                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                            <label class="radio" data-code="gcash">
                              <span style = "background-image: url('/content/dam/globeEcommerce/radio_button_small.png') !important;" 
							  class="radio-button setpayment " data-code="gcash"></span> GCash                            </label>

                
            </div>
            <div></div>
        </div>
        <div class="panel-row row" id="panel-shipping" style="z-index: 3;">
                        <div class="form-group row" style="z-index: 4; height: auto;">
                                    <div class="label-container col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <label class="field-label">Delivery Address:</label>
                    </div>
                
                <div class="input-container col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                            <div class="toggle-select" id="toggle-select-shipping" name="ship-to">
                            <text class="selected">Use my billing address</text>
                            <span class="toggle-arrow"></span>
                            <ul class="toggle-select-options" id="toggle-select-options-shipping">
                                <li data-option="use-my-billing-address" data-value="1" class="selected-option">Use my billing address</li>
                                <li data-option="ship-to-different-address" data-value="0" class="">Ship to different address</li>
                            </ul>
                        </div>
                                        
                    
                </div>
                <span id="toggle-select-content" class="toggle-select-content-shipping col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 50px;">
                    <div id="use-my-billing-address" class="use-my-billing-address-shipping toggle-option-content toggle-option-content-shipping shown">
					<% 
										ServletContext sc = getServletContext();
   										 String address = sc.getAttribute("Address").toString();
//String city = sc.getAttribute("City").toString();
     									String zipcode = sc.getAttribute("Zip Code").toString();
										%>
                        <a onclick="changeAddress()">change address</a><h3><%=address%>, <%=zipcode%></h3>
                        
                    </div>
                    <div id="ship-to-different-address" class="ship-to-different-address-shipping toggle-option-content toggle-option-content-shipping form ">
                        <div class="shipping_note" style="display:none;"><div class="form-alert"><span class="alert-icon">&nbsp;</span>

<p>You will need to provide a proof of address for both billing and shipping address.</p>
</div>
</div>
                        <div class="panel-body">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="panel-row row" style="z-index: 2;">

                                        
                                        <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                                            <label class="field-label">Full Shipping Address (House #/Floor, Street, Village Name, Barangay)</label>
                                        
                                                                                            
                                            
                                                
                                                                                    </div>

                                                                                </div>
                                                                                                                                                                                                                                                                                                                                                            <div class="panel-row row" style="z-index: 1;">
                                        
                                        
                                        <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: auto;">
                                            <label class="field-label">Area (Makati, Quezon City, etc.)</label>
                                        
                                                                                            

                                                   
                                                <div id="shipping-details-val" class="details-val">
                                                    <span class="check-icon"></span>
                                                    <text></text>
                                                </div>
                                                                                    </div>

                                                                                                                                                                                                                            
                                        
                                        <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: auto; padding: 10px 6px;">
                                            <label class="field-label" style="margin: 0px 4px 5px;">Zip Code</label>
                                        
                                                                                            
                                                                                    </div>

                                                                                </div>
                                                                                                                                                                                                    </div>
                    </div>
                </span>
            </div>
        </div>
    
    
    
    
</div>
<div id="panel-bottom" class="row">
        <div class="left col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <!--  -->
        <a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
    </div>
    <div class="right col-lg-6 col-md-6 col-sm-6 col-xs-6">
        
        <a id="prev-step" onclick="setPreviousPanel('additional-personal-details', 'shipping-details')">Previous Step</a>
        <button id="button-shipping-details" class="btn" >  <a href ="http://localhost:4504/content/globeEcommerce/employmentdetailspage.html" style="color:white;">Next Step</a></button>
    </div>
</div>

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/shipping_details.js"></script>


</div>
				    </section>
				    <section id="financial-details" class="panel">
				        <div class="panel-content"></div>
				    </section>
			        <section id="document-upload-details" class="panel">
			            <div class="panel-content"></div>
			        </section>
                    <section id="terms-condition-details" class="panel">
                        <div class="panel-content"></div>
                    </section>
                    <section id="review-order-details" class="panel">
                        <div class="panel-content"></div>
                    </section>
                	</form>
			    </div>
		    </div>
		    
		</div>
		<div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><p><img class="img-responsive" id="norton-secured" src="https://shps3dv01.s3.amazonaws.com/media/data/norton_secured.png"></p>

<div class="row" id="contact-details">
<div class="info-block col-lg-12 col-md-12 col-sm-12 col-xs-4">
<p>Your personal information is safe and will always be kept private.</p>
</div>
</div>
</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">

    $(document).ready(function(){
        
    });
$("#button-shipping-details").click(function(){
   
	$.ajax({

		type:"GET",
		url: "/bin/payment",
		data: "payment_type="+"COD"  ,
		cache:false,
		success:function(data){
       
    }
	});
});
</script>	