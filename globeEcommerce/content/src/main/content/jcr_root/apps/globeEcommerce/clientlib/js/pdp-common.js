/*What Happens Inside?
  Description : Common function for resize of PDP Tabs greater than 768

  1. Detects the greatest number of character a product tab has
  2. Makes Default slick - necessary to make the item slick (Default needs 4 items to add arrows)
  3. Redetects if side of slick track(generated) is greater than the current container
    - Needed because sometimes there are only 4 items but the width of the character exceeds container
  4. If detected that the container is less than width of slick
    Creates another slick
      - If the highest character number of characters is 30 make slide to show 1
      - Default slide to show is 2
*/
var winWidth = $(window).width();

function pdpCommonGreaterThanResize(){
  //Detect if there are items with high number of characters
  //move this to first load only then pass - javascript is too heavy
  var highest = null;
  var hi = 0;
  var slidesToShow = 2;

  $("#tabs .key-tab").each(function(){
    var h = $(this).html().length;
    if(h > hi){
       hi = h;
       highest = $(this).html().length;  
    }    
  });
  
  if(highest > 30 && winWidth < 1200) {
    slidesToShow = 1;
  }

  setTimeout(function(){
      $("#tabs").unslick();
      $('#tabs').slick({
        infinite: false,
        cssEase: 'linear',
        adaptiveHeight: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        variableWidth: false
      });

      if(winWidth < 1025){
        $(".slick-slide a").removeClass("fontsize24").addClass("fontsize20");  
      }
      

      // Detect if the produced Slick track fits container, this is due to the difference in the widths of title
      var currentSlickTrackWidth= $('#tabs .slick-track').width() + 50;
      if(currentSlickTrackWidth > $('.container').width() && ($("#tabs .slick-slide").length < 4 || $('.container').width() < 1100 )){
        $("#tabs").unslick();
        $('#tabs').slick({
          infinite: true,
          cssEase: 'linear',
          adaptiveHeight: false,
          slidesToShow:slidesToShow,
          slidesToScroll: 1
        });
      }


  },200);
}

/*What Happens Inside?
  Description : Change side of popover depending on width to avoid space on the side
*/

function pdpMoveSideofPopOver(){

    if(winWidth < 768) {
       detectOrientation();
       window.onorientationchange = detectOrientation;
       function detectOrientation(){
          if(typeof window.onorientationchange != 'undefined'){
            if ( orientation == 0 ) {
                //Do Something In Portrait Mode
                    $('.popupTooltip').popover('destroy');
                    $('.popupTooltip').popover({
                        trigger:'click',
                        placement: 'left' 
                    });
            }
            else if ( orientation == 90 ) {
                //Do Something In Landscape Mode
                    $('.popupTooltip').popover('destroy');
                    $('.popupTooltip').popover({
                        trigger:'click',
                        placement: 'right' 
                    });
            }
            else if ( orientation == -90 ) {
                //Do Something In Landscape Mode
                $('.popupTooltip').popover('destroy');
                $('.popupTooltip').popover({
                    trigger:'click',
                    placement: 'right' 
                });
            }
            else if ( orientation == 180 ) {
                //Do Something In Portrait Mode
                    $('.popupTooltip').popover('destroy');
                    $('.popupTooltip').popover({
                        trigger:'click',
                        placement: 'left' 
                    });
            }
        }
      }
    } 

    else {
      $('.popupTooltip').popover('destroy');
      $('.popupTooltip').popover({
          trigger:'click',
          placement: 'right' 
      });
    }
  }
