<%@ include file="/libs/foundation/global.jsp"%>

<html dir="ltr" lang="en" data-placeholder-focus="false">
    <cq:includeClientLib categories="productdetails"/>
<head>

<link rel="canonical" href="http://shop.globe.com.ph/" />

<title>Checkout</title>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/checkout-common-form.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/pikaday.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/dropzone.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/dropzone.min.css" media="screen" />



</head>


    <body><div class="mboxDefault" id="mbox-target-global-mbox-1472816267641-364772" style="visibility: visible; display: block;"></div>
<script>
  dataLayer = [{
    'BatchID': 'GLO-000100551',
  }];
</script>
<style>.loading {background: #FFFFFF url('/catalog/view/theme/default/image/loading.gif') 10px center no-repeat;border: 1px solid #B8E2FB;	padding: 10px 10px 10px 33px;margin-bottom: 15px;color: #555555;}</style>
<!-- Google Tag Manager -->
<noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM7FZN" height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PM7FZN');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSV552"
height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSV552');</script>
<!-- End Google Tag Manager -->

<script>
   var a = "//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958"; //script location
   var domainList = ["www.globe.com.ph", "shop.globe.com.ph"]; //List of production domains

   //append “-staging” if hostname is not found in domainList
   if (domainList.indexOf(location.hostname) < 0) {
     a += "-staging"
   }

   document.write(unescape('%3Cscript type="text/javascript" src="'+a+'.js"%3E%3C/script%3E')); //write to page
</script><script type="text/javascript" src="//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958.js"></script><script src="https://assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/mbox-contents-ff1ad802b98ac2538d582ed5a65a3bf97208778c.js"></script><script type="text/javascript" id="">(function(){var a=window._fbq||(window._fbq=[]);if(!a.loaded){var b=document.createElement("script");b.async=!0;b.src="//connect.facebook.net/en_US/fbds.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);a.loaded=!0}a.push(["addPixelId","1394807297452790"])})();window._fbq=window._fbq||[];window._fbq.push(["track","PixelInitialized",{}]);</script>
<noscript>&lt;img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1394807297452790&amp;amp;ev=NoScript"&gt;</noscript><script type="text/javascript" id="">(function(){var a=window._fbq||(window._fbq=[]);if(!a.loaded){var b=document.createElement("script");b.async=!0;b.src="//connect.facebook.net/en_US/fbds.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);a.loaded=!0}a.push(["addPixelId","1394807297452790"])})();window._fbq=window._fbq||[];window._fbq.push(["track","PixelInitialized",{}]);</script>
<noscript>&lt;img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1394807297452790&amp;amp;ev=NoScript"&gt;</noscript>

<script type="text/javascript" id="">(function(){var a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"==document.location.protocol?"https://ph-ssl":"http://ph-cdn")+".effectivemeasure.net/em.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();</script>
<noscript>
	&lt;img src="https://ph.effectivemeasure.net/em_image" alt="" style="position:absolute; left:-5px;"&gt;
</noscript>
<style>.mboxDefault { visibility:hidden; }</style><script src="//cdn.tt.omtrdc.net/cdn/target.js"></script><script type="text/javascript" id="" src="//pixel.mathtag.com/event/js?mt_id=826864&amp;mt_adid=143609&amp;v1=&amp;v2=&amp;v3=&amp;s1=&amp;s2=&amp;s3="></script><script type="text/javascript" id="" src="//pixel.mathtag.com/event/js?mt_id=826865&amp;mt_adid=143609&amp;v1=not%20set&amp;v2=&amp;v3=&amp;s1=not%20set&amp;s2=&amp;s3="></script>

  <!-- Journey type header will be moved here soon -->

<script type="text/javascript">



  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }
    
  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }

  //Input types
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });
  
  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);
    
    // slide up if no value is seen
    if($("#rwd-searchbox").val() == "") {

      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){

      $('.rwd-search-results').slideUp();

      },500)
  }); 


  //Empty input on blur of input 
  $('input[name=\'searchterms\']').on('blur', function(e){
      setTimeout(function(){

      },500);
      var ifIE = msieversion();
      
      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {

        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

   
          var rv = ua.indexOf('rv:');
          return true;
   
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
    }
  });
  
  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }
    
  });
  //rwd of account-sigout-trigger above
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }
    
  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>
<div id="container" class="margTop">
<section class="journey-header row">
    <div class="container">
        
    </div>
</section>
<div id="thank-you" class="container">
    <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
        <div class="panel-heading">Thank You!</div>
        <div id="thank-you-banner" class="row">
            <div class="content col-lg-10 col-md-11 col-sm-11 col-xs-11">
                <h4>${properties.pagetitle}</h4>

<h4><strong>Order Reference No. GLO-<%= (int) (Math.random() * 1000000) %></strong></h4>

                <p>${properties.text}</p>

                <p>${properties.contact}</p>
                <div class="options row">
                    <a id="print-page" href="https://shop.globe.com.ph/checkout/print?ref=GLO-000100551&amp;rtoken=efa155272eacdc5ace01013587d0ee6d&amp;exbwt=1472819250&amp;mtypage=true" target="_blank">
                      <span class="print-icon"></span>
                      <text>Print this page</text>
                    </a><a id="continue-shopping" href="/content/globeEcommerce/homePage.html">
                    ${properties.continue}
                    </a>
                                        
                                    </div>
                
            </div>
        </div>
        
        <div id="recommendations" class="row">
            <h4>${properties.customerslove}</h4>
            <div id="recommended-slider" class="owl-carousel owl-theme" style="opacity: 1; display: block;"><div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 2000px; left: 0px; display: block;"><div class="owl-item" style="width: 200px;"><div class="item recommend-item">    <a href="http://shop.globe.com.ph/products/lte-mobile-wifi-phone"><img src="https://d11fuji4mn7bm2.cloudfront.net/media/cache/data/tattoo/Mobile_WiFi/tattoo-lte-mobile-wifi-175x175.png"></a>    <p><a href="http://shop.globe.com.ph/products/lte-mobile-wifi-phone">LTE Mobile WiFi</a></p></div></div><div class="owl-item" style="width: 200px;"><div class="item recommend-item">    <a href="http://shop.globe.com.ph/products/4g-mobile-wifi-postpaid"><img src="https://d11fuji4mn7bm2.cloudfront.net/media/cache/data/tattoo/Mobile_WiFi/4g-mobile-wifi-175x175.png"></a>    <p><a href="http://shop.globe.com.ph/products/4g-mobile-wifi-postpaid">4G Mobile WiFi</a></p></div></div><div class="owl-item" style="width: 200px;"><div class="item recommend-item">    <a href="http://shop.globe.com.ph/products/line-only-no-lock-up"><img src="https://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/noluckuo-plan-175x175.png"></a>    <p><a href="http://shop.globe.com.ph/products/line-only-no-lock-up">Line Only</a></p></div></div><div class="owl-item" style="width: 200px;"><div class="item recommend-item">    <a href="http://shop.globe.com.ph/products/broadband/plan-6999"><img src="https://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Broadband/Jigsaw/Jigsaw-1-0/6999-175x175.jpg"></a>    <p><a href="http://shop.globe.com.ph/products/broadband/plan-6999">Broadband Plan 6999</a></p></div></div><div class="owl-item" style="width: 200px;"><div class="item recommend-item">    <a href="http://shop.globe.com.ph/products/broadband/plan-4499"><img src="https://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Broadband/Jigsaw/Jigsaw-1-0/4499-175x175.jpg"></a>    <p><a href="http://shop.globe.com.ph/products/broadband/plan-4499">Broadband Plan 4499</a></p></div></div></div></div><div class="owl-controls clickable" style="display: none;"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div></div></div></div>
        </div>
    </div>
</div>

<div id="checkout-footer">
    <a target="_blank" href="http://www.globe.com.ph/privacy-policy">Privacy Policy</a>&nbsp;|&nbsp;<a target="_blank" href="http://www.globe.com.ph/terms-and-conditions">Terms and Conditions</a><br>© 2016 Globe Telecom Inc.</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#no-thanks .info-icon').on('click', function() {
            $(this).siblings('.info-popup').toggle();
        });
        $(window).on('resize', function() {
            if($(window).width() <= 580) {
                $('#print-page').insertAfter('#continue-shopping');
            } else {
                $('#continue-shopping').insertAfter('#print-page');
            }
        }).resize();
    });
    // Script to get the product recommendation
            var sku_temp = "Galaxy S6 edge";
      
    function recommendedProductSlider() {
        var owl = $("#recommended-slider");
                  
        owl.owlCarousel({
          items : 5, 
          itemsDesktop : [1198,5], 
          itemsDesktopSmall : [979,5], 
          itemsTablet: [768,1], 
          itemsMobile : [480,1], 
          itemsScaleUp:true
        });
    }

    function getrecoproduct() {
        $.ajax({
          url: 'index.php?route=product/product/get_recommendations_by_item',
          type: 'get', 
          data: 'type=also_love&sku=' + sku_temp,
          dataType: 'json',
          success: function(json) {
            var html = '';
            var productLength = json.products.length;
  
            for (var i = 0; i < productLength; i++) {
                var temp_image = json.products[i].image;
                var temp_link  = json.products[i].link;
                var temp_name  = json.products[i].name;
                var temp_id    = json.products[i].product_id;
  
                var temp_list       = json.products[i].list;
                var temp_price      = json.products[i].price;
                var temp_brand      = json.products[i].brand;
                var temp_cat        = json.products[i].cat;
                var temp_variant    = json.products[i].variant;
                var temp_position   = json.products[i].position;

                html += '<div class="item recommend-item">';
                html += '    <a href="'+temp_link+'"><img src="'+temp_image +'"></a>';
                html += '    <p><a href="'+temp_link+'">'+temp_name+'</a></p>';
                html += '</div>';
            };
             
            $('#recommended-slider').html(html);
            recommendedProductSlider();
          },
          error: function(xhr, ajaxOptions, thrownError) {
          }
        });
    }
  
    getrecoproduct();
</script>

<script type="text/javascript">
      
        dataLayer.push({
        'ecommerce': {
            'purchase': {
                'actionField': { 
                    'id': '-GLO-000100551',                         
                    'affiliation': 'Globe Telecom Inc.',
                    'revenue': '0.00',                     
                    'tax': '',
                    'shipping': '0.00',  
                    'coupon': ''
                },
                'products': [
                                    ]
            }
       }
    });
    </script>

</div>

  
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Talk2GLOBE CHAT</h4>
      </div>
      <div class="modal-body" align="center">
        <iframe id="chatFrame" src="" frameborder="0" height="459" width="99.6%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Item Added -->
<div class="modal fade" id="pdpCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: green; ">Item successfully added to cart.</h4>
        <br>
        <div class="cart-btn-view">
          <a href="http://shop.globe.com.ph/cart" class="btn btn-bw-orange">View Cart</a>
        </div> 
        <br>
      </div>
    </div>
  </div>
</div>

<!-- Item Out of Stock -->
<div class="modal fade" id="pdpCartErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: red; " id="pdpCartErrorModalMsg">Item is out of stock.</h4>
        <br>
        <div class="cart-btn-view">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
        <br>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.btn-chat').click(function() {
      $('#chatFrame').attr('src', 'http://webchat.globe.com.ph/chatc41/');
    });
  });
</script>

<!-- Ethnio -->
      <!-- Ethnio Activation Code -->
    <script type="text/javascript" language="javascript" src="//ethn.io/47103.js" async="true" charset="utf-8"></script>
  <!-- End Ethnio -->

<!-- Custom Script -->
<script type="text/javascript">_satellite.pageBottom();</script>

<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=ZKgM1Va";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  &lt;iframe src="//s.thebrighttag.com/iframe?c=ZKgM1Va" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"&gt;&lt;/iframe&gt;

</noscript>


<!---Mouseflow--->

<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/ded37ad7-9807-4070-ad67-9d72e900c70a.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<!---Mouseflow--->
<!-- End Custom Script -->

</body>
</html>

