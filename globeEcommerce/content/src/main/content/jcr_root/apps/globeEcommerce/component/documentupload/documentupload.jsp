<%@ include file="/libs/foundation/global.jsp"%>

 <cq:includeClientLib categories="productdetails"/>



<link href="https://d11fuji4mn7bm2.cloudfront.net/media/data/Favicon/favicon.png" rel="icon" />

<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="checkout-form-container">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
<div class="row" id="form-container" style="height: 829px;">
<div id="section-container">
<form action="" enctype="multipart/form-data" id="app_form" method="post">
<section class="panel current" id="document-upload-details">
<div class="panel-content"><div class="panel-heading">Step 5: Document Upload (Optional)</div>
<div class="document_note"><div class="form-alert"><span class="alert-icon">&nbsp;</span>

<p>This step is optional, but application processing is faster if you upload these documents now!</p>
</div>
</div>
<div class="panel-body">
                                                                                                                                                                <div class="panel-row row" style="z-index: 3;">
                                                
                                                
                                                <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                                                                <label class="field-label">
                                                                                Upload Proof of Identification (optional)                                                                               <a class="accepted-ids">View list of accepted IDs</a>
                                                                </label>

                                                                <div class="accepted-id-container">
                                                                                <p>Valid documents for Proof of Identification with signature</p>
                                                                                
                                                                                                                                                                                <ul>
                                                                                                                                                                                                                                <li>Driver’s License (Permanent or Temporary) - Signature and details clearly visible</li>
                                                                                                                                                                                                                                <li>Company ID - Front and back, with signature</li>
                                                                                                                                                                                                                                <li>Passport - Signature must be clearly visible</li>
                                                                                                                                                                                                                </ul>
                                                                                
                                                                                <span>Click Drop files to upload your document</span>
                                                                </div>
                                                                <label class="field-warning">
                                                                                <img src="/content/dam/shoppingcart/warning.png "> <span>Processing is faster if you complete this step</span>
                                                                </label>
                                                                <form action="" class="dropzone needsclick dz-clickable" id="documents_valid_id">
                                                                                <div class="dz-default dz-message">
                                                                                                Drop files here or
                                                                                                <div id="doc-upload-btn">Click here</div>
                                                                                </div>
                                                       
                                                    </form>
                                                    <input type="hidden" data-required="optional" name="documents_valid_id[filename]" id="input_documents_valid_id_filename" class=" " value="">
                                                    <input type="hidden" name="documents_valid_id[filename_orig]" id="input_documents_valid_id_filename_orig" value="">
                                                    <input type="hidden" name="documents_valid_id[upload_name]" id="input_documents_valid_id_upload_name" value="">
                                                    <input type="hidden" name="documents_valid_id[size]" id="input_documents_valid_id_size" value="">
                                                    <input type="hidden" name="documents_valid_id[subdir]" id="input_documents_valid_id_subdir" value="">
                                                    <input type="hidden" name="documents_valid_id[ext]" id="input_documents_valid_id_ext" value="">
                                                    <input type="hidden" name="documents_valid_id[name]" id="input_documents_valid_id_name" value="">
                                                </div>

                                                                                                </div>
                                                
                                                                                                                                                                                                                                                <div class="panel-row row" style="z-index: 2;">
                                                
                                                
                                                <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                                                                <label class="field-label">
                                                                                Upload Proof of Billing Address (optional)                                                                            <a class="accepted-ids">View list of accepted documents</a>
                                                                </label>

                                                                <div class="accepted-id-container">
                                                                                <p>Valid documents for Proof of Billing</p>
                                                                                
                                                                                                                                                                                <ul>
                                                                                                                                                                                                                                <li>Any Acceptable Proof of Identification - Address must be clearly visible, must include "In case of emergency" address, must be unexpired</li>
                                                                                                                                                                                                                               <li>Latest Public Utility Bills (electric, water, telephone, cable tv subscription, internet service.) - Bill must be in your name, address must be clearly visible, must not be more than 1 month old </li>
                                                                                                                                                                                                                                <li>Latest credit card statement - Bill must be in your name, address must be clearly visible, must not be more than 1 month old</li>
                                                                                                                                                                                                                </ul>
                                                                                
                                                                                <span>Click Drop files to upload your document</span>
                                                                </div>
                                                                <label class="field-warning">
                                                                                <img src="/content/dam/shoppingcart/warning.png "> <span>Processing is faster if you complete this step</span>
                                                                </label>
                                                                <form action="" class="dropzone needsclick dz-clickable" id="documents_proof_billing">
                                                                                <div class="dz-default dz-message">
                                                                                                Drop files here or
                                                                                                <div id="doc-upload-btn">Click here</div>
                                                                                </div>
                                                       
                                                    </form>
                                                    <input type="hidden" data-required="optional" name="documents_proof_billing[filename]" id="input_documents_proof_billing_filename" class=" " value="">
                                                    <input type="hidden" name="documents_proof_billing[filename_orig]" id="input_documents_proof_billing_filename_orig" value="">
                                                    <input type="hidden" name="documents_proof_billing[upload_name]" id="input_documents_proof_billing_upload_name" value="">
                                                    <input type="hidden" name="documents_proof_billing[size]" id="input_documents_proof_billing_size" value="">
                                                    <input type="hidden" name="documents_proof_billing[subdir]" id="input_documents_proof_billing_subdir" value="">
                                                    <input type="hidden" name="documents_proof_billing[ext]" id="input_documents_proof_billing_ext" value="">
                                                    <input type="hidden" name="documents_proof_billing[name]" id="input_documents_proof_billing_name" value="">
                                                </div>

                                                                                                </div>
                                                
                                                                                                                                                                                                                                                <div class="panel-row row" style="z-index: 1;">
                                                

                                                <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                                                                <label class="field-label">
                                                                                Upload Proof of Financial Capacity (optional)                                                                      <a class="accepted-ids">View list of accepted documents</a>
                                                                </label>

                                                                <div class="accepted-id-container">
                                                                                <p>Valid documents for Proof of Income</p>
                                                                                
                                                                                                                                                                                <ul>
                                                                                                                                                                                                                                <li>Latest Certificate of  Employment and Compensation - NOT more than three (3) months old, with signature of the authorized HR/Admin Personnel</li>
                                                                                                                                                                                                                                <li>Latest Computerized  Pay Slips (Private Organization) - Must be one full month equivalent, but NOT more than 3 months old</li>
                                                                                                                                                                                                                                <li>Credit Card ID including Internationally-issued Cards - Advance MSF payment shall be charged to the credit card</li>
                                                                                                                                                                                                                </ul>
                                                                                
                                                                                <span>Click Drop files to upload your document</span>
                                                                </div>
                                                                <label class="field-warning">
                                                                                <img src="/content/dam/shoppingcart/warning.png "> <span>Processing is faster if you complete this step</span>
                                                                </label>
                                                                <form action="" class="dropzone needsclick dz-clickable" id="documents_proof_income">
                                                                                <div class="dz-default dz-message">
                                                                                                Drop files here or
                                                                                                <div id="doc-upload-btn">Click here</div>
                                                                                </div>
                                                       
                                                    </form>
                                                    <input type="hidden" data-required="optional" name="documents_proof_income[filename]" id="input_documents_proof_income_filename" class=" " value="">
                                                    <input type="hidden" name="documents_proof_income[filename_orig]" id="input_documents_proof_income_filename_orig" value="">
                                                    <input type="hidden" name="documents_proof_income[upload_name]" id="input_documents_proof_income_upload_name" value="">
                                                    <input type="hidden" name="documents_proof_income[size]" id="input_documents_proof_income_size" value="">
                                                    <input type="hidden" name="documents_proof_income[subdir]" id="input_documents_proof_income_subdir" value="">
                                                    <input type="hidden" name="documents_proof_income[ext]" id="input_documents_proof_income_ext" value="">
                                                    <input type="hidden" name="documents_proof_income[name]" id="input_documents_proof_income_name" value="">
                                                </div>

                                                                                                </div>
                                                
                                                                                                                
                <input type="hidden" name="occupation" id="occupation" value="">
                <input type="hidden" id="checkout_type" value="postpaid">
</div>
<div id="panel-bottom" class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <a id="prev-step" onclick="setPreviousPanel('financial-details', 'document-upload-details')">Previous Step</a>
                    <button id="button-document-upload-details" class="btn"><a href="/content/globeEcommerce/termsconditionpage.html?wcmmode=disabled" style="color:white"> Next Step</a></button>
                </div>
</div>

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/document_upload.js"></script>
<script type="text/javascript">
$(document).ready(function() {
                                    dropzoneDragDrop('documents_valid_id', '', '', '');
                                    dropzoneDragDrop('documents_proof_billing', '', '', '');
                                    dropzoneDragDrop('documents_proof_income', '', '', '');
                
                getSidebar('document_upload', 'postpaid');
});
</script>

</div>
</section>
    </div>
    </div>
    </div>
    </div>
