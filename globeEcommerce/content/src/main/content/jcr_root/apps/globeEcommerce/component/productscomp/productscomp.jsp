<html>
<cq:includeClientLib categories="productdetails"/>
    <head>
                                <style>

            .slick-dots { position: absolute; bottom: -22px; }
  .homepage-prod-headers-container{
    position: relative;
    text-align: center;
  }
  .homepage-prod-headers-text{
    background-color: #333333;
    border: 2px solid #ffffff;
    color: #ffffff;
    display: inline-block;
    font-size: 18px;
    padding: 5px 25px;
    position: relative;
    z-index: 1;
    width: 250px;
  }
  .homepage-prod-headers-line{
    background-color: #333333;
    position: absolute;
    height: 2px;
    top: 50%;
    width: 100%;

  }
  .home-prod-custom-text {
    padding: 20px 0px;
  }
  .home-prod-custom-text-padding {
    padding-top: 40px;
  }
  .home-prod-custom-link-padding {
    padding: 30px 0 20px 0;
  }
  .prodh1Title1 {
    font-weight: bold;
    font-size: 14px;
    padding-top: 30px;
  }
  .prodsubtitle1 {
    font-size: 13px;
  }
  /*
  .home-prod-item img {
    width: 140px;
    height: 185px;
  }
  */
  .home-prod-item-imgs {
    height: 185px;
    vertical-align: bottom;
    text-align: left;
  }


  .home-prod-item-imgs img{
                height: 100%;
                width: auto;
  }

  a.prodlink {
    color: #ffffff;
    font-size: 15px;
    padding: 5px 30px;
    background-color: #1769B2;
  }

  @media (min-width: 1200px) and (max-width: 1024px) {
    .prodh1Title1 {
      padding-top: 10px;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .home-prod-item img {
      width: auto;
      height: auto;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .home-prod-custom-text-padding {
      padding-top: 30px;
    }
    .home-prod-item img {
      width: auto;
      height: auto;

    }
    .home-prod-item-imgs {
      height: 130px;
      vertical-align: bottom;
    }
    .prodh1Title1 {
      padding-top: 10px;
    }
  }
  @media (max-width: 767px) {
    .home-prod-custom-text-padding {
      padding-top: 10px;
    }
    .home-prod-item-imgs {
                text-align: center;
    }
    .home-prod-item {
      text-align: center;
    }
    .prodsubtitle1 {
      padding-bottom: 20px;
    }
    .home-prod-custom-link-padding {
      padding: 0 0 10px;
    }
    .home-prod-item img {
      margin: auto;
      width: auto;
      display: inline-block;
    }
 }
  @media (max-width: 400px) {
    .home-prod-custom-text-padding {
      padding-top: 10px;
    }
    .home-prod-item {
      text-align: center;
    }
    .prodsubtitle1 {
      padding-bottom: 20px;
    }
    .home-prod-custom-link-padding {
      padding: 0 0 10px;
    }
  }

        </style>
    </head>


<body>
<div class="container-fluid">
<div class="container home-prod-custom-text">
<div class="homepage-prod-headers-container">
    <div class="homepage-prod-headers-text" style="border:0px;  !important font-family: fs_elliot_proregular,Arial,sans-serif!important;">DEVICES YOU'LL LOVE</div>

<div class="homepage-headers-line" style="background-color: #333333;
    position: absolute;
    height: 2px;
    top: 50%;
    width: 100%;">&nbsp;</div>
</div>

<div class="row home-prod-custom-text-padding"><div class="col-lg-1 col-md-1 col-sm-1 home-prod-item-blank">&nbsp;</div>


<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
    <div align="center" class="home-prod-item-imgs"><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-j1-2016"><img class="img-responsive lazy" src="${properties.image1}" data-original="http://shop.globe.com.ph/image/data/homepage_folds/homepage-samsung-j1-2016.png" style="display: block;"> </a></div>

<div class="prodh1Title1"><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-j1-2016" style="color:#000000; text-decoration:none">Samsung Galaxy J1 2016</a></div>

<div class="prodsubtitle1">Free at Plan 599</div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
    <div align="center" class="home-prod-item-imgs"><a href="http://shop.globe.com.ph/products/mobile/lenovo-a6000"><img class="img-responsive lazy" src="${properties.image2}" data-original="http://shop.globe.com.ph/image/data/homepage_folds/homepage-lenovo-a6000-1.png" style="display: block;"> </a></div>

<div class="prodh1Title1"><a href="http://shop.globe.com.ph/products/mobile/lenovo-a6000" style="color:#000000; text-decoration:none">Lenovo A6000</a></div>

<div class="prodsubtitle1">Free at Plan 599</div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
    <div align="center" class="home-prod-item-imgs"><a href="http://shop.globe.com.ph/products/mobile/huawei-p9"><img class="img-responsive lazy" src="${properties.image3}" data-original="http://shop.globe.com.ph/image/data/homepage_folds/Devices-Youll-love/homepage-huawei-p9.jpg" style="display: block;"> </a></div>

<div class="prodh1Title1"><a href="http://shop.globe.com.ph/products/mobile/huawei-gr3" style="color:#000000; text-decoration:none">Huawei P9</a></div>

<div class="prodsubtitle1">Free at Plan 1499</div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
    <div align="center" class="home-prod-item-imgs"><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-j5-2016"><img class="img-responsive lazy" src="${properties.image4}" data-original="http://shop.globe.com.ph/image/data/homepage_folds/Devices-Youll-love/homepage-samsung-j5-2016.jpg" style="display: block;"> </a></div>

<div class="prodh1Title1"><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-j5-2016" style="color:#000000; text-decoration:none">Samsung Galaxy J5 2016</a></div>

<div class="prodsubtitle1">Free at Plan 999</div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
    <div align="center" class="home-prod-item-imgs"><a href="http://shop.globe.com.ph/products/electronics/sony-playstation-4"><img class="img-responsive lazy" src="${properties.image5}" data-original="http://d11fuji4mn7bm2.cloudfront.net/media/data/homepage_folds/Devices-Youll-love/ps4-1.jpg" style="display: block;"> </a></div>

<div class="prodh1Title1"><a href="http://shop.globe.com.ph/products/electronics/sony-playstation-4" style="color:#000000; text-decoration:none">Sony PlayStation 4</a></div>

<div class="prodsubtitle1">Get this at Plan 1957</div>
</div>


<div class="col-lg-1 col-md-1 col-sm-1 home-prod-item-blank">&nbsp;</div></div>

<div class="row home-prod-custom-link-padding" style="padding-top: 20px;">
<div align="center" class="col-lg-12 col-sm-12"><a class="prodlink" href="http://shop.globe.com.ph/shop-by-device">View All Devices</a></div>
</div>
</div>
</div>
</body>
</html>
