<%@ include file="/libs/foundation/global.jsp" %>
<html dir="ltr" lang="en" data-placeholder-focus="false">
    <cq:includeClientLib categories="productdetails"/>
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="cache-control" content="public">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<link rel="canonical" href="http://shop.globe.com.ph/" />

<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Shopping Cart</title>

<link href="http://d11fuji4mn7bm2.cloudfront.net/media/data/Favicon/favicon.png" rel="icon" />


<script type="text/javascript">
  function lumiaFix() {
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) { 
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }

    if (navigator.userAgent.match(/IEMobile\/9\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }
  }

  lumiaFix();
</script>
</head>
<body>
<style>.loading {background: #FFFFFF url('/catalog/view/theme/default/image/loading.gif') 10px center no-repeat;border: 1px solid #B8E2FB;	padding: 10px 10px 10px 33px;margin-bottom: 15px;color: #555555;}</style>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM7FZN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PM7FZN');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSV552"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSV552');</script>
<!-- End Google Tag Manager -->

<script>
   var a = "//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958"; //script location
   var domainList = ["www.globe.com.ph", "shop.globe.com.ph"]; //List of production domains

   //append â-stagingâ if hostname is not found in domainList
   if (domainList.indexOf(location.hostname) < 0) {
     a += "-staging"
   }

   document.write(unescape('%3Cscript type="text/javascript" src="'+a+'.js"%3E%3C/script%3E')); //write to page
</script>

<!-- BROADWAY HEADER -->
<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  <style type="text/css">.back2site a{
color:#fff;
}
.top-header-new{
background: #58595b;
padding: 3px 3% 3px 3%;
color: #fff;
}
span.call-text-top{
color: #fff;
}
span.call-text-top a{
color: #fff;
text-decoration: none;
}
span.m-left-right{
margin-left:10px;
margin-right: 10px;
}
.store-locator a{
	color:#fff;
}
.store-locator a:hover{
	color:#fff;
}

  .back2site a u, span.store-locator a{
      font-size:12px !important;
    
    }
</style>
<div class="container-fluid top-header-new">
<div class="pull-left back2site"><a href="http://www.globe.com.ph/" target="_blank"><u>&lt; Go to globe.com.ph</u></a></div>

<div class="pull-right"><span class="store-locator"><a href="https://www.globe.com.ph/store-locator" target="_blank"><u>Store Locator</u></a></span></div>
</div>
 
</div>
					<% 
										ServletContext sc = getServletContext();
   										 String plan = sc.getAttribute("planValue").toString();
                                       String price = sc.getAttribute("monthlyprice").toString();
									   String productprice = sc.getAttribute("productprice").toString();
     									String planDesc = sc.getAttribute("planDetails").toString();

										%>
										
										
<div class="headerNavTop">
  <div id="new-header" class="">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 logo-wrapper header-blocks">
            <div class="dropdown">
              <div class="menu-list">
                <button class="btn dropdown-toggle" type="button" id="dropdown-button" data-toggle="dropdown">
                  <span class="icon-menu"></span>
                </button>
                <div class="dropdown-menu" role="menu" aria-labelledby="dropdown-button">
                  <div class="nav-list">
                    <ul>
                      <li id="1" role="presentation"><a id="header-about-globe" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
                      <li id="2" role="presentation"><a id="header-personal" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Personal</a></li>
                      <li id="3" role="presentation"><a id="header-sme" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://mybusiness.globe.com.ph/">SME</a></li>
                      <li id="4" role="presentation"><a id="header-enterprise" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://business.globe.com.ph/">Enterprise</a></li>
                      <li id="5" role="presentation"><a id="header-help-support" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Help & Support</a></li>
                      <li id="6" role="presentation"><a id="header-myaccount" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">My Account</a></li>
                    </ul>
                  </div>
                  <div class="nav-container">
                    <div class="nav-result1">
                    </div>
                    <div class="nav-result2">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-personal-shop" href="http://shop.globe.com.ph">
                                  <span class="icon icon-phones"></span>
                                  <p class="text-center">Shop</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-postpaid" href="http://www.globe.com.ph/postpaid">
                                  <span class="icon icon-postpaid"></span>
                                  <p class="text-center">Postpaid</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-prepaid" href="http://www.globe.com.ph/prepaid ">
                                  <span class="icon icon-prepaid"></span>
                                  <p class="text-center">Prepaid</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-internet" href="http://www.globe.com.ph/internet">
                                  <span class="icon icon-internet"></span>
                                  <p class="text-center">Internet</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-international" href="http://www.globe.com.ph/international">
                                  <span class="icon icon-international"></span>
                                  <p class="text-center">International</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-gocash" href="http://www.globe.com.ph/gcash">
                                  <span class="icon icon-gcash"></span>
                                  <p class="text-center">GCash</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-rewards" href="http://www.globe.com.ph/rewards">
                                  <span class="icon icon-rewards"></span>
                                  <p class="text-center">Rewards</p>
                              </a>
                          </li>
                           <li>
                              <a id="header-personal-entertainments" href="http://downloads.globe.com.ph/">
                                  <span class="icon icon-entertainment"></span>
                                  <p class="text-center">Entertainment</p>
                              </a>
                          </li>
                      </ul>
                    </div>
                    <div class=" nav-result3"></div>
                    <div class=" nav-result4"></div>
                    <div class=" nav-result5">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-help&support-faq" href="http://www.globe.com.ph/help">
                                  <span class="icon icon-faq"></span>
                                  <p class="text-center">FAQs</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-phoneconfig" href="http://www.globe.com.ph/help/guide">
                                  <span class="icon icon-phoneconfig"></span>
                                  <p class="text-center">Phone Configuration</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-troubleshoot" href="http://www.globe.com.ph/help/troubleshooting">
                                  <span class="icon icon-troubleshoot"></span>
                                  <p class="text-center">Basic Troubleshooting</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-community" href="http://community.globe.com.ph">
                                  <span class="icon icon-community"></span>
                                  <p class="text-center">Ask the Community</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-storelocator" href="http://www.globe.com.ph/store-locator">
                                  <span class="icon icon-locator"></span>
                                  <p class="text-center">Store Locator</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-contactus" href="http://www.globe.com.ph/contactus">
                                  <span class="icon icon-contactus"></span>
                                  <p class="text-center">Contact Us</p>
                              </a>
                          </li>
                                                    <li>
                              <a id="header-help&support-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">
                                  <span class="icon icon-livechat"></span>
                                  <p class="text-center">Chat</p>
                              </a>
                          </li>
                                                </ul>
                    </div>

                    <div class=" nav-result6">
                      <div class="container">
                        <div class="row">
                          <div class="col-xs-1 button-group-holder text-center">
                            <h2 class="h2-first">Enjoy more of Globe</h2>
                                                          <a id="header-myaccount-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart" id="login" class="btn btn-block">Login</a>
                              <hr>
                              <a id="header-myaccount-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart" id="signup" class="btn btn-block">Sign Up</a>
                                                        </div>
                          <div class="col-xs-6 want-info-holder">
                              <h2 class="h2-second"><span class="icon-myaccount"></span>I want to</h2>
                              <div class="row">
                                <div class="col-xs-5">
                                  <a id="header-myaccount-iwant1" href="http://www.globe.com.ph/enrolled-accounts"> <p>&#8226; View/edit my account details</p> </a>
                                  <a id="header-myaccount-iwant2" href="http://www.globe.com.ph/paybill"><p> &#8226; Pay my bill</p></a>
                                  <a id="header-myaccount-iwant3" href="http://www.globe.com.ph/paperlessreg"><p> &#8226; Sign up for paperless billing</p></a>
                                  <a id="header-myaccount-iwant4" href="http://www.globe.com.ph/autopayreg"><p> &#8226; Enroll in Auto Pay service</p></a>
                                  <a id="header-myaccount-iwant5" href="http://www.globe.com.ph/form-transfer-of-ownership" target="_blank"><p> &#8226; Transfer my account</p></a>
                                </div>
                                <div class="col-xs-7">
                                  <a id="header-myaccount-iwant6" href="https://mygcash.globe.com.ph/gcashonline"><p> &#8226; Access my GCash </p></a>
                                  <a id="header-myaccount-iwant7" class="modal-trigger" href="//www.globe.com.ph/form-check-activation-status"><p> &#8226; Check my line activation status</p></a>
                                  <a id="header-myaccount-iwant8" class="modal-trigger" href="//www.globe.com.ph/form-check-outstanding-balance"><p> &#8226; Check my postpaid outstanding balance</p></a>
                                  <a id="header-myaccount-iwant9" href="http://dbschenkerepod.com.ph/globe/customerview.php" target="_blank"><p> &#8226; Track the delivery status of my device/SIM</p></a>
                                  <a id="header-myaccount-iwant10" href="http://www.globe.com.ph/updateinfo" target="_blank"><p> &#8226; Update my account information </p></a>
                                </div>
                              </div>
                          </div>
                          <div class="col-xs-5 help-info-holder">
                            <h2 class="h2-third"><span class="icon-question"></span>Get help with</h2>
                              <a id="header-myaccount-gethelp1" href="http://www.globe.com.ph/help/guide"><p> &#8226; Configuring my phone</p></a>
                              <a id="header-myaccount-gethelp2" href="http://www.globe.com.ph/help/troubleshooting"><p> &#8226; Troubleshooting my device</p></a>
                              <a id="header-myaccount-gethelp3" href="http://www.globe.com.ph/help/roaming/about-international-roaming"><p> &#8226; Activating my international roaming</p></a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <script>
                      if($('#new-header .nav-container .nav-result6').is('.f1731'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"min-height": "initial !important"});
                      }
                      if($('#new-header .nav-container .nav-result6').is('.f1731x'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"height": "172px"});
                      }
                    </script>

                  </div>
                </div>
              </div>
            </div>
                            <div id="logo-holder">
                <a href="http://shop.globe.com.ph/">
                  <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
                </a>
              </div>
                        </div>
          
          <div class="col-lg-3 category-dropdown header-blocks">
              <a id="header-shop-by-category" class="btn fontsize24 btn-lg absolute-center dropdown-toggle " data-toggle="dropdown">
                Shop by Category <span class="glyphicon caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                              <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                              <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                            </ul>
          </div>

          <div class="col-lg-4 search-wrapper header-blocks ">
            <div class="search-inner-addon ">
              <form>
                <fieldset>
                  <img class="searchIcon" src="/content/dam/shoppingcart/icon-search-white.png">
                  <input id="searchbox" name="searchterms" type="search" class="form-control" placeholder="Search Here" maxlength="50" />
                </fieldset>
                <ul id="searchResult">
                   <li class="result-holder"></li>
                   <li class="plainText">We Recommend</li>
                   <li class="searchRecommend"></li>
                   <li class="see-all">See All</li>
                </ul>
            </form>
            </div>
          </div>

            <div class="search-account-wrapper">
              <div class="col-lg-1 cart-wrapper header-blocks" id="cart-header">
  <div class="btn-cart ">
    <a id="header-cart-btn" class="dropdown-toggle" data-toggle="dropdown" onclick="toggleCart()">
      <span class="badge custom-badge " id="cart-total">1</span>
      <img class="absolute-center cart-image" src="/content/dam/shoppingcart/icon-cart.png">
    </a>
           <input type="hidden" class="quantityc-134" value="1">
        <ul class="dropdown-menu" id="cart-content" role="menu">
                <li>
        <div class="cart-item-thumbnail">
          <a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-s6-edge"><img src="/content/dam/shoppingcart/Galaxy-S6.png" alt="Samsung Galaxy S6 edge" title="Samsung Galaxy S6 edge" /></a>
        </div>
        <div class="cart-item-detail">
          <h4><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-s6-edge">Samsung Galaxy S6 edge</a></h4>
          <div>
                        <small>Plan: myLifestyle Plan 999</small><br />
                        <small>Term: 24 months</small><br />
                        <small>Color: White Pearl</small><br />
                        <small>Capacity: 64 GB</small><br />
                                  </div>
          <small>Quantity: 1</small>
        </div>
      </li>
                  <li>
        <div class="cart-btn-view">
        <a href="http://shop.globe.com.ph/cart" class="btn btn-block btn-bw-orange">View Cart (1 item)</a>
        </div>
      </li>
        </ul>

    <ul class="dropdown-menu" id="cart-notif" role="menu" style="display: none;">
      <li>
        <div class="cart-item-thumbnail">
          <img src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/check-50x50.png" alt="" title="" />
        </div>
        <div class="cart-item-detail cart-item-detail-success">
          <h4>Item successfully added to cart</h4>
        </div>
      </li>
      <li>
        <div class="cart-btn-view">
        <a href="http://shop.globe.com.ph/cart" class="btn btn-block btn-bw-orange">View Cart</a>
        </div>
      </li>
    </ul>
  </div>
</div>
              <div class="col-lg-1 account-wrapper ">
                  
                                      <a id="header-account-signin-btn" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart">
                      <img class="" src="/content/dam/shoppingcart/icon-user2.png">
                      <span class="account-text">Sign In</span>
                    </a>
                                </div><!--End of account wrapper--> 
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="rwd-header-tablet" class="collapse hidden-xs-*">
  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
      <li class="dropdown">
        <a id="header-personal" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-shop" href="http://shop.globe.com.ph">Shop</a></li>
          <li><a id="header-postpaid" href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
          <li><a id="header-prepaid" href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
          <li><a id="header-internet" href="http://www.globe.com.ph/internet">Internet</a></li>
          <li><a id="header-international" href="http://www.globe.com.ph/international">International</a></li>
          <li><a id="header-gcash" href="http://www.globe.com.ph/gcash">GCash</a></li>
          <li><a id="header-rewards" href="http://www.globe.com.ph/rewards">Rewards</a></li>
          <li><a id="header-entertainment" href="http://downloads.globe.com.ph/">Entertainment</a></li>
        </ul>
      </li>
      <li><a id="header-sme" href="http://mybusiness.globe.com.ph/">SME</a></li>
      <li><a id="header-enterprise" href="http://business.globe.com.ph/">Enterprise</a></li>
      <li class="dropdown">
        <a id="header-help&support" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-faqs" href="http://www.globe.com.ph/help">FAQs</a></li>
          <li><a id="header-phoneconfig" href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
          <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
          <li><a id="header-community" href="http://community.globe.com.ph">Ask the Community</a></li>
          <li><a id="header-store-locator" href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
          <li><a id="header-contactus" href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                    <li><a id="header-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                  </ul>
      </li>
      <li class="dropdown">
        <a id="header-myaccount" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
                      <li><a id="header-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart">Login</a></li>
            <li><a id="header-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart">Sign Up</a></li>
                  </ul>
      </li>
    </ul>
  </div>
</div>

<nav id="rwd-header" class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button class="btn dropdown-toggle rwd-header-dropdown-button" type="button" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="icon-menu"></span>
      </button>
              <div class="logo-holder">
          <a href="http://shop.globe.com.ph/">
            <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
          </a>
        </div>
              <div class="user-holder ">
            <div class="user-holder-indicator"></div>
                      <a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart">
              <img class="" src="catalog/view/theme/broadwaytheme/images/icons/icon-user2.png" />
            </a>
                  </div>

        <div class="cart-holder"></div>
        <script type="text/javascript">
          function headerRwdChangeLocation() {
            var winWidth = $(window).width();
            var isFirefox = typeof InstallTrigger !== 'undefined';
            if(isFirefox) {
              if(winWidth <= 950) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 950) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }

            else {
              if(winWidth <= 949) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 949) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }
            
            if(winWidth > 991) {
              $("#rwd-search").css('display', 'none');
            }
          }
          $(document).ready(function($) {
            $(window).resize(function() {
              headerRwdChangeLocation();
            });
            headerRwdChangeLocation();
          });
        </script>
        <div class="search-holder">
          <a href="" class="rwd-search-trigger">
            <img class="searchIcon hidden-xs hidden-sm hidden-md" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
            <img class="searchIconRwd visible-md-*" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
          </a>
        </div>
    </div>
    <div id="rwd-navbar" class="navbar-collapse collapse">
      <button class="rwd-navbar-close" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="blue-close"></span>
      </button>
      <br />
      <ul class="nav navbar-nav">
        <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle open rwd-header-personal-trigger" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://shop.globe.com.ph">Shop</a></li>
            <li><a href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
            <li><a href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
            <li><a href="http://www.globe.com.ph/internet">Internet</a></li>
            <li><a href="http://www.globe.com.ph/international">International</a></li>
            <li><a href="http://www.globe.com.ph/gcash">GCash</a></li>
            <li><a href="http://www.globe.com.ph/rewards">Rewards</a></li>
            <li><a href="http://downloads.globe.com.ph/">Entertainment</a></li>
          </ul>
        </li>
        <li><a href="http://mybusiness.globe.com.ph/">SME</a></li>
        <li><a href="http://business.globe.com.ph/">Enterprise</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://www.globe.com.ph/help">FAQs</a></li>
            <li><a href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
            <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
            <li><a href="http://community.globe.com.ph">Ask the Community</a></li>
            <li><a href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
            <li><a href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                        <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                      </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
                          <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart">Login</a></li>
              <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/cart">Sign Up</a></li>
                      </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="rwd-search">
  <input id="rwd-searchbox"  type="search" class="form-control" placeholder="Search Here" maxlength="50" />
  <img id="rwd-search-trigger" src="catalog/view/theme/broadwaytheme/images/icons/icon-search.png" />
  <div class="rwd-search-results">
      <ul id="rwd-search-results-list">
         <li class="rwd-result-holder">
            <div class="text-result">test</div>
         </li>
         <li class="plain-text blue-line">WE RECOMMEND</li>
         <li class="img-result first-img-result"></li>
         <li class="plain-text rwd-search-see-all"><a>See All</a></li>
      </ul>
  </div> <!-- End of rwd-search-results-recommended -->
</div>

<nav id="rwd-sub-header" class="navbar navbar-default ">
  <div class="container rwd-sub-header-medium">
    <div class="navbar-header">
      <div class="menu-item">
        <a type="button" data-toggle="collapse" data-target="#rwd-sub-navbar" aria-expanded="false" aria-controls="rwd-sub-navbar">
          Shop by Category <span class="caret"></span>
        </a>
      </div>
      <a class="dot-menu" type="button" data-toggle="collapse" data-target="#rwd-sub-navbar-2" aria-expanded="false" aria-controls="rwd-sub-navbar-2">
        <img src="catalog/view/theme/broadwaytheme/images/icons/dot-menu.png">
      </a>
    </div>
    <div id="rwd-sub-navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
              </ul>
    </div>
    <div id="rwd-sub-navbar-2" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
            
                                                    
                          <li><a href="http://shop.globe.com.ph/contact">Request a call</a></li>
                                                    
                          <li><a href="tel:+027301010">(02) 730-1010</a></li>
                                    </ul>
    </div>
  </div>
  <div class="container rwd-sub-header-small">
    <div class="collapse navbar-collapse sub-header-sm" id="bs-example-navbar-collapse-7">
      <ul class="nav navbar-nav">
        <li role="presentation" class="dropdown offset">
            <a class="link" href="#" data-toggle="dropdown">
              Shop by Category <span class="glyphicon caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                          <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                          <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                        </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right offset-right">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
            
                                                    
                          <li><a href="http://shop.globe.com.ph/contact">Request a call</a></li>
                                                    
                          <li><a href="tel:+027301010">(02) 730-1010</a></li>
                                    </ul>
    </div>
  </div>
</nav>

<script type="text/javascript">



  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }
    
  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }

  //Input types
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });
  
  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);
    
    // slide up if no value is seen
    if($("#rwd-searchbox").val() == "") {
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
      //$('#rwd-searchbox').val('');
      },500)
  }); 


  //Empty input on blur of input 
  $('input[name=\'searchterms\']').on('blur', function(e){
      //$('.searchItems').html('');
      //#1417
      setTimeout(function(){
        // $('#searchResult').slideUp();
      },500);
      var ifIE = msieversion();
      
      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {
          //$('input[name=\'searchterms\']').val('');
        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
          // IE 10 or older => return version number
          //return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

          // IE 11 => return version number
          var rv = ua.indexOf('rv:');
          return true;
          //return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
         // IE 12 => return version number
         //return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  //General function for search - not used in input because of preventdefault in input
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
      /*$('#searchResult .ui-autocomplete .ui-menu-item').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
      });*/
    }
  });
  
  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }
    
  });
  //rwd of account-sigout-trigger above
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }
    
  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>
<div id="container" class="margTop">
<!-- BREADCRUMBS START -->
<div class="container-fluid breadcrumb-container headerNavx" itemscope itemtype="http://schema.org/BreadcrumbList"><!--ticket 1093 defer muna 020615-->
  <div class="container">
    <div class="row">
      <div class="breadcrumb pull-left">
                <div></div>
        <div id="breadcrumb-0" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/" itemprop="item">
            <span class="blue-link" itemprop="name">Shop Home</span>
            <meta itemprop="position" content="1" />
          </a>
        </div>
                <div> | </div>
        <div id="breadcrumb-1" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/cart" itemprop="item">
            <span class="black-link" itemprop="name">Shopping Cart</span>
            <meta itemprop="position" content="2" />
          </a>
        </div>
              </div>
      <div class="chat-call-holder pull-right hidden-xs hidden-sm">
                                            <a data-toggle="modal" data-target="#chatModal" class="btn btn-bw-clear btn-chat">Chat</a>
            
                                                    
                          <a href="http://shop.globe.com.ph/contact" class="btn btn-bw-clear">Request a call</a>
                                                    
                          <a href="tel:+027301010" class="btn btn-bw-clear">(02) 730-1010</a>
                                    </div>
    </div>
  </div>
</div>
<!-- BREADCRUMBS END -->

<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  </div>

<div id="cart-notification" class="notification_cart row">
      </div>

<div id="cart-content" class="container">
  <form action="http://shop.globe.com.ph/index.php?route=checkout/cart/update" id="form-cart" method="post" enctype="multipart/form-data">
    <section id="cart">
            
        <h2>${properties.cart}</h2>  
              <div id="product_0" class="product-row row">
          <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][product_id]" value="134" />
          <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][mode]" value="postpaid" />
          <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][key]" value="134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:" />
          <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][cnt]" value="0" />
          <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][product_cnt]" value="0" />

          <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-10 col-xs-10">
              <div class="product-img-container col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <a class="product-img" href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-s6-edge"><img class="img-responsive" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Samsung/Galaxy S6/galaxy-s6-edge-white-cart-150x200.png"></a>
              </div>
              <div class="product-details col-lg-9 col-md-9 col-sm-9 col-xs-9">
                <a class="product-name" href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-s6-edge">
                    <%=planDesc%>                                  </a>

                
                <ul class="product-details-list">
                                      <li>
                      <text><%=plan%>   </text>
                      <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][option][275]" value="525">
                    </li>
                                      <li>
                      <text>24 months lock-in</text>
                      <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][option][278]" value="531">
                    </li>
                                      <li>
                      <text>White Pearl </text>
                      <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][option][276]" value="536">
                    </li>
                                      <li>
                      <text>64 GB </text>
                      <input type="hidden" name="product[134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:][option][277]" value="530">
                    </li>
                                  </ul>

                                  <a class="view-details">View plan details</a>
                              </div>
            </div>

            <div class="product-quantity col-lg-3 col-md-3 col-sm-2 col-xs-2">
              <label>QTY</label>

              <input id = "quantity"  value="1" type="text"  name="quantity"  />
              <a class="product-remove" class="btn btn-default" onclick="removeItem('134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:', 'http://shop.globe.com.ph/cart?remove=134:YTo0OntpOjI3NTtzOjM6IjUyNSI7aToyNzY7czozOiI1MzYiO2k6Mjc3O3M6MzoiNTMwIjtpOjI3ODtzOjM6IjUzMSI7fQ==:')">Remove</a>
            </div>

            <div class="product-prices col-lg-4 col-md-4 col-sm-12 col-xs-12">
                              <span class="price">
                  <label>Monthly Fee</label>
                  <text><%=price%></text>
                </span>
                <span class="price">
                  <label>Cashout</label>
                  <text><%=productprice%></text>
                </span>
                          </div>
          </div>

                    <div class="row">
            <div class="product-more-details col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-12 col-xs-12">
              <span class="arrow-up"></span>
              <table class="table table-condensed">
                                  <thead>
                    <th>Plan Breakdown</th>
                    <th>Price</th>
                  </thead>
                  <tbody>
                                        <tr>
                      <td>Unlimited Call &amp; Texts to Globe/TM</td>
                      <td>PHP 499.00</td>
                    </tr>
                                        <tr>
                      <td>GoSURF 499 (3GB Data Allowance)</td>
                      <td>PHP 499.00</td>
                    </tr>
                                        <tr>
                      <td>FREE 3 months 1GB Spotify Premium or HOOQ</td>
                      <td><strong>FREE</strong></td>
                    </tr>
                                        <tr>
                      <td>FREE choice between Navigation Pack, Explore Pack, or Fitness Pack for 1 month</td>
                      <td><strong>FREE</strong></td>
                    </tr>
                                        <tr>
                      <td>FREE 1 month Gadget Care</td>
                      <td><strong>FREE</strong></td>
                    </tr>
                                        <tr>
                      <td>FREE 1GB Globe Cloud</td>
                      <td><strong>FREE</strong></td>
                    </tr>
                                        <tr>
                      <td>P1 Consumable</td>
                      <td>PHP 1.00</td>
                    </tr>
                                        <tr>
                      <td>FREE Shipping</td>
                      <td><strong>FREE</strong></td>
                    </tr>
                                                              <tr class="table-total">
                        <th>PLAN MONTHLY FEE</th>
                        <th>PHP 999.00</th>
                      </tr>
                                      </tbody>
                              </table>
            </div>
          </div>
          
        </div> 
              
      <div id="cart-summary" class="row">
        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                    <div id="store-or-pickup">
            <div class="radio-group row">
              <label class="radio selected" data-val="delivery">
                <span class="radio-button"></span> 
                <div>
                  <b>Ship to Address</b>
                  <p>Your order will be delivered to your preferred address.</p>
                </div>
              </label>
              <label class="radio " data-val="store-pickup">
                <span class="radio-button"></span> 
                <div>
                  <b>Free Store Pick-up</b>
                  <p>Complete your order at a Globe Store. </p>
                </div>
              </label>
            </div>
          </div>
          
          <div id="enter-coupon">
              <label>${properties.coupon}</label>            
                          <input id="coupon-input" type="text" name="coupon" maxlength="100" autocomplete="off"/><button type="submit">Redeem</button>
                                  
            <div id="coupon-result">
                          </div>
          </div>
        </div>
		<script>
var quantity = $("#quantity").val();


</script>
        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
          <div id="proceed-to-checkout-container">
                                          <span class="amount">
                  <label>Order Subtotal:</label>
                  <text><span id ="total"></span></text>
                </span>
                                                        <span class="amount">
                  <label>Shipping:</label>
                  <text>FREE</text>
                </span>
                                                        <div class="clearfix"></div>
                <hr>
                <span class="amount total">
                  <label>Order Total:</label>
                  <text><span id="orderTotal"></span></text>
                </span>
                                      <a id="proceed-to-checkout" href="http://localhost:4504/content/globeEcommerce/personaldetailspage.html">Proceed to Checkout</a>
          </div>
        </div>
      </div>
    </section>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(window).on('resize', function() {
      $('.product-row').each(function() {
        if($(window).width() <= 992) {
          $(this).find('.product-more-details').insertAfter($(this).find('.product-quantity'));
        } else {
          $(this).find('.product-more-details').appendTo($(this).find('.row:last-child'));
        }
      });
    }).resize();

    // On Click of View Plan Details
    $('.view-details').on('click', function() {
      $(this).toggleClass('opened');
      if($(this).hasClass('opened')) {
        $(this).closest('.product-row').find('.product-more-details').slideDown();
      } else {
        $(this).closest('.product-row').find('.product-more-details').slideUp(); 
      }
    });

    $('.radio-group .radio').on('click', function() {
      $(this).addClass('selected');
      $(this).siblings().removeClass('selected');

      $.ajax({
        url: 'index.php?route=checkout/cart/setDelivery',
        type: 'post',
        data: 'delivery=' + $(this).data('val'),
        dataType: 'json',
        success: function(json) {
          location.href = json['redirect'];
        }
      });
    });

    $('.bundle-product > p').on('click', function(e) {
      var checkbox = $(this).siblings(':checkbox');
      if($(e.target).prop('tagName') != "SPAN") {
        checkbox.prop("checked", !checkbox.prop("checked"));
      } else {
        return false;
      }
    });
  });

  // Toggle Bundle Note
  function toggleBundle(key) {
    $('#bundle_' + key).toggle();
  }

  // Coupon Code Validation
  $("#coupon-input").bind("keypress", function(event) { 
      var charCode = event.which;
      if(charCode == 8 || charCode == 0) {
        return;
      } else {
        var keyChar = String.fromCharCode(charCode); 
        return /[a-zA-Z0-9]/.test(keyChar); 
      }
  });

  // Update Quantity
  $('.productqty').blur(function() {
    updateItem($(this).data('key'));
  });

  var enableUpdate = true;
  function updateItem(parent_id) {
    parent_id = 'product_' + parent_id;

    // This prevents double ajax requests that causes double add to carts
    if(enableUpdate) {
      enableUpdate = false;

      $.ajax({
        url: 'index.php?route=checkout/cart/updateItem',
        type: 'post',
        data: $('#'+parent_id+' input[type=\'text\'], #'+parent_id+' input[type=\'number\'], #'+parent_id+' input[type=\'hidden\'], #'+parent_id+' input[type=\'radio\']:checked, #'+parent_id+' input[type=\'checkbox\']:checked, #'+parent_id+' select, #'+parent_id+' textarea'),
        dataType: 'json',
        success: function(json) {
          $('.success, .warning, .attention, information, .error').remove();

          location.href = json['redirect'];
        }
      });
    }
  }

  // Update Bundle
  $('.bundle_product_item').on('click', function() {
    bundle = ($(this).is(':checked')) ? 1 : 0;

    $.ajax({
        url: 'index.php?route=checkout/cart/setBundle',
        type: 'post',
        data: 'key=' + $(this).data('key') + '&bundle=' + bundle,
        dataType: 'json',
        success: function(json) {
          location.href = json['redirect'];
        }
      });
  });

  // Remove Item 
  function removeItem(key, url) {
    $.ajax({
      url: 'index.php?route=checkout/cart/getProductDetails',
      type: 'post',
      data: 'key=' + key,
      dataType: 'json',
      success: function(json) {
        dataLayer.push({
          'event': 'removeFromCart',
          'ecommerce': {
            'remove': {                               
              'products': [{                          
                  'name': json['name'],
                  'id': json['product_id'],
                  'price': json['price_val'],
                  'brand': json['manufacturer'],
                  'category': '',
                  'variant': json['variant'],
                  'quantity': json['quantity']
              }]
            }
          }
        });
        location.href = url;
      }
    });

    return false;
  }
</script>

</div>

    <div id="footer-top-wrap" class="container-fluid" style="background: #333333;">
    <div class="container footer-top-holder">
      <!-- INSERT FOOTER CONTENT HERE -->
      <style type="text/css">#footer hr.footer-divider{
    border-top: none !important;
  }

  #footer-top-wrap{
  background: #f6f6f6 !important;
  }

  .bg-gray{
   background:#f6f6f6;
  }
  .padd-top-bot{
     padding-top: 5px;
    padding-bottom: 0px;
  }
  .padd-top-bot10{
  padding-top: 10px;
  padding-bottom: 10px;
  }
  .f-shipping{
    text-align: center;
  }
  .f-payment{
    text-align: center;
  }
  .f-call{
    text-align: center;
  }
  .f-shipping a {
     text-decoration: none;
     color: #333;
  }
  .footer-options div{
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .f-shipping img{
  margin-right: 10px;
  margin-bottom: 0px;

  }
  .f-payment img{
  margin-right: 10px;
  margin-bottom: 0px;
  }
  .f-call img{
  margin-right: 10px;
  margin-bottom: 0px;
  }

   @media screen and (max-width: 768px) {
   .f-shipping{
      border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-payment{
       border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-call{
    text-align: center;
  }
}
@media screen and (max-width: 445px) {
.indent-mobile{
margin-left: 12%;
}
}

@media screen and (max-width: 391px) {
.indent-mobile{
margin-left: 12%;
}
}
@media screen and (max-width: 389px) {
.indent-mobile{
margin-left: 0%;
}
}
</style>
<div id="footer-top">
<div class="container-fluid bg-gray padd-top-bot">
<div class="container">
<div class="row footer-options">
    <div class="col-sm-4 col-md-4 col-lg-4 f-shipping"><a href="http://shop.globe.com.ph/help" style="color:black" title="Click to learn more about our shipping, delivery and returns policies"><img src="${properties.image1}" /><span class="m-top" title="Click to learn more about our shipping, delivery and returns policies">${properties.title1}</span></a></div>

    <div class="col-sm-5 col-md-5 col-lg-5 f-payment"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="${properties.image2}" /><span class="m-top">${properties.title2}</span></a></div>

    <div class="col-sm-3 col-md-3 col-lg-3 f-call"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="${properties.image3}" /><span class="m-top">${properties.title3}</span></a></div>
</div>
</div>
</div>
</div>
    </div>
  </div>
  <div id="footer">
    <hr class="footer-divider">
   <div> <cq:include path="footercomponent"       
      resourceType="/apps/shoppingcart/component/footer" /> </div>
    <div class="footer-legals">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div class="content-holder text-center footer-links">
              <p>&copy; 2016 Globe Telecom Inc.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Talk2GLOBE CHAT</h4>
      </div>
      <div class="modal-body" align="center">
        <iframe id="chatFrame" src="" frameborder="0" height="459" width="99.6%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Item Added -->
<div class="modal fade" id="pdpCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: green; ">Item successfully added to cart.</h4>
        <br />
        <div class="cart-btn-view">
          <a href="http://shop.globe.com.ph/cart" class="btn btn-bw-orange">View Cart</a>
        </div> 
        <br />
      </div>
    </div>
  </div>
</div>

<!-- Item Out of Stock -->
<div class="modal fade" id="pdpCartErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: red; " id="pdpCartErrorModalMsg">Item is out of stock.</h4>
        <br />
        <div class="cart-btn-view">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
        <br />
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.btn-chat').click(function() {
      $('#chatFrame').attr('src', 'http://webchat.globe.com.ph/chatc41/');
    });
  });
</script>

<!-- Ethnio -->
  <!-- End Ethnio -->

<!-- Custom Script -->
<script type="text/javascript">_satellite.pageBottom();</script>

<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=ZKgM1Va";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//s.thebrighttag.com/iframe?c=ZKgM1Va" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

</noscript>


<!---Mouseflow--->

<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/ded37ad7-9807-4070-ad67-9d72e900c70a.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<!---Mouseflow--->
<!-- End Custom Script -->

</body>
</html>
 <script type="text/javascript">
$(document).ready(function(){
    //alert("dd");
$.ajax({
	    type:"GET",
	    data: "quantity="+"1", 
	url : "/bin/epcortex/addToCart",
	    success:function(data){
   $('#total').text(data);
        $('#orderTotal').text(data);
    // window.location.href="http://localhost:4504/content/globeEcommerce/shopbyplanpage.html";
		}
	});

});
     $('#quantity').on('change', function(){ 
         //alert("change");
         // var text = $('#quantity').value();
         var quantity = $("#quantity").val();
		
			//alert(qty);
		
	$.ajax({
	    type:"GET",
	    data: "quantity="+quantity, 
	url : "/bin/epcortex/addToCart",
	    success:function(data){

        //window.location.href="http://localhost:4504/content/globeEcommerce/shopbyplanpage.html";
        $('#total').text(data);
        $('#orderTotal').text(data);
		}
	});
                       });




     </script>

