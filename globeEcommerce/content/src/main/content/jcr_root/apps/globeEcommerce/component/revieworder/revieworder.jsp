<%@ include file="/libs/foundation/global.jsp"%>

<html dir="ltr" lang="en" data-placeholder-focus="false">
    <cq:includeClientLib categories="productdetails"/>
<head>

<link rel="canonical" href="http://shop.globe.com.ph/" />

<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Checkout</title>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/checkout-common-form.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/pikaday.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/dropzone.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/dropzone.min.css" media="screen" />



</head>
<body>


	  <%
    	ServletContext sc= getServletContext();
		String address = sc.getAttribute("Address").toString();
		String city = sc.getAttribute("City").toString();
		String zipcode = sc.getAttribute("Zip Code").toString(); 
		String payment = sc.getAttribute("payment").toString(); 
		String plandetails = sc.getAttribute("planDetails").toString(); 
		String monthlyprice = sc.getAttribute("monthlyprice").toString(); 
		String price = sc.getAttribute("productprice").toString(); 
		String qty = sc.getAttribute("qty").toString(); 
		String total = sc.getAttribute("total").toString();

	%>


<script type="text/javascript">


  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }
    
  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }



  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });
  
  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);
    
    // slide up if no value is seen
    if($("#rwd-searchbox").val() == "") {
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
      //$('#rwd-searchbox').val('');
      },500)
  }); 


  //Empty input on blur of input 
  $('input[name=\'searchterms\']').on('blur', function(e){
      
      setTimeout(function(){
        // $('#searchResult').slideUp();
      },500);
      var ifIE = msieversion();
      
      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {
         
        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
         
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

          // IE 11 => return version number
          var rv = ua.indexOf('rv:');
          return true;
         
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
         
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  //General function for search - not used in input because of preventdefault in input
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
     
    }
  });
  
  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }
    
  });
  //rwd of account-sigout-trigger above
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }
    
  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>

<section class="journey-header row">
    <div class="container">
      
    </div>
</section>
<div id="notification" class="notification_cart">
  </div>

<div id="checkout-content" class="container" style="min-height: 208px; display: none;"><div class="required-prompt"><span></span></div>

<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="checkout-form-container">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
<div class="row" id="form-container" style="height: 15px;">
<div id="section-container">
<form action="" enctype="multipart/form-data" id="app_form" method="post">
<section class="panel current" id="personal-details">
<div class="panel-content">
<div class="panel-heading">Step 1: Personal Details</div>

<div class="panel-body">
<div class="panel-row row" style="z-index: 3;">
<div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 2; height: auto;"><label class="field-label">Application Type (New/Existing)</label>

<div class="radio-group row">
<div class="radio-inline personal_application_type selected"><span class="radio-button"></span> <text>New Postpaid Application</text></div>

<div class="radio-inline personal_application_type "><span class="radio-button"></span> <text>Additional Line</text>

<div class="form-group personal_account_number" style="z-index: 1; height: auto;"><label class="field-label">Mobile/Account #</label> <input class="checkout-forms-text-numeric required-data" maxlength="20" name="personal_account_number" placeholder="" type="text" value=""></div>
</div>
</div>
<input id="personal_application_type" name="personal_application_type" type="hidden" value=""></div>
</div>

<div class="panel-row row" style="z-index: 2;">
<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="z-index: 4; height: auto; padding: 10px 6px;"><label class="field-label" style="margin: 0px 4px 5px;">Salutation</label> <select class="required-data" data-default="ms" data-required="required" name="personal_salutation"><option class="hidden" value="">Please Select</option><option value="arch">Arch.</option><option value="dr">Dr.</option><option value="engr">Engr.</option><option value="msgr">Msgr.</option><option value="rev">Rev.</option><option value="mr">Mr.</option><option value="mrs">Mrs.</option><option selected="selected" value="ms">Ms.</option> </select></div>

<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="z-index: 3; height: auto;"><label class="field-label">First Name</label> <input class="required-data" data-required="required" maxlength="100" name="personal_first_name" type="text" value=""></div>

<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: auto;"><label class="field-label">Middle Name</label> <input class="required-data" data-required="required" maxlength="100" name="personal_middle_name" type="text" value=""></div>

<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: auto;"><label class="field-label">Last Name</label> <input class="required-data" data-required="required" maxlength="100" name="personal_last_name" type="text" value=""></div>
</div>

<div class="panel-row row" style="z-index: 1;">
<div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: auto;"><label class="field-label">Email Address</label> <input class="required-data" data-required="required" maxlength="255" name="personal_email_address" type="text" value=""></div>

<div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: auto;"><label class="field-label">Contact Number</label> <input class="checkout-forms-text-numeric required-data" data-required="required" maxlength="15" name="personal_primary_contact_number" type="text" value=""></div>
</div>
<input id="checkout_type" type="hidden" value="postpaid"></div>

<div class="row" id="panel-bottom">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a></div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><button class="btn" id="button-personal-details">Next Step</button></div>
</div>
<script type="text/javascript" src="https://shop.globe.com.ph/catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script><script type="text/javascript" src="https://shop.globe.com.ph/catalog/view/theme/broadwaytheme/js/checkout/personal_details.js"></script><script type="text/javascript">
$(document).ready(function() {
	getSidebar('personal_details', 'postpaid');
})
</script><script type="text/javascript">
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 1, 'option': ''},
        'products': [
                        {                            
                'name': 'iPhone 6 Whitebox',     
                'id': '1199',
                'price': '2299.00',
                'brand': '',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 1799, 24 months, Gold, 16 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                    ]
     }
   }
});
</script></div>
</section>

<section class="panel" id="additional-personal-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="shipping-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="financial-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="document-upload-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="terms-condition-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="review-order-details">
<div class="panel-content"></div>
</section>
</form>
</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="sidebar"><p><img class="img-responsive" id="norton-secured" src="https://shps3dv01.s3.amazonaws.com/media/data/norton_secured.png"></p>

<div class="row" id="contact-details">
<div class="info-block col-lg-12 col-md-12 col-sm-12 col-xs-4">
<p>Your personal information is safe and will always be kept private.</p>
</div>
</div>
</div>
</div></div>

<div id="review-order-content"><div id="review-order" class="container">
    <div id="review-order-container" class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
        <div class="row panel-row">
            <div class="panel-heading">${properties.pagetitle}</div>
            <div class="panel-body no-border">
                <div id="your-info" class="row">
                    <h3>${properties.delivery}</h3>
                    <div id="info-row" class="row">
                                                <div class="info col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <h4 class="info-heading">${properties.address}</h4>
                            <a onclick="setPreviousPanel('personal-details', 'review-order-details')" class="info-edit">edit</a>
                            <div class="info-content">


                                <p><%=address%></p>
                                <p><%=city%><br><%=zipcode%></p>
                            </div>
                        </div>
                        

                        
                        <div class="info col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <h4 class="info-heading">${properties.payment}</h4>
                            <a onclick="setPreviousPanel('shipping-details', 'review-order-details')" class="info-edit">edit</a>
                            <div class="info-content">
                                <p><%=payment%></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="your-orders" class="row">
                    <h3>${properties.order}</h3>
                                            <div class="form-alert">
                            <span class="alert-icon"></span>
                            <p>The payment for this item will be due upon delivery.</p>
                        </div>
                                        <div class="table-container">
                        <table class="table">   
                            <thead>
                                <tr>
                                    <th colspan="1">Item</th>
                                    <th>Qty</th>

                                                                            <th>Cashout</th>
                                        <th>Monthly Fee</th>
                                    
                                                                        <th>
                                        Total 
                                                                                <div class="info-container">
                                            <span class="icon info"></span>
                                            <div class="info-popup">
                                                <span class="arrow-up"></span>
                                                <p>Payment will be collected upon delivery</p>
                                            </div>  
                                        </div>
                                                                            </th>
                                </tr>
                            </thead>
                            <tbody>
                                                                <tr>
                                    <td>
                                        <span class="icon expand plus"></span>                                        
                                        <p class="product-name"><%=plandetails%></p>
                                        
                                                                                <ul class="expanded">
                                                                                        <li>myLifestyle Plan 999</li>
                                                                                        <li>24 months</li>
                                                                                        <li>White Pearl</li>
                                                                                        <li>64 GB</li>
                                                                                    </ul>
                                                                            </td>
                                    <td><%=qty%></td>

                                                                                                                    <td><%=price%></td>
                                        
                                                                                <td><%=monthlyprice%> / monthly</td>
                                                                            
                                    
                                    <td><%=total%></td>
                                    <td class="mobile row">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>QTY</td>
                                                    <td><%=qty%></td>
                                                </tr>

                                                                                                <tr>
                                                    <td>Cash Out</td>
                                                    <td><%=price%></td>
                                                </tr>
                                                
                                                                                                <tr>
                                                    <td>Monthly Fee</td>
                                                    <td><%=monthlyprice%> / monthly</td>
                                                </tr>
                                                
                                                <tr class="subtotal">
                                                    <td>Subtotal</td>
                                                    <td><%=total%></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="order-summary" class="row">
                    <div class="left col-lg-8 col-md-8 col-sm-8 col-xs-7">
                        
                                            </div>
                    <div id="summary" class="right col-lg-4 col-md-4 col-sm-4 col-xs-5">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Subtotal:</td>
                                    <td><%=total%></td>
                                </tr>

                                
                                                                <tr>
                                    <td>Shipping Fee:</td>
                                    <td>FREE</td>
                                </tr>
                                
                                <tr class="total">
                                    <td>Total:</td>
                                    <td><%=total%></td>
                                </tr>
                            </tbody>
                        </table>
                        <div id="panel-bottom" class="row">
    <div class="col-lg-offset-6 col-lg-6 col-md-offset-6 col-md-6 col-sm-offset-6 col-sm-6 col-xs-12">
    	<form id="review-form" action="https://shop.globe.com.ph/index.php?route=payment/default/confirm" method="post">
        	<input type="hidden" name="payment_method" value="default">
    	</form>
    	
        <button id="place-order" class="button-review-order-details"><a href ="/content/globeEcommerce/thankupage.html">${properties.button}</a></button></div>

    </div>                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="form_action" id="form_action" value="https://shop.globe.com.ph/checkout/review/validate">
<input type="hidden" id="checkout_type" value="postpaid">

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/review.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    getSidebar('review', 'postpaid');
})
</script>

<script type="text/javascript">
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 7, 'option': ''},
        'products': [
                        {                            
                'name': 'Samsung Galaxy S6 edge',     
                'id': '134',
                'price': '25299.00',
                'brand': 'Samsung',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 999, 24 months, White Pearl, 64 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                    ]
     }
   }
});
</script>
</div>

<div id="checkout-footer">
    <a target="_blank" href="http://www.globe.com.ph/privacy-policy">Privacy Policy</a>&nbsp;|&nbsp;<a target="_blank" href="http://www.globe.com.ph/terms-and-conditions">Terms and Conditions</a><br>© 2016 Globe Telecom Inc.</div>

<script type="text/javascript">
$(document).ready(function() {
			// Review
		$.ajax({
            url: 'checkout/review',
            dataType: 'html',
            beforeSend: function() {
            	$('#terms-condition-details .panel-content').html('');
		        $('#review-order-details .panel-content').html('<div align="center"><img src="catalog/view/theme/broadwaytheme/images/loader.gif" alt="" /></div>');
		      },
            success: function(html) {
            	$('.panel').removeClass('current');
              	$('.panel').removeClass('prev');

              	$('#review-order-details').addClass('current');

                $('#terms-condition-details .panel-content').html('');
                $('#review-order-content').html(html);
                $('#checkout-content').hide();
                adjustFormHeight();

                
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }); 
	});
</script>



  
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Talk2GLOBE CHAT</h4>
      </div>
      <div class="modal-body" align="center">
        <iframe id="chatFrame" src="" frameborder="0" height="459" width="99.6%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Item Added -->
<div class="modal fade" id="pdpCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: green; ">Item successfully added to cart.</h4>
        <br>
        <div class="cart-btn-view">
          <a href="http://shop.globe.com.ph/cart" class="btn btn-bw-orange">View Cart</a>
        </div> 
        <br>
      </div>
    </div>
  </div>
</div>

<!-- Item Out of Stock -->
<div class="modal fade" id="pdpCartErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: red; " id="pdpCartErrorModalMsg">Item is out of stock.</h4>
        <br>
        <div class="cart-btn-view">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
        <br>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.btn-chat').click(function() {
      $('#chatFrame').attr('src', 'http://webchat.globe.com.ph/chatc41/');
    });
  });
</script>

<!-- Ethnio -->
  <!-- End Ethnio -->

<!-- Custom Script -->
<script type="text/javascript">_satellite.pageBottom();</script>

<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=ZKgM1Va";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  &lt;iframe src="//s.thebrighttag.com/iframe?c=ZKgM1Va" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"&gt;&lt;/iframe&gt;

</noscript>


<!---Mouseflow--->

<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/ded37ad7-9807-4070-ad67-9d72e900c70a.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<!---Mouseflow--->
<!-- End Custom Script -->


<iframe src="https://pixel.mathtag.com/sync/iframe?mt_adid=143609&amp;v1=&amp;v2=&amp;v3=&amp;s1=&amp;s2=&amp;s3=&amp;mt_uuid=3a2e57c9-6292-4e00-b0b7-90656b4e9c8a&amp;no_iframe=1" id="mm_sync_back_ground" style="visibility: hidden; display: none;"></iframe>
    </body>
</html>
