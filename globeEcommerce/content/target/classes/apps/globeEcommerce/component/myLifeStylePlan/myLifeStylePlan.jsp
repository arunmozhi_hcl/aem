<%@ include file="/libs/foundation/global.jsp" %>

<html dir="ltr" lang="en" data-placeholder-focus="false">
  <cq:includeClientLib categories="productdetails"/>
<head>

<title>myLifestyle Plan 999 | Postpaid  | Globe Online Shop</title>






</head>
<body>
    <% 
										ServletContext sc = getServletContext();
   										 String plan = sc.getAttribute("planValue").toString();
                                       String price = sc.getAttribute("monthlyprice").toString();
     									String planDesc = sc.getAttribute("planDetails").toString();%>
<div itemscope="" itemtype="http://schema.org/Product">
  <meta itemprop="brand" content="Globe">

<div id="notification"></div>

  <!-- Modal FULL SCREEN -->
<div class="responsive-modal container pdp-social-container">
  <div class="modal fade text-center" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a target="_blank" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999"><span class="social-facebook"></span></a>
        <a target="_blank" href="http://twitter.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999&amp;text=myLifestyle Plan 999 - Postpaid  - Globe Online Shop"><span class="social-twitter"></span></a>
        <a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999"><span class="social-gplus"></span></a>
        <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999"><span class="social-linkedin"></span></a>
        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="social-pinterest"></span></a>
        <a href="" data-dismiss="modal"><span class="big-close"></span></a>
      </div>
    </div>
  </div>
</div>

<div class="pdp-container hidden-xs pdp-container-plan-only">

  <section class="mainProducts pdp">
      <div class="container">
          <div class="row">
              <div class="col-xs-8 col-sm-7 col-md-7 col-lg-8">
                  <div class="col-xs-4 col-sm-6 col-md-6 col-lg-4 productImageCol">
                      <div class="pdp-image-container text-center">
                        <div class="pdp-images w100">
                            <img class="imageZoom lazy" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-300x400.png" data-zoom-image="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-800x800.png" title="myLifestyle Plan 999" alt="myLifestyle Plan 999" data-original="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-300x400.png" style="display: inline;">
                                                  </div>
                        <div class="rotationButton">
                                                      <a class="slider-btn active" onclick="showImage(this)" data-thumb="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-300x400.png" data-popup="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-800x800.png">&nbsp;&nbsp;&nbsp;</a>
                          
                        </div>
                      </div>
                      <div class="cta-group">
                        <!-- <div class="add-to-wishlist"><a href="#"><span class="icon-bookmark"></span>Add to Wishlist</a></div> -->
                        <div class="share-this">
                            <a id="pdp-sharethis" href="#" data-toggle="modal" data-target="#myModal"><span class="icon-share"></span>Share This</a>
                          </div>
                      </div>
                  </div>
                  <div class="col-xs-8 col-sm-6 col-md-6 col-lg-8 productInfo">
                      <div class="productContent">
                        <div class="product-info-badge-holder">
                                                  </div>
                        <h1 class="title-bw-light"><span itemprop="name"><%= plan %></span></h1>
                        <div class="pdp-description">
                          <span itemprop="description"><p><span style="font-family: fs_elliot_proregular, Helvetica; font-size: 1em; line-height: normal;">myLifestyle Plan 999 comes with built-in Unlimited calls and texts to Globe/TM, and any combination of at least one mandatory Surf Pack and Lifestyle or Classic Packs worth up to P500.*</span></p>
<script>
  $('#myTabs').click(function (event) {
	$("#tab-0").css('display', 'none');
    $("#tab-1").css('display', 'block');
    $('div.slick-track a.fontsize24:contains("Plan")').removeClass("selectedTab selected");
    $('div.slick-track a.fontsize24:contains("Promos")').addClass("selectedTab selected");
    $('div.slick-slide:contains("Plan")').toggleClass("slick-active");
    $('div.slick-slide:contains("Promos")').toggleClass("slick-active");
  });
</script>

<h2 class="pdesc" style="font-size: 13px; line-height: 20.7999992370605px; color: rgb(0, 0, 139);">Online orders are for new and additional line applications only. You can also order this device by calling our Sales Hotline at (02) 730-1010, open Monday to Sunday from 6AM to 10PM. During the call, please enter the following promo code when prompted: 21211. For existing subscribers who wish to recontract, kindly call (02) 730-1300. This plan is available at <a href="http://www.globe.com.ph/store-locator" target="_blank">Globe stores</a> so visit the one near you!</h2>
<style type="text/css">.pdesc{
    font: 1em fs_elliot_proregular, Helvetica; }
</style>
</span>
                        </div>
                      </div><div class="productVariation productVariationParent">
                        <div class="options">
                          <div id="product-select" class="option storage-variation-edit pull-left optionFields remWi" style="">

                            <div class="dropdown dropdown-plan-select">
                              <button class="btn btn-default dropdown-toggle attrHver " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><div style="padding-right: 5px;">
                                <!-- <!?php echo $plan_products[0]['name']; ?> -->
                                <span class="drpdwn iPadV">Samsung Galaxy S6 edge</span><span class="drpdwn desktopV">Samsung Galaxy S6 edge</span>
                                                                <span class="caret caretx"></span>
                                                                </div>
                              </button>
                                                            <ul class="dropdown-menu dropdown-attributes-menu attrHverLI" data-id="plan-product" role="menu" aria-labelledby="dropdownMenu1" style="index: 99;">
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S6 edge" data-id="134">Samsung Galaxy S6 edge</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S6" data-id="138">Samsung Galaxy S6</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy Note 5" data-id="309">Samsung Galaxy Note 5</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S6 edge+" data-id="311">Samsung Galaxy S6 edge+</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="BlackBerry Classic" data-id="358">BlackBerry Classic</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone 6s" data-id="378">iPhone 6s</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone 6s Plus" data-id="380">iPhone 6s Plus</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Cloudfone Thrill 500x" data-id="394">Cloudfone Thrill 500x</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J1 2016" data-id="907">Samsung Galaxy J1 2016</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei G8" data-id="909">Huawei G8</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S7" data-id="911">Samsung Galaxy S7</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S7 edge" data-id="913">Samsung Galaxy S7 edge</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Lenovo A6000 + Bluetooth Speaker" data-id="975">Lenovo A6000 + Bluetooth Speaker</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Lenovo A6000 + Lenovo Tab 2 A7-30 Bundle" data-id="987">Lenovo A6000 + Lenovo Tab 2 A7-30 Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei Y6 Pro" data-id="1016">Huawei Y6 Pro</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Cherry Mobile Flare Lite 2 + Superion Vector Bundle" data-id="1034">Cherry Mobile Flare Lite 2 + Superion Vector Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei GR3 + B0 Watch" data-id="1040">Huawei GR3 + B0 Watch</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei Mate 8" data-id="1079">Huawei Mate 8</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Disney Mobile Frozen" data-id="1081">Disney Mobile Frozen</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Disney Mobile Mickey" data-id="1083">Disney Mobile Mickey</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Disney Mobile Princess" data-id="1085">Disney Mobile Princess</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone SE" data-id="1089">iPhone SE</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Asus Zenfone Selfie" data-id="1096">Asus Zenfone Selfie</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Oppo Neo 5s" data-id="1102">Oppo Neo 5s</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="LG G5 LTE + LG Cam Plus Bundle" data-id="1104">LG G5 LTE + LG Cam Plus Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J1 2016 + Tab 3 V Bundle" data-id="1108">Samsung Galaxy J1 2016 + Tab 3 V Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Lenovo Vibe K4 Note + Virtual Reality Headset" data-id="1157">Lenovo Vibe K4 Note + Virtual Reality Headset</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone 6 Whitebox" data-id="1199">iPhone 6 Whitebox</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J5 2016" data-id="1225">Samsung Galaxy J5 2016</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J7 2016" data-id="1227">Samsung Galaxy J7 2016</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei P9" data-id="1463">Huawei P9</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy Note7" data-id="1497">Samsung Galaxy Note7</a></li>
                                                              </ul>
                                                          </div>

                            <select name="product_id" id="product-plan-select" data-id="plan-product" class="storage-size updateProduct collapse">
                                                            <option data-id="Samsung Galaxy S6 edge" value="134" selected="selected">Samsung Galaxy S6 edge</option>
                                                            <option data-id="Samsung Galaxy S6" value="138">Samsung Galaxy S6</option>
                                                            <option data-id="Samsung Galaxy Note 5" value="309">Samsung Galaxy Note 5</option>
                                                            <option data-id="Samsung Galaxy S6 edge+" value="311">Samsung Galaxy S6 edge+</option>
                                                            <option data-id="BlackBerry Classic" value="358">BlackBerry Classic</option>
                                                            <option data-id="iPhone 6s" value="378">iPhone 6s</option>
                                                            <option data-id="iPhone 6s Plus" value="380">iPhone 6s Plus</option>
                                                            <option data-id="Cloudfone Thrill 500x" value="394">Cloudfone Thrill 500x</option>
                                                            <option data-id="Samsung Galaxy J1 2016" value="907">Samsung Galaxy J1 2016</option>
                                                            <option data-id="Huawei G8" value="909">Huawei G8</option>
                                                            <option data-id="Samsung Galaxy S7" value="911">Samsung Galaxy S7</option>
                                                            <option data-id="Samsung Galaxy S7 edge" value="913">Samsung Galaxy S7 edge</option>
                                                            <option data-id="Lenovo A6000 + Bluetooth Speaker" value="975">Lenovo A6000 + Bluetooth Speaker</option>
                                                            <option data-id="Lenovo A6000 + Lenovo Tab 2 A7-30 Bundle" value="987">Lenovo A6000 + Lenovo Tab 2 A7-30 Bundle</option>
                                                            <option data-id="Huawei Y6 Pro" value="1016">Huawei Y6 Pro</option>
                                                            <option data-id="Cherry Mobile Flare Lite 2 + Superion Vector Bundle" value="1034">Cherry Mobile Flare Lite 2 + Superion Vector Bundle</option>
                                                            <option data-id="Huawei GR3 + B0 Watch" value="1040">Huawei GR3 + B0 Watch</option>
                                                            <option data-id="Huawei Mate 8" value="1079">Huawei Mate 8</option>
                                                            <option data-id="Disney Mobile Frozen" value="1081">Disney Mobile Frozen</option>
                                                            <option data-id="Disney Mobile Mickey" value="1083">Disney Mobile Mickey</option>
                                                            <option data-id="Disney Mobile Princess" value="1085">Disney Mobile Princess</option>
                                                            <option data-id="iPhone SE" value="1089">iPhone SE</option>
                                                            <option data-id="Asus Zenfone Selfie" value="1096">Asus Zenfone Selfie</option>
                                                            <option data-id="Oppo Neo 5s" value="1102">Oppo Neo 5s</option>
                                                            <option data-id="LG G5 LTE + LG Cam Plus Bundle" value="1104">LG G5 LTE + LG Cam Plus Bundle</option>
                                                            <option data-id="Samsung Galaxy J1 2016 + Tab 3 V Bundle" value="1108">Samsung Galaxy J1 2016 + Tab 3 V Bundle</option>
                                                            <option data-id="Lenovo Vibe K4 Note + Virtual Reality Headset" value="1157">Lenovo Vibe K4 Note + Virtual Reality Headset</option>
                                                            <option data-id="iPhone 6 Whitebox" value="1199">iPhone 6 Whitebox</option>
                                                            <option data-id="Samsung Galaxy J5 2016" value="1225">Samsung Galaxy J5 2016</option>
                                                            <option data-id="Samsung Galaxy J7 2016" value="1227">Samsung Galaxy J7 2016</option>
                                                            <option data-id="Huawei P9" value="1463">Huawei P9</option>
                                                            <option data-id="Samsung Galaxy Note7" value="1497">Samsung Galaxy Note7</option>
                                                          </select>
                            <input type="hidden" id="producthidden" name="product_hidden" value="134">

                            <input type="hidden" id="product-hidden-val" value="134">
                            <input type="hidden" id="product-hidden-label" value="Samsung Galaxy S6 edge">
                            <input type="hidden" id="product-hidden-val-orig" value="134">
                            <input type="hidden" id="product-hidden-label-orig" value="Samsung Galaxy S6 edge">
                          </div>

                          <div id="plan-product-options"><div class="productVariation">    <div class="options"><div id="option-275" class="option storage-variation-edit pull-left optionFields"><input type="hidden" id="optionhidden_275" name="optionhidden[275]" value="525"></div><div id="prepaid_kit"><input type="hidden" class="myLifestyle Plan 999" id="optionhidden_275" name="option[275]" value="525"></div><div id="option-278" class="option storage-variation-edit pull-left optionFields"><input type="hidden" id="optionhidden_278" name="optionhidden[278]" value="531"></div><div id="prepaid_kit"><input type="hidden" class="24 months" id="optionhidden_278" name="option[278]" value="531"></div><div id="option-276" class="option storage-variation-edit pull-left optionFields remWi"><div class="dropdown dropdown-276"><button class="btn btn-default dropdown-toggle attrHver " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><div style="padding-right: 5px;"><span class="drpdwn">White Pearl</span><span class="caret caretx"></span></div></button><ul class="dropdown-menu dropdown-attributes-menu attrHverLI" role="menu" aria-labelledby="dropdownMenu1" style="index: 99;"><li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="Gold Platinum" data-id="535">Gold Platinum</a></li><li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="Emerald Green" data-id="707">Emerald Green</a></li><li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="Black Sapphire" data-id="537">Black Sapphire</a></li><li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="White Pearl" data-id="536">White Pearl</a></li></ul></div><select name="option[276]" id="optionSelect-276" data-id="276" class="storage-size updateVal collapse"><option data-id="Gold Platinum" value="535">Gold Platinum</option><option data-id="Emerald Green" value="707">Emerald Green</option><option data-id="Black Sapphire" value="537">Black Sapphire</option><option data-id="White Pearl" value="536" selected="selected">White Pearl</option></select><input type="hidden" id="optionhidden_276" name="optionhidden[276]" value="536"><input type="hidden" id="option-hidden-val-276" value="536"><input type="hidden" id="option-hidden-label-276" value="White Pearl"><input type="hidden" id="option-hidden-val-orig-276" value="536"><input type="hidden" id="option-hidden-label-orig-276" value="White Pearl"></div><div id="option-text-276" class="pull-left" style="margin-top: 2%; width: 50%; display:none;"><div class="storage-label label label-default">White Pearl</div><div class="change-btn"><a class="color" onclick="changeBtn(276)">Change</a></div></div><div id="option-277" class="option storage-variation-edit pull-left optionFields remWi"><div class="dropdown dropdown-277"><button class="btn btn-default dropdown-toggle attrHver " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><div style="padding-right: 5px;"><span class="drpdwn">64 GB</span><span class="caret caretx"></span></div></button><ul class="dropdown-menu dropdown-attributes-menu attrHverLI" role="menu" aria-labelledby="dropdownMenu1" style="index: 99;"><li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="277" data-value="32 GB" data-id="529">32 GB</a></li><li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="277" data-value="64 GB" data-id="530">64 GB</a></li></ul></div><select name="option[277]" id="optionSelect-277" data-id="277" class="storage-size updateVal collapse"><option data-id="32 GB" value="529">32 GB</option><option data-id="64 GB" value="530" selected="selected">64 GB</option></select><input type="hidden" id="optionhidden_277" name="optionhidden[277]" value="530"><input type="hidden" id="option-hidden-val-277" value="530"><input type="hidden" id="option-hidden-label-277" value="64 GB"><input type="hidden" id="option-hidden-val-orig-277" value="530"><input type="hidden" id="option-hidden-label-orig-277" value="64 GB"></div><div id="option-text-277" class="pull-left" style="margin-top: 2%; width: 50%; display:none;"><div class="storage-label label label-default">64 GB</div><div class="change-btn"><a class="color" onclick="changeBtn(277)">Change</a></div></div></div></div></div>

                          <!--
                                                                                                          -->

                        </div>
                      </div>

                                            
                                            <!--
                      <div class="productReviews collapse">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <p>5 out of 5 stars <a href="#" class="btn btn-inverse">(6 reviews)</a></p>
                      </div>
                      -->
                  </div>
              </div>
                            <div class="col-xs-4 col-sm-5 col-md-5 col-lg-4 heroCol">
                  
              <div id="div-prepaid">
                    <div itemprop="offers">
                      <link itemprop="availability" ><span style="display: none;">IN STOCK</span>
                      <meta itemprop="priceCurrency" content="PHP">
                        <span style="display: none;">PHP </span>
                        <span itemprop="price" style="display: none;">999.00</span> 

                      <div class="heroOffer text-center heroUpdateWd">
                          <div id="plan-loading"></div>
                          <div class="stock-level instock">IN STOCK</div>
                          <div class="hero-offer-title"><%= planDesc%> </div>
                          <div class="cashout"><strong>with</strong> <span id="prepaid-cashout"><meta itemprop="priceCurrency" content="PHP"><span itemprop="price">PHP 24,300.00</span></span> cashout</div>
                          <div class="add-cart-btn-orange-container" id="prepaid_kit">
                            <input type="hidden" name="quantity" value="1">

                            <div id="prepaid-hidden"><input type="hidden" name="product_id" value="134"><input type="hidden" name="option[275]" value="525"><input type="hidden" name="option[278]" value="531"><input type="hidden" name="option[276]" value="536"><input type="hidden" name="option[277]" value="530"></div>
<a href ="http://localhost:4504/content/globeEcommerce/checkoutpage.html">
                            <button id="hero-offer-addtocart-btn" class="btn btn-lg btn-addcart btn-bw-orange button-cart" data-id="prepaid_kit" style="">ADD TO CART</button>
                          </div></a>
                          <div class="hero-offer-details" id="prepaid-details"><ul>
	<li>499 Unli Call &amp; Text to Globe/TM</li>
	<li>GoSURF 499 (3GB Data Allowance)</li>
	<li>FREE 3 months 1GB Spotify Premium or HOOQ</li>
	<li>FREE choice between Navigation Pack, Explore Pack, or Fitness Pack for 1 month</li>
	<li>FREE 1 month Gadget Care</li>
	<li>FREE 1GB Globe Cloud</li>
	<li>FREE Shipping</li>
</ul>
</div>



                      </div>
                    </div>
                  </div></div>
                        </div>
      </div>
  </section><section class="sub-menu-tabs">
    <div class="container arrowPadd">
       <div class="navbar-collapse">
          <div id="tabs" class="htabs nav navbar-nav nav-center slick-initialized slick-slider" align="center">
                          <div class="slick-list draggable" tabindex="0"><div class="slick-track" style="opacity: 1; width: 460px; transform: translate3d(0px, 0px, 0px);"><div class="slick-slide slick-active" style="width: 230px;" index="0"><a class="font-bold key-tab fontsize20 selectedTab selected" href="#tab-0">Plan Details</a></div><div class="slick-slide slick-active" style="width: 230px;" index="1"><a class="font-bold key-tab fontsize20" href="#tab-1">myLifestyle Plan</a></div></div></div>
                          
                      </div>
      </div>
    </div>
  </section><section class="productTab">
            <div id="tab-0" class="tab-content container" style="display: block;">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <div class="container overview__mylifestyleplan">
<style type="text/css">.pdp-container .productTab .tab-content.container {
  padding: 0;
  width: auto !important; }
  .pdp-container .productTab .tab-content.container [class^="col-"] {
    padding: 0; }
  .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
    padding: 60px 60px 40px 60px;
    width: auto !important; }
    @media (min-width: 768px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 750px !important; } }
    @media (min-width: 992px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 970px !important; } }
    @media (min-width: 1200px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 1200px !important; } }

.rwd-pdp-container .productTab .container.tab-content [class^="col-"] {
  padding: 0; }

.productTab .overview__mylifestyleplan div h2 {
  margin: 0.67em 0px;
  font: 1.8em/1 fs_elliot_proregular, Helvetica; }
.productTab .overview__mylifestyleplan div p {
  font: 1em/1.6 fs_elliot_proregular, Helvetica; }
.productTab .overview__mylifestyleplan div ul li {
  font: 1em/1.4 fs_elliot_proregular, Helvetica; }
.productTab .overview__mylifestyleplan > row {
  margin: 0; }
.productTab iframe {
  min-height: 253px;
  min-width: 80%;
  border: 0; }
  
#wrap {
    margin: 0em 1em 0em 1em;
}
.left {
    float: left;
    margin-right: .3em;
}
.right {
    float: right;
    margin-left: 1em;
}
p {
    margin-bottom: .5em;
    margin-top: 0em;
    overflow: hidden; /* this is what keep the <p> from wrapping */
}
</style>
<div id="container"><img alt="myLifestyle Plan 999" class="img-responsive lazy" src="${properties.plan}"  style="display: block;">&nbsp;
<p>Stay connected with your pals—online or offline—with myLifestyle Plan 999. Get your daily dose of chitchat and LOL from your BFFs with unlimited calls and texts to Globe and TM, mobile Internet, and other Lifestyle Packs. You even get to choose among Navigation, Explore, and Fitness Packs as your freebie for one (1) month, plus one (1) month Gadget Care subscription and one (1) GB Globe Cloud storage for 24 months—all FREE when you apply for myLifestyle Plan 999!</p>
&nbsp;

<div class="row">
<div class="col-md-4">
<div id="wrap"><img class="left lazy" height="25px" src="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/wifi.png" width="25px" data-original="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/wifi.png">
<p>Surf your favorite sites or download the trending games and apps with <b>GoSURF</b>.</p>
</div>

<div id="wrap"><img class="left lazy" height="25px" src="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/chatpack.png" width="25px" data-original="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/chatpack.png" style="display: block;">
<p>Spread good vibes and brighten up someone’s day with <b>Chat Pack</b> using the cutest stickers and emoticons.</p>
</div>
</div>

<div class="col-md-4">
<div id="wrap"><img class="left lazy" height="25px" src="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/navigation.PNG" width="25px" data-original="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/navigation.PNG" style="display: block;">
<p>Keep track of the weather, roads, and destinations or discover fun ways of staying healthy with your <b>choice between a FREE Navigation Pack, Explore Pack, or Fitness Pack for one month</b>.</p>
</div>

<div id="wrap"><img class="left lazy" height="25px" src="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/spotify_icon2.png" width="25px" data-original="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/spotify_icon2.png" style="display: block;">
<p>Find your rhythm or create your own playlist from the 30 million songs on <b>Spotify</b>.</p>
</div>
</div>

<div class="col-md-4">
<div id="wrap"><img class="left lazy" height="25px" src="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/gadget_care.png" width="25px" data-original="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/gadget_care.png" style="display: block;">
<p>Stay worry-free when you protect your device from accidental damage or theft with <b>Gadget Care</b>.</p>
</div>

<div id="wrap"><img class="left lazy" height="25px" src="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/globe_cloud.png" width="25px" data-original="http://shop.globe.com.ph/image/data/myLifestyle%20Plan/icons/globe_cloud.png" style="display: block;">
<p>Keep your important office and personal files handy, and access them wherever, whenever with <b>Globe Cloud</b>.</p>
</div>
</div>
</div>
&nbsp;

<p>With myLifestyle Plan 999, you can experience life's best moments and share them with your friends. Plus, you can even customize your own Postpaid Plan to match the way you live for as low as P999 per month.</p>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
              <div id="tab-1" class="tab-content container" style="display: none;">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <div class="container overview__mylifestyleplan">
<style type="text/css">.pdp-container .productTab .tab-content.container {
  padding: 0;
  width: auto !important; }
  .pdp-container .productTab .tab-content.container [class^="col-"] {
    padding: 0; }
  .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
    padding: 60px 60px 40px 60px;
    width: auto !important; }
    @media (min-width: 768px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 750px !important; } }
    @media (min-width: 992px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 970px !important; } }
    @media (min-width: 1200px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 1200px !important; } }

.rwd-pdp-container .productTab .container.tab-content [class^="col-"] {
  padding: 0; }

.productTab .overview__mylifestyleplan div h2 {
  margin: 0.67em 0px;
  font: 1.8em/1 fs_elliot_proregular, Helvetica; }
.productTab .overview__mylifestyleplan div p {
  font: 1em/1.6 fs_elliot_proregular, Helvetica; }
.productTab .overview__mylifestyleplan div ul li {
  font: 1em/1.4 fs_elliot_proregular, Helvetica; }
.productTab .overview__mylifestyleplan > row {
  margin: 0; }
.productTab iframe {
  min-height: 253px;
  min-width: 80%;
  border: 0; }
</style>
<div class="col-sm-12">
<h2>Imagine a plan that's built for the way you live now. With the new myLifestyle Plan, everything that matters to you everyday comes easy.</h2>
</div>

<div class="col-sm-6">
<p>Your base plan comes with built-in unlimited calls and texts to Globe/TM, starting at P499.</p>

<p>+ Add&nbsp;<b>Surf Packs*</b>&nbsp;for your mobile internet needs.<br>
+ Choose among&nbsp;<b>Lifestyle Packs</b>&nbsp;that are perfect for you!<br>
+ Get extra consumables, calls and texts to other networks, to landline, and abroad with&nbsp;<b>Classic Packs.</b></p>

<h3>The more packs you add to your Plan, the better handset you get!</h3>
</div>

<div class="col-sm-6 text-center"><iframe frameborder="0" src="${properties.video}"></iframe></div>

<div class="col-sm-12">&nbsp;
<p><small>*Minimum GoSURF 99 required if availing of handset. To modify the Promo Packs that come with our preset plans, complete your order and an Online Sales agent will be in touch to help you build your perfect plan. To learn more about the Promo Packs, visit our <a href="http://www.globe.com.ph/mylifestyleplan-info" rel="nofollow" style="font-size:14px;" target="_blank" title="Click here to learn more about the different myLifestyle Plan Surf, Lifestyle and Classic Packs!">myLifestyle Plan</a> info page.</small></p>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
  </section><section class="seoContent">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  </div>
      </div>
    </div>
  </section><section class="crossSell">
    <h1 class="text-center bw-subtitle-sm">FREQUENTLY BOUGHT TOGETHER</h1>
    <div class="container" id="crossSell">
      <div class="row">
        <div class="col-sm-12 col-md-8">
          <div class="itemHolder">

            <div class="freqItem" align="center">
              <div id="imageHolder0" class="image-holder">
                <span class="icon-tick"></span>
                <img class="" id="hero_offer_fb_image_0" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Samsung/Galaxy S6 Edge Plus/Galaxy-S6-edge-plus-front-Silver-150x150.png" border="0">
              </div>
              <div class="content-holder text-center">
                <h3><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-s6-edge" id="hero_offer_fb_url_0" class="fontsize24"><span id="hero_offer_fb_name_0">Samsung Galaxy S6 edge</span></a></h3>
                <p id="hero_offer_fb_price_0">PHP 25,299.00</p>
                
                <input type="hidden" id="hero_offer_fb_add_0" name="addcart[0]" value="1">
                <input type="hidden" id="hero_offer_fb_priceval_0" name="price[0]" value="25299.00">
                <input type="hidden" id="hero_offer_fb_productid_0" name="product_id[0]" value="134">
                <input type="hidden" id="hero_offer_fb_minimum_0" name="quantity[0]" size="2" value="1">

                <div id="hero_offer_fb_options_0"><input type="hidden" name="option[0][275]" value="525"><input type="hidden" name="option[0][278]" value="531"><input type="hidden" name="option[0][276]" value="536"><input type="hidden" name="option[0][277]" value="530"></div>
              </div>
            </div>

            <div id="fb_items">  <div class="plusIcon">    <span class="icon-plus"></span>  </div><div class="freqItem" align="center">  <div id="imageHolder1" class="image-holder" onclick="setFBItem(1)">    <span class="icon-tick"></span>    <img class="" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Digital Connectors/Lexar-Jumpdrive-USB/lexar-jumpdrive-64gb-150x150.png">  </div>  <div class="content-holder text-center">    <h3><a href="http://shop.globe.com.ph/products/gadgets-and-accessories/lexar-64gb-jumpdrive-m20-mobile-usb-flash-drive-3-0" class="fontsize24">Lexar 64GB JumpDrive M20 Mobile USB Flash Drive 3.0</a></h3>   <p id="hero_offer_fb_price_1">PHP 1,700.00</p>    <input type="hidden" id="hero_offer_fb_add_1" name="addcart[1]" value="1">    <input type="hidden" id="hero_offer_fb_priceval_1" name="price[1]" value="1700.00">    <input type="hidden" name="product_id[1]" value="637">    <input type="hidden" name="quantity[1]" size="2" value="1">    <div id="hero_offer_fb_options_1">    <input type="hidden" name="option[1][953]" value="2049">    <input type="hidden" name="option[1][954]" value="2050">  </div>  </div></div>  <div class="plusIcon">    <span class="icon-plus"></span>  </div><div class="freqItem" align="center">  <div id="imageHolder2" class="image-holder" onclick="setFBItem(2)">    <span class="icon-tick"></span>    <img class="" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Digital Connectors/Samsung-Accessories/EB-PA5000-gold-150x150.png">  </div>  <div class="content-holder text-center">    <h3><a href="http://shop.globe.com.ph/products/gadgets-and-accessories/samsung-universal-slim-battery-pack-5200mah" class="fontsize24">Samsung Universal Slim Battery Pack 5,200mAh</a></h3>   <p id="hero_offer_fb_price_2">PHP 2,499.00</p>    <input type="hidden" id="hero_offer_fb_add_2" name="addcart[2]" value="1">    <input type="hidden" id="hero_offer_fb_priceval_2" name="price[2]" value="2499.00">    <input type="hidden" name="product_id[2]" value="1002">    <input type="hidden" name="quantity[2]" size="2" value="1">    <div id="hero_offer_fb_options_2">    <input type="hidden" name="option[2][1602]" value="3407">  </div>  </div></div></div>
            
          </div>
        </div>
        <div class="col-sm-12 col-md-4 cross-sell-total">
          <h3>Total: <span id="fb_total">P29,498.00</span></h3>
          <button id="pdp-frequent-bought-add-btn" class="btn btn-bw-blue changeB btn-block btn-fb-cart">ADD TO CART</button>
          <!-- <button class="btn btn-default btn-bw-white btn-block">Add all two to Wishlist</button> -->
          <br>
          <div id="fb_notif"></div>
        </div>
      </div>
    </div>
  </section>
    
    

  

    
  </div>

<div class="rwd-pdp-container visible-xs rwd-pdp-container-plan-only">

  <br><!--RWD HELPER :) -->

  <section class="rwd-mainProducts container-fluid">
    <div class="rwd-product-info-badge-holder">
          </div>
    <div class="rwd-pdp-images slick-initialized slick-slider">
                  <div class="slick-list draggable" tabindex="0" style="height: 100px;"><div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);"><div class="text-center slick-slide slick-active" index="0" style="width: 0px;">
            <img src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-300x400.png">
            <br>
          </div></div></div>
            </div>

    <script type="text/javascript">
      $(document).ready(function($) {
        if(!isIE() || (isIE() && $(window).width() < 600)) {
          $('.rwd-pdp-images').slick({
            dots: true,
            infinite: true,
            speed: 500,
            slide: 'div',
            cssEase: 'linear',
            adaptiveHeight: true
          });
        }
      });
    </script>

    <div class="rwd-productContent">
      <h1 class="title-bw-light">myLifestyle Plan 999</h1>
      <div class="rwd-pdp-description">
        <p><span style="font-family: fs_elliot_proregular, Helvetica; font-size: 1em; line-height: normal;">myLifestyle Plan 999 comes with built-in Unlimited calls and texts to Globe/TM, and any combination of at least one mandatory Surf Pack and Lifestyle or Classic Packs worth up to P500.*</span></p>
<script>
  $('#myTabs').click(function (event) {
	$("#tab-0").css('display', 'none');
    $("#tab-1").css('display', 'block');
    $('div.slick-track a.fontsize24:contains("Plan")').removeClass("selectedTab selected");
    $('div.slick-track a.fontsize24:contains("Promos")').addClass("selectedTab selected");
    $('div.slick-slide:contains("Plan")').toggleClass("slick-active");
    $('div.slick-slide:contains("Promos")').toggleClass("slick-active");
  });
</script>

<h2 class="pdesc" style="font-size: 13px; line-height: 20.7999992370605px; color: rgb(0, 0, 139);">Online orders are for new and additional line applications only. You can also order this device by calling our Sales Hotline at (02) 730-1010, open Monday to Sunday from 6AM to 10PM. During the call, please enter the following promo code when prompted: 21211. For existing subscribers who wish to recontract, kindly call (02) 730-1300. This plan is available at <a href="http://www.globe.com.ph/store-locator" target="_blank">Globe stores</a> so visit the one near you!</h2>
<style type="text/css">.pdesc{
    font: 1em fs_elliot_proregular, Helvetica; }
</style>
      </div>
    </div>

    <!-- RWD Product Variation Container -->
    <div class="rwd-productVariation"></div>

    <div class="rwd-share-this">
        <a href="#" data-toggle="modal" data-target="#myModal"><span class="icon-share"></span>Share This</a>
    </div>

    <!-- RWD Hero Container -->
    <div class="rwd-heroCol"></div>
  </section>

  <section class="rwd-sub-menu-tabs">
  </section>

  <section class="rwd-prodcutTab">
  </section>

  <section class="rwd-seoContent">
  </section>

  <section class="rwd-crossSell">
  </section>

  <!-- INPUT HIDDEN FOR DISABLING -->
  <input type="hidden" name="disable-add-cart" value="0" id="disable-add-cart">

  <script type="text/javascript">
    function pdpRwdChangeLocation() {
      var winWidth = $(window).width();
      if(winWidth <= 767) {
        $(".productVariationParent").appendTo('.rwd-productVariation');
        $("#div-hero-offer").appendTo('.rwd-heroCol');
        $("#div-prepaid").appendTo('.rwd-heroCol');
        $(".sub-menu-tabs").appendTo('.rwd-sub-menu-tabs');
        $(".productTab").appendTo('.rwd-prodcutTab');
        $(".seoContent").appendTo('.rwd-seoContent');
        $(".crossSell").appendTo('.rwd-crossSell');
        setTimeout(function(){
          $("#tabs").unslick();
          var infinite = isIE() ? false : true;
          $('#tabs').slick({
            infinite: infinite,
            cssEase: 'linear',
            adaptiveHeight: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            onAfterChange: function() {
              $("#tabs .slick-active a").trigger("click");
            }
          });
        },200);
      }
      else if(winWidth > 767 && !isIE()) {
        $(".productVariationParent").insertAfter('.productContent');
        $("#div-hero-offer").appendTo('.heroCol');
        $("#div-prepaid").appendTo('.heroCol');
        $(".sub-menu-tabs").insertAfter('.mainProducts');

        if($('section').hasClass('sub-menu-tabs')){
          $(".productTab").insertAfter('.sub-menu-tabs');
        } else {
          
          $(".productTab").insertAfter('.mainProducts');
        }
        
        $(".crossSell").insertAfter('.productTab');
        $(".seoContent").insertAfter('.productTab');

        pdpCommonGreaterThanResize();
      }
      else if(isIE()){
        setTimeout(function(){
          $("#tabs").unslick();
          $('#tabs').slick({
            infinite: true,
            cssEase: 'linear',
            adaptiveHeight: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
          });
        },200);

      }
    }
    $(document).ready(function($) {

      function dropItdown()
      {
        $('.attrHver .drpdwn').each(function(){
          $.trim($(this).text($.trim($(this).text())));
          $(this).parent('div').css({'padding-right':'5px'});
          cutit = $(this).text().length;
          cutitTxt = $(this).text();

          var isFirefox = typeof InstallTrigger !== 'undefined';
          var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

          if($.trim(cutit)> 25 && $(window).width() <= 768)
          {
            $(this).text(cutitTxt.substring(0,25));
          }
          
          if($.trim(cutit)>45)
          {
           if(isSafari)
            {
              $(this).siblings('.caretx').css({'margin-top':'-10px'});
            }
           $(this).text(cutitTxt.substring(0,45));
           if($(window).width() <= 1024 && $(window).width() >= 769)
           {
              $(this).text(cutitTxt.substring(0,25));
           }
           else if($(window).width() <= 768)
           {
              $(this).text(cutitTxt.substring(0,18));
           }
          }
          else if($.trim(cutit)<=45)
          {
            if(isSafari)
            {
              $(this).siblings('.caretx').css({'margin-top':'10px'});
            }
          }

          if(isFirefox && $(window).width() < 768){
              $(this).siblings('.caretx').css({'margin-top':'-10px'});
          } else if (isFirefox && $(window).width() >= 768){
              $(this).siblings('.caretx').css({'margin-top':'10px'});
          }

        });
      }
      dropItdown();
    function fixedHeader()
    {
      if($(window).width() >= 1024) {
        $('.headerNavTop').addClass('navbar-fixed-top');//ticket #1093
        $('.headerNav').addClass('navbar-fixed-top toFixed');//ticket #1093
        $('#container').addClass('margTop');//ticket #1093
      }
      else
      {
        $('.headerNavTop').removeClass('navbar-fixed-top');//ticket #1093
        $('.headerNav').removeClass('navbar-fixed-top toFixed');//ticket #1093
        $('#container').removeClass('margTop');//ticket #1093
      }
    }
    //fixedHeader();
      var isFirefox = typeof InstallTrigger !== 'undefined';
      $(window).resize(function() {
        //fixedHeader();
        dropItdown();
        pdpRwdChangeLocation();
      });
      pdpRwdChangeLocation();
      if(isIE() || isFirefox) {
        $('#tabs').slick({
          infinite: true,
          cssEase: 'linear',
          adaptiveHeight: true,
          slidesToShow: 5,
          slidesToScroll: 1
        });
      }
    });
  </script>
</div>

<input type="hidden" id="popover" value="-1">
</div>

<div id="footer-top-wrap" class="container-fluid" style="background: #333333;">
    <div class="container footer-top-holder">
      <!-- INSERT FOOTER CONTENT HERE -->
      <style type="text/css">#footer hr.footer-divider{
    border-top: none !important;
  }

  #footer-top-wrap{
  background: #f6f6f6 !important;
  }

  .bg-gray{
   background:#f6f6f6;
  }
  .padd-top-bot{
     padding-top: 5px;
    padding-bottom: 0px;
  }
  .padd-top-bot10{
  padding-top: 10px;
  padding-bottom: 10px;
  }
  .f-shipping{
    text-align: center;
  }
  .f-payment{
    text-align: center;
  }
  .f-call{
    text-align: center;
  }
  .f-shipping a {
     text-decoration: none;
     color: #333;
  }
  .footer-options div{
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .f-shipping img{
  margin-right: 10px;
  margin-bottom: 0px;

  }
  .f-payment img{
  margin-right: 10px;
  margin-bottom: 0px;
  }
  .f-call img{
  margin-right: 10px;
  margin-bottom: 0px;
  }

   @media screen and (max-width: 768px) {
   .f-shipping{
      border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-payment{
       border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-call{
    text-align: center;
  }
}
@media screen and (max-width: 445px) {
.indent-mobile{
margin-left: 12%;
}
}

@media screen and (max-width: 391px) {
.indent-mobile{
margin-left: 12%;
}
}
@media screen and (max-width: 389px) {
.indent-mobile{
margin-left: 0%;
}
}
</style>
<div id="footer-top">
<div class="container-fluid bg-gray padd-top-bot">
<div class="container">
<div class="row footer-options">
<div class="col-sm-4 col-md-4 col-lg-4 f-shipping"><a href="http://shop.globe.com.ph/help" style="color:black" title="Click to learn more about our shipping, delivery and returns policies"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/shippingA.png" class="lazy" data-original="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/shippingA.png" style="display: inline;"><span class="m-top" title="Click to learn more about our shipping, delivery and returns policies">FREE SHIPPING ON ALL ORDERS</span></a></div>

<div class="col-sm-5 col-md-5 col-lg-5 f-payment"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/payment-method.png" class="lazy" data-original="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/payment-method.png" style="display: inline;"><span class="m-top">CASH ON DELIVERY, MAJOR<span class="indent-mobile"> CREDIT CARDS &amp; GCASH</span></span></a></div>

<div class="col-sm-3 col-md-3 col-lg-3 f-call"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/call.png" class="lazy" data-original="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/call.png" style="display: inline;"><span class="m-top">CALL (02)730-1010</span></a></div>
</div>
</div>
</div>
</div>
    </div>
  </div>
</body>
</html>
<!--<script type="text/javascript">
$(document).ready(function(){
    alert("dd");
    });
    $("#hero-offer-addtocart-btn").click(function(){
$.ajax({
	    type:"GET",
	    data: "quantity="+"1", 
	url : "/bin/epcortex/addToCart",
	    success:function(data){
    // $('#total').text(data);
    //  $('#orderTotal').text(data);
    // window.location.href="http://localhost:4504/content/globeEcommerce/shopbyplanpage.html";
		}
	});
    });


</script>-->