<%@ include file="/libs/foundation/global.jsp"%>

<html dir="ltr" lang="en" data-placeholder-focus="false">

     <cq:includeClientLib categories="productdetails"/>
<head>

<link rel="canonical" href="http://shop.globe.com.ph/" />

<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Checkout</title>

<link href="https://d11fuji4mn7bm2.cloudfront.net/media/data/Favicon/favicon.png" rel="icon" />

</head>

<body>


<div id="container" class="margTop">

<div id="notification" class="notification_cart">
  </div>

<div id="checkout-content" class="container" style="min-height: 256px;">
                <div class="required-prompt"><span></span></div>
                <div id="checkout-form-container" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <div id="form-container" class="row" style="height: 842px;">
                                                                <div id="section-container">
                                                                                <form id="app_form" class="" action="" method="post" enctype="multipart/form-data">
                                                                    <section id="personal-details" class="panel">
                                                                        <div class="panel-content"></div>
                                                                    </section>
                                                                    <section id="additional-personal-details" class="panel">
                                                                        <div class="panel-content"></div>
                                                                    </section>
                                                                    <section id="shipping-details" class="panel prev">
                                                                        <div class="panel-content"></div>
                                                                    </section>
                                                                    <section id="financial-details" class="panel current">
                        <div class="panel-content"><div class="panel-heading">${properties.pagetitle}</div>
<div class="panel-body">
                                        <div class="panel-row row" id="occupation-container" style="z-index: 7;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-12" id="occupation-form-group" style="z-index: 2; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.occupation}</label>
            
                                    <select name="financial_occupation" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="accountant">
                                    Accountant                                </option>
                                                            <option value="administrative_staff">
                                    Administrative Staff                                </option>
                                                            <option value="architect">
                                    Architect                                </option>
                                                            <option value="call_center_agent">
                                    Call Center Agent                                </option>
                                                            <option value="consultant">
                                    Consultant                                </option>
                                                            <option value="dentist">
                                    Dentist                                </option>
                                                            <option value="doctor">
                                    Doctor                                </option>
                                                            <option value="engineer">
                                    Engineer                                </option>
                                                            <option value="executive">
                                    Executive                                </option>
                                                            <option value="government_employee">
                                    Goverment Employee                                </option>
                                                            <option value="government_legislative_officials">
                                    Government Legislative Officials                                </option>
                                                            <option value="hospital_employee">
                                    Hospital Employee                                </option>
                                                            <option value="junior_official">
                                    Junior Official                                </option>
                                                            <option value="lawyer">
                                    Lawyer                                </option>
                                                            <option value="lgu_officials">
                                    LGU Officials                                </option>
                                                            <option value="manager_director">
                                    Manager/Director                                </option>
                                                            <option value="medical_field_employee">
                                    Medical Field Employee                                </option>
                                                            <option value="middle_management">
                                    Middle Management                                </option>
                                                            <option value="national_government_officials">
                                    National Government Officials                                </option>
                                                            <option value="ofw_beneficiary_allottee">
                                    OFW Benificiary/Allottee                                </option>
                                                            <option value="owner">
                                    Owner                                </option>
                                                            <option value="president_coo_ceo">
                                    President/COO/CEO                                </option>
                                                            <option value="private_corporate_employee">
                                    Private Corporate Employee                                </option>
                                                            <option value="retired">
                                    Retired                                </option>
                                                            <option value="sales_staff">
                                    Sales Staff                                </option>
                                                            <option value="school_employee">
                                    School Employee                                </option>
                                                            <option value="self_employed">
                                    Self Employed                                </option>
                                                            <option value="senior_officer">
                                    Senior Officer                                </option>
                                                            <option value="sole_proprietor_entrepreneur">
                                    Sole Proprietor/Entrepreneur                                </option>
                                                            <option value="staff_associate_clerk">
                                    Staff/Associate/Clerk                                </option>
                                                            <option value="student">
                                    Student                                </option>
                                                            <option value="supervisor">
                                    Supervisor                                </option>
                                                            <option value="unemployed">
                                    Unemployed                                </option>
                                                            <option value="vice_president_svp">
                                    Vice President/SVP                                </option>
                                                                        </select>
                            </div>


                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-12" style="z-index: 1; height: 73px;">
                <label class="field-label">${properties.employerbusiness}</label>
            
                                    <input type="text" data-required="required" class=" required-data" name="financial_employer" value="" maxlength="255">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 6;">
            
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 3; height: 73px;">
                <label class="field-label">${proeprties.years}</label>
            
                                    <input type="text" data-required="required" class="checkout-forms-text-numeric required-data" name="financial_office_years_in_company" value="" maxlength="3">
                            </div>

            

            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 2; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.grossincome}</label>
            
                                    <select name="financial_office_gross_monthly_income" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="8,000 - 9,999">
                                    8,000 - 9,999                                </option>
                                                            <option value="10,000 - 12,999">
                                    10,000 - 12,999                                </option>
                                                            <option value="13,000 - 14,999">
                                    13,000 - 14,999                                </option>
                                                            <option value="15,000 - 19,999">
                                    15,000 - 19,999                                </option>
                                                            <option value="20,000 - 24,999">
                                    20,000 - 24,999                                </option>
                                                            <option value="25,000 - 29,999">
                                    25,000 - 29,999                                </option>
                                                            <option value="30,000 - 34,999">
                                    30,000 - 34,999                                </option>
                                                            <option value="35,000 - 49,999">
                                    35,000 - 49,999                                </option>
                                                            <option value="50,000 - 59,999">
                                    50,000 - 59,999                                </option>
                                                            <option value="60,000 - 89,999">
                                    60,000 - 89,999                                </option>
                                                            <option value="90,000 and up">
                                    90,000 and up                                </option>
                                                                        </select>
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.industry}</label>
            
                                    <select name="financial_industry" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="banking_and_finance">
                                    Banking and Finance                                </option>
                                                            <option value="it_telecommunication">
                                    IT/Telecommunication                                </option>
                                                            <option value="manufacturing">
                                    Manufacturing                                </option>
                                                            <option value="other">
                                    Other                                </option>
                                                            <option value="power_utilities">
                                    Power/Utilities                                </option>
                                                            <option value="service">
                                    Service                                </option>
                                                                        </select>
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 5;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">${properties.currentpos}</label>
            
                                    <input type="text" data-required="required" class=" required-data" name="financial_office_current_position" value="" maxlength="100">
                            </div>

                        </div>
            
                        
                                        <div class="panel-row row" style="z-index: 4;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">${properties.fullcompadd}</label>
            
                                    <input type="text" data-required="required" class=" required-data" name="financial_office_house_number" value="" maxlength="255">
                
                    <input type="hidden" id="financial_office_street" name="financial_office_street" value="">
                    <input type="hidden" id="financial_office_barangay" name="financial_office_barangay" value="">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 3;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: 73px;">
                <label class="field-label">${properties.area}</label>
            
                                    <input type="text" placeholder="Makati, Quezon City, etc" data-required="required" class="required-data ui-autocomplete-input" name="financial_office_city_temp" value="" maxlength="255" autocomplete="off">

                    <input type="hidden" id="financial_office_city" name="financial_office_city" class=" required-data" value="">
                    <input type="hidden" id="financial_office_province" name="financial_office_province" class=" required-data" value="">
                    <input type="hidden" id="financial_office_region" name="financial_office_region" class=" required-data" value="">
                    <input type="hidden" id="financial_office_country" name="financial_office_country" class=" required-data" value="">    
                    <div id="financial-details-val" class="details-val">
                        <span class="check-icon"></span>
                        <text></text>
                    </div>
                            </div>


                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.zip}</label>
            
                                    <select name="financial_office_zip_code" class="required-data" data-required="required" data-default=""><option value="" selected="selected">Please Select</option></select>
                            </div>

                        </div>

                        
                                        <div class="panel-row row" style="z-index: 2;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">Office Email Address (optional)</label>
            
                                    <input type="text" data-required="optional" class=" optional-data" name="financial_office_email_address" value="" maxlength="255">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 1;">
            

            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: 73px;">
                <label class="field-label">Office Phone Number (optional)</label>
            
                                    <input type="text" data-required="optional" class="checkout-forms-text-numeric optional-data" name="financial_office_phone_number" value="" maxlength="15">
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: 73px;">
                <label class="field-label">Company/HR Number (optional)</label>
            
                                    <input type="text" data-required="optional" class="checkout-forms-text-numeric optional-data" name="financial_office_company_number" value="" maxlength="15">
                            </div>

                        </div>
            
                        
    <input type="hidden" id="checkout_type" value="postpaid">
</div>
<div id="panel-bottom" class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a id="prev-step" onclick="setPreviousPanel('shipping-details', 'financial-details')">Previous Step</a>
        <button id="button-financial-details" class="btn"><a href="/content/globeEcommerce/documentuploadpage.html?wcmmode=disabled" style= "color:white;">Next Step</a></button>
    </div>
</div>



</div>
                                                                    </section>

                                </form>
                                                    </div>
                                    </div>

                                </div>
        <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><p><img class="img-responsive" id="norton-secured" src="${properties.fileReference}" ></p>

<div class="row" id="contact-details">
<div class="info-block col-lg-12 col-md-12 col-sm-12 col-xs-4">
    <p>${properties.imgdesc}</p>
</div>
</div>
</div>
                </div>
</div>


</div>


</body>
</html>
