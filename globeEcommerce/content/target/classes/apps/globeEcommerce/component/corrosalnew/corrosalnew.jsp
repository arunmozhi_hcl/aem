<%@ include file="/libs/foundation/global.jsp" %>
<!doctype html>
<cq:includeClientLib categories="carousel"/>
<head>
</head>
 <body>
  <div id="custom-banner-wrapper">
      <div id="custom-banner" class="carousel slide " data-ride="carousel" data-interval="5000">

        <div class="carousel-inner custom-banner-inner">

                                            <div id="custom-banner-fullwidth" class="item custom-banner-item active">
              <style type="text/css">#custom-banner-wrapper #custom-banner .carousel-control {
    
    top: 42% !important;
    background: #000 !important;
    height: 45px !important;
    width: 45px !important;
    padding: 5px !important;

}
</style>
<div class="custom-banner-bg container-fluid" style="background: rgb(255, 255, 255); padding: 0px;"><a href="http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox"><img alt="Apple iPhone 6 whitebox" src="${properties.image4}" title="Introducing the new whitebox. Now with iPhone 6" width="100%" class="lazy" style="display: inline;"> </a>
<div class="container"><!--
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 custom-banner-caption">&nbsp;</div>
</div>
--></div>
</div>
            </div>
                                              <div id="custom-banner-fullwidth" class="item custom-banner-item">
              <div class="custom-banner-bg container-fluid" style="background: rgb(255, 255, 255); padding: 0px;"><a href="http://shop.globe.com.ph/products/bundled-devices/lenovo-a6000-and-lenovo-tab-2-a7-30-bundle"><img alt="Lenovo A6000 + Tab 2 A7-30 bundle" src="${properties.image3}" title="Apply for Lenovo A6000 + Tab 2 A7-30 bundle at Plan 999 799 and get 1.5 3GB mobile data." width="100%"> </a>

<div class="container"><!--
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 custom-banner-caption">&nbsp;</div>
</div>
--></div>
</div>
            </div>
                                              <div id="custom-banner-fullwidth" class="item custom-banner-item">
              <div class="custom-banner-bg container-fluid" style="background: rgb(255, 255, 255); padding: 0px;"><a href="http://shop.globe.com.ph/broadband"><img alt="Plans 1299 and up come with HOOQ and Netflix for 6 months. Starts at 10Mbps with 50GB Data" src="${properties.image2}" title="Watch the Greatest Movies Anytime You Want" width="100%"> </a>

<div class="container"><!--
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 custom-banner-caption">&nbsp;</div>
</div>
--></div>
</div>
            </div>
                                              <div id="custom-banner-fullwidth" class="item custom-banner-item">
              <div class="custom-banner-bg container-fluid" style="background: rgb(255, 255, 255); padding: 0px;"><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale"><img alt="Great Gadget Sale" src="${properties.image1}" title="Globe Great Gadget Sale" width="100%"> </a>

<div class="container"><!--
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 custom-banner-caption">&nbsp;</div>
</div>
--></div>
</div>
            </div>
                              </div>

        <ol class="carousel-indicators" style="bottom:0px;">
                      <li data-target="#custom-banner" data-slide-to="0" class="active"></li>
                                  <li data-target="#custom-banner" data-slide-to="1" class=""></li>
                                  <li data-target="#custom-banner" data-slide-to="2" class=""></li>
                                  <li data-target="#custom-banner" data-slide-to="3" class=""></li>
                              </ol>

        <a style="background-image:none;" class="left carousel-control" href="#custom-banner" data-slide="prev">
           <span><img src="/content/dam/shoppingcart/cb-arrow-left.png" class="lazy"  style="display: inline;"></span>
        </a>
        <a style="background-image:none;" class="right carousel-control" href="#custom-banner" data-slide="next">
            <span><img src="/content/dam/shoppingcart/cb-arrow-right.png" class="lazy"  style="display: inline;"></span>
        </a>
      </div>
    </div>
 </body>
</html>
