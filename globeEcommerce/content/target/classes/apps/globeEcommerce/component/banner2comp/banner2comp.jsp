<%@ include file="/libs/foundation/global.jsp" %>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">

     <title>Document</title>
 </head>
 <body>
  <div id="foldsContainer" style="position:relative; -ms-overflow-x: hidden; -ms-overflow-y: hidden;">
<div id="foldsContainerForCMS">
<style type="text/css">#banner1, #banner2, #banner3, #banner4, #banner5 {
        font-family:fs_elliot_pro-light,Arial,sans-serif!important;
      }

      #foldsContainerForCMS h1 {
        font-size:72px !important;
      }
      #foldsContainerForCMS h2 {
        font-size:32px !important;
        font-weight: bold !important;
      }
      #foldsContainerForCMS h3 {
        font-size:26px !important; 
      }
      #foldsContainerForCMS h4 {
        font-size:18px !important;
        margin-top: 5px !important;
        margin-bottom: 5px !important;
        font-weight: normal !important
      }

      #banner5 {
        height: 600px;
        position: relative;
      }
      #banner5 .sec1 {
        background:url('/content/dam/globeEcommerce/Netflix-Broadband.jpg');
        background-repeat:no-repeat;
        background-size:cover;
        width:55%;
        height:600px;
        display: inline-block;
        background-position-x: 97%;
    	
  
      }
      #banner5 .sec1 div {
        font-size:20px;
        position: absolute;
        bottom:0px;
        background: #ffffff;
        opacity:0.6;
        padding:15px
      }
      #banner5 .sec2 {
        display: inline-block;
        width:45%;
        position: absolute;
      }
      #banner5 .sec2 .portion1 {
        background:url('/content/dam/globeEcommerce/mlp1.jpg');
        background-repeat:no-repeat;
        background-size:cover;
        height: 300px;
        position:relative
      }
      #banner5 .sec2 .portion1 div {
        font-size:60px;
        background: #ffffff;
        opacity:0.6;
        padding:15px;
        bottom:0px;
        position: absolute;
        right: 0px;
        width: 60%
      }
      #banner5 .sec2 .portion2 {
        background:url('/content/dam/globeEcommerce/MSP3.jpg');
        background-repeat:no-repeat;
        background-size:cover;
        background-position: 0px;
        height: 300px;
        position:relative
      }
      #banner5 .sec2 .portion2 div {
        font-size:60px;
        background: #ffffff;
        opacity:0.6;
        padding:15px;
        bottom:0px;
        position: absolute;
        right: 0px;
        width: 60%
      }

      /*Tablet*/
      @media all and (max-width: 1024px) {
        #foldsContainerForCMS h1 {
          font-size:40px !important;
        }
        #foldsContainerForCMS h2 {
          font-size:26px !important;
          font-weight: bold;
        }
        #foldsContainerForCMS h3 {
          font-size:22px !important; 
        }
        #foldsContainerForCMS h4 {
          font-size:18px !important;
          margin-top: 10px;
          margin-bottom: 10px;
        }
        #banner5 .sec2 .portion1, #banner5 .sec2 .portion2 {
          background-position: center;
        }
        #banner5 .sec1 {
          position: relative;
        }
      }
      @media all and (max-width: 768px) {
        #foldsContainerForCMS h1 {
          font-size: 35px !important;
          margin-bottom: 10px;  
        }
        #foldsContainerForCMS h2 {
          font-size:22px !important;
          font-weight: bold;
        }
        #foldsContainerForCMS h3 {
          font-size:18px !important; 
        }
        #foldsContainerForCMS h4 {
          font-size:16px !important;
          margin-top: 10px;
          margin-bottom: 10px;
        }

        #banner5 .sec2 .portion1 div, #banner5 .sec2 .portion2 div {
          width: 100%
        }
        #banner5 .sec1 {
          position: relative;
        }
        #banner5 .sec1 div {
          width: 100%;
          padding: 10px;
        }
        #banner5, #banner5 .sec1 {
          height: 350px;
        }
        #banner5 .sec2 .portion1, #banner5 .sec2 .portion2 {
          height: 175px; 
        }
        #foldsContainerForCMS h2 {
          font-size:20px !important;
          font-weight: bold;
          margin-bottom: 0px;
        }
        #foldsContainerForCMS h4 {
          font-size:14px !important;
          margin-top: 5px !important;
          margin-bottom: 5px !important;
        }
      }
      @media all and (max-width: 380px) {
        #foldsContainerForCMS h1 {
          font-size: 32px !important;
          margin-bottom: 10px;  
        }
        #foldsContainerForCMS h2 {
          font-size:18px !important;
          font-weight: bold;
        }
        #foldsContainerForCMS h3 {
          font-size:16px; 
        }
        #foldsContainerForCMS h4 {
          font-size:14px !important;
          margin-top: 10px;
          margin-bottom: 10px;
        }

        #banner5 .sec1,  #banner5 .sec2 {
          width: 100%
        }

        #banner5 {
          height: 690px;
        }
        #banner5, #banner5 .sec1 {
          position: relative;
        }
        #banner5 .sec1 div {
          width: 100%;
          padding: 10px;
        }
        #banner5 .sec2 .portion2 {
          background-position: center;
          background-size: 500px;
        }
        #banner5 .sec2 {
          position: absolute;
          top: 230px;
        }
        #banner5 .sec2 .portion1 div, #banner5 .sec2 .portion2 div {
          width: 100%
        }
        #banner5 .sec1, #banner5 .sec2 .portion1, #banner5 .sec2 .portion2 {
          height: 230px;
        }
        #banner5 .sec2 .portion2 div, #banner5 .sec2 .portion1 div {
          font-size: 40px;
          padding: 10px;

        }
        #banner5 .sec2 {
          left: 0px;
        }

      }
      @media all and (max-width: 320px) {

        #banner5 .sec1 div {
          padding: 10px;
          font-size: 16px;
        }
        #banner5 .sec2 .portion2 {
          background-position: center;
        }
        #banner5 .sec2 {
          left: 0px;
        }
        #foldsContainerForCMS h2 {
          font-size:20px !important;
          font-weight: bold;
        }
        #foldsContainerForCMS h4 {
          font-size:14px !important;
          margin-top: 0px !important;
          margin-bottom: 0px !important;
        }
      }

  .alink{
      text-decoration: none;
    font-size: inherit;
    font-weight: inherit;
  
  }
  
  #banner5 .sec1 div:hover{
    opacity:0.8;
  }
  
  #banner5 .sec2 .portion1 div:hover{
    opacity:0.8;
  }
  #banner5 .sec2 .portion2 div:hover{
    opacity:0.8;
  }
</style>
<div id="banner5">
    <div class="sec1" onclick="window.location.href='http://shop.globe.com.ph/broadband'" style="cursor:pointer" >
<div>
    <h2><a class="alink" href="http://bw-shop.globe.com.ph/broadband" style="color:inherit">${properties.pagetitle}</a></h2>

    <h4>${properties.desc}</h4>
</div>
</div>

<div class="sec2">
<div class="portion1" onclick="window.location.href='http://shop.globe.com.ph/shop-by-plan/mylifestyle-plans'" style="cursor:pointer">
<div>
    <h2><a class="alink" href="http://shop.globe.com.ph/shop-by-plan/mylifestyle-plans" style="color:inherit">${properties.style}</a></h2>

    <h4>${properties.styledesc}</h4>
</div>
</div>

<div class="portion2" onclick="window.location.href='http://shop.globe.com.ph/shop-by-plan/mystarter-plan'" style="cursor:pointer">
<div>
    <h2><a class="alink" href="http://shop.globe.com.ph/shop-by-plan/mystarter-plan" style="color:inherit">${properties.starter}</a></h2>

    <h4>${properties.starterdesc}</h4>
</div>
</div>
</div>
</div>
</div>
</div>
 </body>
</html>
