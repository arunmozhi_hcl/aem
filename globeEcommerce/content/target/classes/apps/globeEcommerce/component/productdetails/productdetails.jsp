
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html dir="ltr" lang="en" data-placeholder-focus="false">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="cache-control" content="public">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<meta name="robots" content="index,follow" />
<script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o||e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(t,e,n){function r(t){try{s.console&&console.log(t)}catch(e){}}var o,i=t("ee"),a=t(14),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,-1!==o.indexOf("dev")&&(s.dev=!0),-1!==o.indexOf("nr_dev")&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,e,n){r(n.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,e){return t}).join(", ")))},{}],2:[function(t,e,n){function r(t,e,n,r,o){try{d?d-=1:i("err",[o||new UncaughtException(t,e,n)])}catch(s){try{i("ierr",[s,(new Date).getTime(),!0])}catch(c){}}return"function"==typeof f?f.apply(this,a(arguments)):!1}function UncaughtException(t,e,n){this.message=t||"Uncaught error with no additional information",this.sourceURL=e,this.line=n}function o(t){i("err",[t,(new Date).getTime()])}var i=t("handle"),a=t(15),s=t("ee"),c=t("loader"),f=window.onerror,u=!1,d=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(l){"stack"in l&&(t(8),t(7),"addEventListener"in window&&t(5),c.xhrWrappable&&t(9),u=!0)}s.on("fn-start",function(t,e,n){u&&(d+=1)}),s.on("fn-err",function(t,e,n){u&&(this.thrown=!0,o(n))}),s.on("fn-end",function(){u&&!this.thrown&&d>0&&(d-=1)}),s.on("internal-error",function(t){i("ierr",[t,(new Date).getTime(),!0])})},{}],3:[function(t,e,n){t("loader").features.ins=!0},{}],4:[function(t,e,n){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(8),s=t(7);t("loader").features.stn=!0,t(6);var c=NREUM.o.EV;o.on("fn-start",function(t,e){var n=t[0];n instanceof c&&(this.bstStart=Date.now())}),o.on("fn-end",function(t,e){var n=t[0];n instanceof c&&i("bst",[n,e,this.bstStart,Date.now()])}),a.on("fn-start",function(t,e,n){this.bstStart=Date.now(),this.bstType=n}),a.on("fn-end",function(t,e){i("bstTimer",[e,this.bstStart,Date.now(),this.bstType])}),s.on("fn-start",function(){this.bstStart=Date.now()}),s.on("fn-end",function(t,e){i("bstTimer",[e,this.bstStart,Date.now(),"requestAnimationFrame"])}),o.on("pushState-start",function(t){this.time=Date.now(),this.startPath=location.pathname+location.hash}),o.on("pushState-end",function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),"addEventListener"in window.performance&&(window.performance.clearResourceTimings?window.performance.addEventListener("resourcetimingbufferfull",function(t){i("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.clearResourceTimings()},!1):window.performance.addEventListener("webkitresourcetimingbufferfull",function(t){i("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.webkitClearResourceTimings()},!1)),document.addEventListener("scroll",r,!1),document.addEventListener("keypress",r,!1),document.addEventListener("click",r,!1)}},{}],5:[function(t,e,n){function r(t){for(var e=t;e&&!e.hasOwnProperty(u);)e=Object.getPrototypeOf(e);e&&o(e)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,e){return t[1]}var a=t("ee").get("events"),s=t(16)(a),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";e.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,e){if(t[1]){var n=t[1];if("function"==typeof n){var r=c(n,"nr@wrapped",function(){return s(n,"fn-",null,n.name||"anonymous")});this.wrapped=t[1]=r}else"function"==typeof n.handleEvent&&s.inPlace(n,["handleEvent"],"fn-")}}),a.on(d+"-start",function(t){var e=this.wrapped;e&&(t[1]=e)})},{}],6:[function(t,e,n){var r=t("ee").get("history"),o=t(16)(r);e.exports=r,o.inPlace(window.history,["pushState","replaceState"],"-")},{}],7:[function(t,e,n){var r=t("ee").get("raf"),o=t(16)(r);e.exports=r,o.inPlace(window,["requestAnimationFrame","mozRequestAnimationFrame","webkitRequestAnimationFrame","msRequestAnimationFrame"],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],8:[function(t,e,n){function r(t,e,n){t[0]=a(t[0],"fn-",null,n)}function o(t,e,n){this.method=n,this.timerDuration="number"==typeof t[1]?t[1]:0,t[0]=a(t[0],"fn-",this,n)}var i=t("ee").get("timer"),a=t(16)(i);e.exports=i,a.inPlace(window,["setTimeout","setImmediate"],"setTimer-"),a.inPlace(window,["setInterval"],"setInterval-"),a.inPlace(window,["clearTimeout","clearImmediate"],"clearTimeout-"),i.on("setInterval-start",r),i.on("setTimer-start",o)},{}],9:[function(t,e,n){function r(t,e){d.inPlace(e,["onreadystatechange"],"fn-",s)}function o(){var t=this,e=u.context(t);t.readyState>3&&!e.resolved&&(e.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,v,"fn-",s)}function i(t){w.push(t),h&&(g=-g,b.data=g)}function a(){for(var t=0;t<w.length;t++)r([],w[t]);w.length&&(w=[])}function s(t,e){return e}function c(t,e){for(var n in t)e[n]=t[n];return e}t(5);var f=t("ee"),u=f.get("xhr"),d=t(16)(u),l=NREUM.o,p=l.XHR,h=l.MO,m="readystatechange",v=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],w=[];e.exports=u;var y=window.XMLHttpRequest=function(t){var e=new p(t);try{u.emit("new-xhr",[e],e),e.addEventListener(m,o,!1)}catch(n){try{u.emit("internal-error",[n])}catch(r){}}return e};if(c(p,y),y.prototype=p.prototype,d.inPlace(y.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,e){r(t,e),i(e)}),u.on("open-xhr-start",r),h){var g=1,b=document.createTextNode(g);new h(a).observe(b,{characterData:!0})}else f.on("fn-end",function(t){t[0]&&t[0].type===m||a()})},{}],10:[function(t,e,n){function r(t){var e=this.params,n=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;l>r;r++)t.removeEventListener(d[r],this.listener,!1);if(!e.aborted){if(n.duration=(new Date).getTime()-this.startTime,4===t.readyState){e.status=t.status;var i=o(t,this.lastSize);if(i&&(n.rxSize=i),this.sameOrigin){var a=t.getResponseHeader("X-NewRelic-App-Data");a&&(e.cat=a.split(", ").pop())}}else e.status=0;n.cbTime=this.cbTime,u.emit("xhr-done",[t],t),c("xhr",[e,n,this.startTime])}}}function o(t,e){var n=t.responseType;if("json"===n&&null!==e)return e;var r="arraybuffer"===n||"blob"===n||"json"===n?t.response:t.responseText;return i(r)}function i(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(e){return}}}function a(t,e){var n=f(e),r=t.params;r.host=n.hostname+":"+n.port,r.pathname=n.pathname,t.sameOrigin=n.sameOrigin}var s=t("loader");if(s.xhrWrappable){var c=t("handle"),f=t(11),u=t("ee"),d=["load","error","abort","timeout"],l=d.length,p=t("id"),h=t(13),m=window.XMLHttpRequest;s.features.xhr=!0,t(9),u.on("new-xhr",function(t){var e=this;e.totalCbs=0,e.called=0,e.cbTime=0,e.end=r,e.ended=!1,e.xhrGuids={},e.lastSize=null,h&&(h>34||10>h)||window.opera||t.addEventListener("progress",function(t){e.lastSize=t.loaded},!1)}),u.on("open-xhr-start",function(t){this.params={method:t[0]},a(this,t[1]),this.metrics={}}),u.on("open-xhr-end",function(t,e){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&e.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),u.on("send-xhr-start",function(t,e){var n=this.metrics,r=t[0],o=this;if(n&&r){var a=i(r);a&&(n.txSize=a)}this.startTime=(new Date).getTime(),this.listener=function(t){try{"abort"===t.type&&(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof e.onload))&&o.end(e)}catch(n){try{u.emit("internal-error",[n])}catch(r){}}};for(var s=0;l>s;s++)e.addEventListener(d[s],this.listener,!1)}),u.on("xhr-cb-time",function(t,e,n){this.cbTime+=t,e?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof n.onload||this.end(n)}),u.on("xhr-load-added",function(t,e){var n=""+p(t)+!!e;this.xhrGuids&&!this.xhrGuids[n]&&(this.xhrGuids[n]=!0,this.totalCbs+=1)}),u.on("xhr-load-removed",function(t,e){var n=""+p(t)+!!e;this.xhrGuids&&this.xhrGuids[n]&&(delete this.xhrGuids[n],this.totalCbs-=1)}),u.on("addEventListener-end",function(t,e){e instanceof m&&"load"===t[0]&&u.emit("xhr-load-added",[t[1],t[2]],e)}),u.on("removeEventListener-end",function(t,e){e instanceof m&&"load"===t[0]&&u.emit("xhr-load-removed",[t[1],t[2]],e)}),u.on("fn-start",function(t,e,n){e instanceof m&&("onload"===n&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=(new Date).getTime()))}),u.on("fn-end",function(t,e){this.xhrCbStart&&u.emit("xhr-cb-time",[(new Date).getTime()-this.xhrCbStart,this.onload,e],e)})}},{}],11:[function(t,e,n){e.exports=function(t){var e=document.createElement("a"),n=window.location,r={};e.href=t,r.port=e.port;var o=e.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=e.hostname||n.hostname,r.pathname=e.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!e.protocol||":"===e.protocol||e.protocol===n.protocol,a=e.hostname===document.domain&&e.port===n.port;return r.sameOrigin=i&&(!e.hostname||a),r}},{}],12:[function(t,e,n){function r(t,e){return function(){o(t,[(new Date).getTime()].concat(a(arguments)),null,e)}}var o=t("handle"),i=t(14),a=t(15);"undefined"==typeof window.newrelic&&(newrelic=NREUM);var s=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit"],c=["addPageAction"],f="api-";i(s,function(t,e){newrelic[e]=r(f+e,"api")}),i(c,function(t,e){newrelic[e]=r(f+e)}),e.exports=newrelic,newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),o("err",[t,(new Date).getTime()])}},{}],13:[function(t,e,n){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),e.exports=r},{}],14:[function(t,e,n){function r(t,e){var n=[],r="",i=0;for(r in t)o.call(t,r)&&(n[i]=e(r,t[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],15:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(0>o?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=r},{}],16:[function(t,e,n){function r(t){return!(t&&"function"==typeof t&&t.apply&&!t[a])}var o=t("ee"),i=t(15),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;e.exports=function(t){function e(t,e,n,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof n?n(r,a):n||{}}catch(u){d([u,"",[r,a,o],s])}f(e+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(l){throw f(e+"err",[r,a,l],s),l}finally{f(e+"end",[r,a,c],s)}}return r(t)?t:(e||(e=""),nrWrapper[a]=t,u(t,nrWrapper),nrWrapper)}function n(t,n,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<n.length;c++)s=n[c],a=t[s],r(a)||(t[s]=e(a,f?s+o:o,i,s))}function f(e,n,r){if(!c){c=!0;try{t.emit(e,n,r)}catch(o){d([o,e,n,r])}c=!1}}function u(t,e){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(t);return n.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(r){d([r])}for(var o in t)s.call(t,o)&&(e[o]=t[o]);return e}function d(e){try{t.emit("internal-error",e)}catch(n){}}return t||(t=o),e.inPlace=n,e.flag=a,e}},{}],ee:[function(t,e,n){function r(){}function o(t){function e(t){return t&&t instanceof r?t:t?s(t,a,i):i()}function n(n,r,o){t&&t(n,r,o);for(var i=e(o),a=l(n),s=a.length,c=0;s>c;c++)a[c].apply(i,r);var u=f[v[n]];return u&&u.push([w,n,r,i]),i}function d(t,e){m[t]=l(t).concat(e)}function l(t){return m[t]||[]}function p(t){return u[t]=u[t]||o(n)}function h(t,e){c(t,function(t,n){e=e||"feature",v[n]=e,e in f||(f[e]=[])})}var m={},v={},w={on:d,emit:n,get:p,listeners:l,context:e,buffer:h};return w}function i(){return new r}var a="nr@context",s=t("gos"),c=t(14),f={},u={},d=e.exports=o();d.backlog=f},{}],gos:[function(t,e,n){function r(t,e,n){if(o.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[e]=r,r}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){o.buffer([t],r),o.emit(t,e,n)}var o=t("ee").get("handle");e.exports=r,r.ee=o},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!m++){var t=h.info=NREUM.info,e=u.getElementsByTagName("script")[0];if(t&&t.licenseKey&&t.applicationID&&e){c(l,function(e,n){t[e]||(t[e]=n)});var n="https"===d.split(":")[0]||t.sslForHttp;h.proto=n?"https://":"http://",s("mark",["onload",a()],null,"api");var r=u.createElement("script");r.src=h.proto+t.agent,e.parentNode.insertBefore(r,e)}}}function o(){"complete"===u.readyState&&i()}function i(){s("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var s=t("handle"),c=t(14),f=window,u=f.document;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:f.XMLHttpRequest,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},t(12);var d=""+location,l={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"D:\AEM\Globe\cart\js\newrelic-min.js"},p=window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent),h=e.exports={offset:a(),origin:d,features:{},xhrWrappable:p};u.addEventListener?(u.addEventListener("DOMContentLoaded",i,!1),f.addEventListener("load",r,!1)):(u.attachEvent("onreadystatechange",o),f.attachEvent("onload",r)),s("mark",["firstbyte",a()],null,"api");var m=0},{}]},{},["loader",2,10,4,3]);;NREUM.info={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",licenseKey:"be00a683c0",applicationID:"9280697",sa:1,agent:"D:\AEM\Globe\cart\js\newrelic-min.js"}
</script><link rel="canonical" href="http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox" />



<title>Apple iPhone 6 Whitebox | Globe Online Shop</title>

<meta name="description" content="Get iPhone 6 Whitebox at Plan 1799 with P500 cash-out, exclusively here at Globe Online Shop!" />
<link href="http://d11fuji4mn7bm2.cloudfront.net/media/data/Favicon/favicon.png" rel="icon" />

<!--<cq:includeClientLib categories="productdetails"/>-->

    <link rel="stylesheet" type="text/css" href="/apps/shoppingcart/clientlib/css/colorbox.css" media="screen">
 	<link rel="stylesheet" type="text/css" href="/apps/shoppingcart/clientlib/css/build.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/apps/shoppingcart/clientlib/css/jquery.min.css" media="screen">

<script type="text/javascript">
  function lumiaFix() {
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) { 
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }

    if (navigator.userAgent.match(/IEMobile\/9\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }
  }

  lumiaFix();
</script>
</head>
<body>
<style>.loading {background: #FFFFFF url('/catalog/view/theme/default/image/loading.gif') 10px center no-repeat;border: 1px solid #B8E2FB;	padding: 10px 10px 10px 33px;margin-bottom: 15px;color: #555555;}</style>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM7FZN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//D:\AEM\Globe\cart\js\gtmPM.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PM7FZN');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSV552"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//D:\AEM\Globe\cart\js\gtmPM.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSV552');</script>
<!-- End Google Tag Manager -->

<script>
   var a = "//assets.adobedtm.com/09e448d55636ef986d22fd41e1f6c9f074c8d4fe/satelliteLib-a4886562bfd213316699cddca9dc81d34294e958"; //script location
   var domainList = ["www.globe.com.ph", "shop.globe.com.ph"]; //List of production domains

   //append “-staging” if hostname is not found in domainList
   if (domainList.indexOf(location.hostname) < 0) {
     a += "-staging"
   }

   document.write(unescape('%3Cscript type="text/javascript" src="'+a+'.js"%3E%3C/script%3E')); //write to page
</script>

<!-- BROADWAY HEADER -->
<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  <style type="text/css">.back2site a{
color:#fff;
}
.top-header-new{
background: #58595b;
padding: 3px 3% 3px 3%;
color: #fff;
}
span.call-text-top{
color: #fff;
}
span.call-text-top a{
color: #fff;
text-decoration: none;
}
span.m-left-right{
margin-left:10px;
margin-right: 10px;
}
.store-locator a{
	color:#fff;
}
.store-locator a:hover{
	color:#fff;
}

  .back2site a u, span.store-locator a{
      font-size:12px !important;
    
    }
</style>
<div class="container-fluid top-header-new">
<div class="pull-left back2site"><a href="http://www.globe.com.ph/" target="_blank"><u>&lt; Go to globe.com.ph</u></a></div>

<div class="pull-right"><span class="store-locator"><a href="https://www.globe.com.ph/store-locator" target="_blank"><u>Store Locator</u></a></span></div>
</div>
 
</div>

<div class="headerNavTop">
  <div id="new-header" class="">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 logo-wrapper header-blocks">
            <div class="dropdown">
              <div class="menu-list">
                <button class="btn dropdown-toggle" type="button" id="dropdown-button" data-toggle="dropdown">
                  <span class="icon-menu"></span>
                </button>
                <div class="dropdown-menu" role="menu" aria-labelledby="dropdown-button">
                  <div class="nav-list">
                    <ul>
                      <li id="1" role="presentation"><a id="header-about-globe" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
                      <li id="2" role="presentation"><a id="header-personal" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Personal</a></li>
                      <li id="3" role="presentation"><a id="header-sme" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://mybusiness.globe.com.ph/">SME</a></li>
                      <li id="4" role="presentation"><a id="header-enterprise" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://business.globe.com.ph/">Enterprise</a></li>
                      <li id="5" role="presentation"><a id="header-help-support" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Help & Support</a></li>
                      <li id="6" role="presentation"><a id="header-myaccount" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">My Account</a></li>
                    </ul>
                  </div>
                  <div class="nav-container">
                    <div class="nav-result1">
                    </div>
                    <div class="nav-result2">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-personal-shop" href="http://shop.globe.com.ph">
                                  <span class="icon icon-phones"></span>
                                  <p class="text-center">Shop</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-postpaid" href="http://www.globe.com.ph/postpaid">
                                  <span class="icon icon-postpaid"></span>
                                  <p class="text-center">Postpaid</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-prepaid" href="http://www.globe.com.ph/prepaid ">
                                  <span class="icon icon-prepaid"></span>
                                  <p class="text-center">Prepaid</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-internet" href="http://www.globe.com.ph/internet">
                                  <span class="icon icon-internet"></span>
                                  <p class="text-center">Internet</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-international" href="http://www.globe.com.ph/international">
                                  <span class="icon icon-international"></span>
                                  <p class="text-center">International</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-gocash" href="http://www.globe.com.ph/gcash">
                                  <span class="icon icon-gcash"></span>
                                  <p class="text-center">GCash</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-rewards" href="http://www.globe.com.ph/rewards">
                                  <span class="icon icon-rewards"></span>
                                  <p class="text-center">Rewards</p>
                              </a>
                          </li>
                           <li>
                              <a id="header-personal-entertainments" href="http://downloads.globe.com.ph/">
                                  <span class="icon icon-entertainment"></span>
                                  <p class="text-center">Entertainment</p>
                              </a>
                          </li>
                      </ul>
                    </div>
                    <div class=" nav-result3"></div>
                    <div class=" nav-result4"></div>
                    <div class=" nav-result5">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-help&support-faq" href="http://www.globe.com.ph/help">
                                  <span class="icon icon-faq"></span>
                                  <p class="text-center">FAQs</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-phoneconfig" href="http://www.globe.com.ph/help/guide">
                                  <span class="icon icon-phoneconfig"></span>
                                  <p class="text-center">Phone Configuration</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-troubleshoot" href="http://www.globe.com.ph/help/troubleshooting">
                                  <span class="icon icon-troubleshoot"></span>
                                  <p class="text-center">Basic Troubleshooting</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-community" href="http://community.globe.com.ph">
                                  <span class="icon icon-community"></span>
                                  <p class="text-center">Ask the Community</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-storelocator" href="http://www.globe.com.ph/store-locator">
                                  <span class="icon icon-locator"></span>
                                  <p class="text-center">Store Locator</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-contactus" href="http://www.globe.com.ph/contactus">
                                  <span class="icon icon-contactus"></span>
                                  <p class="text-center">Contact Us</p>
                              </a>
                          </li>
                                                    <li>
                              <a id="header-help&support-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">
                                  <span class="icon icon-livechat"></span>
                                  <p class="text-center">Chat</p>
                              </a>
                          </li>
                                                </ul>
                    </div>

                    <div class=" nav-result6">
                      <div class="container">
                        <div class="row">
                          <div class="col-xs-1 button-group-holder text-center">
                            <h2 class="h2-first">Enjoy more of Globe</h2>
                                                          <a id="header-myaccount-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox" id="login" class="btn btn-block">Login</a>
                              <hr>
                              <a id="header-myaccount-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox" id="signup" class="btn btn-block">Sign Up</a>
                                                        </div>
                          <div class="col-xs-6 want-info-holder">
                              <h2 class="h2-second"><span class="icon-myaccount"></span>I want to</h2>
                              <div class="row">
                                <div class="col-xs-5">
                                  <a id="header-myaccount-iwant1" href="http://www.globe.com.ph/enrolled-accounts"> <p>&#8226; View/edit my account details</p> </a>
                                  <a id="header-myaccount-iwant2" href="http://www.globe.com.ph/paybill"><p> &#8226; Pay my bill</p></a>
                                  <a id="header-myaccount-iwant3" href="http://www.globe.com.ph/paperlessreg"><p> &#8226; Sign up for paperless billing</p></a>
                                  <a id="header-myaccount-iwant4" href="http://www.globe.com.ph/autopayreg"><p> &#8226; Enroll in Auto Pay service</p></a>
                                  <a id="header-myaccount-iwant5" href="http://www.globe.com.ph/form-transfer-of-ownership" target="_blank"><p> &#8226; Transfer my account</p></a>
                                </div>
                                <div class="col-xs-7">
                                  <a id="header-myaccount-iwant6" href="https://mygcash.globe.com.ph/gcashonline"><p> &#8226; Access my GCash </p></a>
                                  <a id="header-myaccount-iwant7" class="modal-trigger" href="//www.globe.com.ph/form-check-activation-status"><p> &#8226; Check my line activation status</p></a>
                                  <a id="header-myaccount-iwant8" class="modal-trigger" href="//www.globe.com.ph/form-check-outstanding-balance"><p> &#8226; Check my postpaid outstanding balance</p></a>
                                  <a id="header-myaccount-iwant9" href="http://dbschenkerepod.com.ph/globe/customerview.php" target="_blank"><p> &#8226; Track the delivery status of my device/SIM</p></a>
                                  <a id="header-myaccount-iwant10" href="http://www.globe.com.ph/updateinfo" target="_blank"><p> &#8226; Update my account information </p></a>
                                </div>
                              </div>
                          </div>
                          <div class="col-xs-5 help-info-holder">
                            <h2 class="h2-third"><span class="icon-question"></span>Get help with</h2>
                              <a id="header-myaccount-gethelp1" href="http://www.globe.com.ph/help/guide"><p> &#8226; Configuring my phone</p></a>
                              <a id="header-myaccount-gethelp2" href="http://www.globe.com.ph/help/troubleshooting"><p> &#8226; Troubleshooting my device</p></a>
                              <a id="header-myaccount-gethelp3" href="http://www.globe.com.ph/help/roaming/about-international-roaming"><p> &#8226; Activating my international roaming</p></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <script>
                      if($('#new-header .nav-container .nav-result6').is('.f1731'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"min-height": "initial !important"});
                      }
                      if($('#new-header .nav-container .nav-result6').is('.f1731x'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"height": "172px"});
                      }
                    </script>

                  </div>
                </div>
              </div>
            </div>
                            <div id="logo-holder">
                <a href="http://shop.globe.com.ph/">
                  <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
                </a>
              </div>
                        </div>
          
          <div class="col-lg-3 category-dropdown header-blocks">
              <a id="header-shop-by-category" class="btn fontsize24 btn-lg absolute-center dropdown-toggle " data-toggle="dropdown">
                Shop by Category <span class="glyphicon caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                              <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                              <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                            </ul>
          </div>

          <div class="col-lg-4 search-wrapper header-blocks ">
            <div class="search-inner-addon ">
              <form>
                <fieldset>
                  <img class="searchIcon" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
                  <input id="searchbox" name="searchterms" type="search" class="form-control" placeholder="Search Here" maxlength="50" />
                </fieldset>
                <ul id="searchResult">
                   <li class="result-holder"></li>
                   <li class="plainText">We Recommend</li>
                   <li class="searchRecommend"></li>
                   <li class="see-all">See All</li>
                </ul>
            </form>
            </div>
          </div>

            <div class="search-account-wrapper">
              <div class="col-lg-1 cart-wrapper header-blocks" id="cart-header">
  <div class="btn-cart  cart-total-hide-empty">
    <a id="header-cart-btn" class="dropdown-toggle" data-toggle="dropdown" onclick="toggleCart()">
      <span class="badge custom-badge " id="cart-total">0</span>
      <img class="absolute-center cart-image" src="catalog/view/theme/broadwaytheme/images/icons/icon-cart.png">
    </a>
        <ul class="dropdown-menu" id="cart-content" role="menu">
          <li>
        <h4 style="text-align: center; margin-top: 0px;">Your shopping cart is empty!</h4>
      </li>
        </ul>

    <ul class="dropdown-menu" id="cart-notif" role="menu" style="display: none;">
      <li>
        <div class="cart-item-thumbnail">
          <img src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/check-50x50.png" alt="" title="" />
        </div>
        <div class="cart-item-detail cart-item-detail-success">
          <h4>Item successfully added to cart</h4>
        </div>
      </li>
      <li>
        <div class="cart-btn-view">
        <a href="http://shop.globe.com.ph/cart" class="btn btn-block btn-bw-orange">View Cart</a>
        </div>
      </li>
    </ul>
  </div>
</div>
              <div class="col-lg-1 account-wrapper ">
                  
                                      <a id="header-account-signin-btn" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox">
                      <img class="" src="catalog/view/theme/broadwaytheme/images/icons/icon-user2.png">
                      <span class="account-text">Sign In</span>
                    </a>
                                </div><!--End of account wrapper--> 
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="rwd-header-tablet" class="collapse hidden-xs-*">
  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
      <li class="dropdown">
        <a id="header-personal" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-shop" href="http://shop.globe.com.ph">Shop</a></li>
          <li><a id="header-postpaid" href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
          <li><a id="header-prepaid" href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
          <li><a id="header-internet" href="http://www.globe.com.ph/internet">Internet</a></li>
          <li><a id="header-international" href="http://www.globe.com.ph/international">International</a></li>
          <li><a id="header-gcash" href="http://www.globe.com.ph/gcash">GCash</a></li>
          <li><a id="header-rewards" href="http://www.globe.com.ph/rewards">Rewards</a></li>
          <li><a id="header-entertainment" href="http://downloads.globe.com.ph/">Entertainment</a></li>
        </ul>
      </li>
      <li><a id="header-sme" href="http://mybusiness.globe.com.ph/">SME</a></li>
      <li><a id="header-enterprise" href="http://business.globe.com.ph/">Enterprise</a></li>
      <li class="dropdown">
        <a id="header-help&support" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-faqs" href="http://www.globe.com.ph/help">FAQs</a></li>
          <li><a id="header-phoneconfig" href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
          <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
          <li><a id="header-community" href="http://community.globe.com.ph">Ask the Community</a></li>
          <li><a id="header-store-locator" href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
          <li><a id="header-contactus" href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                    <li><a id="header-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                  </ul>
      </li>
      <li class="dropdown">
        <a id="header-myaccount" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
                      <li><a id="header-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox">Login</a></li>
            <li><a id="header-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox">Sign Up</a></li>
                  </ul>
      </li>
    </ul>
  </div>
</div>

<nav id="rwd-header" class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button class="btn dropdown-toggle rwd-header-dropdown-button" type="button" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="icon-menu"></span>
      </button>
              <div class="logo-holder">
          <a href="http://shop.globe.com.ph/">
            <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
          </a>
        </div>
              <div class="user-holder ">
            <div class="user-holder-indicator"></div>
                      <a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox">
              <img class="" src="catalog/view/theme/broadwaytheme/images/icons/icon-user2.png" />
            </a>
                  </div>

        <div class="cart-holder"></div>
        <script type="text/javascript">
          function headerRwdChangeLocation() {
            var winWidth = $(window).width();
            var isFirefox = typeof InstallTrigger !== 'undefined';
            if(isFirefox) {
              if(winWidth <= 950) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 950) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }

            else {
              if(winWidth <= 949) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 949) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }
            
            if(winWidth > 991) {
              $("#rwd-search").css('display', 'none');
            }
          }
          $(document).ready(function($) {
            $(window).resize(function() {
              headerRwdChangeLocation();
            });
            headerRwdChangeLocation();
          });
        </script>
        <div class="search-holder">
          <a href="" class="rwd-search-trigger">
            <img class="searchIcon hidden-xs hidden-sm hidden-md" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
            <img class="searchIconRwd visible-md-*" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
          </a>
        </div>
    </div>
    <div id="rwd-navbar" class="navbar-collapse collapse">
      <button class="rwd-navbar-close" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="blue-close"></span>
      </button>
      <br />
      <ul class="nav navbar-nav">
        <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle open rwd-header-personal-trigger" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://shop.globe.com.ph">Shop</a></li>
            <li><a href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
            <li><a href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
            <li><a href="http://www.globe.com.ph/internet">Internet</a></li>
            <li><a href="http://www.globe.com.ph/international">International</a></li>
            <li><a href="http://www.globe.com.ph/gcash">GCash</a></li>
            <li><a href="http://www.globe.com.ph/rewards">Rewards</a></li>
            <li><a href="http://downloads.globe.com.ph/">Entertainment</a></li>
          </ul>
        </li>
        <li><a href="http://mybusiness.globe.com.ph/">SME</a></li>
        <li><a href="http://business.globe.com.ph/">Enterprise</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://www.globe.com.ph/help">FAQs</a></li>
            <li><a href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
            <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
            <li><a href="http://community.globe.com.ph">Ask the Community</a></li>
            <li><a href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
            <li><a href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                        <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                      </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
                          <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox">Login</a></li>
              <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox">Sign Up</a></li>
                      </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="rwd-search">
  <input id="rwd-searchbox"  type="search" class="form-control" placeholder="Search Here" maxlength="50" />
  <img id="rwd-search-trigger" src="catalog/view/theme/broadwaytheme/images/icons/icon-search.png" />
  <div class="rwd-search-results">
      <ul id="rwd-search-results-list">
         <li class="rwd-result-holder">
            <div class="text-result">test</div>
         </li>
         <li class="plain-text blue-line">WE RECOMMEND</li>
         <li class="img-result first-img-result"></li>
         <li class="plain-text rwd-search-see-all"><a>See All</a></li>
      </ul>
  </div> <!-- End of rwd-search-results-recommended -->
</div>

<nav id="rwd-sub-header" class="navbar navbar-default ">
  <div class="container rwd-sub-header-medium">
    <div class="navbar-header">
      <div class="menu-item">
        <a type="button" data-toggle="collapse" data-target="#rwd-sub-navbar" aria-expanded="false" aria-controls="rwd-sub-navbar">
          Shop by Category <span class="caret"></span>
        </a>
      </div>
      <a class="dot-menu" type="button" data-toggle="collapse" data-target="#rwd-sub-navbar-2" aria-expanded="false" aria-controls="rwd-sub-navbar-2">
        <img src="catalog/view/theme/broadwaytheme/images/icons/dot-menu.png">
      </a>
    </div>
    <div id="rwd-sub-navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
              </ul>
    </div>
    <div id="rwd-sub-navbar-2" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
            
                                                    
                          <li><a href="http://shop.globe.com.ph/contact">Call (02) 730-1010</a></li>
                                                    
                                    </ul>
    </div>
  </div>
  <div class="container rwd-sub-header-small">
    <div class="collapse navbar-collapse sub-header-sm" id="bs-example-navbar-collapse-7">
      <ul class="nav navbar-nav">
        <li role="presentation" class="dropdown offset">
            <a class="link" href="#" data-toggle="dropdown">
              Shop by Category <span class="glyphicon caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                          <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                          <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                        </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right offset-right">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
            
                                                    
                          <li><a href="http://shop.globe.com.ph/contact">Call (02) 730-1010</a></li>
                                                    
                                    </ul>
    </div>
  </div>
</nav>

<script type="text/javascript">

  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }
    
  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }

  //Input types
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });
  
  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);
    
    // slide up if no value is seen
    if($("#rwd-searchbox").val() == "") {
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
      //$('#rwd-searchbox').val('');
      },500)
  }); 


  //Empty input on blur of input 
  $('input[name=\'searchterms\']').on('blur', function(e){
      //$('.searchItems').html('');
      //#1417
      setTimeout(function(){
        // $('#searchResult').slideUp();
      },500);
      var ifIE = msieversion();
      
      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {
          //$('input[name=\'searchterms\']').val('');
        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
          // IE 10 or older => return version number
          //return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

          // IE 11 => return version number
          var rv = ua.indexOf('rv:');
          return true;
          //return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
         // IE 12 => return version number
         //return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  //General function for search - not used in input because of preventdefault in input
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
      /*$('#searchResult .ui-autocomplete .ui-menu-item').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
      });*/
    }
  });
  
  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }
    
  });
  //rwd of account-sigout-trigger above
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }
    
  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>
<div id="container" class="margTop">
<!-- BREADCRUMBS START -->
<div class="container-fluid breadcrumb-container headerNav" itemscope itemtype="http://schema.org/BreadcrumbList">
  <div class="container">
    <div class="row">
      <div class="breadcrumb pull-left">
                <div></div>
        <div id="breadcrumb-0" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/" itemprop="item">
            <span class="blue-link" itemprop="name">Shop Home</span>
            <meta itemprop="position" content="1" />
          </a>
        </div>
                <div> | </div>
        <div id="breadcrumb-1" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox" itemprop="item">
            <span class="black-link" itemprop="name">iPhone 6 Whitebox</span>
            <meta itemprop="position" content="2" />
          </a>
        </div>
              </div>
      <div class="chat-call-holder pull-right hidden-xs hidden-sm">
                                            <a data-toggle="modal" data-target="#chatModal" class="btn btn-bw-clear btn-chat">Chat</a>
            
                                                    
                          <a href="http://shop.globe.com.ph/contact" class="btn btn-bw-clear">Call (02) 730-1010</a>
                                                    
                                    </div>
    </div>
  </div>
</div>
<!-- BREADCRUMBS END -->

<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  </div>

<div itemscope itemtype="http://schema.org/Product">
  <meta itemprop="brand" content="">

<div id="notification"></div>

  <!-- Modal FULL SCREEN -->
<div class="responsive-modal container pdp-social-container">
  <div class="modal fade text-center" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a id="pdp-share-facebook-btn" target="_blank" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox"><span class="social-facebook"></span></a>
        <a id="pdp-share-twitter-btn" target="_blank" href="http://twitter.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox&text=Apple iPhone 6 Whitebox - Globe Online Shop"><span class="social-twitter"></span></a>
        <a id="pdp-share-googleplus-btn" target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox"><span class="social-gplus"></span></a>
        <a id="pdp-share-linkedin-btn" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox"><span class="social-linkedin"></span></a>
        <a id="pdp-share-pinterest-btn" id="" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','D:\AEM\Globe\cart\js\pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="social-pinterest"></span></a>
        <a id="pdp-share-close-btn" href="" data-dismiss="modal"><span class="big-close"></span></a>
      </div>
    </div>
  </div>
</div>

<div class="pdp-container hidden-xs">
  
  <section class="mainProducts pdp">
      <div class="container">
          <div class="row">
              <div class="col-xs-8 col-sm-7 col-md-7 col-lg-8">
                  <div class="col-xs-4 col-sm-6 col-md-6 col-lg-4 productImageCol">
                      <div class="pdp-image-container text-center">
                        <div class="pdp-images w100">
                                                      <img id="pdp_main_image" class="imageZoom" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Apple/iPhone 6/iPhone_6_Gold-300x400.png" data-zoom-image="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Apple/iPhone 6/iPhone_6_Gold-800x800.png" title="iPhone 6 Whitebox" alt="iPhone 6 Whitebox" />
                                                  </div>
                        <div class="rotationButton" id="pdp_other_image"></div>
                      </div>
                      <div class="cta-group">
                        <!-- <div class="add-to-wishlist"><a href="#"><span class="icon-bookmark"></span>Add to Wishlist</a></div> -->
                        <div class="share-this">
                            <a id="pdp-sharethis" href="#" data-toggle="modal" data-target="#myModal"><span class="icon-share"></span>Share This</a>
                        </div>
                      </div>

                      <!-- Modal FULL SCREEN -->
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-body">
                              <a target="_blank" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox"><span class="social-facebook"></span></a>
                              <a target="_blank" href="http://twitter.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox&text=iPhone 6 Whitebox"><span class="social-twitter"></span></a>
                              <a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox"><span class="social-gplus"></span></a>
                              <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fmobile%2Fapple-iphone-6-whitebox"><span class="social-linkedin"></span></a>
                              <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','D:\AEM\Globe\cart\js\pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="social-pinterest"></span></a>
                              <a href="" data-dismiss="modal"><span class="big-close"></span></a>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="col-xs-8 col-sm-6 col-md-6 col-lg-8 productInfo">
                      <div class="productContent">
                        <div class="product-info-badge-holder">
                                                  </div>
                        <h1 class="title-bw-light"><span itemprop="name">iPhone 6 Whitebox</span></h1>
                        <div class="pdp-description">
                          <span itemprop="description"><style type="text/css">.pdesc{
    font: 1em fs_elliot_proregular, Helvetica; }
</style>
<p class="pdesc"><span style="font-size: 1em;">Introducing the new Whitebox. Now with iPhone 6.</span><br />
<span style="font-size: 1em;">​Apple certified pre-owned with one-year warranty and new accessories, available exclusively here at Globe.</span></p>

<p class="pdesc">For P1,799 per month, enjoy 5GB mobile data, unlimited call &amp; text to Globe/TM, unlimited text to all networks, FREE lifestyle pack for 1 month, and FREE HOOQ or Spotify Premium for 3 months. Get your iPhone 6 Whitebox 16GB today!</p>

<h2 class="pdesc" style="font-size: 13px; line-height: 20.7999992370605px; color: rgb(0, 0, 139);">Online orders are for new and additional line applications only. You can also order this device by calling our Sales Hotline at (02) 730-1010, open Monday to Sunday from 6AM to 10PM. During the call, please enter the following promo code when prompted: 21211. For existing subscribers who wish to recontract, kindly call (02) 730-1300. This product is available at <a href="http://www.globe.com.ph/store-locator" target="_blank">Globe stores</a> so visit the one near you!</h2>
</span>
                        </div>
                      </div>

                                            <div class="productVariation">
                        <div class="options">

                                                                                                                                                                  
                              <div id="option-1485" class="option storage-variation-edit pull-left optionFields remWi" style="display:none;">

                                <div class="dropdown dropdown-1485">
                                  <button class="btn btn-default dropdown-toggle attrHver unbindHover" disabled type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><div><span class="drpdwn">
                                    Gold                                                                      </button>
                                  
                                                                  </div>

                                <select name="option[1485]" id="optionSelect-1485" data-id="1485" class="storage-size updateVal collapse">
                                                                      <option data-id="Gold" value="3165" selected="selected">Gold</option>
                                                                  </select>

                                
                                <input type="hidden" id="optionhidden_1485" name="optionhidden[1485]" value="3165" />
                                <input type="hidden" id="option-hidden-val-1485" value="3165" />
                                <input type="hidden" id="option-hidden-label-1485" value="Gold" />
                                <input type="hidden" id="option-hidden-val-orig-1485" value="3165" />
                                <input type="hidden" id="option-hidden-label-orig-1485" value="Gold" />
                                                                <input type="hidden" id="hero_offer_var" name="hero_offer_var" value="3171:3166:3165:3167" />
                                                              </div>
                                                                                  
                              <div id="option-1487" class="option storage-variation-edit pull-left optionFields remWi" style="display:none;">

                                <div class="dropdown dropdown-1487">
                                  <button class="btn btn-default dropdown-toggle attrHver unbindHover" disabled type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><div><span class="drpdwn">
                                    16 GB                                                                      </button>
                                  
                                                                  </div>

                                <select name="option[1487]" id="optionSelect-1487" data-id="1487" class="storage-size updateVal collapse">
                                                                      <option data-id="16 GB" value="3167" selected="selected">16 GB</option>
                                                                  </select>

                                
                                <input type="hidden" id="optionhidden_1487" name="optionhidden[1487]" value="3167" />
                                <input type="hidden" id="option-hidden-val-1487" value="3167" />
                                <input type="hidden" id="option-hidden-label-1487" value="16 GB" />
                                <input type="hidden" id="option-hidden-val-orig-1487" value="3167" />
                                <input type="hidden" id="option-hidden-label-orig-1487" value="16 GB" />
                                                                <input type="hidden" id="hero_offer_var" name="hero_offer_var" value="3171:3166:3165:3167" />
                                                              </div>
                                                                              </div>
                      </div>
                                            <!--
                      <div class="productReviews collapse">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <img src="img/star.png">
                        <p>5 out of 5 stars <a href="#" class="btn btn-inverse">(6 reviews)</a></p>
                      </div>
                      -->
                  </div>
              </div>
              <div class="col-xs-4 col-sm-5 col-md-5 col-lg-4 heroCol">
                  <div id="div-hero-offer">
                                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                      <div class="heroOffer text-center heroUpdateWd">
                          <div id="hero-loading"></div>

                          <div class="stock-level instock"><link itemprop="availability" href="http://schema.org/InStock" />IN STOCK</div>
                          <meta itemprop="priceCurrency" content="PHP">
                            <span style="display: none;">PHP </span>
                            <span itemprop="price" style="display: none;">1799.00</span> 

                                                      <div class="hero-offer-title"><span class="hero-offer-plan">GET THIS</span> @ MYLIFESTYLE PLAN 1799</div>
                            <div class="cashout">with PHP 500.00 cashout</div>
                          
                          <div class="add-cart-btn-orange-container" id="hero_offer">
                            <input type="hidden" name="product_id" value="1199" />
                            <input type="hidden" name="quantity" size="2" value="1" />

                            <div id="hero_offer_options">
                                                            <input type="hidden" name="option[1488]" value="3171" />
                                                            <input type="hidden" name="option[1486]" value="3166" />
                                                            <input type="hidden" name="option[1485]" value="3165" />
                                                            <input type="hidden" name="option[1487]" value="3167" />
                                                          </div>

                            <button id="hero-offer-addtocart-btn" class="btn btn-lg btn-addcart btn-bw-orange button-cart" data-id="hero_offer">ADD TO CART</button>
                          </div>
                          <div class="hero-offer-details"><ul>
	<li>499 Unli Call &amp; Text to Globe/TM</li>
	<li>GoSURF 999 (5GB Data Allowance)</li>
	<li>Unli Text Allnet 299</li>
	<li>FREE 3 months 1GB Spotify Premium or HOOQ</li>
	<li>FREE choice between Navigation Pack, Explore Pack, or Fitness Pack for 1 month</li>
	<li>FREE 1 month Gadget Care</li>
	<li>FREE 1GB Globe Cloud</li>
	<li>FREE Shipping</li>
</ul>
</div>
                      </div>
                    </div>
                    
                                                                <div class="cart-buttons">
                        <a id="hero-offer-choose-plan" class="btn btn-default btn-bw-white cnter" href="http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox#goToProductTableMatrix">Choose other plan</a>
                      </div>
                                                            </div>

                  <div id="div-prepaid" style="display: none;">
                    <div class="heroOffer text-center heroUpdateWd">
                        <div id="prepaid-loading"></div>

                        <div class="stock-level"></div>
                        <div class="hero-offer-title">Device Only <span id="prepaid-cashout"></span></div>
                        <div class="add-cart-btn-orange-container" id="prepaid_kit">
                          <input type="hidden" name="product_id" value="1199" />
                          <input type="hidden" name="quantity" value="1" />
                          <input type="hidden" name="mode" value="prepaid_kit" />

                          <div id="prepaid-hidden"></div>

                          <button id="hero-offer-addtocart-btn" class="btn btn-lg btn-addcart btn-bw-orange button-cart" data-id="prepaid_kit">ADD TO CART</button>
                        </div>
                        <div class="hero-offer-details" id="prepaid-details"></div>
                    </div>
                                                                <div class="cart-buttons" id="postpaid_kit">
                        <a class="btn btn-default btn-bw-white pull-left btn-postpaid" data-id="postpaid_kit">Get this w/ postpaid kit</a>
                      </div>
                      <div class="cart-buttons">
                        <a id="hero-offer-choose-plan" class="btn btn-default btn-bw-white pull-right" href="http://shop.globe.com.ph/products/mobile/apple-iphone-6-whitebox#goToProductTableMatrix">Choose other plan</a>
                      </div>
                                                            </div>

                  <div id="div-hidden-fields" style="display: none;">
                    <input type="hidden" name="product_id" value="1199" />
                  </div>
              </div>
          </div>
      </div>
  </section>
    <section class="sub-menu-tabs">
    <div class="container arrowPadd">
       <div class="navbar-collapse">
          <div id="tabs" class="htabs nav navbar-nav nav-center" align="center">
                          <div><a class="fontsize24 key-tab" href="#tab-0">Features</a></div>
                          <div><a class="fontsize24 key-tab" href="#tab-1">Specifications</a></div>
                          <div><a class="fontsize24 key-tab" href="#tab-2">myLifestyle Plan</a></div>
                      </div>
      </div>
    </div>
  </section>
    <section class="productTab">
            <div  id="tab-0" class="tab-content container">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <style type="text/css">/* === Full Width Product Details */
.pdp-container .productTab .container.tab-content {
  padding: 0;
  width: auto !important; }
  .pdp-container .productTab .container.tab-content [class^="col-"] {
    padding: 0; }

.rwd-pdp-container .productTab .container.tab-content [class^="col-"] {
  padding: 0; }

  .productTab [class^="overview__"] div  .link {
    color: #2a6496;
        font-size: 1.4em;
}
  .productTab [class^="overview__"] div  .link:hover {
    text-decoration: underline;
  }
  .productTab .btn-bw-blue:hover {
    background-color: #2a6496;    
}
  
.productTab [class^="overview__"] div h2 {
  font-size: 2.5em;
  margin: 0.67em 0px;
  padding: 0px 0px 5px;
  font-weight: lighter;
  font-family: fs_elliot_pro-light, Helvetica; }
.productTab [class^="overview__"] div p {
  font-size: 1.7em;
  line-height: 1.4;
  font-family: fs_elliot_pro-light; }
.productTab .overview__iphone--fold {
  margin: 0px;
  height: 518px;
  font-family: fs_elliot_proregular, Helvetica;
  font-size: 16px;
  line-height: 22.4px;
  padding-top: 2.5em;
  background-repeat: no-repeat;
  /* background-size: 100%; */
  background-size: contain; }
.productTab #second_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=0bdc83f0-f010-4f26-9704-39e61320c5cf&groupId=7122541&t=1420527440029);
  background-color: #f0f0f0;
  background-position: center; }
.productTab #third_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=85400a2f-52f9-4186-ab16-69193d64a233&groupId=7122541&t=1420527440021);
  background-position: 75% bottom;
  /* height: 500px; */ }
.productTab #second_fold .overview__text, .productTab .second_fold .overview__text {
  width: 800px;
  margin-left: auto;
  margin-right: auto; }
.productTab #third_fold .overview__text, .productTab .third_fold .overview__text {
  margin: 50px 0px 0px 150px;
  width: 600px;
  float: left; }
.productTab #sixth_fold, .productTab .sixth_fold {
  background-color: #f0f0f0;
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=02347c39-965d-4d16-9e47-b671d43be95e&groupId=7122541&t=1420527440018); }
  .productTab #sixth_fold .overview__text, .productTab .sixth_fold .overview__text {
    margin: 100px 100px 0;
    width: 550px;
    float: right; }
.productTab #top_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=78a56f2f-d3f0-46a4-a623-3209e694bf82&groupId=7122541&t=1420527440030);
  background-position: center top;
  background-size: auto; }
.productTab #fourth_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=9a648cc6-d40b-43c5-9735-eaf9cd9eb6d3&groupId=7122541&t=1420527440020);
  background-position: left center;
  padding-top: 0; }
.productTab #fifth_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=c12edbf3-df2d-46bd-9f07-716df1e3d081&groupId=7122541&t=1420527440019);
  background-size: 80%;
  background-position: right;
  /* height: 500px; */ }
.productTab #sixth_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=02347c39-965d-4d16-9e47-b671d43be95e&groupId=7122541&t=1420527440018);
  background-position: left center; }
.productTab #seventh_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=6b5f4eca-318f-4242-91d5-bcbf10bd90ff&groupId=7122541&t=1420527440016);
  background-position: 75% bottom;
  background-size: 62%; }
.productTab #eight_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=a0094056-691a-4ec8-9f8c-9a2ee91f2ea4&groupId=7122541&t=1420527440015);
  background-position: 20% center; }
.productTab #nineth_fold {
  background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=89d3ec42-d16f-4f4d-a452-5d7406203008&groupId=7122541&t=1420527439996);
  background-size: 80%;
  background-position: 80% bottom;
  height: 535px; }
.productTab #last_fold {
  background-color: #f0f0f0;
  height: auto;
  padding: 0; }
  .productTab #last_fold .compare_image {
    height: 260px;
    width: 250px;
    background-image: url(http://www.globe.com.ph/image/image_gallery?uuid=469052ee-67a4-49f1-a999-ca0ef1cbc31a&groupId=7122541&t=1420527439994);
    background-size: cover;
    background-position: -90px center;
    float: right; }
.productTab .overview__default--fold {
  overflow: auto;
  margin-bottom: 1em; }
.productTab .overview__default div h2 {
  font-size: 2em; }
.productTab .overview__default div p {
  font-size: 1.4em; }
.productTab .overview__icon {
  background: url(./features_icon_placeholder.png) no-repeat;
  background-size: 100%;
  width: 150px;
  height: 150px;
  float: left;
  margin-right: 2em; }

/* === Overview Tab */
.pad__sides, .productTab #last_fold .overview__text {
  padding-left: 30px;
  padding-right: 30px; }

/*==========  Mobile First Method  ==========*/
/* Large Devices, Wide Screens */
@media only screen and (max-width: 1200px) {
  .productTab #second_fold, .productTab .second_fold {
    background-color: #eee;
    background-position: center bottom; }
  .productTab #third_fold, .productTab .third_fold {
    /* height: 560px; */
    background-position: 100% bottom; }
    .productTab #third_fold .overview__text, .productTab .third_fold .overview__text {
      width: 50%;
      margin: 2em 0 0 70px; }
  .productTab #sixth_fold, .productTab .sixth_fold {
    background-position: -70px 0;
    background-size: 90%;
    background-color: #eee; }
    .productTab #sixth_fold .overview__text, .productTab .sixth_fold .overview__text {
      margin: 100px 0 0 0;
      width: 50%; }
  .productTab #fifth_fold {
    height: 400px;
    background-position: right bottom; }
  .productTab #seventh_fold {
    height: 400px;
    background-size: 58%; }
  .productTab #eight_fold {
    background-size: 80%; }
  .productTab #nineth_fold {
    height: 430px; } }

/* Medium Devices, Desktops */
@media only screen and (max-width: 992px) {
  .productTab #second_fold, .productTab .second_fold {
    background-color: #eee;
    background-size: 100%;
    background-position: center 4em;
    height: auto;
    padding-bottom: 1em;
    min-height: 500px; } }

/* Small Devices, Tablets */
@media only screen and (max-width: 768px) {
  .productTab #top_fold {
    /* background-size: 160%; */
    height: 510px; }
  .productTab .overview__iphone--fold {
    width: auto;
    padding-left: 2em;
    padding-right: 2em;
    padding-bottom: 1em; }
  .productTab [class^="overview__"] div h2 {
    font-size: 2em;
    margin-top: 0; }
  .productTab [class^="overview__"] div p {
    font-size: 1.2em; }
  .productTab #second_fold, .productTab .second_fold {
    height: 400px; }
    .productTab #second_fold .overview__text, .productTab .second_fold .overview__text {
      width: auto;
      margin: 0; }
  .productTab #third_fold, .productTab .third_fold {
    background-size: 100%;
    background-position: 60% 10%;
    height: 400px; }
    .productTab #third_fold .overview__text, .productTab .third_fold .overview__text {
      width: auto;
      margin-left: 30px; }
  .productTab #sixth_fold, .productTab .sixth_fold {
    background-color: #eee;
    background-size: 100%;
    background-position: center 4em;
    height: 400px;
    padding-bottom: 1em;
    overflow: auto; }
    .productTab #sixth_fold .overview__text, .productTab .sixth_fold .overview__text {
      margin: 50px 0 0 0;
      width: auto;
      float: right; }
  .productTab .overview__text {
    width: auto; }
  .productTab #fifth_fold {
    background-size: 100%; }
  .productTab #seventh_fold {
    background-size: 80%;
    background-position: bottom; }
  .productTab #eight_fold {
    background-position: left bottom; }
  .productTab #nineth_fold {
    height: 350px;
    background-size: 100%; } }

/* Extra Small Devices, Phones */
@media only screen and (max-width: 480px) {
  .productTab #top_fold {
    height: 280px;
    background-size: 150%; }
  .productTab #second_fold, .productTab .second_fold {
    min-height: 330px;
    background-position: center 85%; }
    .productTab #second_fold h2, .productTab .second_fold h2 {
      margin-bottom: 1em;
      text-align: center; }
  .productTab #third_fold, .productTab .third_fold {
    min-height: 465px; }
  .productTab #third_fold {
    background-position: -95px bottom; }
  .productTab #fourth_fold {
    min-height: 400px;
    background-position: left 310%;
    background-size: 170%;
    padding-top: 2em; }
  .productTab #fifth_fold {
    background-size: 145%; }
  .productTab #sixth_fold, .productTab .sixth_fold {
    background-color: #eee;
    background-size: 140%;
    min-height: 450px;
    height: auto; }
    .productTab #sixth_fold h2, .productTab .sixth_fold h2 {
      text-align: center; }
  .productTab #sixth_fold {
    background-position: left 120%; }
  .productTab #seventh_fold {
    background-size: 115%;
    background-position: 175% bottom; }
  .productTab #eight_fold {
    background-size: 140%; }
  .productTab #eight_fold {
    background-position: 60px bottom;
    background-size: 100%; }
  .productTab #nineth_fold {
    background-size: 130%;
    background-position: 120% bottom; }
  .productTab #second_fold .overview__text, .productTab #third_fold .overview__text, .productTab #sixth_fold .overview__text, .productTab .second_fold .overview__text, .productTab .third_fold .overview__text, .productTab .sixth_fold .overview__text {
    width: auto;
    margin: 0; }
  .productTab #last_fold .compare_image {
    margin: 0 auto;
    float: none; }
  .productTab #last_fold .overview__text {
    padding-bottom: 1em; } }

@media only screen and (max-width: 320px) {
  .productTab #third_fold {
    background-size: 130%;
    background-position: 140% bottom; }
  .productTab #fourth_fold {
    background-position: left 122%; }
  .productTab #fifth_fold {
    background-size: 160%; }
  .productTab #sixth_fold {
    background-position: left bottom; }
  .productTab #eight_fold {
    background-size: 140%;
    background-position: left bottom; }
  .productTab #nineth_fold {
    background-size: 160%;
    background-position: right bottom; } }

/*
Custom, iPhone Retina
@media only screen and (max-width: 320px) {
    .productTab {

        #second_fold,
        .second_fold {
            background-color: #eee;
            background-size: 200%;
            background-position: left -3em;
            height: auto;
            h2 {
                margin-bottom: 5em;
                text-align: center;
            }
        }
        #sixth_fold,
        .sixth_fold {
            background-size: 185%;
            background-position: left 5em;
        }
        #third_fold,
        .third_fold {
            min-height: 800px;
            background-size: 180%;
            background-position: 95% bottom;
        }
    }
}
 */
</style>
<div class="overview__iphone--fold" id="second_fold">
<div class="overview__text text-center">
<h2>All-new design</h2>

<p>A seamless, continuous aluminum and glass design means that while it's bigger, iPhone 6 feels just right.</p>
</div>
<!-- <div class="overview__text">

     </div> --></div>

<div class="overview__iphone--fold" id="third_fold">
<div class="overview__text">
<h2>Retina HD displays</h2>

<p>Even at 4.7 inches, they're the thinnest, most advanced Multi-Touch displays ever made for iPhone.</p>
</div>
</div>

<div class="overview__iphone--fold sixth_fold" id="fourth_fold">
<div class="overview__text">
<h2>A8 chip with 64-bit architecture</h2>

<p>The A8 chip is not only faster than the A7 chip, it's up to 50 percent more energy efficient too. <span style="font-size: 13px; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em;">1</span></p>
</div>
</div>

<div class="overview__iphone--fold third_fold" id="fifth_fold">
<div class="overview__text">
<h2>M8 motion coprocessor</h2>

<p>Allows you to track your speed and distance, and with a new barometer, even your elevation.</p>
</div>
</div>

<div class="overview__iphone--fold" id="sixth_fold">
<div class="overview__text">
<h2>New 8MP iSight camera with Focus Pixels</h2>

<p>Now with faster autofocus, 1080p HD video at 60 fps, and slo-mo video at 240 fps.</p>
</div>
</div>

<div class="overview__iphone--fold third_fold" id="seventh_fold">
<div class="overview__text">
<h2>Faster wireless performance</h2>

<p>Experience ultrafast wireless download speeds and faster Wi-Fi with 802.11ac. <span style="font-size: 13px; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em;">2</span></p>
</div>
</div>

<div class="overview__iphone--fold sixth_fold" id="eight_fold">
<div class="overview__text">
<h2>Touch ID</h2>

<p>Your fingerprint unlocks your iPhone 6 and lets you make App Store purchases.</p>
</div>
</div>

<div class="overview__iphone--fold third_fold" id="nineth_fold">
<div class="overview__text">
<h2>iOS 8</h2>

<p>The world's most advanced mobile OS is optimized for a larger display and filled with new features.</p>
</div>
</div>

<div class="overview__iphone--fold" id="last_fold">
<div class="col-sm-10 center-block" style="float:none;">
<div class="container">
<div class="col-sm-5">
<div class="compare_image">&nbsp;</div>
</div>

<div class="col-sm-7">
<div class="overview__text">
<h2>Which iPhone?</h2>

<p>Compare the features of the different iPhone models and see which one is right for you.</p>

<div><a class="link" href="http://www.globe.com.ph/help/postpaid/iphone6-whitebox" id="hero-offer-get-prepaid=btn" target="_blank">View FAQs</a></div>
</div>
</div>
</div>
</div>
</div>

<div class="container">
<div class="col-md-12">
<p align="center" style="margin: 1em 0px; padding: 0px; font-family: fs_elliot_proregular, Helvetica; font-size: 11px;"><span style="font-size: 9px; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em;">1</span>Compared with the previous generation.&nbsp; <span style="font-size: 9px; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em;">2</span>Data plan required. Check with your carrier for details. Speeds will vary based on site conditions. TM and © 2014 Apple Inc. All rights reserved.</p>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
              <div  id="tab-1" class="tab-content container">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <div class="pad__sides">
<style type="text/css">.pad__sides {
  padding-left: 30px;
  padding-right: 30px; }

.pdp-container .productTab .container.spec__table {
  padding: 0; }

.productTab .spec__head {
  background-repeat: no-repeat;
  background-position: 90% center;
  font-family: fs_elliot_proregular;
  text-transform: uppercase;
  margin: 0 0 10px;
  line-height: 40px; }
.productTab .spec__detail .row {
  padding-bottom: 1em;
  margin: 0; }
.productTab .spec__detail .col-md-9 {
  font-family: fs_elliot_pro-light; }
.productTab .spec__row {
  border-bottom: 1px solid #eee;
  padding: 3em 0; }
  .productTab .spec__row:last-child {
    border-bottom: 0; }
.productTab .icon-spec-body {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAArlBMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7vHgI57AAAAOXRSTlMAAAMFBwgJDxATGRobICIkKjBAQUhJTk9UWF9gZ3B9f4STlJWepKWmqKmztsDBwt3e4Ovs8vT4/P4LAEznAAABLklEQVR42p3V7VKDMBBA0a0R0IJGwW9RAYWqWKCliPv+L2bFJl1S6Kzef2TOTAYmWaCX8KO8bBCbKo980S1NNlEm4xpJdSwHoZu2aNSm7i4MVzjQKjSgNcORZhaFzhxHmztbaBnOkJaGat+v5NLXXb/p3RUM1coD9HpW6+EvnKr3/Tzsw1P97tMOpup5Af2OUJX+QNkSeHaukwS2cg1jJHCBug8CMZ6AWHLgUoCPHIg+RDwYQc6DOZQ8WELDgw0gDyIb8reueLBifx7+Bw94MABRc2AtABIKb+51FxQm5sHtuns6hnXmwYXMgFeIrwbMujvjGZfrFvHduFze4HU9eHw5Gbyu/AEAdoF7KmwypIo9zqFjzx4fe/ZfB6nOzXZHc+b9Z9jTRLD9fQT938c3i4nsDEIe8mMAAAAASUVORK5CYII=); }
.productTab .icon-spec-connectivity {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAABxVBMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7sUTJiiAAAAlnRSTlMAAAECAwQFBggJDQ8QERUWGBkaGxweISIjJCUmJykqKzA0NTY4OkBBREhLTU5PUFFSVVhZYWJkZmdoamxvcHFyeXp/gISHiImTlJaXmZqdnqCjpKWmp6ipqq6vsLKztLW2u7y9vr/AwcLFy8zNzs/Q09TV1tfY2tvd3t/h4uPk5efo6uvs7e7v8PLz9PX29/j5+vv8/f46Kb9JAAACRUlEQVR4AZ3V+VcSURQH8O9gjZAtCpahlbYolZVlZdiSLWRgSypULla0mGa7LUnZklKJuGRj37837zgzPGioTp9f5t3Dlzn3vfO4II8eiiRTWTKbSkZCOoRmgSIYTVORjgZdg/64wQJG3P97MJyhi0y4IOhNsIiEVw2WDbGoobJc0Cu54kmvE0zwjxJ2MMy/CC8H/ep+3/S2n2hpOXm+b0zdu98Mdtv1z8HDlXCsP5LrvFuCQYOmhUtBFKjptD4zghoQpel2NZas3tfe//DVy5G+c41lUm+5R1NUg56WxVwrAM+eGz/oWOjf7QE8x75LkdbRIM+PdQB2jXLZ7ByXPdkJaFsnZNmAiLxvI7DmmtSTF5s36YC+ufnyF6m7Vmla9SzJCJJSx1fUviX5tFmHo/TgCzmuGs8FSSQxRnHrK5k9WgLhq6z0Qaw8PkNOXqcYQ5aW0Rp5zYGr41K8624qBVD7mpYsaJlaC5S0fqBj/JAHWPfNLu2g9FlxX57zjwYGHs/L6m659GfBDG2970ne2es1+2ySL6W6aJtBioqJRjj2p6lIIalUcxsAT/3ZzitntgMIZpmTNA9c6XPbCE0P6nL9iQhCVA3O0zJ7k6qQXIpCnz6zUFoHYsz3fAdQ94z5YnJxF6marsCS8imqFuXiIk5VB0wdVMU1CQYyVJyG6RQVmYDLz7UHph4qwm4DwKjHknrDbQDAN6zupq2qqm2aOcM+ZUhJsohhZ0gJX/Gx5/uvQSoCbqM54D7sY/nDPqYO+3/++/gFnKFWmSriNEEAAAAASUVORK5CYII=); }
.productTab .icon-spec-display {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAAw1BMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7uF4AZVAAAAQHRSTlMAAAMFBwgJDxATFhkaGyAiJCotMD9AQUhJTk9YX2Bna3B9f4STlJWepKWmqKmztsDBwt3e4OHj5+vs8fL0+Pz+T6aKWQAAAUJJREFUeNqd1W1TgkAQwPG1CyiFIgt6pgegwIoErCSJ9vt/qsQ6WE7OOfu/g/nN3MCwC3Rijp9kJWKZJ77DoG7wF2V2UCCpCOxeaEYVClWRuQ69Bfa08ASoTVDSRKPQmKK0qdFCTXCC1BrIz/0Oz5ymi+fmdA49fucWOj3w+94vHPHn/drtwkMOF6MVjPj1DGhD2ENeVEO7IvDomHfyMW5hZS9hgATOsOkdWojBANhcAlMK5wwclMBXCtEBXw36kKjBBDI1mEGpBktANYjKUP3oXA3myq9H/YW7Hfgmgy6wgsJPCSwYQEjguLi55p1SGIof7hCWXd3vQ53w4UIsjMI54pMA49XMWMJwXSK+CMNl9Y7rzt3jgTCu2y4A0FPcUKqTJZVucAZde7p87elbL1KeGa+v5tj6z7KnMbf9fbjd38cPVFoIaKTXUx0AAAAASUVORK5CYII=); }
.productTab .icon-spec-model {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAABRFBMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7uEPAhwAAAAa3RSTlMAAAEDBAUGBwgJCwwPERITFxkaHB0eHyAhIiMkJTA8PkBCRkdLTU5PUFFTW19gZGVnam5wcnN0d3h5en5/goSFhoqSk56foKKjpKWmsrO2uLq9wMHDxcbH0dLa3N3e6uvs7fHy9PX4+vz9/v74DQwAAAHeSURBVHgBjdXfVxJBGMbxx8W1pRCL1FLSyFIDpUgzLYk0KkWSFIN+QAVChjz//306uzPMDFzs55z37nsOZ5bZd2FwU4VKs0t2m5VCysW1sQA0yWKLmlYxOTJMHPVo6ZUSw2GuwxE6OSv0yvT9/EtT2dPD2Bl9VWeJlrPYIPRkx0+4x6HSU2GZRmgryzBHqfEEXirwokEp54cJdd4fMWhi39XZ74iwRGkJmF/LCmvzwGNKpesw2aN0E6uXDFyu4halXvIqLFIBjqkcA1SKY3BbelilUtXDPy5SDBMyhUK4sIBKuLCCZriwiW64sAsa4SmVUyOkFe5R2bPCf0Y4kT+pCif5CfOnrcNgahKIx4HJKRjhL3wxwsWLi0U51uN5Z4S75K4c64E/MsLZb7XpmVptZrpWn7X+Qreth1hYUKOHLde6ZmlyeYVcWSafWtcMD/pa+Ibc3iF3tsnXgHlxcaCF8cPDuBwtPBLvzN3OIHQyacfJZBwnnXEGYSchQjxnIIIsub7uTxYR63UFPtA3hy1yY5Pc3CBfYs5eALjxlcI+ovm85/kTxf7QSkHML/tb41DGX/XNJSVEP1Jo16uBettae9Kz8xCLVLh9EGY1C/ff/w6x7IXIw7efG+cjPx//AdMUsJV4SyPSAAAAAElFTkSuQmCC); }
.productTab .icon-spec-multimedia {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAABKVBMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7uyTs2KAAAAYnRSTlMAAAMFBgcICQoMDg8XGRwhIiQlJiswPj9AQkRFTlBRXF9kZmdoaWpscXJ0gIKDhImMkpOVlp2foaSmp6mqrK2xsrS1tre4vsHD0tPU3N3e4OTn6Onq6+3u8PHy8/T2+fr8/vAovkcAAAG1SURBVHgBjdVpc9JAAMbxJ0bxQMRoa+OBWvFAsSjxaGg9LB7UI1SroRFTwef7fwg3odmkmw2zv1e8+M9kdmb3AcfYbm8YxGQcDHuujYR1BAWOF7Ig9Bxt2PDnVMz9RjlsT6kxbSthbcAKg1oxrI9YaVTPw5rstGVNhgMuNcjCNsuiKPrDzJ1F2NCcdxvCd3n2Rhr6TBxGRc8hfBY/Dpnwk9CZkzy4hgrXD0jOHRF6FDpormo1rQ4Fz4IdUmhhTIXf7XZ3OLZaFEIbLhfhHoWfz27fvP9mxsQqgBb3FiFdbBTCp2eRcEblcAPDPHyAIyd3SG6JT3+Q4RCBDLch1fe5IMMAsQwvI/dIDWMwC7+i4KIaMg9fo2hWCv+ahXF+mC9LPx3gYxbSQe6hGr5HT4ZbkM79UMMersqQ97LOfkc1dGFPZMgnZ9Ku+YlqGNpAPw+537914+6rGUuhZwGX/slrpjFOw/TiYjO9uCuu1orVyZ4CLkzJyRoqrE3k48I6hTjSiim0swF4YTgAOLVrOCmo7xqNlHD6pdnsCeu/jYZUOL9pNs1Cs//LYOxTJ648fvst0v59/Af+/GUYRBjv4QAAAABJRU5ErkJggg==); }
.productTab .icon-spec-price {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAABtlBMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7tdTEwxAAAAkXRSTlMAAAECAwQFBgcICQoMDQ8QFBUXGBobHB0fICIjJCUnLjAyNDU6PD0+QEFCQ0RGR0lLUFFUVVlaW19gYmNlZ2hpb3Bzd35/gIGCg4SGiYqOkJOUlZeZm5ydoKGkpaanqKuusrO0tba4ur2/wMHCw8THyMrMz9DR1dnc3d7f4OTl5ufp6uzu8PHy9PX29/j7/P3+iR/SugAAAlRJREFUeAGd1elfEmsYxvFrPOdwglKyoMLQaIeWdLJFSyxKKakEWsqE9mhJsn2RbEkyMBO4/uN47mFwRsY3fV//Pry4Pw/XwMYVSeQKZbJcyCXCLihaAyyCqSItiqmgY+jLVLhCJeNrDaMlOihFV4TuLFeRdVvDjimuaqpjOXTbul9f5pZspbsZZmlauNm/CcB/PUOPajRlzTDKht/nvWjqvkVT1Ah9JRredMOmd46Gkk/CSRoerMUKXR9pmFRhsELx9H9AH0+KC0M72lC35RtFJagBKYqvnQAm2DQ7pNI9VYqUBleRoh91N2gxDjR/puhCmOIVlCQ5PXhYP3Y6s0hyN4D1ZYowEhTHAV3X76uwLhZLk3wZj4dxnSKBnHHBdQANtXnl+xxZm303jP0UOcxQyQPwej1JMmMekaz+A8C9RGUGZSqXAbi83ivk40Dd1lDoLMl94QC091TKoBgFMM9WcWhPKEBxBkCfrt8mp8Ph7aFQ6EiNvBcItEN7SIEFKmMQcQkP6CeulkgeQp02TWUBBSoZM7RIQcLPVAqN83xoCT8NtkHZSJEzD94FZYQs3p2YuHZpeKfKlJPmwSMU56DEyGewe04RMR9Fsd053NV8FEhTJB3Df19TpNXDrVKpHTTCKViNUVTVw0WG4uc2Cd/CYoCGjKZCf4nix1509gR8WBarUpT89r9rZXQNLDbcYUO0ZQBmTrWjYXNivmUA4MnTtJgbOdrbN3DxRY2mvMcyUlI6y5sjJTyrz57nb4ZU+J2m2e889mn72Kcdx77l8xGxfz7+ANIm7sekuzXjAAAAAElFTkSuQmCC); }
.productTab .icon-spec-software {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAA81BMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7vIDxcMAAAAUHRSTlMAAAECAwUICQsMDQ8ZGhsgIiQrNDY3OD5AQUlKS1RYYGNnaHB0dnp7goSQk5SZmp2go6Slpqmwsba+wcLNz9DS09vd3uDn6+zy9PX2+Pr8/SA6H9kAAAF0SURBVHjaldVrU4JAFIDhYwlUJkVZQTcjuyF2EyrSrmIhmnj+/6/J3WRBF5jtnWGGmX0+wAIHmEvSLc8PEUPfswwJSKVZaabZAaYKbC0TVp0IF4qcKg/NAWY0MBeg7GJOrpyGlQ7m1qkkUCYuX8oMuliYG0MTaV9vXO9DJJl/cJ3e78/xEvCt3tJ7r1LYRtIF7Op82vIrWWwTqEUUbkEfkZxO6DGrCVd057UptJG2Af2xIV1jo3yKD8r2MIYNJNklkL4Z/ADYQQXKk32A53kYSKAjg9HB2j1eKg18quyN5iHqYCUQuRJogcfgUZ2vFkMPegxmFsMehAzePPKdxDAEFLtGFIehGAzBF4M+eGLQE99wQwwaIAUiMJAAWkjbzIHnSGolL+5hzpO5I4sTbQrBofBlBbKqjcmiUyJQHVD5eVbna47ox6Wyz7U4878DAJRukesqqSHVLXDxkKIp+WNP+e8gZalZo1kVGvYtbtizJCP39/EL8YpGTv8383EAAAAASUVORK5CYII=); }
.productTab .icon-spec-battery {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAAn1BMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7vDw8PQ0NDq6enz8fH49/f6+fn7+fn//f3Fl3HPAAAALHRSTlMAAAIEBQYKExQVHB43OFZXWFlaeHmHiJWXmJmssbO0ttLT4uPk6+zt7vj5+jjkTi8AAAE3SURBVHjajdXbboJAEIDhWVCxSKsWEOhqi6KgMJ55/2erxdqdLiDzXxHyJXtIGEDcgyrDcWWc7XZZLF3HAJKg0PZXSFr5diO0ghy18sCqw7cNNrQZa9AMsaXQpHCwwNYWAwXNOT5pbv7BCJ8WPeAYVftrWXXdo2p6hxY976H87YCq7KWC5MDH8+UBL+cjWfwHjgr1ovyXel+MbtDHboi+ACPhwMQABzkQHfA0SB9JLkgelLDkwSWkvD2mkPNgXoPqUYMpD6b8w0jeHiV4POjCK29pB4yEAxMDhM+BvgBh5917zO0bFEE3DO8fl7rzE3UnddtWBcUEO3pnDoAPeMD+85HSJ0Pqq919DujY67WuHvW0QTppHqTT+mgehvXRHA6bh/1sTdl6Roe9/vvwZLwtim0sPe338Q0IlN9i48nlOQAAAABJRU5ErkJggg==); }
.productTab .icon-spec-hardware {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAABXFBMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u8vLy9vb2+vr6/v7/AwMDBwcHCwcHCwsLEw8PExMTFxMTGxsbHx8fIx8fIyMjJyMjKycnLysrNzMzOzc3Pz8/R0NDR0dHS0dHS0tLU09PW1tbX1tbY19fZ2dna2dnb2trc29vd3Nzf3t7h4ODi4eHk4+Pl4+Pl5OTm5OTm5eXn5ubo5ubo5+fq6Ojq6enr6urs6urt6+vt7Ozu7e3v7e3w7u7x7+/x8PDy8PDy8fHz8vL08/P18/P29PT29fX39fX49vb59/f5+Pj6+Pj7+fn+/Pz//f0ZKDsvAAAALHRSTlMAAAIEBQYKExQVHB43OFZXWFlaeHmHiJWXmJmssbO0ttLT4uPk6+zt7vj5+jjkTi8AAAIGSURBVHgBjZXrVxJBGMZnQcWQUhNkabXwIujyYIREBpnd75lZmISCWaAWEMv+/+e0zDSzs8Olfp+eD79z5sy77z5DNBdCfKE5I57c2EjGjbmQT5ORxWBkGRLLkeBAMRA1oWBGA/3itTUMYG1aEf06hqD7ZXFiEUNZnHBF/wJGsOAXYgwjiXFxGpz0t1fAlw+4U3/MMmOWiQH3vhm7BFgNFOw9lhnJK1TU4RU7P1QRsZ44lYLLywfAs12k39ylmZOacsQIXNKvv1YE5UcQRDTiWwGj1rzY3rNlug+3L5o19FjxkRD+UnXES9vDR0esghIiYUg0vWIJgjliKOLJFk1bJ17RIEs87lfKOUdknmP2xFy5sg/KEklwsWV3dxwRnJ6407VboCSIKUaT2YQqYjOTBsVkIqdPFJju0XWrXVDFQtuq86PFZY4aZ3lVzJ81jvhl1PHwW2fV8agDP87SlDtWB36Vx8PTav6X7eEgXz095J9QLMW51Sl+8opPix3rnC+FZ80ypbYl+PnWs2ZaUJ7kk3vAbhF4kaWZYwYdUYtCcNv+DPz+jvv2e5o5Ovu5Ev/6uRIBKmoz4KQv3wG1A2Rbz1lmXP/PArihcXF8dKWMSyV1a7h3k5cUZWzo6bExpUhnBhfpbH81T+r91axPDi77+VVZW51Xy15+PsJGfD2VWo8bYeX5+APaMBuJfk7AsgAAAABJRU5ErkJggg==); }
.productTab .icon-spec-memory {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAA/1BMVEW7u7v///+7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u7u8vLy9vb2/v7/AwMDBwcHCwsLDw8PJycnLy8vQ0NDR0dHS0tLT09PU1NTW1tbX19fY2NjZ2dnd3d3i4uLj4+Pk5OTm5ubs7Ozw8PDx8fHy8vLz8/P09PT19fX29vb39/f4+Pj5+fn6+vr7+/v8/Pz9/f3+/v7///81IKvuAAAALHRSTlMAAAIEBQYKExQVHB43OFZXWFlaeHmHiJWXmJmssbO0ttLT4uPk6+zt7vj5+jjkTi8AAAGxSURBVHjajdXZUsIwFIDhFGQRq4BQKBYUkEXLQXGlbIooIDRQkbz/s1gJktDSyH/Tm28mk0x6giQaWuWTE1q+en1dzWsJ2Ye4JB5GlCJwFZXIThhK6+BIT4fc8KQMOypHHdCvgkeqn4fBHHiWCzLoz4KgrH8DMyAs8wej8E9xCkNsvw+Dz6lpmnjV9KMJtOrhCrIN307IdsZmcRse1TawR8xWZ9E33rHRsvpGn4yAVjuyoQKb3sgQwGpCzwbmMzQJhnWKhHyXPBzBjdWGlzHUsQFtMoN1lz4kA2tAhlCft34h4Ca0GAQZJbfg+O5h3m28fjbuZ51Gl4MJpG3BpfW1XFjfS8uyPwsOaqiwBR0xWECV/WAF6cw9zb2hzsFHi4ggW3pMBLDCbWYmggXueLAIaii5H0yg4/2gbF8KAcTsUnDXzBRARUJSRGfQa2k9YkMpDbSRG06AptKfa33mhht21qcdWkEpBrT2EM+48KgHtNM9B8AZ+oMB8UgJcEPqwtudB/mxd+C5eubAMUhjuwdp3D2aw6p7NKvh3cM+VeJZKeUe9uz5SGr5q1rtKq8lHc/HD45CETmNvSOpAAAAAElFTkSuQmCC); }

/* Medium Devices, Desktops */
@media only screen and (min-width: 992px) {
  .productTab .spec__head {
    background-position: left center;
    padding-left: 60px; } }
</style>
<div class="spec__table container">
<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-price">Price</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Recommended Plan</div>

<div class="col-md-9">myLifestyle Plan 1799</div>
</div>

<div class="row">
<div class="col-md-3">Cashout</div>

<div class="col-md-9">PhP 500 (16 GB)</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-body">Body</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Weight</div>

<div class="col-md-9">129 g (4.55 oz)</div>
</div>

<div class="row">
<div class="col-md-3">Dimensions</div>

<div class="col-md-9">138.1 x 67 x 6.9 mm (5.44 x 2.64 x 0.27 in)</div>
</div>

<div class="row">
<div class="col-md-3">Finish</div>

<div class="col-md-9">
<p>Gold</p>
</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-display">Display</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Type</div>

<div class="col-md-9">LED-backlit IPS LCD, capacitive touchscreen, 16M colors</div>
</div>

<div class="row">
<div class="col-md-3">Screen Size</div>

<div class="col-md-9">750 x 1334 pixels, 4.7 inches (~326 ppi pixel density)</div>
</div>

<div class="row">
<div class="col-md-3">Multitouch</div>

<div class="col-md-9">Yes</div>
</div>

<div class="row">
<div class="col-md-3">Protection</div>

<div class="col-md-9">Shatter Proof Glass, Oleophobic Coating</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-connectivity">Connectivity</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">SIM Card Type</div>

<div class="col-md-9">Nano-SIM</div>
</div>

<div class="row">
<div class="col-md-3">Network</div>

<div class="col-md-9">GSM: 850, 900, 1800, 1900MHz<br />
WCDMA: Bands: 1/2/4/5/8<br />
LTE: Bands: 1/3/4/7/17/38/40</div>
</div>

<div class="row">
<div class="col-md-3">EDGE</div>

<div class="col-md-9">Yes</div>
</div>

<div class="row">
<div class="col-md-3">WLAN</div>

<div class="col-md-9">Dual-band Wi-Fi (2.4G/5G) 802.11 b/g/n/ac</div>
</div>

<div class="row">
<div class="col-md-3">USB</div>

<div class="col-md-9">Lightning USB Connector</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-software">Software</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Operating System</div>

<div class="col-md-9">iOS 8</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-hardware">Hardware</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Processor</div>

<div class="col-md-9">Dual-core 1.4 GHz Cyclone (ARM v8-based)</div>
</div>

<div class="row">
<div class="col-md-3">Chipset</div>

<div class="col-md-9">AppleA8</div>
</div>

<div class="row">
<div class="col-md-3">GPU</div>

<div class="col-md-9">PowerVR GX6450 (quad-core graphics)</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-memory">Memory</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Internal</div>

<div class="col-md-9">ROM: 16 GB, RAM: 1 GB</div>
</div>

<div class="row">
<div class="col-md-3">External</div>

<div class="col-md-9">No</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-multimedia">Camera</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Primary</div>

<div class="col-md-9">8 MP, 3264 x 2448 pixels, phase detection autofocus, dual-LED (dual tone) flash</div>
</div>

<div class="row">
<div class="col-md-3">Video</div>

<div class="col-md-9">1080p @ 60 fps, 720p @ 240 fps</div>
</div>

<div class="row">
<div class="col-md-3">Secondary</div>

<div class="col-md-9">1.2 MP, 720p @ 30 fps, face detection, HDR, FaceTime</div>
</div>
</div>
</div>

<div class="spec__row row">
<h3 class="spec__head col-md-3 icon-spec-battery">Battery</h3>

<div class="spec__detail col-md-9">
<div class="row">
<div class="col-md-3">Type</div>

<div class="col-md-9">Non-removable Li-Po 1810 mAh battery (6.9 Wh)</div>
</div>

<div class="row">
<div class="col-md-3">Standby</div>

<div class="col-md-9">Up to 250 hours</div>
</div>

<div class="row">
<div class="col-md-3">Talk Time</div>

<div class="col-md-9">Up to 14h</div>
</div>

<div class="row">
<div class="col-md-3">Music Play</div>

<div class="col-md-9">Up to 50h</div>
</div>
</div>
</div>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
              <div  id="tab-2" class="tab-content container">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <div class="container overview__mylifestyleplan">
<style type="text/css">.pdp-container .productTab .tab-content.container {
  padding: 0;
  width: auto !important; }
  .pdp-container .productTab .tab-content.container [class^="col-"] {
    padding: 0; }
  .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
    padding: 60px 60px 40px 60px;
    width: auto !important; }
    @media (min-width: 768px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 750px !important; } }
    @media (min-width: 992px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 970px !important; } }
    @media (min-width: 1200px) {
      .pdp-container .productTab .tab-content.container .overview__mylifestyleplan {
        width: 1200px !important; } }

.rwd-pdp-container .productTab .container.tab-content [class^="col-"] {
  padding: 0; }

.productTab iframe {
  min-height: 253px;
  min-width: 80%;
  border: 0; }
</style>
<div class="col-sm-12">
<h2>Imagine a plan that's built for the way you live now. With the new myLifestyle Plan, everything that matters to you everyday comes easy.</h2>
</div>

<div class="col-sm-6">
<p>Your base plan comes with built-in unlimited calls and texts to Globe/TM, starting at P499.</p>

<p>+ Add&nbsp;<b>Surf Packs*</b>&nbsp;for your mobile internet needs.<br />
+ Choose among&nbsp;<b>Lifestyle Packs</b>&nbsp;that are perfect for you!<br />
+ Get extra consumables, calls and texts to other networks, to landline, and abroad with&nbsp;<b>Classic Packs.</b></p>

<h3>The more packs you add to your Plan, the better handset you get!</h3>
</div>

<div class="col-sm-6 text-center"><iframe frameborder="0" src="https://www.youtube.com/embed/DctvEAnal6k"></iframe></div>

<div class="col-sm-12">&nbsp;
<p><small>*Minimum GoSURF 99 required if availing of handset. To modify the Promo Packs that come with our preset plans, complete your order and an Online Sales agent will be in touch to help you build your perfect plan. To learn more about the Promo Packs, visit our <a href="http://www.globe.com.ph/mylifestyleplan-info" rel="nofollow" style="font-size:14px;" target="_blank" title="Click here to learn more about the different myLifestyle Plan Surf, Lifestyle and Classic Packs!">myLifestyle Plan</a> info page.</small></p>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
  </section>

    <!--googleoff: all-->
  <section class="productTableMatrix">
    <a name="goToProductTableMatrix"></a>
    <div class="container">
      <h4 class="text-center">Also available with other plans:</h4>
      <p><div id="plan_matrix" class="changeW"></div></p>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><br /></div>
      </div>
    </div>
  </section>
  <!--googleon: all-->
  
  <section class="seoContent">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <p>If you’ve held iPhone 6 before, you know that it isn’t only bigger, but better in every way. It has a large yet dramatically thinner body, more powerful but remarkably power-efficient chip, and a seamless glass and metal surface that features the most advanced Multi-Touch display. Now, you have a chance to own iPhone 6 and enjoy doing the things you love, at a much lower price. Exclusively here at Globe, welcome the new iPhone 6 Whitebox!</p>

<p>If you want to know what Whitebox means, here’s a quick fact-check:</p>

<p><strong>Apple Certified Pre-Owned Gadget</strong><br />
Some brand-new iPhone 6 units are returned due to technical issues. The Whitebox is a pre-owned and refurbished iPhone 6 that underwent Apple's stringent quality process and full burn-in testing:</p>

<ul>
	<li>Thoroughly cleaned and inspected</li>
	<li>Defective modules replaced with genuine Apple components</li>
	<li>Repackaged with new accessories (manuals, cables, headphones, box, etc.)&nbsp;</li>
	<li>Installed with original operating software</li>
	<li>Tagged with new part number and serial number&nbsp;</li>
	<li>Placed into a final QA inspection prior to reselling</li>
</ul>

<p>With this process, you are ensured that what you hold on your hand is like new, and that you’ll experience the same advanced features of iPhone 6.</p>

<p><strong>Full One (1)-Year Apple Warranty</strong><br />
With your new Whitebox, enjoy the same premium customer service provided by Apple. All Apple Certified Refurbished Products are covered by Apple's One (1)-Year Limited Warranty. You also have the option to extend you coverage for up to two (2) years by purchasing the AppleCare Protection Plan with your Whitebox.</p>

<p><strong>Doing the Things you Love</strong><br />
This latest product from iPhone Philippines if available exclusively with Globe myLifestyle Plan. Enjoy the following offers for P1,799 per month:</p>

<ul>
	<li>Surf, browse, and stream the net with 5GB mobile data</li>
	<li>Stay connected with unlimited call &amp; text to Globe/TM</li>
	<li>Get in touch with family and friends with unlimited text to all networks</li>
	<li>Be efficient at work or play with FREE choice between Navigation Pack, Explore Pack, or Fitness Pack for one (1) month&nbsp;</li>
	<li>Access music or movies with FREE HOOQ or Spotify Premium for three (3) months</li>
</ul>

<p>iPhone 6 Whitebox price starts at P599 per month with P15,600 one-time cash-out. Get your <a href="http://www.apple.com/shop/browse/home/specialdeals" rel="nofollow" style="color:black" target="_blank">iPhone 6 Whitebox</a> and apply for your new Globe iPhone plan today!</p>
        </div>
      </div>
    </div>
  </section>

  </div>

<div class="rwd-pdp-container visible-xs">

  <br /><!--RWD HELPER :) -->

  <section class="rwd-mainProducts container-fluid">
    <div class="rwd-product-info-badge-holder">
            </div>
    <div class="rwd-pdp-images"></div>

    <div class="rwd-productContent">
      <h1 class="title-bw-light">iPhone 6 Whitebox</h1>
      <div class="rwd-pdp-description">
        <style type="text/css">.pdesc{
    font: 1em fs_elliot_proregular, Helvetica; }
</style>
<p class="pdesc"><span style="font-size: 1em;">Introducing the new Whitebox. Now with iPhone 6.</span><br />
<span style="font-size: 1em;">​Apple certified pre-owned with one-year warranty and new accessories, available exclusively here at Globe.</span></p>

<p class="pdesc">For P1,799 per month, enjoy 5GB mobile data, unlimited call &amp; text to Globe/TM, unlimited text to all networks, FREE lifestyle pack for 1 month, and FREE HOOQ or Spotify Premium for 3 months. Get your iPhone 6 Whitebox 16GB today!</p>

<h2 class="pdesc" style="font-size: 13px; line-height: 20.7999992370605px; color: rgb(0, 0, 139);">Online orders are for new and additional line applications only. You can also order this device by calling our Sales Hotline at (02) 730-1010, open Monday to Sunday from 6AM to 10PM. During the call, please enter the following promo code when prompted: 21211. For existing subscribers who wish to recontract, kindly call (02) 730-1300. This product is available at <a href="http://www.globe.com.ph/store-locator" target="_blank">Globe stores</a> so visit the one near you!</h2>
      </div>
    </div>

    <!-- RWD Product Variation Container -->
    <div class="rwd-productVariation"></div>

    <div class="rwd-share-this">
        <a href="#" data-toggle="modal" data-target="#myModal"><span class="icon-share"></span>Share This</a>
    </div>

    <!-- RWD Hero Container -->
    <div class="rwd-heroCol"></div>
  </section>

  <section class="rwd-sub-menu-tabs">
  </section>

  <section class="rwd-prodcutTab">
  </section>

  <section class="rwd-productTableMatrix">
  </section>

  <section class="rwd-seoContent">
  </section>

  <section class="rwd-crossSell">
  </section>

  <!-- INPUT HIDDEN FOR DISABLING -->
  <input type="hidden" name="disable-add-cart" value="0" id="disable-add-cart">

  <script type="text/javascript">

    function pdpRwdChangeLocation() {
      var winWidth = $(window).width();

      if(winWidth <= 767) {

        $(".productVariation").appendTo('.rwd-productVariation');
        $("#div-hero-offer").appendTo('.rwd-heroCol');
        $("#div-prepaid").appendTo('.rwd-heroCol');
        $(".sub-menu-tabs").appendTo('.rwd-sub-menu-tabs');
        $(".productTab").appendTo('.rwd-prodcutTab');
        $(".productTableMatrix").appendTo('.rwd-productTableMatrix');
        $(".seoContent").appendTo('.rwd-seoContent');
        $(".crossSell").appendTo('.rwd-crossSell');
        setTimeout(function(){
              $("#tabs").unslick();
              var infinite = isIE() ? false : true;
              $('#tabs').slick({
                infinite: infinite,
                cssEase: 'linear',
                adaptiveHeight: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                onAfterChange: function() {
                  $("#tabs .slick-active a").trigger("click");
                }
              });
        },200);
      }
      else if(winWidth > 767 && !isIE()) {
        $(".productVariation").insertAfter('.productContent');
        $("#div-hero-offer").appendTo('.heroCol');
        $("#div-prepaid").appendTo('.heroCol');
        $(".sub-menu-tabs").insertAfter('.mainProducts');

        if($('section').hasClass('sub-menu-tabs')){
          $(".productTab").insertAfter('.sub-menu-tabs');
        } else {
          
          $(".productTab").insertAfter('.mainProducts');
        }
        
        $(".crossSell").insertAfter('.productTab');
        $(".seoContent").insertAfter('.productTab');
        $(".productTableMatrix").insertAfter('.productTab');
        
        pdpCommonGreaterThanResize();
      }
      else if(isIE()){
        setTimeout(function(){
          $("#tabs").unslick();
          $('#tabs').slick({
            infinite: true,
            cssEase: 'linear',
            adaptiveHeight: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
          });
        },200);

      }
    }
    
    $(document).ready(function($) {

      var isFirefox = typeof InstallTrigger !== 'undefined';
      var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
      if(isFirefox)
      {
        //$('.caretx').css({'margin-top':'10px'});
      }

      function dropItdown()
      {
        $('.attrHver .drpdwn').each(function(){
          $.trim($(this).text($.trim($(this).text())));
          $(this).parent('div').css({'padding-right':'5px'});
          cutit = $(this).text().length;
          cutitTxt = $(this).text();

          var isFirefox = typeof InstallTrigger !== 'undefined';
          var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

          if($.trim(cutit)> 25 && $(window).width() <= 768){
            $(this).text(cutitTxt.substring(0,15) + '...');
          
          }
          
          if($.trim(cutit)>45)
          {
           if(isSafari)
            {
              //$(this).siblings('.caretx').css({'margin-top':'-10px'});
            }
           $(this).text(cutitTxt.substring(0,40) + '...');
           if($(window).width() <= 1024 && $(window).width() >= 768)
           {
              $(this).text(cutitTxt.substring(0,17) + '...');
           }
           else if($(window).width() <= 768)
           {
              $(this).text(cutitTxt.substring(0,15) + '...');
           }
          }
          else if($.trim(cutit)<=45)
          {
            if(isSafari)
            {
              $(this).siblings('.caretx').css({'margin-top':'10px'});
            }
          }

          if(isFirefox && $(window).width() < 768){
              $(this).siblings('.caretx').css({'margin-top':'-10px'});
          } else if (isFirefox && $(window).width() >= 768){
              $(this).siblings('.caretx').css({'margin-top':'10px'});
          }

        });
      }
      dropItdown();
      $('.attrHverLI li a').click(function(){
        setTimeout(function(){
          dropItdown();
        }, 500);
      });

      function fixedHeader() {
        if($(window).width() >= 1024) {
          $('.headerNavTop').addClass('navbar-fixed-top');//ticket #1093
          $('.headerNav').addClass('navbar-fixed-top toFixed');//ticket #1093
          $('#container').addClass('margTop');//ticket #1093
        }
        else
        {
          $('.headerNavTop').removeClass('navbar-fixed-top');//ticket #1093
          $('.headerNav').removeClass('navbar-fixed-top toFixed');//ticket #1093
          $('#container').removeClass('margTop');//ticket #1093
        }
      }
      //fixedHeader();

      $(window).resize(function() {
        //fixedHeader();
        pdpRwdChangeLocation();
        dropItdown();
      });

      pdpRwdChangeLocation();
      if(isIE() || isFirefox) {
        $('#tabs').slick({
          infinite: true,
          cssEase: 'linear',
          adaptiveHeight: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          variableWidth: false // ticket 943
        });
      }
    });
  </script>
</div>

<input type="hidden" id="popover" value="-1" />
</div>

<script type="text/javascript">
  //SUBMENU TABS - JS for changing the selected tabs to blue.
  $('.key-tab').click( function(e){
    var currentTab = e.target;

    $('.slick-slide a').removeClass('selectedTab');
    $(currentTab).addClass('selectedTab');

    var tabCount = $('.key-tab').length;

    $('.popupTooltip').popover('hide');
  });
  //IMAGE ROTATION GALLERY - JS for changing the button into blue.
  $('.rotationButton a:first-child').addClass('active');
  $('.rotationButton a').click(function(){
    $('.slider-btn').removeClass('active');
    $(this).addClass('active');
  });

  $('#tabs a').tabs();

  // Frequently Bought
  
  function showImage(field) {
    $('.slider-btn').removeClass('active');
    $(field).addClass('active');

    thumb = $(field).data('thumb');
    popup = $(field).data('popup');

    $('.imageZoom').prop('src', thumb);
    $('.imageZoom').data('zoom-image', popup);

    imageZoom();
  }

  $(document).ready(function() {
    $('.optionFields').show();

    $('.popupTooltip').unbind('click').on('click', function() {
      $('.popupTooltip').popover('hide');
      $(this).popover('show');
    });

    $('.attribute-option').on('click', function() {
      var id = $(this).data('parentid');
      var newVal = $(this).data('id');
      var newText = $(this).data('value');

      $('#optionSelect-' + id).find("option").attr("selected", false);

      $('#option-hidden-val-' + id).val(newVal);
      $('#option-hidden-label-' + id).val(newText);
      $('#optionSelect-' + id).val(newVal);
      $('#optionhidden_' + id).val(newVal);

      $('#option-text-' + id + ' .storage-label').text(newText);
      $('.dropdown-' + id + ' .dropdown-toggle').html('<div><span class="drpdwn">'+newText + '</span><span class="caret caretx"></span></div>');
      $('.dropdown-' + id + ' ul li').removeClass('active');
      $(this).parent().addClass('active');
      $('.dropdown-' + id).toggleClass('open');

      updateMatrix();

      if ($('#div-prepaid').is(':visible')) {
        getPrepaidKit();
      } else {
        getHeroProduct();
      }

      return false;
    });

    updateMatrix();

          $('#div-prepaid').hide();
      $('#div-hero-offer').show();
      getHeroProduct();
      });

  function updateMatrix() {
    // Set Plan Matrix
    var optionArray = [];
            optionArray.push($("#optionSelect-1485").val());
            optionArray.push($("#optionSelect-1487").val());
    
    $.ajax({
      url: 'index.php?route=product/product/getPlanMatrix&product_id=1199&attributes=' + optionArray.join(':'),
      dataType: 'json',
      beforeSend: function() {
        $('#plan_matrix').html('<div align="center"><span class="wait">&nbsp;<img src="catalog/view/theme/broadwaytheme/images/loader.gif" alt="" style="width: 20px; height: 20px;" border="0" style="width: 20px; height: 20px;" /></span></div>');
      },
      complete: function() {
        $('.wait').remove();
      },
      success: function(json) {
        html = '';

        if (json['output'] != '') {
          html += json['output'];
        } else {
          html += 'No Available Products';
        }

        $('#plan_matrix').html(html);

        $('.popupTooltip').popover({
          container: 'body',
          placement: get_popover_placement
        });
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }

  function getHeroProduct() {
    $.ajax({
      url: 'index.php?route=product/product/getHeroProduct',
      type: 'post',
      data: $('#div-hidden-fields input[type=\'hidden\'], .optionFields select, .optionFields input[type=\'hidden\']'),
      dataType: 'json',
      beforeSend: function() {
        $('.heroOffer .hero-offer-plan').html('');
        $('.heroOffer .cashout').html('');
        $('.heroOffer .hero_offer_options').html('');
        $('.heroOffer .hero-offer-details').html('');
        $('#hero_offer_cart_options').html('');
        $('.heroOffer .stock-level').html('');
        $('#div-hero-offer .hero-offer-title').html('');
        $('#div-hero-offer #hero-offer-addtocart-btn').hide();

        $('.heroOffer .stock-level').removeClass('outofstock low limited instock');

        $('#hero-loading').html('<div style="padding: 30px 10px 15px 10px;"><img src="catalog/view/theme/broadwaytheme/images/loader.gif" alt="" width="20" height="20" border="0" style="width: 20px; height: 20px;" /><br />Updating</div>');
      },
      complete: function() {

      },
      success: function(json) {
        $('#hero-loading').html('');

        html = '';
        if (json['output']) {
          $('.heroOffer .hero-offer-details').html(json['output']['details']);
          $('.heroOffer #hero_offer_options').html(json['output']['options']);
          $('.heroOffer .stock-level').html('<link itemprop="availability" href="http://schema.org/' + json['output']['stock_label'] + '" />' + json['output']['stock']);
          $('.heroOffer .stock-level').addClass(json['output']['stock_status']);
          
          if ( json['output']['stock_status'] === 'outofstock' ) {
              $('.btn-addcart').prop('disabled',true);
          }
          
          $('#hero_offer_cart_options').html(json['output']['options']);
          $('#hero_offer_presets').html(json['output']['presets']);

          if (json['output']['cashout_val']>0) {
                          $('#div-hero-offer .hero-offer-title').html('<span class="hero-offer-plan">GET THIS</span> @ ' + "myLifestyle Plan 1799");
            
            $('.heroOffer .cashout').html('with ' + json['output']['cashout'] + ' cashout');
            $('#hero_offer_cart_cashout').html(json['output']['cashout'] + ' cashout');
          } else {
                          $('#div-hero-offer .hero-offer-title').html('<span class="hero-offer-plan">FREE</span> @ ' + "myLifestyle Plan 1799");
                      }

          $('#div-hero-offer #hero-offer-addtocart-btn').show();

          // Frequently Bought
          $('#hero_offer_fb_image_0').attr('src', json['output']['image']);
          $('#hero_offer_fb_price_0').html(json['output']['price']);
          $('#hero_offer_fb_priceval_0').val(json['output']['price_val']);
          $('#hero_offer_fb_options_0').html(json['output']['optionsFB']);

          getFBTotal();

          // Other Product Images

          // Main Image
          $('#pdp_main_image').attr('src', json['output']['thumb']);
          $('.zoomWindow').css('background-image', 'url("' + json['output']['popup'] + '")');

          html_image_rwd = '<div class="text-center"><img src="' + json['output']['thumb'] + '" /><br /></div>';

          // Other Gallery
          html_img = '<a class="slider-btn active" onclick="showImage(this)" data-thumb="' + json['output']['thumb'] + '" data-popup="' + json['output']['popup'] + '">&nbsp;&nbsp;&nbsp;</a>&nbsp;';
          $.each( json['output']['images'], function( key, value ) {
            html_img += '<a class="slider-btn" onclick="showImage(this)" data-thumb="' + value.thumb + '" data-popup="' + value.popup + '">&nbsp;&nbsp;&nbsp;</a>&nbsp;';
            html_image_rwd += '<div class="text-center"><img src="' + value.thumb + '" /><br /></div>';
          });

          $('#pdp_other_image').html(html_img);

          // Update RWD
          $('.rwd-pdp-images').html(html_image_rwd);

          if(!isIE() || (isIE() && $(window).width() < 600) ) {
            $('.rwd-pdp-images').unslick();

            $('.rwd-pdp-images').slick({
              dots: true,
              infinite: true,
              speed: 500,
              slide: 'div',
              cssEase: 'linear',
              adaptiveHeight: true
            });
          }
        } else {
          $('#div-prepaid').hide();
          $('#div-hero-offer').show();
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
</script>
<script type="text/javascript">
$('.button-cart').bind('click', function() {
  addToCart($(this).data('id'));
});

function addToCartMatrix(id) {
  addToCart('matrix_' + id);
}

function get_popover_placement(pop, dom_el) {
  var width = window.innerWidth;
  if (width<500) return 'bottom';
  var left_pos = $(dom_el).offset().left;
  if (width - left_pos > 400) return 'right';
  return 'left';
}

function addToCart(parent_id) {
  if($("#disable-add-cart").val() == 1) {
    return false;
  } 

  prepaid_fields = '';
  if (parent_id == 'prepaid_kit') {
    prepaid_fields = ', .optionFields select';
  }
  $("#disable-add-cart").val(1);
  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#'+parent_id+' input[type=\'text\'], #'+parent_id+' input[type=\'hidden\'], #'+parent_id+' input[type=\'radio\']:checked, #'+parent_id+' input[type=\'checkbox\']:checked, #'+parent_id+' select, #'+parent_id+' textarea' + prepaid_fields),
    dataType: 'json',
    success: function(json) {
      $('.success, .warning, .attention, information, .error').remove();

      $('.popupTooltip').popover('hide');

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
          }
        }

        if (json['error']['profile']) {
            $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
        }

        if (json['error']['stock']) {
          $('#pdpCartErrorModal').modal('show');
        }
      }

      if (json['success']) {
        // Analytics
        dataLayer.push({
          'event': 'addToCart',
          'ecommerce': {
            'currencyCode': 'PHP',
            'add': {
              'products': [{
                'name': json['product']['name'],
                'id': json['product']['id'],
                'price': json['product']['price'],
                'brand': json['product']['brand'],
                'category': '',
                'variant': json['product']['variant'],
                'quantity': json['product']['quantity']
               }]
            }
          }
        });

        $('.cart-total-hide-empty').removeClass('cart-total-hide-empty');
        $('#cart-total').html(json['total']);
        $('#pdpCartModal').modal('show');
      }

      $("#disable-add-cart").val(0);
    }
  });
}
//--></script>

<script type="text/javascript"><!--
// Prepaid Kit
  $('.btn-prepaid').click(function() {
    $('#div-prepaid').show();
    $('#div-hero-offer').hide();
    getPrepaidKit();
  });

  // Postpaid Kit
  $('.btn-postpaid').click(function() {
    $('#div-prepaid').hide();
    $('#div-hero-offer').show();
    getHeroProduct();
  });

  function isIE () {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
  }

  function getPrepaidKit() {
    $.ajax({
      url: 'index.php?route=product/product/getPrepaidKit',
      type: 'post',
      data: $('#div-prepaid input[type=\'hidden\'], .optionFields select, .optionFields input[type=\'hidden\']'),
      dataType: 'json',
      beforeSend: function() {
        $('#prepaid-cashout').html('');
        $('#prepaid-details').html('');
        $('#prepaid-hidden').html('');
        $('#div-prepaid .hero-offer-title').html('');
        $('#div-prepaid #hero-offer-addtocart-btn').hide();

        $('#div-prepaid .stock-level').html('');
        $('#div-prepaid .stock-level').removeClass('outofstock low limited instock');

        $('#prepaid-loading').html('<div style="padding: 30px 10px 15px 10px;"><img src="catalog/view/theme/broadwaytheme/images/loader.gif" alt="" style="width: 20px; height: 20px;" border="0" style="width: 20px; height: 20px;" /><br />Updating</div>');
      },
      complete: function() {

      },
      success: function(json) {
        $('#prepaid-loading').html('');

        html = '';

        if (json['output']) {

          // please dont remove this fix
          //---------------------------
          if (isIE () == 9) {
           console.log("--->ie9fix");
          }
          //---------------------------

          $('#div-prepaid .hero-offer-title').html('Device Only <span id="prepaid-cashout">' + json['output']['cashout'] + '</span>');

          $('#prepaid-details').html(json['output']['details']);
          $('#prepaid-hidden').html(json['output']['options']);

          $('#div-prepaid .stock-level').html(json['output']['stock']);

          $('#div-prepaid .stock-level').addClass(json['output']['stock_status']);

          $('#div-prepaid #hero-offer-addtocart-btn').show();

          // Frequently Bought
          $('#hero_offer_fb_image_0').attr('src', json['output']['image']);
          $('#hero_offer_fb_price_0').html(json['output']['price']);
          $('#hero_offer_fb_priceval_0').val(json['output']['price_val']);
          $('#hero_offer_fb_options_0').html(json['output']['optionsFB']);

          getFBTotal();

          // Other Product Images

          // Main Image
          $('#pdp_main_image').attr('src', json['output']['thumb']);
          $('.zoomWindow').css('background-image', 'url("' + json['output']['popup'] + '")');

          html_image_rwd = '<div class="text-center"><img src="' + json['output']['thumb'] + '" /><br /></div>';

          // Other Gallery
          html_img = '<a class="slider-btn active" onclick="showImage(this)" data-thumb="' + json['output']['thumb'] + '" data-popup="' + json['output']['popup'] + '">&nbsp;&nbsp;&nbsp;</a>&nbsp;';
          $.each( json['output']['images'], function( key, value ) {
            html_img += '<a class="slider-btn" onclick="showImage(this)" data-thumb="' + value.thumb + '" data-popup="' + value.popup + '">&nbsp;&nbsp;&nbsp;</a>&nbsp;';
            html_image_rwd += '<div class="text-center"><img src="' + value.thumb + '" /><br /></div>';
          });

          $('#pdp_other_image').html(html_img);

          // Update RWD
          $('.rwd-pdp-images').html(html_image_rwd);

          if(!isIE() || (isIE() && $(window).width() < 600) ) {
            $('.rwd-pdp-images').unslick();
            
            $('.rwd-pdp-images').slick({
              dots: true,
              infinite: true,
              speed: 500,
              slide: 'div',
              cssEase: 'linear',
              adaptiveHeight: true
            });
          }
        } else {
          $('#div-prepaid').hide();
          $('#div-hero-offer').show();
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }

</script>

<script type='text/javascript' src='D:\AEM\Globe\cart\js\elevatezoom.js'></script>
<script type="text/javascript"><!--
  $(document).ready(function() {
      imageZoom();
  });


/*  window.onload = function() {
    imageZoom();
    
  }*/

  function imageZoom() {
      $('.zoomContainer').remove();
      $('.imageZoom').removeData('elevateZoom');
      $('.imageZoom').elevateZoom({
          responsive: true,
          easing: true,
          borderSize: 2,
          borderColour: '#ccc',
          zoomWindowOffetx: 5,
          scrollZoom : true,
          zoomWindowWidth: 520,
          zoomWindowHeight: 400
      });
  }
//--></script>

<script>
dataLayer.push({
  'ecommerce': {
    'detail': {
      'actionField': {'list': ''},    // 'detail' actions have an optional list property.
      'products': [{
        'name': 'iPhone 6 Whitebox',         // Name or ID is required.
        'id': '1199',
        'price': '2299.00',
        'brand': '',
        'category': 'Mobile Postpaid',
        'variant': 'myLifestyle Plan 1799, 24 months, Gold, 16 GB'
       }]
     }
   }
});
</script>

</div>

    <div id="footer-top-wrap" class="container-fluid" style="background: #333333;">
    <div class="container footer-top-holder">
      <!-- INSERT FOOTER CONTENT HERE -->
      <style type="text/css">#footer hr.footer-divider{
    border-top: none !important;
  }

  #footer-top-wrap{
  background: #f6f6f6 !important;
  }

  .bg-gray{
   background:#f6f6f6;
  }
  .padd-top-bot{
     padding-top: 5px;
    padding-bottom: 0px;
  }
  .padd-top-bot10{
  padding-top: 10px;
  padding-bottom: 10px;
  }
  .f-shipping{
    text-align: center;
  }
  .f-payment{
    text-align: center;
  }
  .f-call{
    text-align: center;
  }
  .f-shipping a {
     text-decoration: none;
     color: #333;
  }
  .footer-options div{
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .f-shipping img{
  margin-right: 10px;
  margin-bottom: 0px;

  }
  .f-payment img{
  margin-right: 10px;
  margin-bottom: 0px;
  }
  .f-call img{
  margin-right: 10px;
  margin-bottom: 0px;
  }

   @media screen and (max-width: 768px) {
   .f-shipping{
      border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-payment{
       border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-call{
    text-align: center;
  }
}
@media screen and (max-width: 445px) {
.indent-mobile{
margin-left: 12%;
}
}

@media screen and (max-width: 391px) {
.indent-mobile{
margin-left: 12%;
}
}
@media screen and (max-width: 389px) {
.indent-mobile{
margin-left: 0%;
}
}
</style>
<div id="footer-top">
<div class="container-fluid bg-gray padd-top-bot">
<div class="container">
<div class="row footer-options">
<div class="col-sm-4 col-md-4 col-lg-4 f-shipping"><a href="#" title="Click to learn more about our shipping, delivery and returns policies"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/shippingA.png" /><span class="m-top" title="Click to learn more about our shipping, delivery and returns policies">FREE SHIPPING ON ALL ORDERS</span></a></div>

<div class="col-sm-5 col-md-5 col-lg-5 f-payment"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/payment-method.png" /><span class="m-top">CASH ON DELIVERY, MAJOR<span class="indent-mobile"> CREDIT CARDS &amp; GCASH</span></span></div>

<div class="col-sm-3 col-md-3 col-lg-3 f-call"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/call.png" /><span class="m-top">CALL (02)730-1010</span></div>
</div>
</div>
</div>
</div>
    </div>
  </div>
  <div id="footer">
    <hr class="footer-divider">
    <div class="container footer-holder">
        <footer class="main shadow row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl>
                  <dt><a href="http://www.globe.com.ph/about-globe">Globe Telecom</a></dt>
                  <dd><a href="http://www.globe.com.ph/about-globe/company-info">Corporate Information</a></dd>
                  <dd><a href="http://www.globe.com.ph/corporate-governance">Corporate Governance</a></dd>
                  <dd><a href="http://www.globe.com.ph/investor-relations">Investor Relations</a></dd>
                  <dd><a href="http://www.globe.com.ph/careers">Careers</a></dd>
                  <dd><a href="http://www.globe.com.ph/press-room">Press Room</a></dd>
                  <dd><a href="http://www.globe.com.ph/globelife">GlobeLife</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl>
                  <dt>Products and Services</dt>
                  <dd><a href="http://www.globe.com.ph/postpaid">Postpaid Plans</a></dd>
                  <dd><a href="http://www.globe.com.ph/prepaid/call-and-text-offers">Prepaid Promos</a></dd>
                  <dd><a href="http://www.globe.com.ph/surf">Mobile Internet</a></dd>
                  <dd><a href="http://tattoo.globe.com.ph/">Tattoo Broadband</a></dd>
                  <dd><a href="http://www.globe.com.ph/gcash">GCash</a></dd>
                  <dd><a href="http://shop.globe.com.ph/home">Online Shop</a></dd>
              </dl>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear1">
                  <dt><a href="http://business.globe.com.ph/">Business</a></dt>
                      <dd><a href="http://business.globe.com.ph">Enterprise and Wholesale</a></dd>
                      <dd><a href="http://mybusiness.globe.com.ph">Small and Medium Business</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear2">
                  <dt><a href="http://www.globe.com.ph/worry-free-guarantee">Globe Guarantee</a></dt>
                  <dd><a href="http://www.globe.com.ph/terms-and-conditions">Terms and Conditions</a></dd>
                  <dd><a href="http://www.globe.com.ph/privacy-policy">Privacy Policy</a></dd>
                  <dd><a href="http://www.globe.com.ph/aup">Fair Use Policy</a></dd>
              </dl>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear3">
                  <dt><a href="http://www.globe.com.ph/contactus">Contact Us</a></dt>
                  <dd><a href="http://www.globe.com.ph/help">Help and Support</a></dd>
                  <dd><a href="http://community.globe.com.ph/">Ask the Community</a></dd>
                  <dd><a href="http://www.globe.com.ph/store-locator">Store Locator</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=shop.globe.com.ph&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script>            </div>
        </footer>
    </div>
    <div class="footer-legals">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div class="content-holder text-center footer-links">
              <p>&copy; 2016 Globe Telecom Inc.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Talk2GLOBE CHAT</h4>
      </div>
      <div class="modal-body" align="center">
        <iframe id="chatFrame" src="" frameborder="0" height="459" width="99.6%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Item Added -->
<div class="modal fade" id="pdpCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: green; ">Item successfully added to cart.</h4>
        <br />
        <div class="cart-btn-view">
          <a href="http://shop.globe.com.ph/cart" class="btn btn-bw-orange">View Cart</a>
        </div> 
        <br />
      </div>
    </div>
  </div>
</div>

<!-- Item Out of Stock -->
<div class="modal fade" id="pdpCartErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: red; " id="pdpCartErrorModalMsg">Out Of Stock</h4>
        <br />
        <div class="cart-btn-view">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
        <br />
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.btn-chat').click(function() {
      $('#chatFrame').attr('src', 'http://webchat.globe.com.ph/');
    });
  });
</script>

<!-- Ethnio -->
  <!-- End Ethnio -->

<!-- Custom Script -->
<script type="text/javascript">_satellite.pageBottom();</script>

<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//D:\AEM\Globe\cart\js\tag.js#site=ZKgM1Va";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//s.thebrighttag.com/iframe?c=ZKgM1Va" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

</noscript>


<!---Mouseflow--->

<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//D:\AEM\Globe\cart\js\mouseflow.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<!---Mouseflow--->
<!-- End Custom Script -->

</body>
</html>