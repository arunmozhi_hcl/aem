<%@ include file="/libs/foundation/global.jsp" %>

<head>

<style>



	.center {
    	display: block;
    	margin: 0 auto;
	}

	.editFooter ul {
    	float: left;
    	text-decoration: none;
    	color: #cdcece;
    	list-style: none;
    	font-size: 14px;
    	width: 20%;
    	padding: 0px;
	}

	.editFooter ul li a:hover {
    	color: #FFF;
	}

	.editFooter ul li a {
    	color: #cdcece;
    	text-decoration: none;
    	font-size: 14px;
	}

	body {
    	font-family: "fs_elliot_proregular", Helvetica;
	}

	.txtWhite {
    	color: #FFF !important;
	}

	footer.main {
    	padding: 20px 0px;
	}

	footer.main {
    	background: #58595b;
	}


</style>
</head>


<body>
    <div class="main1" style="padding: 0px 0px;">

 <div class="fixed-width center editFooter" style="background: #58595b;
padding-bottom: 95px; width:100%;">
<ul class="topFoot phoneFoot">
 <%

        String title[] = properties.get("title", new String[0]);
        String links[] = properties.get("path", new String[0]);
        for(int i=0; i<title.length ;i++)
        {  %> 

        <li><a href="<%=links[i]%>"><%=title[i]%></a></li> 
      <% }

	%>
</ul> 

<ul class="topFoot phoneFoot">
 <%
        String title1[] = properties.get("title1", new String[0]);
        String links1[] = properties.get("path1", new String[0]);
        for(int i=0; i<title1.length ;i++)
        {  %> 

        <li><a href="<%=links1[i]%>"><%=title1[i]%></a></li> 
      <% }

	%>
</ul> 

<ul class="topFoot phoneFoot">
 <%
        String title2[] = properties.get("title2", new String[0]);
        String links2[] = properties.get("path2", new String[0]);
        for(int i=0; i<title2.length ;i++)
        {  %> 

        <li><a href="<%=links2[i]%>"><%=title2[i]%></a></li> 
      <% }

	%>
</ul> 

<ul class="myfooter">
 <%
        String title3[] = properties.get("title3", new String[0]);
        String links3[] = properties.get("path3", new String[0]);
        for(int i=0; i<title3.length ;i++)
        {  %> 

        <li><a href="<%=links3[i]%>"><%=title3[i]%></a></li> 
      <% }

	%>
</ul> 

<ul class="myfooter">
 <%
        String title4[] = properties.get("title4", new String[0]);
        String links4[] = properties.get("path4", new String[0]);
        for(int i=0; i<title4.length ;i++)
        {  %> 

        <li><a href="<%=links4[i]%>"><%=title4[i]%></a></li> 
      <% }

	%>
</ul> 

 <div class="clear"></div> </div> </div>

</body>
