<%@ include file="/libs/foundation/global.jsp" %>
<html lang="en">
    <cq:includeClientLib categories="productdetails"/>
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <title>Document</title>
 </head>
 <body>
  <div id="foldsContainer-banner6" style="position:relative">
<div id="foldsContainerForCMS-banner6">
<style type="text/css">
    #banner6{
        padding-top: 50px;
      }
      #banner6 .container-fluid .container{
        padding-right: 0px;
      } 
  
      #banner6 img{
        display: inline-block;
        margin: 0 auto;
        vertical-align: top;
      }

      .banner-6-shopby-row{
        text-align: center;
      }

      .banner-6-shopby-container{
        display: inline-block;
        width: 25%;
      }

      .banner6-shopby{
        text-align: center;
      }

      .banner6-shopby .banner6-shopby-text{
        display: inline-block;
        margin-left: 5px;
        margin-top: 12px;
        line-height: 1;
        text-align: left;
        vertical-align: top;
      }

      .banner6-shopby .banner6-shopby-text a{
        color: #1769b3;
        font-size: 20px;
        text-transform: none;
        text-decoration: none;
      }

      .banner6-line{
      margin-top: 40px;
      text-align: center;
      }
      .banner6-line-border{
        background-color: #333333;
        display: inline-block;
        height: 2px;
        width: 80%;
    
      }

      @media all and (max-width: 1200px) {
        #banner6 .container-fluid .container{
          padding-right: 15px;
          margin-right: auto;
      } 

      }
      
      @media all and (max-width: 991px) {
    #banner6 img{
            height: 65px;
          width: auto;
        }
        .banner6-shopby .banner6-shopby-text a{
          font-size: 15px;
        }       

      }

      @media all and (max-width: 750px) {
        #banner6{
          padding-top: 25px;
        }
        .banner-6-shopby-container{
          width: 30%;
        }
        #banner6 img{
          height: 60px;
        width: auto;
        }
        .banner6-shopby .banner6-shopby-text{
          margin-top: 13px;
        }
        .banner6-shopby .banner6-shopby-text a{
          font-size: 16px;
        }
        .banner6-line{
          margin-top: 15px;
        }
        .banner6-line-border{
          
          width: 89%;
        }

      }

      @media all and (max-width: 630px) {
        #banner6 img{
          height: 50px;
        width: auto;
        }
        .banner6-shopby .banner6-shopby-text{
          margin-top: 11px;
        }
        .banner6-shopby .banner6-shopby-text a{
          font-size: 14px;
        }

      }

      @media all and (max-width: 565px) {
        #banner6 img{
          height: 72px;
        width: auto;
        }

        #banner6 img.banner6-shopby-plan-img{
          margin-left: 15%;
        }
        .banner6-shopby .banner6-shopby-text{
          display: block;
          text-align: center;
        }
        .banner6-shopby .banner6-shopby-text a{
          font-size: 16px;
        }



      }

      @media all and (max-width: 400px) {

        .banner6-shopby .banner6-shopby-text a{
          font-size: 14px;
        }

      }

      @media all and (max-width: 350px) {
          #banner6{
            padding-top: 35px;
          }
          #banner6 img{
            height: 55px;
            width: auto;
          }
        .banner6-shopby .banner6-shopby-text a{
          font-size:10px;
        }
        .banner6-line{
          margin-top: 25px;
        }


      }
</style>
<%
    String title1 = properties.get("title1", "");
%>
<div id="banner6">
<div class="container-fluid">
<div class="container">
<div class="row banner-6-shopby-row">
<div class="banner-6-shopby-container">
    <div class="banner6-shopby"><a href="http://shop.globe.com.ph/shop-by-device"><img alt="Shop by Device" height="72px" src="${properties.image1}" title="Shop by Device" width="42px" class="lazy" data-original="http://shop.globe.com.ph/image/data/homepage_folds/shop-device-btn.png" style="display: inline-block;"></a>
        <div style="width:70%;" class="banner6-shopby-text"><a href="http://shop.globe.com.ph/shop-by-device"><%=title1%><br> Device</a></div>
</div>
</div>

<div class="banner-6-shopby-container">
    <div class="banner6-shopby"><a href="http://shop.globe.com.ph/shop-by-plan"><img alt="Shop by Plan" class="banner6-shopby-plan-img lazy" height="76px" src="${properties.image2}" title="Shop by Plan" width="70px" data-original="http://shop.globe.com.ph/image/data/homepage_folds/shop-plan-btn.png" style="display: inline-block;"></a>

        <div class="banner6-shopby-text"><a href="http://shop.globe.com.ph/shop-by-plan">Shop By <br> Plan</a></div>
</div>
</div>

<div class="banner-6-shopby-container">
    <div class="banner6-shopby"><a href="http://shop.globe.com.ph/gadgets-and-accessories"><img alt="Shop for Gadgets &amp; Accessories" height="73px" src="${properties.image3}" title="Shop for Gadgets &amp; Accessories" width="73px" class="lazy" data-original="http://shop.globe.com.ph/image/data/homepage_folds/shop-accessories-btn.png" style="display: inline-block;"></a>

        <div class="banner6-shopby-text"><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop By <br> Accessories</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
 </body>
</html>
