<%@ include file="/libs/foundation/global.jsp"%>

<html>
    <cq:includeClientLib categories="productdetails"/>
    <head>


<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Checkout</title>


<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/checkout-common-form.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/pikaday.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/dropzone.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/dropzone.min.css" media="screen" />


</head>


 <body>

<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="checkout-form-container">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
<div class="row" id="form-container" style="height: 769px;">
<div id="section-container">
    <form action="" enctype="multipart/form-data" id="app_form">
<section class="panel prev" id="personal-details">
<div class="panel-content"></div>
</section>

<section class="panel current" id="additional-personal-details">
    <div class="panel-content"><div class="panel-heading">${properties.pagetitle}</div>
<div class="panel-body">
                                        <div class="panel-row row" style="z-index: 6;">


            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">${properties.bill}</label>

                                    <input type="text" data-required="required" class=" required-data" name="billing_house_number" value="" maxlength="255">
                
                    <input type="hidden" id="billing_street" name="billing_street" value="">
                    <input type="hidden" id="billing_barangay" name="billing_barangay" value="">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 5;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: 73px;">
                <label class="field-label">${properties.area}</label>

            
                                    <input type="text" placeholder="Makati, Quezon City, etc" data-required="required" class="required-data ui-autocomplete-input" name="billing_city_temp" value="" maxlength="255">

                    <input type="hidden" id="billing_city" name="billing_city" class=" required-data" value="">
                    <input type="hidden" id="billing_province" name="billing_province" class=" required-data" value="">
                    <input type="hidden" id="billing_region" name="billing_region" class=" required-data" value="">
                    <input type="hidden" id="billing_country" name="billing_country" class=" required-data" value="">    
                    <div id="billing-details-val" class="details-val">
                        <span class="check-icon"></span>
                    </div>
                            </div>

            

            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.zipcode}   </label>

                <input type="text" data-required="required" name="billing_zip_code" value="">

                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 4;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: 73px;">
                <label class="field-label">
                    ${properties.birthday}                         <span id="birthday-datepicker-trigger" class="icon calendar"></span>
                                    </label>
            
                                    <input type="text" data-required="required" id="birthday-datepicker" class="datepicker  required-data" name="personal_birthday" value="" readonly="readonly">
                            </div>

            

            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.citizenship}  </label>

            
                                    <select name="personal_citizenship" class="required-data" data-required="required" data-default="">
                        <option value="">Please Select</option>
                                                                                    <option value="filipino">
                                    Filipino                                </option>
                                                            <option value="other">
                                    Other                                </option>
                                                                        </select>
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 3;">
            
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 2; height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.civilstatus} </label>

                                    <select name="personal_civil_status" class="required-data" data-required="required" data-default="single">
                        <option value="">Please Select</option>
                                                                                    <option value="single" selected="selected">
                                    Single                                </option>
                                                            <option value="married">
                                    Married                                </option>
                                                            <option value="widowed">
                                    Widowed                                </option>
                                                            <option value="separated">
                                    Separated                                </option>
                                                                        </select>
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="z-index: 1; height: 73px;">
                <label class="field-label"> ${properties.children} </label>

                                    <input type="text" data-required="required" class="checkout-forms-text-numeric required-data" name="personal_number_of_children" value="0" maxlength="2">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 2;">
            
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 3; height: 73px;">
                <label class="field-label">${properties.firstname} </label>

                                    <input type="text" data-required="required" class=" required-data" name="personal_mother_first_name" value="" maxlength="100">
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 2; height: 73px;">
                <label class="field-label">${properties.middlename}</label>

                                    <input type="text" data-required="required" class=" required-data" name="personal_mother_middle_name" value="" maxlength="100">
                            </div>

            
                                
            
            <div class="form-group required-field col-lg-4 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: 73px;">
                <label class="field-label">${properties.lastname} </label>

                                    <input type="text" data-required="required" class=" required-data" name="personal_mother_last_name" value="" maxlength="100">
                            </div>

                        </div>
            
                                                            <div class="panel-row row" style="z-index: 1;">
            
            
            <div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 1; height: auto;">
                <label class="field-label">${properties.contact} </label>

                                    <input type="text" data-required="optional" class="checkout-forms-text-numeric optional-data" name="personal_alternate_contact_number" value="" maxlength="15">
                            </div>

                        </div>
            
                        
    <input type="hidden" id="checkout_type" value="postpaid">
</div>
<div id="panel-bottom" class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a id="prev-step" onclick="setPreviousPanel('personal-details', 'additional-personal-details')">Previous Step</a>
        <button id="button-additional-personal-details" class="btn"><a href ="/content/globeEcommerce/shippingdetailspage.html">Next Step</a></button>
    </div>
</div>

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/addl_information.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    getSidebar('addl_information', 'postpaid');
})
</script>

<script type="text/javascript">
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 2, 'option': ''},
        'products': [
                        {                            
                'name': 'Samsung Galaxy S6 edge',     
                'id': '134',
                'price': '25299.00',
                'brand': 'Samsung',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 999, 24 months, White Pearl, 64 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                        {                            
                'name': 'Samsung Galaxy S6 edge',     
                'id': '134',
                'price': '25299.00',
                'brand': 'Samsung',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 999, 24 months, White Pearl, 64 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                    ]
     }
   }
});
</script>
</div>
</section>

<section class="panel" id="shipping-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="financial-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="document-upload-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="terms-condition-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="review-order-details">
<div class="panel-content"></div>
</section>
</form>
</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="sidebar"><p><img class="img-responsive" id="norton-secured" src="https://shps3dv01.s3.amazonaws.com/media/data/norton_secured.png"></p>

<div class="row" id="contact-details">
<div class="info-block col-lg-12 col-md-12 col-sm-12 col-xs-4">
<p>Your personal information is safe and will always be kept private.</p>
</div>
</div>
</div>
</div>
</body>
</html>
 <script type="text/javascript">
$(document).ready(function(){
    //alert("dd");
    });
$("#button-additional-personal-details").click(function(){
    //alert("dyyd");
$.ajax({
    type:"GET",
    	data: $('#app_form').serialize(),
		cache:false,
url : "/bin/epcortex/billinginfo",
    success:function(data){
        //window.location.href="http://localhost:4504/content/globeEcommerce/shippingdetailspage.html";
	}
});
});

     </script>
