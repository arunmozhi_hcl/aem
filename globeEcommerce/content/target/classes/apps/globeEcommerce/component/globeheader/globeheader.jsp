<%@ include file="/libs/foundation/global.jsp" %>
<header class="main">
	<div class="fixed-width center"> 
		<div class="logo" style="padding-top:0px;">
			<a href="#"><img src="<%=properties.get("imagecomp")%>" alt="Globe"></a>
        </div> 
		<nav class="main-nav"> 
 			<a href="#nav" class="menu launcher"> <span></span> <span></span> <span></span> </a>
 		<ul>
			<li> 
 				<a href="${properties.links}" class="width100 firstNav">
					<span class="icon arrows dropdown"></span>
                	<span class="icon arrows pullup"></span>
					<span class="icon newnav personal"></span>
                	<div>${properties.title}</div>
 				</a>
				<div class="sub z9 firstSubNav">
					<ul class="fixed-width center"> 
						<li>
                            <a href= "http://localhost:4504/content/globeEcommerce/shopbycategorypage.html" id="shopbutton-header" name="visittheshopbutton">
                                <span class="icon new-sub-nav phones"></span>
                                <span class="icon other arrow"></span>
                                <p>Shop</p>
                            </a>
                        </li>
                        <li>
							<a href="http://localhost:4504/cf#/content/globe/PostPaid.html?wcmmode=disabled">
                                <span class="icon new-sub-nav postpaid"></span>
								<span class="icon other arrow"></span>
                                <p>Postpaid</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost:4504/cf#/content/globe/Platinum.html?wcmmode=disabled">
                                <span class="icon new-sub-nav platinum"></span>
                                <span class="icon other arrow"></span>
                                <p>Platinum</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost:4504/cf#/content/globe/prepaid.html?wcmmode=disabled">
                                <span class="icon new-sub-nav prepaid"></span>
                                <span class="icon other arrow"></span>
                                <p>Prepaid</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost:4504/content/globe/internetpage.html?wcmmode=disabled">
                                <span class="icon new-sub-nav internet"></span>
                                <span class="icon other arrow"></span>
                                <p>Internet</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost:4504/cf#/content/globe/international.html?wcmmode=disabled">
                                <span class="icon new-sub-nav international"></span>
                                <span class="icon other arrow"></span>
                                <p>International</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://localhost:4504/cf#/content/globe/gocash.html?wcmmode=disabled"> 
 <span class="icon new-sub-nav gcash"></span> 
 <span class="icon other arrow"></span>
 <p>GCash</p> 
 </a> 
 </li> 
 
 <li> 
 <a href="http://localhost:4504/cf#/content/globe/rewards.html?wcmmode=disabled"> 
 <span class="icon new-sub-nav rewards"></span>
 <span class="icon other arrow"></span> 
 <p>Rewards</p> 
 </a> 
 </li>
 
 <li>
 <a href="http://localhost:4504/cf#/content/globe/testingpage.html?wcmmode=disabled "> 
 <span class="icon new-sub-nav entertainment"></span>
 <span class="icon other arrow"></span> 
 <p>Entertainment</p> 
 </a> 
 </li> 
 </ul> 
 </div>  <!-- end of sub nav --> 
 </li> <!-- End of Li --> 
 
 <li> 
 <a class="width100 secondNav" href="http://mybusiness.globe.com.ph/"> 
 <span class="icon arrows dropdown"></span> 
 <span class="icon arrows pullup"></span>
 <span class="icon newnav business"></span> 
     <div>${properties.Sme}</div> 
 </a>
 </li><!-- End of Li --> 
 
 <li>
 <a class="width100 thirdNav" href="http://business.globe.com.ph/"> 
 <span class="icon arrows dropdown"></span> 
 <span class="icon arrows pullup"></span>
 <span class="icon newnav enterprise"></span>
 <div>Enterprise</div> 
 </a> 
 </li><!-- End of Li --> 

 <li> 
 <a href="#" class="width100 fourthNav"> 
 <span class="icon arrows dropdown"></span>
 <span class="icon arrows pullup"></span> 
 <span class="icon newnav about"></span> 
 <div>About Us</div> 
 </a> <!--Start Sub nav--> 
 
 <style> .governance2{width: 50px;
height: 50px; float:left;margin-left:20px;padding-bottom:10px;line-height: 0;
background-repeat: no-repeat;
background-image: url(http://www.globe.com.ph/image/image_gallery?img_id=8172227&t=1394619418177); }
@media only screen and (max-width: 959px)
{.governance2{display:none;}
}
</style> 
<div class="sub z9 fourthSubNav"> 
<ul class="fixed-width center"> 
<!--<li> <a href="http://www.globe.com.ph/about-globe"> 
<span class="icon new-sub-nav corporate"></span>
 <span class="icon other arrow"></span>
 <p>Corporate Information</p> </a> 
 </li>-->
 <li>
 <a href="http://www.globe.com.ph/corporate-governance"> 
 <span class="governance2"></span> 
 <span class="icon other arrow"></span>
 <p>Corporate Governance </p> 
 </a> 
 </li> 
 
 <li> 
 <a href="http://www.globe.com.ph/investor-relations"> 
 <span class="icon new-sub-nav investor"></span> 
 <span class="icon other arrow"></span>
 <p>Investor Relations</p> 
 </a> 
 </li> 
 
 <li> 
 <a href="http://www.globe.com.ph/annual-report/"> 
 <span class="icon new-sub-nav corporate"></span>
 <span class="icon other arrow"></span>
 <p>Sustainability</p> 
 </a> 
 </li>
 <!-- <li> <a href="#"> 
 <span class="icon new-sub-nav network"></span>
 <span class="icon other arrow"></span>
 <p>Network</p> </a> </li> --> 
 <li> 
 <a href="http://www.globe.com.ph/careers"> 
 <span class="icon new-sub-nav careers"></span>
 <span class="icon other arrow"></span> 
 <p>Careers</p> 
 </a> 
 </li>
 
 <li> 
 <a href="http://www.globe.com.ph/press-room"> 
 <span class="icon new-sub-nav press-room"></span>
 <span class="icon other arrow"></span> 
 <p>News</p> 
 </a>
 </li> 
 
 <li> 
 <a href="http://www.globe.com.ph/gen3">
 <span class="icon new-sub-nav globelife"></span>
 <span class="icon other arrow"></span>
 <p>Gen 3</p>
 </a> 
 </li>
 </ul> 
 </div> <!-- end of sub nav --> 
 </li><!-- End of Li --> 
 
 <li> 
 <a class="width100 fourthNav" href="#"> <span class="icon arrows dropdown"></span> 
 <span class="icon arrows pullup"></span> 
 <span class="icon newnav help-support"></span>
     <div>${properties.help}</div>
 </a> <!--Start Sub nav--> 
 <div class="sub fourthSubNav"> <ul id="sub4" class="fixed-width center">
 
 <li> 
 <a href="http://www.globe.com.ph/help">
 <span class="icon new-sub-nav faq"></span>
 <span class="icon other arrow"></span> 
 <p>FAQs</p>
 </a> </li>
 
 <li> <a href="http://www.globe.com.ph/help/guide">
 <span class="icon new-sub-nav phone-configuration"></span>
 <span class="icon other arrow"></span>
 <p>Phone Configuration</p> </a> 
 </li> 
 <li> <a href="http://www.globe.com.ph/help/troubleshooting"> 
 <span class="icon new-sub-nav basic-troubleshooting"></span>
 <span class="icon other arrow"></span> <p> Basic
                              <medium>Troubleshooting</medium> </p> </a>
							  </li> 
							  <li> 
							  <a href="http://community.globe.com.ph">
							  <span class="icon new-sub-nav community"></span> 
							  <span class="icon other arrow"></span> 
							  <p>Ask the Community</p> </a> 
							  </li> 
							  <li> 
							  <a href="http://www.globe.com.ph/store-locator">
							  <span class="icon new-sub-nav store-locator"></span> 
							  <span class="icon other arrow"></span> <p>Store Locator</p>
							  </a>
							  </li> 
							  
							  <li> 
							  <a href="http://www.globe.com.ph/contactus">
							  <span class="icon new-sub-nav contact-us"></span>
							  <span class="icon other arrow"></span> 
							  <p>Contact Us</p> 
							  </a> 
							  </li> 
							  
							  <li> 
							  <a href="http://webchat.globe.com.ph">
							  <span class="icon new-sub-nav livechat"></span> 
							  <span class="icon other arrow"></span> 
							  <p>Live Chat</p> 
							  </a>
							  </li> 
							  
							  </ul>
							  </div> <!-- end of sub nav --> 
							  </li><!-- End of Li -->
							  <li> <!-- My Account Start --> 
							  <a class="width100 fifthNav hideOnMobile" href="#">
							  <span class="icon arrows dropdown"></span>
							  <span class="icon arrows pullup"></span> 
							  <span class="icon newnav myAccount"></span>
                                  <div>${properties.account}</div> </a> <!--Start Sub nav-->
							  <div class="sub z9 fifthSubNav hideOnMobile"> <!----FIFTHSUBNAV-->
							  <div class="fixed-width center"> <!----> 
							  <div class="firstColAccount"> <!-- FIRST COLUMN --> 
							  <div class="logOut"> 
							  <h2>Enjoy more of Globe</h2> 
							  <a href="https://accounts.globe.com.ph/login" class="newButtonBlue centext"><span>Log in</span></a>
							  <div class="borders"></div> 
							  <a href="https://accounts.globe.com.ph/registration" class="newButtonGreen centext"><span>Sign up</span></a>
							  </div> </div> <!----SECOND COLUMN----> 
							  <div class="secondColAccount"> <h2><span class="myAccountIcon"></span>I want to</h2> 
							  <ul> 
							  <li><a href="http://www.globe.com.ph/enrolled-accounts"> View/edit my account details</a></li>
							  <li><a href="http://www.globe.com.ph/paybill">Pay my bill</a></li> 
							  <li><a href="http://www.globe.com.ph/paperlessreg">Sign up for paperless billing</a></li>
							  <li><a href="http://www.globe.com.ph/autopayreg">Enroll in Auto Pay service</a></li> 
							  <li><a href="http://www.globe.com.ph/web/globe/form-transfer-of-ownership" target="_blank">Transfer my account</a></li> 
							  <li><a href="/gcash-advisory">Access my GCash</a></li> 
							  <li><a class="modal-trigger" href="//www.globe.com.ph/form-check-activation-status">Check my line activation status</a></li>
							  </ul> 
							  
							  <ul> 
							  <li><a class="modal-trigger" href="//www.globe.com.ph/form-check-outstanding-balance">Check my postpaid outstanding balance</a></li>
							  <li><a href="http://dbschenkerepod.com.ph/globe/customerview.php" target="_blank">Track the delivery status of my device/SIM</a></li> 
							  <li><a href="http://www.globe.com.ph/updateinfo" target="_blank">Update my account information</a></li>
							  </ul> </div> <!----END SECOND COLUMN----> <div class="thirdColAccount"> <!----THIRD COLUMN----> <h2><span class="question"></span>Get help with</h2> <ul> <li><a href="http://www.globe.com.ph/help/guide">Configuring my phone</a></li> <li><a href="http://www.globe.com.ph/help/troubleshooting">Troubleshooting my device </a></li> <li><a href="http://www.globe.com.ph/help/roaming/about-international-roaming">Activating my international roaming</a></li> </ul> </div> <!----END THIRD COLUMN----> <!---CLEAR---> <div class="clear"></div> <!---CLEAR---> </div> <!----> </div> <!----END FIFTHSUBNAV--> <!-- My Account End --> <!-- end of sub nav --> </li><!-- End of Li --> </ul><!-- End of UL --> </nav> <!-- end of main navigation --> <!-- STICKY MOBILE START --> <div class="stickyMobile"> <a title="MyAccount" class="icon other collapse-button-mobile" href="#sticky">&nbsp;</a> <div class="wrapper"> <ul> <li class="login-mobile"> <a href="/c/portal/login"><span class="icon other arrow"></span> Log in / Sign up</a> </li> <li><a href="http://www.globe.com.ph/enrolled-accounts"> <span class="icon other arrow"></span> View/edit my account details</a></li> <li><a href="http://www.globe.com.ph/paybill"> <span class="icon other arrow"></span> Pay my bill</a></li> <li><a href="http://www.globe.com.ph/paperlessreg"> <span class="icon other arrow"></span> Sign up for paperless billing</a> </li> <li><a href="http://www.globe.com.ph/autopayreg"><span class="icon other arrow"></span> Enroll in Auto Pay service</a></li> <li><a href="http://www.globe.com.ph/form-transfer-of-ownership" target="_blank"><span class="icon other arrow"></span> Transfer my account</a></li> <li> <a href="https://mygcash.globe.com.ph/gcashonline"><span class="icon other arrow"></span> Access my GCash</a> </li> <li><a class="modal-trigger" href="//www.globe.com.ph/form-check-activation-status"><span class="icon other arrow"></span> Check my line activation status</a></li> <li><a class="modal-trigger" href="//www.globe.com.ph/form-check-outstanding-balance"> <span class="icon other arrow"></span>Check my postpaid outstanding balance</a> </li> <li><a href="http://dbschenkerepod.com.ph/globe/customerview.php" target="_blank"><span class="icon other arrow"></span> Track the delivery status of my device/SIM</a></li> <li><a href="http://www.globe.com.ph/updateinfo" target="_blank"><span class="icon other arrow"></span> Update my account information</a></li> </ul> </div> </div> <!-- STICKY MOBILE END --> <div class="top search-box" style="width: 14% !important;"> <a href="#search-box" class="search launcher"> <span></span> </a> <form class="searchForm" action="http://search.globe.com.ph/search" method="GET"> <input type="text" class="keyword q" name="q" maxlength="25" value=""> <input type="submit" class="search-btn search_q" value="Search"> </form> </div> <!--				
                <div class="top search-box"> <a href="#search-box" class="search launcher"><span></span></a> <form action="search.php" method="GET"> <input type="hidden" name="cx" value="YOUR SEARCH ENGINE ID goes here"> <input type="hidden" name="ie" value="UTF-8"> <input type="text" class="keyword" name="s" size="15"> <input type="submit" class="search-btn" value="Search"> <input type="submit" class="search-btn2" value="Search"> </form> </div> --> </div>
</header><br><br>
