<%@ include file="/libs/foundation/global.jsp" %>
<html lang="en">
    <cq:includeClientLib categories="productdetails"/>
 <head>

 </head>
 <body>
  <div id="container" class="margTop">

<!-- BREADCRUMBS START -->
<div class="container-fluid breadcrumb-container headerNav" itemscope="" itemtype="http://schema.org/BreadcrumbList">
  <div class="container">
    <div class="row">
      <div class="breadcrumb pull-left">
                <div></div>
        <div id="breadcrumb-0" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/" itemprop="item">
              <span class="blue-link" itemprop="name">${properties.title}</span>
            <meta itemprop="position" content="1">
          </a>
        </div>
                <div> | </div>
        <div id="breadcrumb-1" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/shop-by-device" itemprop="item">
              <span class="black-link" itemprop="name">${properties.title2}</span>
            <meta itemprop="position" content="2">
          </a>
        </div>
              </div>
      <div class="chat-call-holder pull-right hidden-xs hidden-sm">
                                            <a data-toggle="modal" data-target="#chatModal" class="btn btn-bw-clear btn-chat">Chat</a>
            
                                                    
          <a href="http://shop.globe.com.ph/contact" class="btn btn-bw-clear">${properties.title3}</a>
                                                    
                                    </div>
    </div>
  </div>
</div>
<!-- BREADCRUMBS END -->
</div>
 </body>
</html>
