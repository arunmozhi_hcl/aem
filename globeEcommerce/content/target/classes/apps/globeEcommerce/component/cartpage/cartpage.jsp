<%@ include file="/libs/foundation/global.jsp" %>

<html dir="ltr" lang="en" data-placeholder-focus="false">
    <cq:includeClientLib categories="productdetails"/>
<head>

<title>myLifestyle Plan 999 | Postpaid  | Globe Online Shop</title>


<link href="http://d11fuji4mn7bm2.cloudfront.net/media/data/Favicon/favicon.png" rel="icon" />



</head>
<body>
<style>.loading {background: #FFFFFF url('/catalog/view/theme/default/image/loading.gif') 10px center no-repeat;border: 1px solid #B8E2FB;	padding: 10px 10px 10px 33px;margin-bottom: 15px;color: #555555;}</style>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM7FZN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PM7FZN');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSV552"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSV552');</script>
<!-- End Google Tag Manager -->

<script>

   var domainList = ["www.globe.com.ph", "shop.globe.com.ph"]; //List of production domains

   //append “-staging” if hostname is not found in domainList
   if (domainList.indexOf(location.hostname) < 0) {
     a += "-staging"
   }

   document.write(unescape('%3Cscript type="text/javascript" src="'+a+'.js"%3E%3C/script%3E')); //write to page
</script>

<!-- BROADWAY HEADER -->
<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  <style type="text/css">.back2site a{
color:#fff;
}
.top-header-new{
background: #58595b;
padding: 3px 3% 3px 3%;
color: #fff;
}
span.call-text-top{
color: #fff;
}
span.call-text-top a{
color: #fff;
text-decoration: none;
}
span.m-left-right{
margin-left:10px;
margin-right: 10px;
}
.store-locator a{
	color:#fff;
}
.store-locator a:hover{
	color:#fff;
}

  .back2site a u, span.store-locator a{
      font-size:12px !important;
    
    }
</style>
<div class="container-fluid top-header-new">
<div class="pull-left back2site"><a href="http://www.globe.com.ph/" target="_blank"><u>&lt; Go to globe.com.ph</u></a></div>

<div class="pull-right"><span class="store-locator"><a href="https://www.globe.com.ph/store-locator" target="_blank"><u>Store Locator</u></a></span></div>
</div>
 
</div>

<div class="headerNavTop">
  <div id="new-header" class="">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 logo-wrapper header-blocks">
            <div class="dropdown">
              <div class="menu-list">
                <button class="btn dropdown-toggle" type="button" id="dropdown-button" data-toggle="dropdown">
                  <span class="icon-menu"></span>
                </button>
                <div class="dropdown-menu" role="menu" aria-labelledby="dropdown-button">
                  <div class="nav-list">
                    <ul>
                      <li id="1" role="presentation"><a id="header-about-globe" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
                      <li id="2" role="presentation"><a id="header-personal" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Personal</a></li>
                      <li id="3" role="presentation"><a id="header-sme" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://mybusiness.globe.com.ph/">SME</a></li>
                      <li id="4" role="presentation"><a id="header-enterprise" role="menuitem" tabindex="-1" class="bw-p-sm" href="http://business.globe.com.ph/">Enterprise</a></li>
                      <li id="5" role="presentation"><a id="header-help-support" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">Help & Support</a></li>
                      <li id="6" role="presentation"><a id="header-myaccount" role="menuitem" tabindex="-1" class="bw-p-sm" href="#">My Account</a></li>
                    </ul>
                  </div>
                  <div class="nav-container">
                    <div class="nav-result1">
                    </div>
                    <div class="nav-result2">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-personal-shop" href="http://shop.globe.com.ph">
                                  <span class="icon icon-phones"></span>
                                  <p class="text-center">Shop</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-postpaid" href="http://www.globe.com.ph/postpaid">
                                  <span class="icon icon-postpaid"></span>
                                  <p class="text-center">Postpaid</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-prepaid" href="http://www.globe.com.ph/prepaid ">
                                  <span class="icon icon-prepaid"></span>
                                  <p class="text-center">Prepaid</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-internet" href="http://www.globe.com.ph/internet">
                                  <span class="icon icon-internet"></span>
                                  <p class="text-center">Internet</p>
                              </a>
                          </li>

                          <li>
                              <a id="header-personal-international" href="http://www.globe.com.ph/international">
                                  <span class="icon icon-international"></span>
                                  <p class="text-center">International</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-gocash" href="http://www.globe.com.ph/gcash">
                                  <span class="icon icon-gcash"></span>
                                  <p class="text-center">GCash</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-personal-rewards" href="http://www.globe.com.ph/rewards">
                                  <span class="icon icon-rewards"></span>
                                  <p class="text-center">Rewards</p>
                              </a>
                          </li>
                           <li>
                              <a id="header-personal-entertainments" href="http://downloads.globe.com.ph/">
                                  <span class="icon icon-entertainment"></span>
                                  <p class="text-center">Entertainment</p>
                              </a>
                          </li>
                      </ul>
                    </div>
                    <div class=" nav-result3"></div>
                    <div class=" nav-result4"></div>
                    <div class=" nav-result5">
                      <ul class="sub-nav-list">
                          <li>
                              <a id="header-help&support-faq" href="http://www.globe.com.ph/help">
                                  <span class="icon icon-faq"></span>
                                  <p class="text-center">FAQs</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-phoneconfig" href="http://www.globe.com.ph/help/guide">
                                  <span class="icon icon-phoneconfig"></span>
                                  <p class="text-center">Phone Configuration</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-troubleshoot" href="http://www.globe.com.ph/help/troubleshooting">
                                  <span class="icon icon-troubleshoot"></span>
                                  <p class="text-center">Basic Troubleshooting</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-community" href="http://community.globe.com.ph">
                                  <span class="icon icon-community"></span>
                                  <p class="text-center">Ask the Community</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-storelocator" href="http://www.globe.com.ph/store-locator">
                                  <span class="icon icon-locator"></span>
                                  <p class="text-center">Store Locator</p>
                              </a>
                          </li>
                          <li>
                              <a id="header-help&support-contactus" href="http://www.globe.com.ph/contactus">
                                  <span class="icon icon-contactus"></span>
                                  <p class="text-center">Contact Us</p>
                              </a>
                          </li>
                                                    <li>
                              <a id="header-help&support-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">
                                  <span class="icon icon-livechat"></span>
                                  <p class="text-center">Chat</p>
                              </a>
                          </li>
                                                </ul>
                    </div>

                    <div class=" nav-result6">
                      <div class="container">
                        <div class="row">
                          <div class="col-xs-1 button-group-holder text-center">
                            <h2 class="h2-first">Enjoy more of Globe</h2>
                                                          <a id="header-myaccount-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999" id="login" class="btn btn-block">Login</a>
                              <hr>
                              <a id="header-myaccount-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999" id="signup" class="btn btn-block">Sign Up</a>
                                                        </div>
                          <div class="col-xs-6 want-info-holder">
                              <h2 class="h2-second"><span class="icon-myaccount"></span>I want to</h2>
                              <div class="row">
                                <div class="col-xs-5">
                                  <a id="header-myaccount-iwant1" href="http://www.globe.com.ph/enrolled-accounts"> <p>&#8226; View/edit my account details</p> </a>
                                  <a id="header-myaccount-iwant2" href="http://www.globe.com.ph/paybill"><p> &#8226; Pay my bill</p></a>
                                  <a id="header-myaccount-iwant3" href="http://www.globe.com.ph/paperlessreg"><p> &#8226; Sign up for paperless billing</p></a>
                                  <a id="header-myaccount-iwant4" href="http://www.globe.com.ph/autopayreg"><p> &#8226; Enroll in Auto Pay service</p></a>
                                  <a id="header-myaccount-iwant5" href="http://www.globe.com.ph/form-transfer-of-ownership" target="_blank"><p> &#8226; Transfer my account</p></a>
                                </div>
                                <div class="col-xs-7">
                                  <a id="header-myaccount-iwant6" href="https://mygcash.globe.com.ph/gcashonline"><p> &#8226; Access my GCash </p></a>
                                  <a id="header-myaccount-iwant7" class="modal-trigger" href="//www.globe.com.ph/form-check-activation-status"><p> &#8226; Check my line activation status</p></a>
                                  <a id="header-myaccount-iwant8" class="modal-trigger" href="//www.globe.com.ph/form-check-outstanding-balance"><p> &#8226; Check my postpaid outstanding balance</p></a>
                                  <a id="header-myaccount-iwant9" href="http://dbschenkerepod.com.ph/globe/customerview.php" target="_blank"><p> &#8226; Track the delivery status of my device/SIM</p></a>
                                  <a id="header-myaccount-iwant10" href="http://www.globe.com.ph/updateinfo" target="_blank"><p> &#8226; Update my account information </p></a>
                                </div>
                              </div>
                          </div>
                          <div class="col-xs-5 help-info-holder">
                            <h2 class="h2-third"><span class="icon-question"></span>Get help with</h2>
                              <a id="header-myaccount-gethelp1" href="http://www.globe.com.ph/help/guide"><p> &#8226; Configuring my phone</p></a>
                              <a id="header-myaccount-gethelp2" href="http://www.globe.com.ph/help/troubleshooting"><p> &#8226; Troubleshooting my device</p></a>
                              <a id="header-myaccount-gethelp3" href="http://www.globe.com.ph/help/roaming/about-international-roaming"><p> &#8226; Activating my international roaming</p></a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <script>
                      if($('#new-header .nav-container .nav-result6').is('.f1731'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"min-height": "initial !important"});
                      }
                      if($('#new-header .nav-container .nav-result6').is('.f1731x'))
                      {
                        $('#new-header .nav-container .nav-result6').css({"height": "172px"});
                      }
                    </script>

                  </div>
                </div>
              </div>
            </div>
                            <div id="logo-holder">
                <a href="http://shop.globe.com.ph/">
                  <img src="/content/dam/shoppingcart/globeLogo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
                </a>
              </div>
                        </div>
          
          <div class="col-lg-3 category-dropdown header-blocks">
              <a id="header-shop-by-category" class="btn fontsize24 btn-lg absolute-center dropdown-toggle " data-toggle="dropdown">
                Shop by Category <span class="glyphicon caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                              <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                              <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                              <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                            </ul>
          </div>

          <div class="col-lg-4 search-wrapper header-blocks ">
            <div class="search-inner-addon ">
              <form>
                <fieldset>
                  <img class="searchIcon" src="/content/dam/shoppingcart/icon-search-white.png">
                  <input id="searchbox" name="searchterms" type="search" class="form-control" placeholder="Search Here" maxlength="50" />
                </fieldset>
                <ul id="searchResult">
                   <li class="result-holder"></li>
                   <li class="plainText">We Recommend</li>
                   <li class="searchRecommend"></li>
                   <li class="see-all">See All</li>
                </ul>
            </form>
            </div>
          </div>

            <div class="search-account-wrapper">
              <div class="col-lg-1 cart-wrapper header-blocks" id="cart-header">
  <div class="btn-cart  cart-total-hide-empty">
    <a id="header-cart-btn" class="dropdown-toggle" data-toggle="dropdown" onclick="toggleCart()">
      <span class="badge custom-badge " id="cart-total">0</span>
      <img class="absolute-center cart-image" src="/content/dam/shoppingcart/icon-cart.png">
    </a>
        <ul class="dropdown-menu" id="cart-content" role="menu">
          <li>
        <h4 style="text-align: center; margin-top: 0px;">Your shopping cart is empty!</h4>
      </li>
        </ul>

    <ul class="dropdown-menu" id="cart-notif" role="menu" style="display: none;">
      <li>
        <div class="cart-item-thumbnail">
          <img src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/check-50x50.png" alt="" title="" />
        </div>
        <div class="cart-item-detail cart-item-detail-success">
          <h4>Item successfully added to cart</h4>
        </div>
      </li>
      <li>
        <div class="cart-btn-view">
        <a href="http://shop.globe.com.ph/cart" class="btn btn-block btn-bw-orange">View Cart</a>
        </div>
      </li>
    </ul>
  </div>
</div>
              <div class="col-lg-1 account-wrapper ">
                  
                                      <a id="header-account-signin-btn" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999">
                      <img class="" src="/content/dam/shoppingcart/icon-user2.png">
                      <span class="account-text">Sign In</span>
                    </a>
                                </div><!--End of account wrapper--> 
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="rwd-header-tablet" class="collapse hidden-xs-*">
  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
      <li class="dropdown">
        <a id="header-personal" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-shop" href="http://shop.globe.com.ph">Shop</a></li>
          <li><a id="header-postpaid" href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
          <li><a id="header-prepaid" href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
          <li><a id="header-internet" href="http://www.globe.com.ph/internet">Internet</a></li>
          <li><a id="header-international" href="http://www.globe.com.ph/international">International</a></li>
          <li><a id="header-gcash" href="http://www.globe.com.ph/gcash">GCash</a></li>
          <li><a id="header-rewards" href="http://www.globe.com.ph/rewards">Rewards</a></li>
          <li><a id="header-entertainment" href="http://downloads.globe.com.ph/">Entertainment</a></li>
        </ul>
      </li>
      <li><a id="header-sme" href="http://mybusiness.globe.com.ph/">SME</a></li>
      <li><a id="header-enterprise" href="http://business.globe.com.ph/">Enterprise</a></li>
      <li class="dropdown">
        <a id="header-help&support" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a id="header-faqs" href="http://www.globe.com.ph/help">FAQs</a></li>
          <li><a id="header-phoneconfig" href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
          <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
          <li><a id="header-community" href="http://community.globe.com.ph">Ask the Community</a></li>
          <li><a id="header-store-locator" href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
          <li><a id="header-contactus" href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                    <li><a id="header-livechat" data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                  </ul>
      </li>
      <li class="dropdown">
        <a id="header-myaccount" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
                      <li><a id="header-login" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999">Login</a></li>
            <li><a id="header-signup" href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999">Sign Up</a></li>
                  </ul>
      </li>
    </ul>
  </div>
</div>

<nav id="rwd-header" class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button class="btn dropdown-toggle rwd-header-dropdown-button" type="button" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="icon-menu"></span>
      </button>
              <div class="logo-holder">
          <a href="http://shop.globe.com.ph/">
            <img src="http://d11fuji4mn7bm2.cloudfront.net/media/data/logo.png" title="Globe Telecom Inc." alt="Globe Telecom Inc." />
          </a>
        </div>
              <div class="user-holder ">
            <div class="user-holder-indicator"></div>
                      <a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999">
              <img class="" src="catalog/view/theme/broadwaytheme/images/icons/icon-user2.png" />
            </a>
                  </div>

        <div class="cart-holder"></div>
        <script type="text/javascript">
          function headerRwdChangeLocation() {
            var winWidth = $(window).width();
            var isFirefox = typeof InstallTrigger !== 'undefined';
            if(isFirefox) {
              if(winWidth <= 950) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 950) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }

            else {
              if(winWidth <= 949) {
                $("#cart-header").appendTo('#rwd-header .cart-holder');
              }
              else if(winWidth > 949) {
                $('#cart-header').insertBefore("#new-header .account-wrapper");
              }
            }
            
            if(winWidth > 991) {
              $("#rwd-search").css('display', 'none');
            }
          }
          $(document).ready(function($) {
            $(window).resize(function() {
              headerRwdChangeLocation();
            });
            headerRwdChangeLocation();
          });
        </script>
        <div class="search-holder">
          <a href="" class="rwd-search-trigger">
            <img class="searchIcon hidden-xs hidden-sm hidden-md" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
            <img class="searchIconRwd visible-md-*" src="catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png">
          </a>
        </div>
    </div>
    <div id="rwd-navbar" class="navbar-collapse collapse">
      <button class="rwd-navbar-close" data-toggle="collapse" data-target="#rwd-navbar" aria-expanded="false" aria-controls="rwd-navbar">
        <span class="blue-close"></span>
      </button>
      <br />
      <ul class="nav navbar-nav">
        <li><a id="header-about-globe" href="http://www.globe.com.ph/about-globe">About Globe</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle open rwd-header-personal-trigger" data-toggle="dropdown" role="button" aria-expanded="false">Personal <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://shop.globe.com.ph">Shop</a></li>
            <li><a href="http://www.globe.com.ph/postpaid">Postpaid</a></li>
            <li><a href="http://www.globe.com.ph/prepaid">Prepaid</a></li>
            <li><a href="http://www.globe.com.ph/internet">Internet</a></li>
            <li><a href="http://www.globe.com.ph/international">International</a></li>
            <li><a href="http://www.globe.com.ph/gcash">GCash</a></li>
            <li><a href="http://www.globe.com.ph/rewards">Rewards</a></li>
            <li><a href="http://downloads.globe.com.ph/">Entertainment</a></li>
          </ul>
        </li>
        <li><a href="http://mybusiness.globe.com.ph/">SME</a></li>
        <li><a href="http://business.globe.com.ph/">Enterprise</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help & Support <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://www.globe.com.ph/help">FAQs</a></li>
            <li><a href="http://www.globe.com.ph/help/guide">Phone Configuration</a></li>
            <li><a href="http://www.globe.com.ph/help/troubleshooting">Basic Troubleshooting</a></li>
            <li><a href="http://community.globe.com.ph">Ask the Community</a></li>
            <li><a href="http://www.globe.com.ph/store-locator">Store Locator</a></li>
            <li><a href="http://www.globe.com.ph/contactus">Contact Us</a></li>
                        <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>
                      </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
                          <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999">Login</a></li>
              <li><a href="https://shop.globe.com.ph/account/login?redirect_url=http://shop.globe.com.ph/products/plan/mylifestyle-plan-999">Sign Up</a></li>
                      </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="rwd-search">
  <input id="rwd-searchbox"  type="search" class="form-control" placeholder="Search Here" maxlength="50" />
  <img id="rwd-search-trigger" src="catalog/view/theme/broadwaytheme/images/icons/icon-search.png" />
  <div class="rwd-search-results">
      <ul id="rwd-search-results-list">
         <li class="rwd-result-holder">
            <div class="text-result">test</div>
         </li>
         <li class="plain-text blue-line">WE RECOMMEND</li>
         <li class="img-result first-img-result"></li>
         <li class="plain-text rwd-search-see-all"><a>See All</a></li>
      </ul>
  </div> <!-- End of rwd-search-results-recommended -->
</div>

<nav id="rwd-sub-header" class="navbar navbar-default ">
  <div class="container rwd-sub-header-medium">
    <div class="navbar-header">
      <div class="menu-item">
        <a type="button" data-toggle="collapse" data-target="#rwd-sub-navbar" aria-expanded="false" aria-controls="rwd-sub-navbar">
          Shop by Category <span class="caret"></span>
        </a>
      </div>
      <a class="dot-menu" type="button" data-toggle="collapse" data-target="#rwd-sub-navbar-2" aria-expanded="false" aria-controls="rwd-sub-navbar-2">
        <img src="catalog/view/theme/broadwaytheme/images/icons/dot-menu.png">
      </a>
    </div>
    <div id="rwd-sub-navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                  <li class=""><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
              </ul>
    </div>
    <div id="rwd-sub-navbar-2" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>


                          <li><a href="http://shop.globe.com.ph/contact">Request a call</a></li>

                          <li><a href="tel:+027301010">(02) 730-1010</a></li>
                                    </ul>
    </div>
  </div>
  <div class="container rwd-sub-header-small">
    <div class="collapse navbar-collapse sub-header-sm" id="bs-example-navbar-collapse-7">
      <ul class="nav navbar-nav">
        <li role="presentation" class="dropdown offset">
            <a class="link" href="#" data-toggle="dropdown">
              Shop by Category <span class="glyphicon caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                          <li><a href="http://shop.globe.com.ph/shop-by-device">Shop by Device</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/phones">Phones</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/tablets">Tablets</a></li>
                          <li><a href="http://shop.globe.com.ph/gadgets-and-accessories">Shop by Accessories</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-plan">Shop by Plan</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-brand">Shop by Brand</a></li>
                          <li><a href="http://shop.globe.com.ph/shop-by-device/great-gadget-sale">Great Gadget Sale</a></li>
                        </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right offset-right">
                                            <li><a data-toggle="modal" data-target="#chatModal" class="btn-chat">Chat</a></li>


                          <li><a href="http://shop.globe.com.ph/contact">Request a call</a></li>

                          <li><a href="tel:+027301010">(02) 730-1010</a></li>
                                    </ul>
    </div>
  </div>
</nav>

<script type="text/javascript">



  var globalVar = [];

  $('.menu-list button').click(function(){
    hideAllNavResult();
  });

  //make personal default
  $(".header-blocks #dropdown-button").on('click', function(){
    $(".header-blocks #header-personal").trigger('mouseenter');
    $(".header-blocks .dropdown-menu .nav-list #2").addClass('header-blocks-active-link');
  });

  $(".header-blocks .dropdown-menu .nav-list").on('mouseenter', function(){
    $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
  });

  $("#rwd-header .rwd-header-dropdown-button").on('click', function(){
    setTimeout(function(){
      $("#rwd-header .rwd-header-personal-trigger").trigger('click');
    },500);
  });

  $('.nav-list li').hover(function( event ) {
    if(this.id == 1 || this.id == 3 || this.id == 4) {

    } else {
      event.stopPropagation();
      hideAllNavResult();
      $('.nav-result' + this.id).show();
      if(this.id !== 2) {
         $(".header-blocks .dropdown-menu .nav-list #2").removeClass('header-blocks-active-link');
      } 
    }

  });

  function hideAllNavResult(){
    $('.nav-result1').hide();
    $('.nav-result2').hide();
    $('.nav-result3').hide();
    $('.nav-result4').hide();
    $('.nav-result5').hide();
    $('.nav-result6').hide();
  }

  //Input types
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  $('input[name=\'searchterms\']').on('keyup', function(e){
    var value = $(this).val();
    if(value== ''){
      $('#searchResult').slideUp();
    }
    $('#rwd-searchbox').val(value);
  }); 

  //Click of search icon
  $('.searchIcon').click(function(e){
    var value = $('input[name=\'searchterms\']').val();
    if(value != "Search Here") {
      searchAction(value);
    }
  });

  //Focus on searchbox
  $('#searchbox').focusin(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search.png");
  });

  $('#searchbox').focusout(function(){
    $(".searchIcon").attr("src","catalog/view/theme/broadwaytheme/images/icons/icon-search-white.png");
  });

  //Search for rwd
  $('#rwd-search-trigger').click(function(e){
    e.preventDefault();
    var value = $("#rwd-searchbox").val();
    searchAction(value);
  });

  //autocomplete for rwd 

  $('#rwd-searchbox').autocomplete({
    minLength: 2,
    delay: 500,
    source: function(request, response) {

      $.ajax({
        url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
        dataType: 'json',
        beforeSend : function() {
          $('.rwd-search-results .rwd-result-holder').html('');
        },
        success: function(json) {
          html = '';
          var html_count = 0;
          response($.map(json.terms, function(item) {
            html += '<div class="text-result"><a href="' + item.redirect_url + '">' + item.query + '</a></div>';
            html_count +=1;
          }));

           $('.rwd-search-results .rwd-result-holder').html(html);
           $('.rwd-search-results .rwd-result-holder').css('word-wrap','break-word');

          var html_images = '';
          var html_images_count = 0;
          response($.map(json.images, function(item) {
            html_images += '<div class="rwd-imgResult"><a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="color: #333333;">' + item.name + '</a></div></div>';
            html_images_count +=1;
          }));

          if(html_images_count == 0){
            $(".rwd-search-results .rwd-search-see-all").css('display', 'none');
            $(".rwd-search-results .plain-text").css('display', 'none');
            $("#rwd-search-results-list").css('background-color', 'white');
          } else {
            $(".rwd-search-results .rwd-search-see-all").css('display', 'block');
            $(".rwd-search-results .plain-text").css('display', 'block');
            $("#rwd-search-results-list").css('background-color', '#f1f1f1');
          }

          if(html_count ==0 && html_images_count == 0) {
            $('.rwd-search-results .rwd-result-holder').html('<li class="text-result">No Search Results</li>');
          }

          $('.rwd-search-results .img-result').html(html_images);
          $(".rwd-search-results .rwd-search-see-all").html('<a href="'+json.see_all_link+'">See All</a>');
        }
      });
    },
    open: function() {
        $('.ui-autocomplete').css({
          visibility: "hidden"
        });
    },
    select: function(event, ui) {
      return false;
    },
    focus: function(event, ui) {
        return false;
     },
    search: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results .img-result').html('');
    },
    close: function( event, ui ) {
      $('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    },
    response: function( event, ui ) {
      $('.rwd-search-results').slideDown();
    }
  });

  //added on enter of search box
  $('#rwd-searchbox').on('keydown', function(e){
    if(e.keyCode == 13) {
      var value = $("#rwd-searchbox").val();
      searchAction(value);
    }
  });

  $('#rwd-searchbox').on('keyup input', function(e){
    var value = $("#rwd-searchbox").val();
    $('input[name=\'searchterms\']').val(value);

    // slide up if no value is seen
    if($("#rwd-searchbox").val() == "") {
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
    }
  });

  //Empty input on blur of input 
  $('#rwd-searchbox').on('blur', function(e){
    setTimeout(function(){
      //$('.rwd-search-results .rwd-result-holder').html('');
      $('.rwd-search-results').slideUp();
      //$('#rwd-searchbox').val('');
      },500)
  }); 


  //Empty input on blur of input 
  $('input[name=\'searchterms\']').on('blur', function(e){
      //$('.searchItems').html('');
      //#1417
      setTimeout(function(){
        // $('#searchResult').slideUp();
      },500);
      var ifIE = msieversion();

      setTimeout(function(){
        var testPlaceholder = document.createElement("input");
        if(testPlaceholder.placeholder !== void 0 && !ifIE) {
          //$('input[name=\'searchterms\']').val('');
        }
      },1000)
  }); 

  function msieversion() {

      var ua = window.navigator.userAgent;

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
          return true;
          // IE 10 or older => return version number
          //return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {

          // IE 11 => return version number
          var rv = ua.indexOf('rv:');
          return true;
          //return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
          return true;
         // IE 12 => return version number
         //return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      }

      // other browser
      return false;
  }
  i=0;
  $('input[name=\'searchterms\']').on('keydown', function(e){
    var value = $(this).val();
    if(e.keyCode == 13) {
      e.preventDefault();
       $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
    }
  });    

  //General function for search - not used in input because of preventdefault in input
  function searchAction(value)
  {
    $.ajax({
        url: 'index.php?route=product/search/redirect_url&filter_query=' +  encodeURIComponent(value),
        dataType: 'json',
        success: function(json) {
          var decoded = $('<div/>').html(json.redirect_url).text();
          window.location.href = decoded;
        }
      });
  }

  function keyPressAutocomplete(html) {
    var $listItems = $('#searchResult .textResult');
    $('#searchbox').keydown(function(e) {
      var key = e.keyCode,
        $selected = $listItems.filter('.selected'),
        $current;

        if ( key != 40 && key != 38 ) return;
        $listItems.removeClass('selected');

        if ( key == 40 ) {
          if ( ! $selected.length || $selected.is(':last-child') ) {
            $current = $listItems.eq(0);
          }
          else {
              $current = $selected.next();
          }
          $current.addClass('selected');
        }
    });
  }

  $('input[name=\'searchterms\']').autocomplete({
    appendTo: ".result-holder",
    minLength: 2,
    delay: 500,
    source: function(request, response) {

     $.ajax({
      url: 'index.php?route=product/search/autocomplete_terms&filter_query=' +  encodeURIComponent(request.term),
      dataType: 'json',
      minLength: 1,
      success: function(json) {
          html_images = '';
          response($.map(json.images, function(item) {
          //html = ''
          html_images += '<li class="\imgResult" style="vertical-align: top;">';
            html_images += '<a href="'+item.href+'"><img src="' + item.image + '"></a>';
            html_images += '<div align="center"><a href="'+item.href+'" style="font-size: 12px; color: #333333;">' + item.name + '</a></div>';
            html_images += '</li>';

        }));

        response($.map(json.terms, function(item2) {

          $('.searchRecommend').html(html_images);

          a = [{
            label : item2.query,
            value: item2.query,
            url: item2.redirect_url,
            image: html_images


          }];
          return a;
        }));

        $("#searchResult .see-all").html('<a href="'+json.see_all_link+'">See All</a>');
      },
    });

    },
    open: function(event, ui) {
      $('#searchResult .result-holder .ui-autocomplete').attr('style', 'left:-14px!important;display:block;border:0px !important;');
      $('#searchResult').show().addClass('displayed');
      console.log('open search');
      $('#searchResult').slideDown();
      $('#searchResult .result-holder .ui-autocomplete li').attr('style','background:#ffffff;font-weight:normal !important;border:0px;font-size:15px;font-family:fs_elliot_proregular,Arial,sans-serif!important;color:#333333!important');
      $('#searchResult .result-holder .ui-autocomplete li').hover(function(){
        $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');

      });

    },
    focus: function(event, ui) {
      $('#searchResult .ui-autocomplete .ui-menu-item.ui-state-focus').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
       $(this).attr('style','background:#ffffff;font-weight:normal;border:0px !important;color:#1953a5;font-family:fs_elliot_proregular,Arial,sans-serif!important;font-size:15px;margin:0px!important;');
       $('#searchResult .ui-autocomplete .ui-menu-item a').attr('style','color:#333333!important');
       $(this).children('a').attr('style','color:#1953a5!important');
      });
    },
    select: function(event, ui) {
      /*$('#searchResult .ui-autocomplete .ui-menu-item').each(function(){
       $(this).html('<a href="' + ui.item.url + '">' + ui.item.label + '</a>');
      });*/
    }
  });

  //show dropdown signout
  $(".account-signout-trigger").on('click', function(){
    if($('.account-signout-dropdown').css('display') == "none"){
      $('.account-signout-dropdown').show();
    } else {
      $('.account-signout-dropdown').hide();
    }

  });
  //rwd of account-sigout-trigger above
  $(".user-holder").on('click', function(){

    if($('.rwd-account-signout-dropdown').css('display') == "none"){
      $('.rwd-account-signout-dropdown').show();
    } else {
      $('.rwd-account-signout-dropdown').hide();
    }

  });
  var elemt = '';
  $(document).on('click', function(e){
    if($(e.target).closest('.search-inner-addon').length == 0){
      setTimeout(function(){
        if($('#searchResult').is(':visible'))
          $('#searchResult').slideUp();
      },200);
    }
  });
</script>
<div id="container" class="margTop">

<!-- BREADCRUMBS START -->
<div class="container-fluid breadcrumb-container headerNav" itemscope itemtype="http://schema.org/BreadcrumbList">
  <div class="container">
    <div class="row">
      <div class="breadcrumb pull-left">
                <div></div>
        <div id="breadcrumb-0" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/" itemprop="item">
            <span class="blue-link" itemprop="name">Shop Home</span>
            <meta itemprop="position" content="1" />
          </a>
        </div>
                <div> | </div>
        <div id="breadcrumb-1" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <a href="http://shop.globe.com.ph/products/plan/mylifestyle-plan-999" itemprop="item">
            <span class="black-link" itemprop="name">myLifestyle Plan 999</span>
            <meta itemprop="position" content="2" />
          </a>
        </div>
              </div>
      <div class="chat-call-holder pull-right hidden-xs hidden-sm">
                                            <a data-toggle="modal" data-target="#chatModal" class="btn btn-bw-clear btn-chat">Chat</a>


                          <a href="http://shop.globe.com.ph/contact" class="btn btn-bw-clear">Request a call</a>

                          <a href="tel:+027301010" class="btn btn-bw-clear">(02) 730-1010</a>
                                    </div>
    </div>
  </div>
</div>
<!-- BREADCRUMBS END -->

<div id="header-fold-wrap">
  <!-- INSERT HEADER FOLD CONTENT HERE -->
  </div>

<div itemscope itemtype="http://schema.org/Product">
  <meta itemprop="brand" content="Globe">

<div id="notification"></div>

  <!-- Modal FULL SCREEN -->
<div class="responsive-modal container pdp-social-container">
  <div class="modal fade text-center" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a target="_blank" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999"><span class="social-facebook"></span></a>
        <a target="_blank" href="http://twitter.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999&text=myLifestyle Plan 999 - Postpaid  - Globe Online Shop"><span class="social-twitter"></span></a>
        <a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999"><span class="social-gplus"></span></a>
        <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fshop.globe.com.ph%2Fproducts%2Fplan%2Fmylifestyle-plan-999"><span class="social-linkedin"></span></a>
        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="social-pinterest"></span></a>
        <a href="" data-dismiss="modal"><span class="big-close"></span></a>
      </div>
    </div>
  </div>
</div>

<div class="pdp-container hidden-xs pdp-container-plan-only">

  <section class="mainProducts pdp">
      <div class="container">
          <div class="row">
              <div class="col-xs-8 col-sm-7 col-md-7 col-lg-8">

                  <div class="col-xs-4 col-sm-6 col-md-6 col-lg-4 productImageCol">
                      <div class="pdp-image-container text-center">
                        <div class="pdp-images w100">
                                                      <img class="imageZoom" src="${properties.fileReference}"  title="myLifestyle Plan 999" alt="myLifestyle Plan 999" />
                                                  </div>
                        <div class="rotationButton">
                                                      <a class="slider-btn" onclick="showImage(this)" data-thumb="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-300x400.png" data-popup="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/myLifestyle Plan/plan999-plainbutton-800x800.png">&nbsp;&nbsp;&nbsp;</a>

                        </div>
                      </div>
                      <div class="cta-group">

                        <div class="share-this">
                            <a id="pdp-sharethis" href="#" data-toggle="modal" data-target="#myModal"><span class="icon-share"></span>Share This</a>
                          </div>
                      </div>
                  </div>
                 <div class="col-xs-8 col-sm-6 col-md-6 col-lg-8 productInfo">
                      <div class="productContent">
                        <div class="product-info-badge-holder">
                                                  </div>
                        <h1 class="title-bw-light"><span itemprop="name">myLifestyle Plan 999</span></h1>
                        <div class="pdp-description">
                            <span itemprop="description"><p><span style="font-family: fs_elliot_proregular, Helvetica; font-size: 1em; line-height: normal;">${properties.para}</span></p>
<script>
  $('#myTabs').click(function (event) {
	$("#tab-0").css('display', 'none');
    $("#tab-1").css('display', 'block');
    $('div.slick-track a.fontsize24:contains("Plan")').removeClass("selectedTab selected");
    $('div.slick-track a.fontsize24:contains("Promos")').addClass("selectedTab selected");
    $('div.slick-slide:contains("Plan")').toggleClass("slick-active");
    $('div.slick-slide:contains("Promos")').toggleClass("slick-active");
  });
</script>

<h2 class="pdesc" style="font-size: 13px; line-height: 20.7999992370605px; color: rgb(0, 0, 139);">Online orders are for new and additional line applications only. You can also order this device by calling our Sales Hotline at (02) 730-1010, open Monday to Sunday from 6AM to 10PM. During the call, please enter the following promo code when prompted: 21211. For existing subscribers who wish to recontract, kindly call (02) 730-1300. This plan is available at <a href="http://www.globe.com.ph/store-locator" target="_blank">Globe stores</a> so visit the one near you!</h2>
<style type="text/css">.pdesc{
    font: 1em fs_elliot_proregular, Helvetica; }
</style>
</span>
                        </div>
                      </div><div class="productVariation productVariationParent">
                        <div class="options">
                          <div id="product-select" class="option storage-variation-edit pull-left optionFields remWi" style="">

                            <div class="dropdown dropdown-plan-select">
                              <button class="btn btn-default dropdown-toggle attrHver " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><div style="padding-right: 5px;">

                                <span class="drpdwn iPadV">Samsung Galaxy S6 edge</span><span class="drpdwn desktopV">Samsung Galaxy S6 edge</span>
                                                                <span class="caret caretx"></span>
                                                                </div>
                              </button>
                                                            <ul class="dropdown-menu dropdown-attributes-menu attrHverLI" data-id="plan-product" role="menu" aria-labelledby="dropdownMenu1" style="index: 99;">
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S6 edge" data-id="134">Samsung Galaxy S6 edge</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S6" data-id="138">Samsung Galaxy S6</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy Note 5" data-id="309">Samsung Galaxy Note 5</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S6 edge+" data-id="311">Samsung Galaxy S6 edge+</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="BlackBerry Classic" data-id="358">BlackBerry Classic</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone 6s" data-id="378">iPhone 6s</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone 6s Plus" data-id="380">iPhone 6s Plus</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Cloudfone Thrill 500x" data-id="394">Cloudfone Thrill 500x</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J1 2016" data-id="907">Samsung Galaxy J1 2016</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei G8" data-id="909">Huawei G8</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S7" data-id="911">Samsung Galaxy S7</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy S7 edge" data-id="913">Samsung Galaxy S7 edge</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Lenovo A6000 + Bluetooth Speaker" data-id="975">Lenovo A6000 + Bluetooth Speaker</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei Y6 Pro" data-id="1016">Huawei Y6 Pro</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Cherry Mobile Flare Lite 2 + Superion Vector Bundle" data-id="1034">Cherry Mobile Flare Lite 2 + Superion Vector Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei GR3 + B0 Watch" data-id="1040">Huawei GR3 + B0 Watch</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei Mate 8" data-id="1079">Huawei Mate 8</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Disney Mobile Frozen" data-id="1081">Disney Mobile Frozen</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Disney Mobile Mickey" data-id="1083">Disney Mobile Mickey</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Disney Mobile Princess" data-id="1085">Disney Mobile Princess</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone SE" data-id="1089">iPhone SE</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Asus Zenfone Selfie" data-id="1096">Asus Zenfone Selfie</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Oppo Neo 5s" data-id="1102">Oppo Neo 5s</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="LG G5 LTE + LG Cam Plus Bundle" data-id="1104">LG G5 LTE + LG Cam Plus Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J1 2016 + Tab 3 V Bundle" data-id="1108">Samsung Galaxy J1 2016 + Tab 3 V Bundle</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Lenovo Vibe K4 Note + Virtual Reality Headset" data-id="1157">Lenovo Vibe K4 Note + Virtual Reality Headset</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="iPhone 6 Whitebox" data-id="1199">iPhone 6 Whitebox</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J5 2016" data-id="1225">Samsung Galaxy J5 2016</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy J7 2016" data-id="1227">Samsung Galaxy J7 2016</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Huawei P9" data-id="1463">Huawei P9</a></li>
                                                                  <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-product" data-value="Samsung Galaxy Note7" data-id="1497">Samsung Galaxy Note7</a></li>
                                                              </ul>
                                                          </div>

                            <select name="product_id" id="product-plan-select" data-id="plan-product" class="storage-size updateProduct collapse">
                                                            <option data-id="Samsung Galaxy S6 edge" value="134" selected="selected">Samsung Galaxy S6 edge</option>
                                                            <option data-id="Samsung Galaxy S6" value="138">Samsung Galaxy S6</option>
                                                            <option data-id="Samsung Galaxy Note 5" value="309">Samsung Galaxy Note 5</option>
                                                            <option data-id="Samsung Galaxy S6 edge+" value="311">Samsung Galaxy S6 edge+</option>
                                                            <option data-id="BlackBerry Classic" value="358">BlackBerry Classic</option>
                                                            <option data-id="iPhone 6s" value="378">iPhone 6s</option>
                                                            <option data-id="iPhone 6s Plus" value="380">iPhone 6s Plus</option>
                                                            <option data-id="Cloudfone Thrill 500x" value="394">Cloudfone Thrill 500x</option>
                                                            <option data-id="Samsung Galaxy J1 2016" value="907">Samsung Galaxy J1 2016</option>
                                                            <option data-id="Huawei G8" value="909">Huawei G8</option>
                                                            <option data-id="Samsung Galaxy S7" value="911">Samsung Galaxy S7</option>
                                                            <option data-id="Samsung Galaxy S7 edge" value="913">Samsung Galaxy S7 edge</option>
                                                            <option data-id="Lenovo A6000 + Bluetooth Speaker" value="975">Lenovo A6000 + Bluetooth Speaker</option>
                                                            <option data-id="Huawei Y6 Pro" value="1016">Huawei Y6 Pro</option>
                                                            <option data-id="Cherry Mobile Flare Lite 2 + Superion Vector Bundle" value="1034">Cherry Mobile Flare Lite 2 + Superion Vector Bundle</option>
                                                            <option data-id="Huawei GR3 + B0 Watch" value="1040">Huawei GR3 + B0 Watch</option>
                                                            <option data-id="Huawei Mate 8" value="1079">Huawei Mate 8</option>
                                                            <option data-id="Disney Mobile Frozen" value="1081">Disney Mobile Frozen</option>
                                                            <option data-id="Disney Mobile Mickey" value="1083">Disney Mobile Mickey</option>
                                                            <option data-id="Disney Mobile Princess" value="1085">Disney Mobile Princess</option>
                                                            <option data-id="iPhone SE" value="1089">iPhone SE</option>
                                                            <option data-id="Asus Zenfone Selfie" value="1096">Asus Zenfone Selfie</option>
                                                            <option data-id="Oppo Neo 5s" value="1102">Oppo Neo 5s</option>
                                                            <option data-id="LG G5 LTE + LG Cam Plus Bundle" value="1104">LG G5 LTE + LG Cam Plus Bundle</option>
                                                            <option data-id="Samsung Galaxy J1 2016 + Tab 3 V Bundle" value="1108">Samsung Galaxy J1 2016 + Tab 3 V Bundle</option>
                                                            <option data-id="Lenovo Vibe K4 Note + Virtual Reality Headset" value="1157">Lenovo Vibe K4 Note + Virtual Reality Headset</option>
                                                            <option data-id="iPhone 6 Whitebox" value="1199">iPhone 6 Whitebox</option>
                                                            <option data-id="Samsung Galaxy J5 2016" value="1225">Samsung Galaxy J5 2016</option>
                                                            <option data-id="Samsung Galaxy J7 2016" value="1227">Samsung Galaxy J7 2016</option>
                                                            <option data-id="Huawei P9" value="1463">Huawei P9</option>
                                                            <option data-id="Samsung Galaxy Note7" value="1497">Samsung Galaxy Note7</option>
                                                          </select>
                            <input type="hidden" id="producthidden" name="product_hidden" value="134">

                            <input type="hidden" id="product-hidden-val" value="134">
                            <input type="hidden" id="product-hidden-label" value="Samsung Galaxy S6 edge">
                            <input type="hidden" id="product-hidden-val-orig" value="134">
                            <input type="hidden" id="product-hidden-label-orig" value="Samsung Galaxy S6 edge">
                          </div>

                          <div id="plan-product-options"><div class="productVariation">  
                              <div class="options"><div id="option-275" class="option storage-variation-edit pull-left optionFields">
                                  <input type="hidden" id="optionhidden_275" name="optionhidden[275]" value="525"></div>
                                  <div id="prepaid_kit"><input type="hidden" class="myLifestyle Plan 999" id="optionhidden_275" name="option[275]" value="525"></div>
                                  <div id="option-278" class="option storage-variation-edit pull-left optionFields">
                                      <input type="hidden" id="optionhidden_278" name="optionhidden[278]" value="531"></div>
                                  <div id="prepaid_kit"><input type="hidden" class="24 months" id="optionhidden_278" name="option[278]" value="531"></div>
                                  <div id="option-276" class="option storage-variation-edit pull-left optionFields remWi">
                                      <div class="dropdown dropdown-276"><button class="btn btn-default dropdown-toggle attrHver " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                          <div style="padding-right: 5px;"><span class="drpdwn">White Pearl</span><span class="caret caretx"></span></div></button>
                                          <ul class="dropdown-menu dropdown-attributes-menu attrHverLI" role="menu" aria-labelledby="dropdownMenu1" style="index: 99;">
                                              <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="Gold Platinum" data-id="535">Gold Platinum</a></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="Emerald Green" data-id="707">Emerald Green</a></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="Black Sapphire" data-id="537">Black Sapphire</a></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="276" data-value="White Pearl" data-id="536">White Pearl</a></li></ul></div>
                                      <select name="option[276]" id="optionSelect-276" data-id="276" class="storage-size updateVal collapse">
                                          <option data-id="Gold Platinum" value="535">Gold Platinum</option>
                                          <option data-id="Emerald Green" value="707">Emerald Green</option>
                                          <option data-id="Black Sapphire" value="537">Black Sapphire</option>
                                          <option data-id="White Pearl" value="536" selected="selected">White Pearl</option></select>
                                      <input type="hidden" id="optionhidden_276" name="optionhidden[276]" value="536">
                                      <input type="hidden" id="option-hidden-val-276" value="536">
                                      <input type="hidden" id="option-hidden-label-276" value="White Pearl">
                                      <input type="hidden" id="option-hidden-val-orig-276" value="536">
                                      <input type="hidden" id="option-hidden-label-orig-276" value="White Pearl"></div>
                                  <div id="option-text-276" class="pull-left" style="margin-top: 2%; width: 50%; display:none;">
                                      <div class="storage-label label label-default">White Pearl</div>
                                      <div class="change-btn"><a class="color" onclick="changeBtn(276)">Change</a></div></div>
                                  <div id="option-277" class="option storage-variation-edit pull-left optionFields remWi">
                                      <div class="dropdown dropdown-277"><button class="btn btn-default dropdown-toggle attrHver " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                          <div style="padding-right: 5px;"><span class="drpdwn">64 GB</span><span class="caret caretx"></span></div></button>
                                          <ul class="dropdown-menu dropdown-attributes-menu attrHverLI" role="menu" aria-labelledby="dropdownMenu1" style="index: 99;">
                                              <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="277" data-value="32 GB" data-id="529">32 GB</a></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" class="attribute-option" data-parentid="277" data-value="64 GB" data-id="530">64 GB</a></li></ul></div>
                                      <select name="option[277]" id="optionSelect-277" data-id="277" class="storage-size updateVal collapse">
                                          <option data-id="32 GB" value="529">32 GB</option><option data-id="64 GB" value="530" selected="selected">64 GB</option></select>
                                      <input type="hidden" id="optionhidden_277" name="optionhidden[277]" value="530">
                                      <input type="hidden" id="option-hidden-val-277" value="530">
                                      <input type="hidden" id="option-hidden-label-277" value="64 GB">
                                      <input type="hidden" id="option-hidden-val-orig-277" value="530">
                                      <input type="hidden" id="option-hidden-label-orig-277" value="64 GB"></div>
                                  <div id="option-text-277" class="pull-left" style="margin-top: 2%; width: 50%; display:none;">
                                      <div class="storage-label label label-default">64 GB</div><div class="change-btn"><a class="color" onclick="changeBtn(277)">Change</a></div>
                                  </div>
                              </div>
                              </div>
                            </div>


                                                                                                     

                        </div>
                      </div>

       
                  </div>
				 </div>
                            <div class="col-xs-4 col-sm-5 col-md-5 col-lg-4 heroCol">
                  <div id="div-prepaid">
                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                      <link itemprop="availability" href="http://schema.org/InStock" /><span style="display: none;">IN STOCK</span>
                      <meta itemprop="priceCurrency" content="PHP">
                        <span style="display: none;">PHP </span>
                        <span itemprop="price" style="display: none;">999.00</span> 

                      <div class="heroOffer text-center heroUpdateWd">
                          <div id="plan-loading"></div>
                          <div class="stock-level"></div>
                          <div class="hero-offer-title"></div>
                          <div class="cashout"></div>
                          <div class="add-cart-btn-orange-container" id="prepaid_kit">
                            <input type="hidden" name="quantity" value="1" />

                            <div id="prepaid-hidden"></div>

                            <button id="hero-offer-addtocart-btn" class="btn btn-lg btn-addcart btn-bw-orange button-cart" data-id="prepaid_kit" style="display: none;">ADD TO CART</button>
                          </div>
                          <div class="hero-offer-details" id="prepaid-details"></div>
                      </div>
                    </div>
                  </div>
              </div>
                        
    </section>
    <section class="sub-menu-tabs">
    <div class="container arrowPadd">
       <div class="navbar-collapse">
          <div id="tabs" class="htabs nav navbar-nav nav-center slick-initialized slick-slider" align="center">
                          <div class="slick-list draggable" tabindex="0">
                              <div class="slick-track" style="opacity: 1; width: 460px; transform: translate3d(0px, 0px, 0px);">
                                  <div class="slick-slide slick-active" style="width: 230px;" index="0">
                                      <a class="font-bold key-tab fontsize20 selectedTab selected" href="#tab-0">${properties.caption}</a></div>
                                  <div class="slick-slide slick-active" style="width: 230px;" index="1">
                                      <a class="font-bold key-tab fontsize20" href="#tab-1">myLifestyle Plan</a>
                                  </div>
                              </div>
             				</div>

          </div>
      </div>
    </div>
  </section>
    <section class="productTab">
            <div  id="tab-0" class="tab-content container">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <div class="container overview__mylifestyleplan">


<div id="container"><img alt="myLifestyle Plan 999" class="img-responsive" src="/content/dam/shoppingcart/banner_999.jpg" />&nbsp;
<p>Stay connected with your pals—online or offline—with myLifestyle Plan 999. Get your daily dose of chitchat and LOL from your BFFs with unlimited calls and texts to Globe and TM, mobile Internet, and other Lifestyle Packs. You even get to choose among Navigation, Explore, and Fitness Packs as your freebie for one (1) month, plus one (1) month Gadget Care subscription and one (1) GB Globe Cloud storage for 24 months—all FREE when you apply for myLifestyle Plan 999!</p>
&nbsp;

<div class="row">
<div class="col-md-4">
<div id="wrap"><img class="left" height="25px" src="/content/dam/shoppingcart/wifi.png" width="25px" />
<p>Surf your favorite sites or download the trending games and apps with <b>GoSURF</b>.</p>
</div>

<div id="wrap"><img class="left" height="25px" src="/content/dam/shoppingcart/chatpack.png" width="25px" />
<p>Spread good vibes and brighten up someone’s day with <b>Chat Pack</b> using the cutest stickers and emoticons.</p>
</div>
</div>

<div class="col-md-4">
<div id="wrap"><img class="left" height="25px" src="/content/dam/shoppingcart/navigation.PNG" width="25px" />
<p>Keep track of the weather, roads, and destinations or discover fun ways of staying healthy with your <b>choice between a FREE Navigation Pack, Explore Pack, or Fitness Pack for one month</b>.</p>
</div>

<div id="wrap"><img class="left" height="25px" src="/content/dam/shoppingcart/spotify_icon2.png" width="25px" />
<p>Find your rhythm or create your own playlist from the 30 million songs on <b>Spotify</b>.</p>
</div>
</div>

<div class="col-md-4">
<div id="wrap"><img class="left" height="25px" src="/content/dam/shoppingcart/gadget_care.png" width="25px" />
<p>Stay worry-free when you protect your device from accidental damage or theft with <b>Gadget Care</b>.</p>
</div>

<div id="wrap"><img class="left" height="25px" src="/content/dam/shoppingcart/globe_cloud.png" width="25px" />
<p>Keep your important office and personal files handy, and access them wherever, whenever with <b>Globe Cloud</b>.</p>
</div>
</div>
</div>
&nbsp;

<p>With myLifestyle Plan 999, you can experience life's best moments and share them with your friends. Plus, you can even customize your own Postpaid Plan to match the way you live for as low as P999 per month.</p>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
              <div  id="tab-1" class="tab-content container">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-12">
                  <div class="container overview__mylifestyleplan">

<div class="col-sm-12">
<h2>Imagine a plan that's built for the way you live now. With the new myLifestyle Plan, everything that matters to you everyday comes easy.</h2>
</div>

<div class="col-sm-6">
<p>Your base plan comes with built-in unlimited calls and texts to Globe/TM, starting at P499.</p>

<p>+ Add&nbsp;<b>Surf Packs*</b>&nbsp;for your mobile internet needs.<br />
+ Choose among&nbsp;<b>Lifestyle Packs</b>&nbsp;that are perfect for you!<br />
+ Get extra consumables, calls and texts to other networks, to landline, and abroad with&nbsp;<b>Classic Packs.</b></p>

<h3>The more packs you add to your Plan, the better handset you get!</h3>
</div>

<div class="col-sm-6 text-center"><iframe frameborder="0" src="https://www.youtube.com/embed/DctvEAnal6k"></iframe></div>

<div class="col-sm-12">&nbsp;
<p><small>*Minimum GoSURF 99 required if availing of handset. To modify the Promo Packs that come with our preset plans, complete your order and an Online Sales agent will be in touch to help you build your perfect plan. To learn more about the Promo Packs, visit our <a href="http://www.globe.com.ph/mylifestyleplan-info" rel="nofollow" style="font-size:14px;" target="_blank" title="Click here to learn more about the different myLifestyle Plan Surf, Lifestyle and Classic Packs!">myLifestyle Plan</a> info page.</small></p>
</div>
</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
  </section>

  <section class="seoContent">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  </div>
      </div>
    </div>
  </section>

   <section class="crossSell">
    <h1 class="text-center bw-subtitle-sm">FREQUENTLY BOUGHT TOGETHER</h1>
    <div class="container" id="crossSell">
      <div class="row">
        <div class="col-sm-12 col-md-8">
          <div class="itemHolder">

            <div class="freqItem" align="center">
              <div id="imageHolder0" class="image-holder">
                <span class="icon-tick"></span>
                <img class="" id="hero_offer_fb_image_0" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Samsung/Galaxy S6 Edge Plus/Galaxy-S6-edge-plus-front-Silver-150x150.png" border="0">
              </div>
              <div class="content-holder text-center">
                <h3><a href="http://shop.globe.com.ph/products/mobile/samsung-galaxy-s6-edge" id="hero_offer_fb_url_0" class="fontsize24"><span id="hero_offer_fb_name_0">Samsung Galaxy S6 edge</span></a></h3>
                <p id="hero_offer_fb_price_0">PHP 25,299.00</p>
                
                <input type="hidden" id="hero_offer_fb_add_0" name="addcart[0]" value="0">
                <input type="hidden" id="hero_offer_fb_priceval_0" name="price[0]" value="25299.00">
                <input type="hidden" id="hero_offer_fb_productid_0" name="product_id[0]" value="134">
                <input type="hidden" id="hero_offer_fb_minimum_0" name="quantity[0]" size="2" value="1">

                <div id="hero_offer_fb_options_0"><input type="hidden" name="option[0][275]" value="525"><input type="hidden" name="option[0][278]" value="531"><input type="hidden" name="option[0][276]" value="536"><input type="hidden" name="option[0][277]" value="530"></div>
              </div>
            </div>

            <div id="fb_items">  <div class="plusIcon">    <span class="icon-plus"></span>  </div><div class="freqItem" align="center">  <div id="imageHolder1" class="image-holder" onclick="setFBItem(1)">    <span class="icon-tick"></span>    <img class="" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Digital Connectors/Lexar-Jumpdrive-USB/lexar-jumpdrive-64gb-150x150.png">  </div>  <div class="content-holder text-center">    <h3><a href="http://shop.globe.com.ph/products/gadgets-and-accessories/lexar-64gb-jumpdrive-m20-mobile-usb-flash-drive-3-0" class="fontsize24">Lexar 64GB JumpDrive M20 Mobile USB Flash Drive 3.0</a></h3>   <p id="hero_offer_fb_price_1">PHP 1,700.00</p>    <input type="hidden" id="hero_offer_fb_add_1" name="addcart[1]" value="1">    <input type="hidden" id="hero_offer_fb_priceval_1" name="price[1]" value="1700.00">    <input type="hidden" name="product_id[1]" value="637">    <input type="hidden" name="quantity[1]" size="2" value="1">    <div id="hero_offer_fb_options_1">    <input type="hidden" name="option[1][953]" value="2049">    <input type="hidden" name="option[1][954]" value="2050">  </div>  </div></div>  <div class="plusIcon">    <span class="icon-plus"></span>  </div><div class="freqItem" align="center">  <div id="imageHolder2" class="image-holder" onclick="setFBItem(2)">    <span class="icon-tick"></span>    <img class="" src="http://d11fuji4mn7bm2.cloudfront.net/media/cache/data/Digital Connectors/Samsung-Accessories/EB-PA5000-gold-150x150.png">  </div>  <div class="content-holder text-center">    <h3><a href="http://shop.globe.com.ph/products/gadgets-and-accessories/samsung-universal-slim-battery-pack-5200mah" class="fontsize24">Samsung Universal Slim Battery Pack 5,200mAh</a></h3>   <p id="hero_offer_fb_price_2">PHP 2,499.00</p>    <input type="hidden" id="hero_offer_fb_add_2" name="addcart[2]" value="1">    <input type="hidden" id="hero_offer_fb_priceval_2" name="price[2]" value="2499.00">    <input type="hidden" name="product_id[2]" value="1002">    <input type="hidden" name="quantity[2]" size="2" value="1">    <div id="hero_offer_fb_options_2">    <input type="hidden" name="option[2][1602]" value="3407">  </div>  </div></div></div>
            
          </div>
        </div>
        <div class="col-sm-12 col-md-4 cross-sell-total">
          <h3>Total: <span id="fb_total">P4,199.00</span></h3>
          <button id="pdp-frequent-bought-add-btn" class="btn btn-bw-blue changeB btn-block btn-fb-cart">ADD TO CART</button>
          <!-- <button class="btn btn-default btn-bw-white btn-block">Add all two to Wishlist</button> -->
          <br>
          <div id="fb_notif"></div>
        </div>
      </div>
    </div>
  </section>
  </div>

<div class="rwd-pdp-container visible-xs rwd-pdp-container-plan-only">

  <br /><!--RWD HELPER :) -->

  <section class="rwd-mainProducts container-fluid">
    <div class="rwd-product-info-badge-holder">
          </div>
    <div class="rwd-pdp-images">
                  <div class="text-center">

            <br />
          </div>
            </div>

    <script type="text/javascript">
      $(document).ready(function($) {
        if(!isIE() || (isIE() && $(window).width() < 600)) {
          $('.rwd-pdp-images').slick({
            dots: true,
            infinite: true,
            speed: 500,
            slide: 'div',
            cssEase: 'linear',
            adaptiveHeight: true
          });
        }
      });
    </script>

    <div class="rwd-productContent">
      <h1 class="title-bw-light">myLifestyle Plan 999</h1>
      <div class="rwd-pdp-description">
        <p><span style="font-family: fs_elliot_proregular, Helvetica; font-size: 1em; line-height: normal;">myLifestyle Plan 999 comes with built-in Unlimited calls and texts to Globe/TM, and any combination of at least one mandatory Surf Pack and Lifestyle or Classic Packs worth up to P500.*</span></p>
<script>
  $('#myTabs').click(function (event) {
	$("#tab-0").css('display', 'none');
    $("#tab-1").css('display', 'block');
    $('div.slick-track a.fontsize24:contains("Plan")').removeClass("selectedTab selected");
    $('div.slick-track a.fontsize24:contains("Promos")').addClass("selectedTab selected");
    $('div.slick-slide:contains("Plan")').toggleClass("slick-active");
    $('div.slick-slide:contains("Promos")').toggleClass("slick-active");
  });
</script>

<h2 class="pdesc" style="font-size: 13px; line-height: 20.7999992370605px; color: rgb(0, 0, 139);">Online orders are for new and additional line applications only. You can also order this device by calling our Sales Hotline at (02) 730-1010, open Monday to Sunday from 6AM to 10PM. During the call, please enter the following promo code when prompted: 21211. For existing subscribers who wish to recontract, kindly call (02) 730-1300. This plan is available at <a href="http://www.globe.com.ph/store-locator" target="_blank">Globe stores</a> so visit the one near you!</h2>
<style type="text/css">.pdesc{
    font: 1em fs_elliot_proregular, Helvetica; }
</style>
      </div>
    </div>

    <!-- RWD Product Variation Container -->
    <div class="rwd-productVariation"></div>

    <div class="rwd-share-this">
        <a href="#" data-toggle="modal" data-target="#myModal"><span class="icon-share"></span>Share This</a>
    </div>

    <!-- RWD Hero Container -->
    <div class="rwd-heroCol"></div>
  </section>

  <section class="rwd-sub-menu-tabs">
  </section>

  <section class="rwd-prodcutTab">
  </section>

  <section class="rwd-seoContent">
  </section>

  <section class="rwd-crossSell">
  </section>

  <!-- INPUT HIDDEN FOR DISABLING -->
  <input type="hidden" name="disable-add-cart" value="0" id="disable-add-cart">

</div>

<input type="hidden" id="popover" value="-1" />
</div>

<script type="text/javascript">

  //IMAGE ROTATION GALLERY - JS for changing the button into blue.
  $('.rotationButton a:first-child').addClass('active');
  $('.rotationButton a').click(function(){
    $('.slider-btn').removeClass('active');
    $(this).addClass('active');
  });

   //SUBMENU TABS - JS for changing the selected tabs to blue.
  $('.key-tab').click( function(e){
    var currentTab = e.target;

    $('.slick-slide a').removeClass('selectedTab');
    $(currentTab).addClass('selectedTab');

    $('.popupTooltip').popover('hide');
  });

  $('#tabs a').tabs();

  // Frequently Bought
  getFBTotal();

  $('#imageHolder1 span').hide();
  $('#imageHolder2 span').hide();
  $('#imageHolder3 span').hide();

  $('#imageHolder1').click(function(){
    $('#imageHolder1 span').toggle();
  });

  $('#imageHolder2').click(function(){
    $('#imageHolder2 span').toggle();
  });

  $('#imageHolder3').click(function(){
    $('#imageHolder3 span').toggle();
  });

  function showImage(field) {
    thumb = $(field).data('thumb');
    popup = $(field).data('popup');

    $('.imageZoom').prop('src', thumb);
    $('.imageZoom').data('zoom-image', popup);

    imageZoom();
  }

  function changeBtn(id) {
    $('#option-'+id).show();
    $('#option-text-'+id).hide();
  }

  function cancelBtn(id) {
    $('#option-'+id).hide();
    $('#option-text-'+id).show();
  }

  function doneBtn(id) {
    newVal = $('#option-hidden-val-' + id).val();
    newText = $('#option-hidden-label-' + id).val();

    $('#optionSelect-'+id).val(newVal);
    $('#option-hidden-val-orig-' + $(this).data('id')).val(newVal);
    $('#option-hidden-label-orig-' + $(this).data('id')).val(newText);

    $('#option-text-'+id+' .storage-label').html(newText);

    $('#option-'+id).hide();
    $('#option-text-'+id).show();
  }

  function changeProductBtn() {
    $('#product-select').show();
    $('#product-text').hide();
  }

  function cancelProductBtn() {
    $('#product-select').hide();
    $('#product-text').show();
  }

  function changeAttributeSelect() {
    $('.attribute-option').on('click', function() {
      var id = $(this).data('parentid');
      var newVal = $(this).data('id');
      var newText = $(this).data('value');

      $('#option-hidden-val-' + id).val(newVal);
      $('#option-hidden-label-' + id).val(newText);
      $('#optionSelect-' + id).val(newVal);
      $('#optionhidden_' + id).val(newVal);
      // update hidden values
      $('input[name="option[' + id + ']"]').val(newVal);

      $('#option-text-' + id + ' .storage-label').text(newText);
      if(newText.length > 15){
        newText = newText.substr(0,15) + '...';
      }
      $('.dropdown-' + id + ' .dropdown-toggle').html('<div><span class="drpdwn">'+newText + '</span><span class="caret caretx"></span></div>');

      $('.dropdown-' + id + ' ul li').removeClass('active');
      $(this).parent().addClass('active');
      $('.dropdown-' + id).toggleClass('open');

      if ($('#div-prepaid').is(':visible')) {
        getPlanHero();
      }

      return false;
    });
  }

  function getPlanHero() {
    $.ajax({
      url: 'index.php?route=product/product/getPlanKit',
      type: 'post',
      data: $('.optionFields select, .optionFields input[type=\'hidden\']'),
      dataType: 'json',
      beforeSend: function() {
        $('#div-prepaid .cashout').html('');
        $('#prepaid-details').html('');
        $('#prepaid-hidden').html('');
        $('.hero-offer-title').html('');
        $('#div-prepaid #hero-offer-addtocart-btn').hide();

        $('#div-prepaid .stock-level').removeClass('outofstock low limited instock');
        $('#div-prepaid .stock-level').html('');
        $('#plan-loading').html('<div style="padding: 30px 10px 15px 10px;"><img src="catalog/view/theme/broadwaytheme/images/loader.gif" alt="" style="width: 20px; height: 20px;" border="0" /><br />Updating</div>');
      },
      complete: function() {

      },
      success: function(json) {
        html = '';
        
        $('#plan-loading').html('');

        if (json['output']) {
          $('.hero-offer-title').html(json['output']['name']);
          $('#div-prepaid .cashout').html(json['output']['cashout_text']);
          $('#prepaid-details').html(json['output']['presets']);
          $('#prepaid-hidden').html(json['output']['options_val']);
          $('#div-prepaid .stock-level').html(json['output']['stock']);
          $('#div-prepaid .stock-level').addClass(json['output']['stock_status']);
          
          if ( json['output']['stock_status'] === 'outofstock' ) {
              $('.btn-addcart').prop('disabled',true);
          }
          
          $('#div-prepaid #hero-offer-addtocart-btn').show();

          // Frequently Bought
          if (json['output']['frequently_bought']!='') {
            $('#hero_offer_fb_url_0').attr('href', json['output']['url']);
            $('#hero_offer_fb_name_0').html(json['output']['product_name']);
            $('#hero_offer_fb_image_0').attr('src', json['output']['image']);
            $('#hero_offer_fb_price_0').html(json['output']['price']);
            $('#hero_offer_fb_priceval_0').val(json['output']['price_val']);
            $('#hero_offer_fb_options_0').html(json['output']['optionsFB']);
            $('#hero_offer_fb_minimum_0').val(json['output']['minimum']);
            $('#hero_offer_fb_productid_0').val(json['output']['product_id']);

            $('#fb_items').html(json['output']['frequently_bought']);

            getFBTotal();
          } else {
            $('.crossSell').hide();
          }
        } else {
          $('#div-prepaid').hide();
          $('#div-hero-offer').show();
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }

  $(document).ready(function() {
    $('.optionFields').show();

    $('.popupTooltip').click(function() {
      //console.log('here');
      $('.popupTooltip').popover('hide');
      $(this).popover('show');
    });


    // Product
    $('.attribute-product').unbind('click').on('click', function() {
      var newVal = $(this).data('id');
      var newText = $(this).data('value');
      var newText2;

      // var newText = $( "#product-plan-select option:selected" ).text();
      $('#product-hidden-val').val(newVal);
      $('#product-hidden-label').val(newText);
      $('#product-plan-select').val(newVal);
      $('#producthidden').val(newVal);

      // cancelProductBtn();
      $('#product-text .storage-label').text(newText);
      newText2 = newText;
      if(newText.length > 32) {
        newText = newText.substr(0, 32) + '...';
      }
      if(newText2.length > 23) {
        newText2 = newText2.substr(0, 23) + '...';
      }

      var isFirefox = typeof InstallTrigger !== 'undefined';
      if(isFirefox && $(window).width() < 768){
         $('.dropdown-plan-select .dropdown-toggle').html('<div style="padding-right: 5px;"><span class="drpdwn iPadV">' + newText2 + '</span><span class="drpdwn desktopV">' + newText + '</span><span class="caret caretx" style="margin-top:-10px"></span></div>');     
      } else {
         $('.dropdown-plan-select .dropdown-toggle').html('<div style="padding-right: 5px;"><span class="drpdwn iPadV">' + newText2 + '</span><span class="drpdwn desktopV">' + newText + '</span><span class="caret caretx"></span></div>');     
      }
      $('.dropdown-plan-select ul li').removeClass('active');
      $(this).parent().addClass('active');
      $('.dropdown-plan-select').toggleClass('open');

      getProductOptions(newVal);

      return false;
    });

          getProductOptions(134);
      });

  function setPopup(id) {
    $('.popupTooltip').popover('hide');

    popoverval = $('#popover').val();

    if (id != popoverval) {
     $('#popover').val(id);
     $('#popover_' + id).popover('hide');
    } else {
      $('#popover').val('-1');
      $('#popover_' + id).popover('show');
    }
  }
</script>
<script type="text/javascript"><!--
$('.button-cart').bind('click', function() {
  addToCart($(this).data('id'));
});

function addToCartMatrix(id) {
  addToCart('matrix_' + id);
}

function addToCart(parent_id) {
  if($("#disable-add-cart").val() == 1) {
      return false;
  } 
  $("#disable-add-cart").val(1);
  prepaid_fields = '';
  if (parent_id == 'prepaid_kit') {
    prepaid_fields = ', .optionFields select';
  }

  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#div-prepaid input[type=\'hidden\']'),
    dataType: 'json',
    success: function(json) {
      $('.success, .warning, .attention, information, .error').remove();

      $('.popupTooltip').popover('hide');

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
          }
        }

        if (json['error']['profile']) {
            $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
        }

        if (json['error']['stock']) {
          $('#pdpCartErrorModal').modal('show');
        }
      }

      if (json['success']) {
        // Analytics
        dataLayer.push({
          'event': 'addToCart',
          'ecommerce': {
            'currencyCode': 'PHP',
            'add': {
              'products': [{
                'name': json['product']['name'],
                'id': json['product']['id'],
                'price': json['product']['price'],
                'brand': json['product']['brand'],
                'category': '',
                'variant': json['product']['variant'],
                'quantity': json['product']['quantity']
               }]
            }
          }
        });

        $('.cart-total-hide-empty').removeClass('cart-total-hide-empty');
        $('#cart-total').html(json['total']);
        $('#pdpCartModal').modal('show');
      }
      $("#disable-add-cart").val(0);
    }
  });
}
//--></script>

<script type="text/javascript"><!--
function getProductOptions(product_id) {
  var plan_name = 'myLifestyle Plan 999';
  $.ajax({
    url: 'index.php?route=product/product/getPlanProductOptions',
    type: 'post',
    data: '&plan_id=39&product_id=' + product_id + '&plan_name=' + plan_name,
    dataType: 'json',
    beforeSend: function() {
      $('#plan-product-options').html('');
    },
    complete: function() {

    },
    success: function(json) {
      if (json['output'] != '') {
        $('#plan-product-options').html(json['output']);
        getPlanHero();
        changeAttributeSelect();
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
}
//--></script>

<script type='text/javascript' src='catalog/view/javascript/jquery/jquery.elevatezoom.js'></script>
<script type="text/javascript"><!--
  $(document).ready(function() {
      imageZoom();
  });

  function imageZoom() {
    $('.zoomContainer').remove();
    $('.imageZoom').removeData('elevateZoom');
    $('.imageZoom').elevateZoom({
        responsive: true,
        easing: true,
        borderSize: 2,
        borderColour: '#ccc',
        zoomWindowOffetx: 5,
        scrollZoom : true,
        zoomWindowWidth: 520,
        zoomWindowHeight: 400
      });
  }
//--></script>

<script>
dataLayer.push({
  'ecommerce': {
    'detail': {
      'actionField': {'list': ''},    // 'detail' actions have an optional list property.
      'products': [{
        'name': 'myLifestyle Plan 999',         // Name or ID is required.
        'id': '39',
        'price': '0.00',
        'brand': 'Globe',
        'category': 'Mobile Postpaid',
        'variant': 'myLifestyle Plan 999'
       }]
     }
   }
});
</script>

</div>
</div>
</div>

    <div id="footer-top-wrap" class="container-fluid" style="background: #333333;">
    <div class="container footer-top-holder">
      <!-- INSERT FOOTER CONTENT HERE -->
      <style type="text/css">#footer hr.footer-divider{
    border-top: none !important;
  }

  #footer-top-wrap{
  background: #f6f6f6 !important;
  }

  .bg-gray{
   background:#f6f6f6;
  }
  .padd-top-bot{
     padding-top: 5px;
    padding-bottom: 0px;
  }
  .padd-top-bot10{
  padding-top: 10px;
  padding-bottom: 10px;
  }
  .f-shipping{
    text-align: center;
  }
  .f-payment{
    text-align: center;
  }
  .f-call{
    text-align: center;
  }
  .f-shipping a {
     text-decoration: none;
     color: #333;
  }
  .footer-options div{
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .f-shipping img{
  margin-right: 10px;
  margin-bottom: 0px;

  }
  .f-payment img{
  margin-right: 10px;
  margin-bottom: 0px;
  }
  .f-call img{
  margin-right: 10px;
  margin-bottom: 0px;
  }

   @media screen and (max-width: 768px) {
   .f-shipping{
      border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-payment{
       border-bottom: 1px solid #e7e7e7;
    padding-bottom: 8px;
  }
  .f-call{
    text-align: center;
  }
}
@media screen and (max-width: 445px) {
.indent-mobile{
margin-left: 12%;
}
}

@media screen and (max-width: 391px) {
.indent-mobile{
margin-left: 12%;
}
}
@media screen and (max-width: 389px) {
.indent-mobile{
margin-left: 0%;
}
}
</style>
<div id="footer-top">
<div class="container-fluid bg-gray padd-top-bot">
<div class="container">
<div class="row footer-options">
<div class="col-sm-4 col-md-4 col-lg-4 f-shipping"><a href="http://shop.globe.com.ph/help" style="color:black" title="Click to learn more about our shipping, delivery and returns policies"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/shippingA.png" class="lazy" data-original="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/shippingA.png" style="display: inline;"><span class="m-top" title="Click to learn more about our shipping, delivery and returns policies">FREE SHIPPING ON ALL ORDERS</span></a></div>

<div class="col-sm-5 col-md-5 col-lg-5 f-payment"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/payment-method.png" class="lazy" data-original="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/payment-method.png" style="display: inline;"><span class="m-top">CASH ON DELIVERY, MAJOR<span class="indent-mobile"> CREDIT CARDS &amp; GCASH</span></span></a></div>

<div class="col-sm-3 col-md-3 col-lg-3 f-call"><a href="http://shop.globe.com.ph/help" style="color:black"><img src="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/call.png" class="lazy" data-original="http://shps3dv01.s3.amazonaws.com/media/data/footer-top/call.png" style="display: inline;"><span class="m-top">CALL (02)730-1010</span></a></div>
</div>
</div>
</div>
</div>
    </div>
  </div>
  <div id="footer">
    <hr class="footer-divider">
    <div class="container footer-holder">
        <footer class="main shadow row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl>
                  <dt><a href="http://www.globe.com.ph/about-globe">Globe Telecom</a></dt>
                  <dd><a href="http://www.globe.com.ph/about-globe/company-info">Corporate Information</a></dd>
                  <dd><a href="http://www.globe.com.ph/corporate-governance">Corporate Governance</a></dd>
                  <dd><a href="http://www.globe.com.ph/investor-relations">Investor Relations</a></dd>
                  <dd><a href="http://www.globe.com.ph/careers">Careers</a></dd>
                  <dd><a href="http://www.globe.com.ph/press-room">Press Room</a></dd>
                  <dd><a href="http://www.globe.com.ph/globelife">GlobeLife</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl>
                  <dt>Products and Services</dt>
                  <dd><a href="http://www.globe.com.ph/postpaid">Postpaid Plans</a></dd>
                  <dd><a href="http://www.globe.com.ph/prepaid/call-and-text-offers">Prepaid Promos</a></dd>
                  <dd><a href="http://www.globe.com.ph/surf">Mobile Internet</a></dd>
                  <dd><a href="http://tattoo.globe.com.ph/">Tattoo Broadband</a></dd>
                  <dd><a href="http://www.globe.com.ph/gcash">GCash</a></dd>
                  <dd><a href="http://shop.globe.com.ph/home">Online Shop</a></dd>
              </dl>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear1">
                  <dt><a href="http://business.globe.com.ph/">Business</a></dt>
                      <dd><a href="http://business.globe.com.ph">Enterprise and Wholesale</a></dd>
                      <dd><a href="http://mybusiness.globe.com.ph">Small and Medium Business</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear2">
                  <dt><a href="http://www.globe.com.ph/worry-free-guarantee">Globe Guarantee</a></dt>
                  <dd><a href="http://www.globe.com.ph/terms-and-conditions">Terms and Conditions</a></dd>
                  <dd><a href="http://www.globe.com.ph/privacy-policy">Privacy Policy</a></dd>
                  <dd><a href="http://www.globe.com.ph/aup">Fair Use Policy</a></dd>
              </dl>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <dl class="clear3">
                  <dt><a href="http://www.globe.com.ph/contactus">Contact Us</a></dt>
                  <dd><a href="http://www.globe.com.ph/help">Help and Support</a></dd>
                  <dd><a href="http://community.globe.com.ph/">Ask the Community</a></dd>
                  <dd><a href="http://www.globe.com.ph/store-locator">Store Locator</a></dd>
              </dl>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
              <script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=shop.globe.com.ph&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script><img name="seal" src="https://seal.websecurity.norton.com/getseal?at=0&amp;sealid=2&amp;dn=shop.globe.com.ph&amp;lang=en&amp;tpt=opaque" oncontextmenu="return false;" border="0" usemap="#sealmap_small" alt="" class="lazy" data-original="https://seal.websecurity.norton.com/getseal?at=0&amp;sealid=2&amp;dn=shop.globe.com.ph&amp;lang=en&amp;tpt=opaque" style="display: inline;"> <map name="sealmap_small" id="sealmap_small"><area alt="Click to Verify - This site has chosen an SSL Certificate to improve Web site security" title="" href="javascript:vrsn_splash()" shape="rect" coords="0,0,100,50" tabindex="-1" style="outline:none;"><area alt="Click to Verify - This site has chosen an SSL Certificate to improve Web site security" title="" href="javascript:vrsn_splash()" shape="rect" coords="0,50,53,72" tabindex="-1" style="outline:none;"><area alt="" title="" href="javascript:symcBuySSL()" shape="rect" coords="53,50,100,72" style="outline:none;"></map>            </div>
        </footer>
    </div>
    <div class="footer-legals">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div class="content-holder text-center footer-links">
              <p>© 2016 Globe Telecom Inc.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Talk2GLOBE CHAT</h4>
      </div>
      <div class="modal-body" align="center">
        <iframe id="chatFrame" src="" frameborder="0" height="459" width="99.6%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Item Added -->
<div class="modal fade" id="pdpCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: green; ">Item successfully added to cart.</h4>
        <br />
        <div class="cart-btn-view">
          <a href="http://shop.globe.com.ph/cart" class="btn btn-bw-orange">View Cart</a>
        </div> 
        <br />
      </div>
    </div>
  </div>
</div>

<!-- Item Out of Stock -->
<div class="modal fade" id="pdpCartErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="center">
        <h4 style="color: red; " id="pdpCartErrorModalMsg">Out Of Stock</h4>
        <br />
        <div class="cart-btn-view">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
        <br />
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.btn-chat').click(function() {
      $('#chatFrame').attr('src', 'http://webchat.globe.com.ph/chatc41/');
    });
  });
</script>

<!-- Ethnio -->
  <!-- End Ethnio -->

<!-- Custom Script -->
<script type="text/javascript">_satellite.pageBottom();</script>

<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=ZKgM1Va";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//s.thebrighttag.com/iframe?c=ZKgM1Va" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

</noscript>


<!---Mouseflow--->

<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/ded37ad7-9807-4070-ad67-9d72e900c70a.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<!---Mouseflow--->
<!-- End Custom Script -->

</body>
</html>