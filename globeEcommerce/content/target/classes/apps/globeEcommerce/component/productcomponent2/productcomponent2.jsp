<div class="container-fluid">
<div class="container home-prod-custom-text">
<div class="homepage-prod-headers-container">
    <div class="homepage-prod-headers-text" style="width: 307px;">ACCESSORIES YOU'LL LOVE</div>

<div class="homepage-headers-line" style="background-color: #333333;
    position: absolute;
    height: 2px;
    top: 50%;
    width: 100%;">&nbsp;</div>
</div>

<div class="row home-prod-custom-text-padding"><div class="col-lg-1 col-md-1 col-sm-1 home-prod-item-blank">&nbsp;</div>


<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
<div align="center" class="home-prod-item-img"><a href="http://shop.globe.com.ph/gadgets-and-accessories/powerbanks"><img class="img-responsive lazy" src="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-Powerbanks.png" data-original="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-Powerbanks.png" style="display: block;"> </a></div>

<div class="prodh1Title"><a href="http://shop.globe.com.ph/gadgets-and-accessories/powerbanks" style="color:#000000; text-decoration:none">Powerbanks</a></div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
<div align="center" class="home-prod-item-img"><a href="http://shop.globe.com.ph/gadgets-and-accessories/speakers"><img class="img-responsive lazy" src="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-Speakers1.png" data-original="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-Speakers1.png" style="display: block;"> </a></div>

<div class="prodh1Title"><a href="http://shop.globe.com.ph/gadgets-and-accessories/speakers" style="color:#000000; text-decoration:none">Speakers</a></div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
<div align="center" class="home-prod-item-img"><a href="http://shop.globe.com.ph/gadgets-and-accessories/headsets-and-earphones"><img class="img-responsive lazy" src="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-headsets.png" data-original="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-headsets.png" style="display: block;"> </a></div>

<div class="prodh1Title"><a href="http://shop.globe.com.ph/gadgets-and-accessories/headsets-and-earphones" style="color:#000000; text-decoration:none">Headsets</a></div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
<div align="center" class="home-prod-item-img"><a href="http://shop.globe.com.ph/gadgets-and-accessories/memory-devices"><img class="img-responsive lazy" src="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-memory-devices.png" data-original="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-memory-devices.png" style="display: block;"> </a></div>

<div class="prodh1Title"><a href="http://shop.globe.com.ph/gadgets-and-accessories/memory-devices" style="color:#000000; text-decoration:none">Memory Devices</a></div>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 home-prod-item">
<div align="center" class="home-prod-item-img"><a href="http://shop.globe.com.ph/gadgets-and-accessories/cases-and-screen-protectors"><img class="img-responsive lazy" src="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-cases.png" data-original="http://shop.globe.com.ph/image/data/homepage_folds/DCs-Section/DCs-cases.png" style="display: block;"> </a></div>

<div class="prodh1Title"><a href="http://shop.globe.com.ph/gadgets-and-accessories/cases-and-screen-protectors" style="color:#000000; text-decoration:none">Cases &amp; Screen Protectors</a></div>
</div>


<div class="col-lg-1 col-md-1 col-sm-1 home-prod-item-blank">&nbsp;</div></div>

<div class="row home-prod-custom-link-padding" style="padding-top: 20px;">
<div align="center" class="col-lg-12 col-sm-12"><a class="prodlink" href="http://shop.globe.com.ph/gadgets-and-accessories">View All Accessories</a></div>
</div>
</div>
</div>