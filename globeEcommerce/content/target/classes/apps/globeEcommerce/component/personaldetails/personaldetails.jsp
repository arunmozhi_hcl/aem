<%@ include file="/libs/foundation/global.jsp"%>

<html>
    <cq:includeClientLib categories="productdetails"/>
    <head>


<meta property="og:site_name" content="Globe Telecom Inc."/>
<meta property="fb:app_id" content="990172374332349" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_US" />

<title>Checkout</title>


<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/checkout-common-form.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/pikaday.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/css/dropzone.css" media="screen" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/broadwaytheme/stylesheet/dropzone.min.css" media="screen" />



</head>



 <body>

<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="checkout-form-container">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
<div class="row" id="form-container" style="height: 469px;">
<div id="section-container">
<form action="" enctype="multipart/form-data" id="app_form" method="post">
<section class="panel current" id="personal-details">
    <div class="panel-content"><div class="panel-heading">${properties.pagetitle}</div>
<div class="panel-body">
										<div class="panel-row row">
			
			
			<div class="form-group required-field col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: auto;">
                <label class="field-label">${properties.apptitle}</label>

									<div class="radio-group row">
        				<div class="radio-inline personal_application_type selected">
        				  <span class="radio-button"></span> 
        				  <text>New Postpaid Application</text>
        				</div>
        				<div class="radio-inline personal_application_type ">
        				  <span class="radio-button"></span> 
        				  <text>Additional Line</text>
        				  <div class="form-group personal_account_number" style="height: auto;">
        				  	<label class="field-label">Mobile/Account #</label>
        				  	<input type="text" class="checkout-forms-text-numeric required-data" name="personal_account_number" maxlength="20" value="" placeholder="">
        				  </div>
        				</div>
        			</div>
        			<input type="hidden" id="personal_application_type" name="personal_application_type" value="new">
							</div>

						</div>
			
															<div class="panel-row row">
			
			
			<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="height: 73px; padding: 10px 6px;">
                <label class="field-label" style="margin: 0px 4px 5px;">${properties.salutation}</label>
			
									<select name="personal_salutation" class="required-data" data-required="required" data-default="ms">
                        <option value="" class="hidden">Please Select</option>
                                                                                    <option value="arch">
                                    Arch.                                </option>
                                                            <option value="dr">
                                    Dr.                                </option>
                                                            <option value="engr">
                                    Engr.                                </option>
                                                            <option value="msgr">
                                    Msgr.                                </option>
                                                            <option value="rev">
                                    Rev.                                </option>
                                                            <option value="mr">
                                    Mr.                                </option>
                                                            <option value="mrs">
                                    Mrs.                                </option>
                                                            <option value="ms" selected="selected">
                                    Ms.                                </option>
                                                                        </select>
							</div>

			
								
			
			<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="height: 73px;">
                <label class="field-label">${properties.firstname}</label>
			
														<input type="text" data-required="required" class=" required-data" name="personal_first_name" value="" maxlength="100">
							</div>

			
								
			
			<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="height: 73px;">
                <label class="field-label">${properties.middlename}</label>

														<input type="text" data-required="required" class=" required-data" name="personal_middle_name" value="" maxlength="100">
							</div>

			

			
			<div class="form-group required-field col-lg-3 col-md-6 col-sm-6 col-xs-6" style="height: 73px;">
                <label class="field-label">${properties.lastname}</label>
			
														<input type="text" data-required="required" class=" required-data" name="personal_last_name" value="" maxlength="100">
							</div>

						</div>
			
															<div class="panel-row row">
			
			
			<div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="height: 73px;">
                <label class="field-label">${properties.email}</label>

														<input type="text" data-required="required" class=" required-data" name="personal_email_address" value="" maxlength="255">
							</div>

			

			
			<div class="form-group required-field col-lg-6 col-md-6 col-sm-6 col-xs-6" style="height: 73px;">
                <label class="field-label">${properties.contact}</label>

														<input type="text" data-required="required" class="checkout-forms-text-numeric required-data" name="personal_primary_contact_number" value="" maxlength="15">
							</div>

						</div>
			
						
	<input type="hidden" id="checkout_type" value="postpaid">
</div>
<div id="panel-bottom" class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<a onclick="loginToSave('https://shop.globe.com.ph/checkout/login_to_save')">Save this form and exit</a>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <button id="button-personal-details" class="btn"><a href=http://localhost:4504/content/globeEcommerce/additionalinfopage.html>Next Step</a></button>
	</div>
</div>

<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout-functions.js"></script>
<script type="text/javascript" src="catalog/view/theme/broadwaytheme/js/checkout/personal_details.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	getSidebar('personal_details', 'postpaid');
})
</script>

<script type="text/javascript">
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 1, 'option': ''},
        'products': [
                        {                            
                'name': 'Samsung Galaxy S6 edge',     
                'id': '134',
                'price': '25299.00',
                'brand': 'Samsung',
                'category': 'Mobile Postpaid',
                'variant': 'myLifestyle Plan 999, 24 months, White Pearl, 64 GB',
                'quantity': '1',
                'coupon': ''                            
            },
                    ]
     }
   }
});
</script>
</div>
</section>

<section class="panel" id="additional-personal-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="shipping-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="financial-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="document-upload-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="terms-condition-details">
<div class="panel-content"></div>
</section>

<section class="panel" id="review-order-details">
<div class="panel-content"></div>
</section>
</form>
</div>
</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="sidebar"><p><img class="img-responsive" id="norton-secured" src="https://shps3dv01.s3.amazonaws.com/media/data/norton_secured.png"></p>

<div class="row" id="contact-details">
<div class="info-block col-lg-12 col-md-12 col-sm-12 col-xs-4">
<p>Your personal information is safe and will always be kept private.</p>
</div>
</div>
</div>
</div>

</body>
</html>
