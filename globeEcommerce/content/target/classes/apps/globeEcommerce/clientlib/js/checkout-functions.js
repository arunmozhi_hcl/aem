$( document ).ready(function() {
    $('.checkout-forms-text-alphanumeric').alphanum();
    $('.checkout-forms-text-alpha').alpha();
    $('.checkout-forms-text-numeric').numeric({allowDecSep: false});

    //$('.panel.current').css('left', '0');
    $('.panel').each(function() {
        var maxZIndex = $(this).find('.panel-row').length;
        $(this).find('.panel-row').each(function() {
            $(this).css('z-index', maxZIndex);
            maxZIndex --;
        });
    });
    $('.panel .panel-row').each(function() {
        var maxZIndex = $(this).find('.form-group').length;
        $(this).find('.form-group').each(function() {
            $(this).css('z-index', maxZIndex);
            maxZIndex --;
        });
    });
    if($('.panel.current').index() > 0) {
        if(!$('.panel.current').prev().hasClass('skip')) {
            $('.panel.current').prev().addClass('prev');
        } else if(!$('.panel.current').prev().prev().hasClass('skip-prev')) {
            $('.panel.current').prev().prev().addClass('prev');
        } else {
            $('.panel.current').prev().prev().prev().addClass('prev');
        }
    }
    $(window).on('resize', function() {
        hidePopupSelects();
        $('#checkout-content').css('min-height', $(window).height() - ($('.journey-header').outerHeight() + $('#checkout-footer').outerHeight()) + 'px');
        adjustFormGroupsHeight();
        if($(window).width() <= 768) {
            setTimeout(function() { 
                $('#sidebar .popup').appendTo($('.panel .panel-heading')); 
            }, 10);
        } else {
            setTimeout(function() { 
                $('.panel .panel-heading .popup').prependTo($('#sidebar')); 
            }, 10);
        }
        $('select').each(function() {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $(this).closest('.form-group').css('padding', '10px');
                $(this).closest('.form-group').find('label').css('margin', '0 0 5px 0');
            } else {
                $(this).closest('.form-group').css('padding', '10px 6px');
                $(this).closest('.form-group').find('label').css('margin', '0 4px 5px 4px');
            } 
        });
        adjustFormHeight();
    }).resize();
    $(window).on('orientationchange', function() {
        hidePopupSelects();
        $('#checkout-content').css('min-height', $(window).height() - ($('.journey-header').outerHeight() + $('#checkout-footer').outerHeight()) + 'px');
        adjustFormGroupsHeight();
        if($(window).width() <= 768) {
            setTimeout(function() { 
                $('#sidebar .popup').appendTo($('.panel .panel-heading')); 
            }, 10);
        } else {
            setTimeout(function() { 
                $('.panel .panel-heading .popup').prependTo($('#sidebar')); 
            }, 10);
        }
        adjustFormHeight();
    });
});

function adjustFormHeight() {
    setTimeout(function() { 
        if($('.toggle-select-content-shipping').length) {
            $('.toggle-select-content-shipping').css('height', $('.toggle-option-content-shipping.shown').outerHeight() + 'px');
        }
        if($('.toggle-select-content-install').length) {
            $('.toggle-select-content-install').css('height', $('.toggle-option-content-install.shown').outerHeight() + 'px');
        }
        $('#form-container').css('height', $('.panel.current').outerHeight() + 15 + 'px'); 
    }, 10);
}

function adjustFormGroupsHeight() {
    $('.form-group').css('height', 'auto');
    if($(window).width() > 480) {
        setTimeout(function() {
            var height = 72;
            $('.panel-row .form-group').each(function() {
                if($(this).siblings('.form-group').length > 0) {
                    if($(this).outerHeight() > 72) {
                        height = $(this).outerHeight();
                        $(this).parent().find('.form-group').css('height', height + 'px');
                    } else {
                        $(this).parent().find('.form-group').css('height', 'auto');
                    }
                }
            });
        }, 20);
    }
}

function scrollToError() {
    if($(window).width() < 580) {
        $('html, body').animate({
            scrollTop: $('.required-prompt:visible:first').offset().top - 10
        }, '300')
    }
}

function hidePopupSelects() {
    $('.ui-autocomplete:visible').hide();
    $('.pika-single:visible').hide();
}

function validateEmail(email) {
    if (email.length == 0){
        return true;
    }
    var re = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return re.test(email);
}

function validateBirthday(bday) {
    if (bday.length == 0){
        return true;
    }
    var re = /^[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[0-9]{1}\d{1})|([0-9]{1}\d{3}))$/;
    var result = re.test(bday);
    if (result) {
        var nos = bday.split("/");
        if (nos[0] >= 13 || nos[0] == 0) {
            return false;
        }
    }
    return result;
}

function validateBarangay(brgy) {
    if (brgy.length == 0){
        return true;
    }
    var re = /^([ \w#',-]+)$/i;
    var result = re.test(brgy);
    return result;
}

function validateName(name) {
    if (name.length == 0){
        return true;
    }
    var re = /^([ A-Za-z'.-]+)$/i;
    var result = re.test(name);
    return result;
}

function validateNumber(valnum) {
    if(valnum != "") {
        var value = valnum.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        var intRegex = /^\d+$/;
        if(!intRegex.test(value)) {
            return false
        } else {
          return true;
        }
    } else {
        return false;
    }
}

function nextPanel() {
    if($('.panel.current').next().hasClass('skip') || $('.panel.current').next().hasClass('skip-prev')) {
        var next = $('.panel.skip').next();
    } else {
        var next = $('.panel.current').next();
    }
    if($('.panel.current').index() + 1 < $('.panel').length) {
        next.addClass('current');
        next.siblings().removeClass('current');
        setTimeout(function() {
            if($('.panel.current').prev().hasClass('skip')) {
                $('.panel.current').prev().removeClass('prev');
                if($('.panel.current').prev().prev()) {
                    if($('.panel.current').prev().prev().hasClass('skip-prev')) {
                        prev = $('.panel.current').prev().prev().prev();    
                    } else {
                        prev = $('.panel.current').prev().prev();
                    }
                }
            } else {
                prev = $('.panel.current').prev();
            }
            prev.addClass('prev');
            prev.siblings().removeClass('prev');
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');
            if($('.panel.current').next().hasClass('skip') || $('.panel.current').next().hasClass('skip-prev')) {
                next = $('.panel.skip').next();
            } else {
                next = $('.panel.current').next();
            }
        }, 10);
    }
}

function previousPanel() {
    var prev = $('.panel.prev');
    
    prev.removeClass('prev').addClass('current');
    prev.siblings().removeClass('current');
    setTimeout(function() {
        if($('.panel.current').prev().hasClass('skip')) {
            $('.panel.current').prev().removeClass('prev');
            if($('.panel.current').prev().prev()) {
                if($('.panel.current').prev().prev().hasClass('skip-prev')) {
                    prev = $('.panel.current').prev().prev().prev();    
                } else {
                    prev = $('.panel.current').prev().prev();
                }
            }
        } else {
            prev = $('.panel.current').prev();
        }
        prev.addClass('prev');
        prev.siblings().removeClass('prev');
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
        if($('.panel.current').next().hasClass('skip') || $('.panel.current').next().hasClass('skip-prev')) {
            next = $('.panel.skip').next();
        } else {
            next = $('.panel.current').next();
        }
    }, 10);
}

// Get Sidebar
function getSidebar(form_type, checkout_type) {
    $.ajax({
        url: 'checkout/checkout/sidebar',
        type: 'post',
        data: 'form_type=' + form_type + '&checkout_type=' + checkout_type,
        dataType: 'json',
        success: function(json) {
            if($(window).width() <= 768) {
                $('.panel.current .panel-heading').append(json['sidebar']);
                $('#sidebar').html(json['sidebar_general']);
            } else {
                $('#sidebar').html(json['sidebar'] + json['sidebar_general']);
                $('.popup .note').append('<span class="arrow-up"></span>');
            }
            adjustFormHeight();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    }); 
}

// Zip Code for Additional Information
function getZipCodeAdditional(country_id, region_id, zone_id, city_id) {
    country_id = (country_id!=null) ? country_id : $('input[name=\'billing_country\']').val();
    region_id = (region_id!=null) ? region_id : $('input[name=\'billing_region\']').val();
    zone_id = (zone_id!=null) ? zone_id : $('input[name=\'billing_province\']').val();
    city_id = (city_id!=null) ? city_id : $('input[name=\'billing_city\']').val();
    zip_code = $('select[name=\'billing_zip_code\']').data('default');

    $.ajax({
        url: 'checkout/checkout/getZipCode?country_id=' + country_id + '&region_id=' + region_id + '&zone_id=' + zone_id + '&city_id=' + city_id,
        dataType: 'json',
        beforeSend: function() {
          $('select[name=\'billing_zip_code\']').attr('disabled', true);
        },
        complete: function() {
        },
        success: function(json) {
          var html = '';

          if (json['postcode'] != '') {
            html += '<option value="" selected="selected">Please Select</option>';

            for (i = 0; i < json['postcode'].length; i++) {
                html += '<option value="' + json['postcode'][i]['postcode_id'] + '"';

                if (json['postcode'][i]['postcode_id'] == zip_code) {
                    html += ' selected="selected"';
                }

                html += '>' + json['postcode'][i]['name'] + '</option>';
            }
          } else {
            html += '<option value="" selected="selected">Please Select</option>';
          }

          $('select[name=\'billing_zip_code\']').html(html);
          $('select[name=\'billing_zip_code\']').attr('disabled', false);
          if($('select[name=\'billing_zip_code\']').val() != "") {
            $('select[name=\'billing_zip_code\']').siblings('.required-prompt').remove();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('select[name=\'billing_zip_code\']').attr('disabled', false);
        }
    });
}

// Zip Code for Shipping Details
function getZipCodeShipping(country_id, region_id, zone_id, city_id) {
    shipping_country_id = (country_id!=null) ? country_id : $('input[name=\'shipping_country\']').val();
    shipping_region_id = (region_id!=null) ? region_id : $('input[name=\'shipping_region\']').val();
    shipping_zone_id = (zone_id!=null) ? zone_id : $('input[name=\'shipping_province\']').val();
    shipping_city_id = (city_id!=null) ? city_id : $('input[name=\'shipping_city\']').val();
    shipping_zip_code = $('select[name=\'shipping_zip_code\']').data('default');

    $.ajax({
        url: 'checkout/checkout/getZipCode?country_id=' + shipping_country_id + '&region_id=' + shipping_region_id + '&zone_id=' + shipping_zone_id + '&city_id=' + shipping_city_id,
        dataType: 'json',
        beforeSend: function() {
          $('select[name=\'shipping_zip_code\']').attr('disabled', true);
        },
        complete: function() {
        },
        success: function(json) {
          var html = '';

          if (json['postcode'] != '') {
            html += '<option value="" selected="selected">Please Select</option>';

            for (i = 0; i < json['postcode'].length; i++) {
                html += '<option value="' + json['postcode'][i]['postcode_id'] + '"';

                if (json['postcode'][i]['postcode_id'] == shipping_zip_code) {
                    html += ' selected="selected"';
                }

                html += '>' + json['postcode'][i]['name'] + '</option>';
            }
          } else {
            html += '<option value="" selected="selected">Please Select</option>';
          }

          $('select[name=\'shipping_zip_code\']').html(html);
          $('select[name=\'shipping_zip_code\']').attr('disabled', false);
          if($('select[name=\'shipping_zip_code\']').val() != "") {
            $('select[name=\'shipping_zip_code\']').siblings('.required-prompt').remove();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('select[name=\'shipping_zip_code\']').attr('disabled', false);
        }
    });
}

// Zip Code for Installation Details
function getZipCodeInstallation(country_id, region_id, zone_id, city_id) {
    installation_country_id = (country_id!=null) ? country_id : $('input[name=\'installation_country\']').val();
    installation_region_id = (region_id!=null) ? region_id : $('input[name=\'installation_region\']').val();
    installation_zone_id = (zone_id!=null) ? zone_id : $('input[name=\'installation_province\']').val();
    installation_city_id = (city_id!=null) ? city_id : $('input[name=\'installation_city\']').val();
    installation_zip_code = $('select[name=\'installation_zip_code\']').data('default');

    $.ajax({
        url: 'checkout/checkout/getZipCode?country_id=' + installation_country_id + '&region_id=' + installation_region_id + '&zone_id=' + installation_zone_id + '&city_id=' + installation_city_id,
        dataType: 'json',
        beforeSend: function() {
          $('select[name=\'installation_zip_code\']').attr('disabled', true);
        },
        complete: function() {
        },
        success: function(json) {
          var html = '';

          if (json['postcode'] != '') {
            html += '<option value="" selected="selected">Please Select</option>';

            for (i = 0; i < json['postcode'].length; i++) {
                html += '<option value="' + json['postcode'][i]['postcode_id'] + '"';

                if (json['postcode'][i]['postcode_id'] == installation_zip_code) {
                    html += ' selected="selected"';
                }

                html += '>' + json['postcode'][i]['name'] + '</option>';
            }
          } else {
            html += '<option value="" selected="selected">Please Select</option>';
          }

          $('select[name=\'installation_zip_code\']').html(html);
          $('select[name=\'installation_zip_code\']').attr('disabled', false);
          if($('select[name=\'installation_zip_code\']').val() != "") {
            $('select[name=\'installation_zip_code\']').siblings('.required-prompt').remove();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('select[name=\'installation_zip_code\']').attr('disabled', false);
        }
    });
}

// Zip Code for Employer Information
function getZipCodeEmployer(country_id, region_id, zone_id, city_id) {
    country_id = (country_id!=null) ? country_id : $('input[name=\'financial_office_country\']').val();
    region_id = (region_id!=null) ? region_id : $('input[name=\'financial_office_region\']').val();
    zone_id = (zone_id!=null) ? zone_id : $('input[name=\'financial_office_province\']').val();
    city_id = (city_id!=null) ? city_id : $('input[name=\'financial_office_city\']').val();
    zip_code = $('select[name=\'financial_office_zip_code\']').data('default');

    $.ajax({
        url: 'checkout/checkout/getZipCode?country_id=' + country_id + '&region_id=' + region_id + '&zone_id=' + zone_id + '&city_id=' + city_id,
        dataType: 'json',
        beforeSend: function() {
          $('select[name=\'financial_office_zip_code\']').attr('disabled', true);
        },
        complete: function() {
        },
        success: function(json) {
          var html = '';

          if (json['postcode'] != '') {
            html += '<option value="" selected="selected">Please Select</option>';

            for (i = 0; i < json['postcode'].length; i++) {
                html += '<option value="' + json['postcode'][i]['postcode_id'] + '"';

                if (json['postcode'][i]['postcode_id'] == zip_code) {
                    html += ' selected="selected"';
                }

                html += '>' + json['postcode'][i]['name'] + '</option>';
            }
          } else {
            html += '<option value="" selected="selected">Please Select</option>';
          }

          $('select[name=\'financial_office_zip_code\']').html(html);
          $('select[name=\'financial_office_zip_code\']').attr('disabled', false);
          if($('select[name=\'financial_office_zip_code\']').val() != "") {
            $('select[name=\'financial_office_zip_code\']').siblings('.required-prompt').remove();
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          $('select[name=\'financial_office_zip_code\']').attr('disabled', false);
        }
    });
}