package com.hcl.globeEcommerce.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.elasticpath.rest.client.header.HeaderKeys;
@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/checkAvailability", methods = "GET", metatype=true)
public class CheckProductAvailabilityServlet  extends SlingAllMethodsServlet{
	final Logger logger = Logger.getLogger(PlanDetailsServlet.class);

	@Reference
    com.elasticpath.rest.client.CortexClientFactory clientFactory;
	    @Override
	    protected void doGet(final SlingHttpServletRequest req,
	            final SlingHttpServletResponse resp) throws ServletException, IOException {
	    	 int qty = Integer.parseInt(req.getParameter("quantity"));
	 AuthenticationService as= new AuthenticationService();
	 logger.info("getting auth key:-"+as.getAuthKey());
     MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
     headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
     headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
     headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, as.getAuthKey());
     logger.info("client factory object:-"+clientFactory.toString());
     com.elasticpath.rest.client.CortexClient client = clientFactory.create(headers, "Bagel_Store");
     	ProductSearch productSearch = new ProductSearch();
		String productListUrl = productSearch.getProductListByKeyword(client);
		String productSelected = productSearch.getProductBasedOnSelection(client, productListUrl);

		String productAvailability = productSearch.getProductAvailability(client, productSelected);
		String availabilityValue = productSearch.getProductAvailabilityCheck(client, productAvailability);
		if(!availabilityValue.equals("AVAILABLE")){
			resp.getWriter().print("not available");
		}
		else{
			

}
}

}
