package com.hcl.globeEcommerce.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class ItemDetailsView {

	@JsonProperty("display-name")
	private String planDisplayName;

	public String getPlanDisplayName() {
		return planDisplayName;
	}

	public void setPlanDisplayName(String planDisplayName) {
		this.planDisplayName = planDisplayName;
	}

	
	@JsonPath("$.details[?(@.display-name=='Plan Description')].value")
	  private Iterable<String> planValue;
	
	@JsonPath("$.purchase-price[0].display")
	  private String displayValue;
	
	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}


	@JsonPath("$.details[?(@.display-name=='Monthly Rental')].value")
	  private Iterable<String> monthlyRentalValue;
	
	
	
	/**
	 * @return the addtocartform
	 */
	public String getPlanValue() {
		return planValue.iterator().next();
	}
	
	public String getmonthlyRentalValue() {
		return monthlyRentalValue.iterator().next();
	}
	
}
