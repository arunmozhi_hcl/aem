package com.hcl.globeEcommerce.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

/*import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;*/
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
//import org.springframework.context.ApplicationContext;

public class Authentication {
	private String authKey;
	

	/**
	 * @return the authKey
	 */
	public String getAuthKey() {
		return authKey;
	}

	/**
	 * @param authKey the authKey to set
	 */
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	/*public CortexClient getCortexClient(ApplicationContext context){
		Cortex cortex = new Cortex();
		CortexClientFactory clientFactory = cortex
				.cortexClientFactorySetup(context);
		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
		headers.putSingle(HeaderKeys.AUTHORIZATION_KEY,
				"Bearer "+getAuthKey());
		com.elasticpath.rest.client.CortexClient client = clientFactory.create(
				headers, "globe");
		return client;
	}*/
	
	/*private void sendPost() throws Exception {

		String url = "http://localhost:9080/cortex/oauth2/tokens";

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		// add header
		post.setHeader("Content-Type", "application/x-www-form-urlencoded");

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("grant_type", "password"));
		urlParameters.add(new BasicNameValuePair("scope", "Bagel_Store"));
		urlParameters.add(new BasicNameValuePair("role", "PUBLIC"));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " +
                                    response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
		System.out.println(response.getEntity().getContent().toString());

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		JSONObject jObject  = new JSONObject(result.toString());
		System.out.println(jObject.get("access_token"));
		setAuthKey(jObject.get("access_token").toString());


	}*/
	
	public static String generateToken() throws IOException, JSONException 
	{
		final Logger logger = Logger.getLogger(Authentication.class);
		String tokenURL = "http://155.64.130.57:9080/cortex/oauth2/tokens";
		URL url = new URL(tokenURL);
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("grant_type", "password");
		params.put("username", "");
		params.put("password", "");
		params.put("role", "PUBLIC");
		params.put("scope", "nortonstoreplus");
		StringBuilder postData = new StringBuilder();
		for (Map.Entry<String, String> param : params.entrySet()) {
			if (postData.length() != 0)
				postData.append('&');
			postData.append(param.getKey());
			postData.append('=');
			postData.append(param.getValue());
		}
		byte[] postDataBytes = postData.toString().getBytes("UTF-8");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		conn.setDoOutput(true);
		conn.getOutputStream().write(postDataBytes);

		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		for (int c; (c = in.read()) >= 0;)
			sb.append((char) c);
		String response = sb.toString();
		JSONObject responseJson = new JSONObject(response);
		logger.info(responseJson.get("token_type") + " " + responseJson.get("access_token"));
		return responseJson.get("token_type") + " " + responseJson.get("access_token");
	}


	
	public void authentication() throws Exception{
		generateToken();

	}
}
