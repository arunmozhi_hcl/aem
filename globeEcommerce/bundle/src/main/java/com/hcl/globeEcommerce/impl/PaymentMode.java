package com.hcl.globeEcommerce.impl;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import org.apache.felix.scr.annotations.sling.SlingServlet;
	import org.apache.log4j.Logger;
	 

	@SuppressWarnings("serial")
	@SlingServlet(paths="/bin/payment", methods = "GET", metatype=true)
	public class PaymentMode extends HttpServlet
	{
		final Logger logger = Logger.getLogger(BillingAddress.class);
		
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			logger.info("Get Method started login page");
			String payment_type = request.getParameter("payment_type");

			logger.info("Hellooooooooooo" +payment_type);
			response.getWriter().print(payment_type);
		
		
			ServletContext sc= getServletContext();
			
			sc.setAttribute("payment",payment_type);
			
			RequestDispatcher dispatcher=request.getRequestDispatcher("/content/globeEcommerce/reviewpage.html");
			dispatcher.forward(request, response);
		}
	}



