package com.hcl.globeEcommerce.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.CortexResponse;
import com.hcl.globeEcommerce.impl.ItemListView;

public class ProductSearch {
	final Logger logger = Logger.getLogger(ProductSearch.class);
	public String getProductListByKeyword(CortexClient client){
		
		Map<String, String> input = new HashMap<String, String>();
		input.put("keywords", "Plan");
		CortexResponse<Object> clientResponse = client.post(
				"searches/Bagel_Store/keywords/items", input, Object.class);
		logger.info("response:-"+clientResponse.getResponse().getHeaders()
				.get("Location").get(0).toString());
		return clientResponse.getResponse().getHeaders()
				.get("Location").get(0).toString();
		
	}
	
	public String getProductBasedOnSelection(CortexClient client, String itemLocation){
		CortexResponse<ItemListView> clientResponse1 = client.get(
				itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7),
				ItemListView.class);
		String href = clientResponse1.getCortexView().getHref();
		logger.info("href:-"+href);
		return href;
	}
	
	public String getProductDetailsBasedOnHref(CortexClient client, String href){
		CortexResponse<ItemDescriptionView> clientResponse1 = client.get(
				href.substring(href.lastIndexOf("cortex") + 7),
				ItemDescriptionView.class);
		String detailsHref = clientResponse1.getCortexView().getHref();
		logger.info("href:-"+detailsHref);
		return detailsHref;
	}
	public String getPriceBasedOnHref(CortexClient client, String href){
		CortexResponse<ItemDescriptionView> clientResponse1 = client.get(
				href.substring(href.lastIndexOf("cortex") + 7),
				ItemDescriptionView.class);
		String priceHref = clientResponse1.getCortexView().getPricehref();
		logger.info("href:-"+priceHref);
		return priceHref;
	}
	
	public String getPlan(CortexClient client, String detailsHref){
		CortexResponse<ItemDetailsView> clientResponse1 = client.get(
				detailsHref.substring(detailsHref.lastIndexOf("cortex") + 7),
				ItemDetailsView.class);
		String planDisplayName = clientResponse1.getCortexView().getPlanDisplayName();
		logger.info("product details::-"+planDisplayName);
		
		return planDisplayName;
	}
	public String getPlanDesc(CortexClient client, String detailsHref){
		CortexResponse<ItemDetailsView> clientResponse1 = client.get(
				detailsHref.substring(detailsHref.lastIndexOf("cortex") + 7),
				ItemDetailsView.class);
		String planValue = clientResponse1.getCortexView().getPlanValue();
		logger.info("plan details::-"+planValue);
		
		return planValue;
	}
	public String getProductPrice(CortexClient client, String priceHref){
		CortexResponse<ItemDetailsView> clientResponse1 = client.get(
				priceHref.substring(priceHref.lastIndexOf("cortex") + 7),
				ItemDetailsView.class);
		String productPrice = clientResponse1.getCortexView().getDisplayValue();
		logger.info("productPrice::-"+productPrice);
		
		return productPrice;
	}
	 
	
	public String getMonthlyPrice(CortexClient client, String detailsHref){
		CortexResponse<ItemDetailsView> clientResponse1 = client.get(
				detailsHref.substring(detailsHref.lastIndexOf("cortex") + 7),
				ItemDetailsView.class);
		
		String monthlyRentalValue = clientResponse1.getCortexView().getmonthlyRentalValue();
		logger.info("monthly plan details::-"+monthlyRentalValue);
		return  monthlyRentalValue;
	}
	
	public String getProductAvailability(CortexClient client, String href)
	{
        CortexResponse<ItemDescriptionView> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemDescriptionView.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getAvailableHref();
        logger.info("href:-"+definitionHref);
        return definitionHref;
	}
	public String getProductAvailabilityCheck(CortexClient client, String href)
	{
        CortexResponse<ItemAvailability> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemAvailability.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getState();
        return definitionHref;
	}
	
}
