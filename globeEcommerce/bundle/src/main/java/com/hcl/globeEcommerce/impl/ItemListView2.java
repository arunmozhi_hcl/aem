package com.hcl.globeEcommerce.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class ItemListView2 {
	
	@JsonPath("$.links[?(@.rel=='cart')].href")
    private Iterable<String> cartHref;
    @JsonPath("$.links[?(@.rel=='order')].href")
    private Iterable<String> orderHref;
    @JsonPath("$.links[?(@.rel=='tax')].href")
    private Iterable<String> taxHref;
    @JsonPath("$.links[?(@.rel=='total')].href")
    private Iterable<String> totalHref;
    
    public String getCartHref() {
           return cartHref.iterator().next();
    }
    
    public String getOrderHref() {
           return orderHref.iterator().next();
    }
    
    public String getTaxHref() {
           return taxHref.iterator().next();
    }
    
    public String getTotalHref() {
           return totalHref.iterator().next();
    }

	}

