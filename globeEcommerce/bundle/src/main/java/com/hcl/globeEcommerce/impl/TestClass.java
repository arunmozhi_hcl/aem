package com.hcl.globeEcommerce.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.elasticpath.rest.client.header.HeaderKeys;
//import com.hcl.globe.impl.ProductSearch;



@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/globe", methods = "GET", metatype=true)
public class TestClass extends SlingAllMethodsServlet{
	final Logger logger = Logger.getLogger(TestClass.class);

	String token="";
		@Reference
	     com.elasticpath.rest.client.CortexClientFactory clientFactory;
	    @Override
	    protected void doGet(final SlingHttpServletRequest req,
	            final SlingHttpServletResponse resp) throws ServletException, IOException {
	        final Resource resource = req.getResource();
	        Authentication authenticationService = new Authentication();
			try {
				token=authenticationService.generateToken();
				 resp.getWriter().write( "Bearer token :-"+token);
			} catch (Exception e) {
				 resp.getWriter().write("Something went wrong");
			}
	   
	        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
	        headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
	        headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
	        headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, token);
	        com.elasticpath.rest.client.CortexClient client = clientFactory.create(headers, "Bagel_Store");
	       
	        resp.getWriter().write( client.toString());
	       resp .setContentType("text/plain");
	       //resp .setContentType("application/json");
	        resp.getWriter().write( "welcome to symantec Norton landing page.");
	        ProductSearch productSearch = new ProductSearch();
			String productListUrl = productSearch.getProductListByKeyword(client);
			String productSelected = productSearch.getProductBasedOnSelection(
					client, productListUrl);
			resp.getWriter().write(productSelected);
	}
}


