package com.hcl.globeEcommerce.impl;


	import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import org.apache.felix.scr.annotations.sling.SlingServlet;
	import org.apache.log4j.Logger;
	 

	@SuppressWarnings("serial")
	@SlingServlet(paths="/bin/address", methods = "POST", metatype=true)
	public class BillingAddress extends HttpServlet
	{
		final Logger logger = Logger.getLogger(BillingAddress.class);
		
		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			logger.info("Get Method started login page");
			String billing_house_number = request.getParameter("billing_house_number");
			String billing_city_temp = request.getParameter("billing_city_temp");
			String billing_zip_code = request.getParameter("billing_zip_code");
			//logger.info("Hellooooooooooo" +f_name+m_name+l_name);
			response.getWriter().print(billing_house_number+billing_city_temp+billing_zip_code);
		
		
			ServletContext sc= getServletContext();
			
			sc.setAttribute("address",billing_house_number);
			sc.setAttribute("city",billing_city_temp);
			sc.setAttribute("zipcode",billing_zip_code);
			
			RequestDispatcher dispatcher=request.getRequestDispatcher("/content/globeEcommerce/reviewpage.html");
			dispatcher.forward(request, response);
		}
	}

