package com.hcl.globeEcommerce.impl;

import com.elasticpath.rest.client.urlbuilding.annotations.FollowLocation;
import com.elasticpath.rest.client.urlbuilding.annotations.Path;
import com.elasticpath.rest.client.urlbuilding.annotations.Uri;
import com.elasticpath.rest.client.urlbuilding.annotations.Zoom;
import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

@Uri("carts/{scope}/default")
@Zoom(@Path("order"))
@FollowLocation
public class Cart {

	//@JsonProperty("total-quantity")
	private String totalQuantity;
	@JsonPath("$.links[2].href")
	private String href;
	
	//@JsonPath("$._order[0].links[7].uri")
	@JsonPath("$._order[0].links[?(@.rel=='paymentmethodinfo')].uri")
	private Iterable<String> paymentmethodinfo;
	
	//@JsonPath("$._order[0].links[5].uri")
	@JsonPath("$._order[0].links[?(@.rel=='emailinfo')].uri")
	private Iterable<String> emailinfo;
	
	@JsonPath("$._order[0].links[?(@.rel=='billingaddressinfo')].uri")
	private Iterable<String> billingaddressinfo;
	
	
	
	/*@JsonPath("$._order[0].links[?(@.rel == 'billingaddressinfo')].uri")
	private Iterable<String> addToCartActionUri;*/
	
	
	@JsonPath("$._order[0].links[?(@.rel=='purchaseform')].uri")
	private Iterable<String> purchaseform;
	
	/**
	 * @return the purchaseform
	 */
	public String getPurchaseform() {
		return purchaseform.iterator().next();
	}

	/**
	 * @param purchaseform the purchaseform to set
	 */
	/*public void setPurchaseform(String purchaseform) {
		this.purchaseform = purchaseform;
	}*/

	/**
	 * @return the emailinfo
	 */
	public String getEmailinfo() {
		return emailinfo.iterator().next();
	}

	/**
	 * @return the billingaddressinfo
	 */
	public String getBillingaddressinfo() {
		return billingaddressinfo.iterator().next();
	}

	/**
	 * @param billingaddressinfo the billingaddressinfo to set
	 */
	/*public void setBillingaddressinfo(String billingaddressinfo) {
		this.billingaddressinfo = billingaddressinfo;
	}*/

	/**
	 * @return the paymentmethodinfo
	 */
	public String getPaymentmethodinfo() {
		return paymentmethodinfo.iterator().next();
	}

	

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	@JsonPath("$._lineitems[0]._element[0].quantity")
	private int quantity;

	public String getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
}
