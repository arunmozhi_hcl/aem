package com.hcl.globeEcommerce.impl;

import java.util.ArrayList;

import java.util.List;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;


public class ItemListView {
	
	@JsonPath("$.links[0].href")
	  private String href;

	
	 /**
	 * @return the href
	 */
	
	
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	

	


}
