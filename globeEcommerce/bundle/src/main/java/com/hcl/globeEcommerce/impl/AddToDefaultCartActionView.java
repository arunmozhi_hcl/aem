package com.hcl.globeEcommerce.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class AddToDefaultCartActionView {

	
	@JsonPath("$.links[0].href")
	  private String href;

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}
}
