package com.hcl.globeEcommerce.impl;


import java.io.IOException;






import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.header.HeaderKeys;




@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/plandetails", methods = "GET", metatype=true)
public class PlanDetailsServlet extends SlingAllMethodsServlet{
	final Logger logger = Logger.getLogger(PlanDetailsServlet.class);
String productprice;
	@Reference
    com.elasticpath.rest.client.CortexClientFactory clientFactory;
	    @Override
	    protected void doGet(final SlingHttpServletRequest req,
	            final SlingHttpServletResponse resp) throws ServletException, IOException {
	       
	        AuthenticationService as= new AuthenticationService();
	        try {
				as.authentication();
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	       
	        
	        logger.info("getting auth key:-"+as.getAuthKey());
	        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
	        headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
	        headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
	        headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, as.getAuthKey());
	        logger.info("client factory object:-"+clientFactory.toString());
	        com.elasticpath.rest.client.CortexClient client = clientFactory.create(headers, "Bagel_Store");
	        logger.info("client object:-"+client.toString());
	        
	        ProductSearch productSearch = new ProductSearch();
			String productListUrl = productSearch.getProductListByKeyword(client);
			String productSelected = productSearch.getProductBasedOnSelection(
					client, productListUrl);
			String productDescription = productSearch.getProductDetailsBasedOnHref(
					client, productSelected);
			String plan = productSearch.getPlan(
					client, productDescription);
			String Description = productSearch.getPlanDesc(
					client, productDescription);
			String monthlyprice = productSearch.getMonthlyPrice(
					client, productDescription);
			String productAvailability = productSearch.getProductAvailability(client, productSelected);
			String availabilityValue = productSearch.getProductAvailabilityCheck(client, productAvailability);
			if(!availabilityValue.equals("AVAILABLE")){
				resp.getWriter().print("not available");
			}
			else{
				String productUrl = productSearch.getPriceBasedOnHref(
						client, productSelected);
				logger.info("productUrl:-"+productUrl);
				productprice = productSearch.getProductPrice(
						client, productUrl);
				logger.info("productprice:-"+productprice);
				System.out.println(productprice);
			}
			/* HttpSession session= req.getSession();
			    session.setAttribute("planDetails", Description);*/
			   // req.setAttribute("planDetails", Description);
			ServletContext sc=getServletContext();
			sc.setAttribute("planValue", plan);
			sc.setAttribute("planDetails", Description);
			sc.setAttribute("monthlyprice", monthlyprice);
			sc.setAttribute("productprice", productprice);
			
		    RequestDispatcher dispatcher= req.getRequestDispatcher("/content/globeEcommerce/shopbyplanpage.html");
		    
		       dispatcher.forward(req, resp);
			
		       //resp.sendRedirect( "/content/globeEcommerce/shopbyplanpage.html" );
			/*// Adding Product To Cart
			AddProductToCart addProductToCart = new AddProductToCart();
			addProductToCart.addProductToCart(client, productSelected);
			
		// get cart view	
			CortexResponse<Cart> cart = client.get(Cart.class);
			String orderLink = cart.getCortexView().getHref();
			String paymentInfoUrl = cart.getCortexView().getPaymentmethodinfo();
			System.out.println("paymentInfoUrl = " + paymentInfoUrl);
			
		//	System.out.println(cart.getCortexView().getAddToCartActionUri());
			
			String billingAddressUrl = cart.getCortexView().getBillingaddressinfo();
			System.out.println("billingAddressUrl = " + billingAddressUrl);
			String emailInfoUrl = cart.getCortexView().getEmailinfo();
			System.out.println("emailInfoUrl = " + emailInfoUrl);
			String purchaseform = cart.getCortexView().getPurchaseform();


			
			// Add Payment Info
			PaymentInfo paymentInfo = new PaymentInfo();
			paymentInfo.addPaymentInfo(client, paymentInfoUrl);

			// Add Billing Info
			BillingInfo billingInfo = new BillingInfo();
			billingInfo.addBillingProfile(client, billingAddressUrl);

			// Add Email Id

			EmailAddress emailAddress = new EmailAddress();
			emailAddress.addEmailAddress(client, emailInfoUrl);

			// Place Order
			PlaceOrder placeOrder = new PlaceOrder();
			String reviewOrder = placeOrder.submitOrder(client, purchaseform);
			
			// review Order
			ReviewOrder reviewOrder2= new ReviewOrder();
			reviewOrder2.reviewOrder(client, reviewOrder);*/
	}
}


