package com.hcl.globeEcommerce.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;

import com.elasticpath.rest.client.header.HeaderKeys;
@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/addToCart", methods = "GET", metatype=true)
public class AddToCartServlet extends SlingAllMethodsServlet{
	final Logger logger = Logger.getLogger(AddToCartServlet.class);

	@Reference
    com.elasticpath.rest.client.CortexClientFactory clientFactory;
	    @Override
	    protected void doGet(final SlingHttpServletRequest req,
	            final SlingHttpServletResponse resp) throws ServletException, IOException {
	    	String quant = req.getParameter("quantity");
	    	logger.info("quantity++++++++++++++++++"+quant);
	       int qty = Integer.parseInt(quant);
	       logger.info("quantity"+qty);
	 AuthenticationService as= new AuthenticationService();
	 try {
		as.generateToken();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 logger.info("getting auth key:-"+as.getAuthKey());
     MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
     headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
     headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
     headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, as.getAuthKey());
     logger.info("client factory object:-"+clientFactory.toString());
     com.elasticpath.rest.client.CortexClient client = clientFactory.create(headers, "Bagel_Store");
     	ProductSearch productSearch = new ProductSearch();
		String productListUrl = productSearch.getProductListByKeyword(client);
		String productSelected = productSearch.getProductBasedOnSelection(client, productListUrl);

		String productAvailability = productSearch.getProductAvailability(client, productSelected);
		String availabilityValue = productSearch.getProductAvailabilityCheck(client, productAvailability);
		if(!availabilityValue.equals("AVAILABLE")){
			resp.getWriter().print("not available");
		}
		else{
			
			AddProductToCart addProductToCart = new AddProductToCart();
			String value = addProductToCart.addProductToCart(client, productSelected,qty);
			String cartHref = addProductToCart.productAddResponse(client, value);
			String orderHref = addProductToCart.productOrder(client, cartHref);
			//String taxHref = addProductToCart.productTax(client, orderHref);
			//String tax = addProductToCart.getProductTax(client, taxHref);
			String totalHref = addProductToCart.productTotal(client, orderHref);
			String total = addProductToCart.getProductTotal(client, totalHref);
			logger.info("total"+total);
			resp.getWriter().write(total);
			ServletContext sc=getServletContext();
			sc.setAttribute("total", total);
			//RequestDispatcher dispatcher= req.getRequestDispatcher("/content/globeEcommerce/personaldetailspage.html");
		    
		      // dispatcher.forward(req, resp);
}
}
}
