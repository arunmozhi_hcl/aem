package com.hcl.globeEcommerce.impl;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;


public class ItemDescriptionView {
	
	@JsonPath("$.links[3].href")
	  private String href;

	@JsonPath("$.links[0].href")//available
	private String availableHref;
	 /**
	 * @return the href
	 */
	@JsonPath("$.links[5].href")
	  private String pricehref;
	public String getHref() {
		return href;
	}

	/**
	 * @param href the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	
	public String getAvailableHref() {
		return availableHref;
	}

	public void setAvailableHref(String availableHref) {
		this.availableHref = availableHref;
	}

	public String getPricehref() {
		return pricehref;
	}
	public void setPricehref(String pricehref) {
		this.pricehref = pricehref;
	}
}
