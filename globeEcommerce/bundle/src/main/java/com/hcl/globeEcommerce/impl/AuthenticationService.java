package com.hcl.globeEcommerce.impl;

import java.io.BufferedReader;



import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
//import org.springframework.context.ApplicationContext;







import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.CortexClientFactory;
import com.elasticpath.rest.client.header.HeaderKeys;

@Component
public class AuthenticationService {
	
	final static Logger logger = Logger.getLogger(AuthenticationService.class);
	
	private String authKey;
	

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	
	
	/* public CortexClient getCortexClient(CortexClientFactory ccf){
		 
			logger.info("getting auth key:-"+getAuthKey());
	        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
	        headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
	        headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
	        headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, getAuthKey());
	        logger.info("client factory object:-"+ccf.toString());
	        com.elasticpath.rest.client.CortexClient client = ccf.create(headers, "Bagel_Store");
	        logger.info("client object:-"+client.toString());
	
		return client;
	}
	*/
	
	
	public  void generateToken() throws IOException, JSONException 
	{
		
		String tokenURL = "http://172.22.64.11:9080/cortex/oauth2/tokens";
		URL url = new URL(tokenURL);
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("grant_type", "password");
		params.put("username", "");
		params.put("password", "");
		params.put("role", "PUBLIC");
		params.put("scope", "Bagel_Store");
		StringBuilder postData = new StringBuilder();
		for (Map.Entry<String, String> param : params.entrySet()) {
			if (postData.length() != 0)
				postData.append('&');
			postData.append(param.getKey());
			postData.append('=');
			postData.append(param.getValue());
		}
		byte[] postDataBytes = postData.toString().getBytes("UTF-8");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		conn.setDoOutput(true);
		conn.getOutputStream().write(postDataBytes);

		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		for (int c; (c = in.read()) >= 0;)
			sb.append((char) c);
		String response = sb.toString();
		JSONObject responseJson = new JSONObject(response);
		logger.info("Auth Key Generated : " +responseJson.get("token_type") + " " + responseJson.get("access_token"));
		setAuthKey(responseJson.get("token_type") + " " + responseJson.get("access_token"));
	}


	
	public void authentication() throws Exception{
		generateToken();

	}
}
